<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'IndexController@home')->name('home');
Route::get('/login', 'IndexController@login')->name('login');
Route::post('/post/login', 'IndexController@postlogin')->name('postlogin');

Route::get('/register', 'IndexController@register')->name('register');
Route::post('/post/register', 'IndexController@postregister')->name('postregister');
Route::post('/post/social-media/register', 'IndexController@socialmediaregister')->name('socialmediaregister');

Route::get('/category/{slug?}/{subslug?}', 'IndexController@category')->name('category');
Route::get('/ad/{product?}', 'IndexController@adpage')->name('adpage');

Route::get('/search-ad/{product?}', 'IndexController@searchad')->name('searchad');

Route::post('/post/forgot-password', 'IndexController@postforgotpassword')->name('postforgotpassword');
Route::get('/forgot-password/{code?}', 'IndexController@forgotpassword')->name('forgotpassword');
Route::post('/post/change-password', 'IndexController@postchangepassword')->name('postchangepassword');

Route::get('/sales-pro', 'IndexController@salespro')->name('salespro');
Route::get('/about-us', 'IndexController@footerpages');
Route::get('/terms-and-conditions', 'IndexController@footerpages');
Route::get('/privacy-policy', 'IndexController@footerpages');
Route::get('/billing-policy', 'IndexController@footerpages');
Route::get('/safety-tips', 'IndexController@footerpages');
Route::get('/contact-us', 'IndexController@footerpages');
Route::get('/faq', 'IndexController@footerpages');

Route::group(['middleware' => ['logincheck']], function()
{
  Route::get('/favourite/{type?}/{adId?}', 'IndexController@favourite')->name('favourite');
  Route::get('/my-adverts/{type?}', 'IndexController@myadverts')->name('myadverts');
  Route::get('/my-adverts-action/{type?}/{id?}', 'IndexController@myadvertsaction')->name('myadvertsaction');
  Route::get('/shop-adverts/{user_id?}', 'IndexController@shopadverts')->name('shopadverts');
  Route::get('/profile', 'IndexController@userprofile')->name('userprofile');
  Route::post('/post/profile', 'IndexController@postuserprofile')->name('postuserprofile');
  Route::post('/post/update/picture', 'IndexController@postupdatepicture')->name('postupdatepicture');

  Route::post('/post/password-reset', 'IndexController@postpasswordreset')->name('postpasswordreset');
  Route::get('/reset-password/{code?}', 'IndexController@reset_password')->name('reset_password');
  Route::get('/logout', 'IndexController@logout')->name('logout');
  Route::get('/messages', 'IndexController@messages')->name('messages');
  Route::get('/notifications', 'IndexController@notifications')->name('notifications');
  Route::get('/free-ad', 'IndexController@freead')->name('freead');

  Route::post('/delete-advert-media', 'IndexController@delete_advert_media')->name('delete_advert_media');
  
  Route::get('/edit-advert/{slug?}', 'IndexController@editadvert')->name('editadvert');
  
  Route::post('/post-free-ad', 'IndexController@postfreead')->name('postfreead');
  Route::post('/post-free-ad-edit', 'IndexController@postfreeadedit')->name('postfreeadedit');

  Route::post('/post-sponsored-ad', 'IndexController@postsponsoredad')->name('postsponsoredad');
  Route::get('/get-sponsored-ad-paid', 'IndexController@getsponsoredadpaid')->name('getsponsoredadpaid');
  Route::post('/post/sponsored-upload', 'IndexController@sponsorededitupload')->name('sponsorededitupload');
  Route::post('/post-sponsored-ad-edit', 'IndexController@postsponsoredadedit')->name('postsponsoredadedit');

  Route::get('/ad-cat-select/{id?}', 'IndexController@ad_cat_select')->name('ad_cat_select');

  Route::get('/sponsored-ad', 'IndexController@sponsoredad')->name('sponsoredad');
  Route::get('/view-message/{advert_id?}/{message_id?}', 'IndexController@viewmessage')->name('viewmessage');
  Route::post('/post-send-message', 'IndexController@sendmessage')->name('sendmessage');
  Route::post('/post-report-advert', 'IndexController@reportadvert')->name('reportadvert');
  
  Route::post('/post-send-chat', 'IndexController@postsendchat')->name('postsendchat');
});


Route::group(['middleware' => ['admincheck'],'prefix' => 'admin'], function()
{

  Route::get('/', 'AdminController@home')->name('admin.home');
  Route::get('/view-adverts/{type?}', 'AdminController@adverts')->name('admin.adverts');
  Route::get('/reported-adverts', 'AdminController@reported_adverts')->name('admin.reported_adverts');
  Route::get('/user-profile/{id?}', 'AdminController@userprofile')->name('admin.userprofile');
  Route::get('/all-users', 'AdminController@users')->name('admin.allusers');
  Route::get('/update-role/{id?}/{role?}', 'AdminController@updaterole')->name('admin.updaterole');
  Route::get('/moderators', 'AdminController@moderators')->name('admin.moderators');
  Route::get('/deleteuser/{id?}', 'AdminController@deleteuser')->name('admin.deleteuser');
  Route::get('/adverts-action/{type?}/{id?}', 'AdminController@advertsaction')->name('admin.advertsaction');
  
  Route::get('/categories', 'AdminController@categories')->name('admin.categories');
  Route::get('/add-category/{category?}', 'AdminController@add_category')->name('admin.add_category');
  Route::get('/edit-category/{id?}/{category?}', 'AdminController@edit_category')->name('admin.edit_category');
  Route::get('/delete-category/{id?}', 'AdminController@delete_category')->name('admin.delete_category');

  Route::get('/sub-categories/{category_slug?}', 'AdminController@viewsubcategories')->name('admin.viewsubcategories');
  Route::get('/add-sub-category/{id?}/{sub_category?}', 'AdminController@add_sub_category')->name('admin.add_sub_category');
  Route::get('/edit-sub-category/{id?}/{sub_category?}', 'AdminController@edit_sub_category')->name('admin.edit_sub_category');
  Route::get('/delete-sub-category/{id?}', 'AdminController@delete_sub_category')->name('admin.delete_sub_category');

  Route::get('/sub-categories-attributes/{sub_category_slug?}', 'AdminController@viewsubcategoriesattr')->name('admin.viewsubcategoriesattr');
  Route::post('/add-sub-category-attributes', 'AdminController@add_sub_category_attr')->name('admin.add_sub_category_attr');
  Route::post('/edit-sub-category-attributes', 'AdminController@edit_sub_category_attr')->name('admin.edit_sub_category_attr');
  Route::get('/delete-sub-category-attributes/{id?}', 'AdminController@delete_sub_category_attr')->name('admin.delete_sub_category_attr');

  Route::get('/footer-pages', 'AdminController@footer_pages')->name('admin.footer_pages');
  Route::get('/edit-footer-page/{slug}', 'AdminController@edit_footer_page')->name('admin.edit_footer_page');
  Route::post('/update-footer-page', 'AdminController@update_footer_page')->name('admin.update_footer_page');
  Route::get('/delete-footer-page/{slug}', 'AdminController@delete_footer_page')->name('admin.delete_footer_page');

});



Route::get('/faker', 'IndexController@faker')->name('faker');