<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/users', 'Api\ApiController@users');
Route::post('/users/new', 'Api\ApiController@create');
Route::post('/users/new/{type}', 'Api\ApiController@socialMediaCreate');
Route::post('/users/login', 'Api\ApiController@login');
Route::post('/users/login/{type}', 'Api\ApiController@socialMediaLogin');

Route::get('/adverts/all', 'Api\ApiController@allAdverts');
Route::get('/adverts/sponsored/all', 'Api\ApiController@sponsoredAds');
Route::post('/adverts/search', 'Api\ApiController@searchAds');

Route::get('/categories/all', 'Api\ApiController@allCategories');
Route::get('/categories/subs/all', 'Api\ApiController@allCategorySubs');

Route::get('/adverts/single/{id}', 'Api\ApiController@singleAd');

Route::post('/advert/new-audio', 'Api\ApiController@newAudio');

Route::middleware(['auth:api'])->group(function () {
    Route::post('/users/password-reset', 'Api\ApiController@resetPassword');
    Route::post('/users/update-picture/profile', 'Api\ApiController@updateProfilePicture');
    Route::post('/users/{type}/profile', 'Api\ApiController@editProfile');

    Route::get('/favourite', 'Api\ApiController@allFavourites');
    Route::get('/favourite/{type}/{adId}', 'Api\ApiController@favourite');
    
    Route::post('/adverts/new/free', 'Api\ApiController@newFreeAd');
    Route::get('/adverts/toggle/{id}/{type}', 'Api\ApiController@toggleAds');
    Route::post('/adverts/free/{id}/edit', 'Api\ApiController@editFreeAd');
    Route::get('/adverts/seller/{id}', 'Api\ApiController@getSellerAds');
    Route::get('/adverts/{type}', 'Api\ApiController@getUserAds');
    Route::post('/adverts/report/{id}', 'Api\ApiController@reportAds');
    Route::post('/adverts/new/sponsored', 'Api\ApiController@newSponsoredAd');

    Route::get('/messages/all', 'Api\ApiController@allMessages');
    Route::post('/messages/new', 'Api\ApiController@sendMessage');
    Route::post('/messages/image/new', 'Api\ApiController@sendImage');
    Route::get('/messages/check/{advert}', 'Api\ApiController@checkForMessage');
    Route::get('/messages/{advert}/{message}', 'Api\ApiController@viewMessage');

    Route::get('/notifications/all', 'Api\ApiController@allNotifications');
});
