-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 03, 2019 at 01:27 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.0.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `marketam`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_activities`
--

CREATE TABLE `admin_activities` (
  `id` int(11) UNSIGNED NOT NULL,
  `admin` text NOT NULL,
  `activity` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_activities`
--

INSERT INTO `admin_activities` (`id`, `admin`, `activity`, `created_at`, `updated_at`) VALUES
(9, 'Olaogun Toluwalase', 'Updated Category Attribute Make for Heavy Equiomentsfor Vehicles', '2018-10-16 10:16:55', '2018-10-16 10:16:55'),
(10, 'Olaogun Toluwalase', 'View Sub Category Attributes for Heavy Equioments [Vehicles]', '2018-10-16 10:17:00', '2018-10-16 10:17:00'),
(11, 'Olaogun Toluwalase', 'Viewed all categories', '2018-10-16 10:17:08', '2018-10-16 10:17:08'),
(12, 'Olaogun Toluwalase', 'Edited category from Vehicles to Vehicles)', '2018-10-16 10:17:12', '2018-10-16 10:17:12'),
(13, 'Olaogun Toluwalase', 'Viewed all categories', '2018-10-16 10:17:12', '2018-10-16 10:17:12'),
(14, 'Olaogun Toluwalase', 'View subcategories for (Vehicles)', '2018-10-16 10:17:14', '2018-10-16 10:17:14'),
(15, 'Olaogun Toluwalase', 'Changed subcategory from Heavy Equioments to Heavy Equioments for (Vehicles)', '2018-10-16 10:17:17', '2018-10-16 10:17:17'),
(16, 'Olaogun Toluwalase', 'View subcategories for (Vehicles)', '2018-10-16 10:17:17', '2018-10-16 10:17:17'),
(17, 'Olaogun Toluwalase', 'Changed subcategory from Heavy Equioments to Heavy Equipments for (Vehicles)', '2018-10-16 10:17:25', '2018-10-16 10:17:25'),
(18, 'Olaogun Toluwalase', 'View subcategories for (Vehicles)', '2018-10-16 10:17:25', '2018-10-16 10:17:25'),
(19, 'Olaogun Toluwalase', 'Viewed all footer pages', '2018-10-16 10:17:29', '2018-10-16 10:17:29'),
(20, 'Olaogun Toluwalase', 'Tried to edit footer page for ', '2018-10-16 10:17:33', '2018-10-16 10:17:33'),
(21, 'Olaogun Toluwalase', 'Viewed all footer pages', '2018-10-16 10:17:35', '2018-10-16 10:17:35'),
(22, 'Olaogun Toluwalase', 'Visited admin home', '2018-10-16 10:17:48', '2018-10-16 10:17:48'),
(23, 'Olaogun Toluwalase', 'Viewed all categories', '2018-10-16 10:19:12', '2018-10-16 10:19:12'),
(24, 'Olaogun Toluwalase', 'Visited admin home', '2018-10-16 13:34:25', '2018-10-16 13:34:25'),
(25, 'Olaogun Toluwalase', 'Visited admin home', '2018-10-16 14:46:17', '2018-10-16 14:46:17'),
(26, 'Olaogun Toluwalase', 'Viewed all adverts', '2018-10-16 14:47:04', '2018-10-16 14:47:04'),
(27, 'Olaogun Toluwalase', 'Visited admin home', '2018-10-16 14:48:45', '2018-10-16 14:48:45'),
(28, 'Olaogun Toluwalase', 'Viewed all adverts', '2018-10-16 14:48:48', '2018-10-16 14:48:48'),
(29, 'Olaogun Toluwalase', 'Visited admin home', '2018-10-16 14:50:26', '2018-10-16 14:50:26'),
(30, 'Olaogun Toluwalase', 'Visited admin home', '2018-10-17 07:13:36', '2018-10-17 07:13:36'),
(31, 'Olaogun Toluwalase', 'Viewed all adverts', '2018-10-17 07:13:49', '2018-10-17 07:13:49'),
(32, 'Olaogun Toluwalase', 'Viewed all adverts', '2018-10-17 07:17:37', '2018-10-17 07:17:37'),
(33, 'Olaogun Toluwalase', 'Viewed all adverts', '2018-10-17 07:19:40', '2018-10-17 07:19:40'),
(34, 'Olaogun Toluwalase', 'Viewed all adverts', '2018-10-17 07:20:02', '2018-10-17 07:20:02'),
(35, 'Olaogun Toluwalase', 'Viewed all adverts', '2018-10-17 07:22:30', '2018-10-17 07:22:30'),
(36, 'Olaogun Toluwalase', 'Viewed all adverts', '2018-10-17 07:25:55', '2018-10-17 07:25:55'),
(37, 'Olaogun Toluwalase', 'Viewed all adverts', '2018-10-17 07:26:11', '2018-10-17 07:26:11'),
(38, 'Olaogun Toluwalase', 'Viewed all adverts', '2018-10-17 07:26:17', '2018-10-17 07:26:17'),
(39, 'Olaogun Toluwalase', 'Viewed all adverts', '2018-10-17 07:29:45', '2018-10-17 07:29:45'),
(40, 'Olaogun Toluwalase', 'Viewed blocked adverts', '2018-10-17 07:29:45', '2018-10-17 07:29:45'),
(41, 'Olaogun Toluwalase', 'Viewed all adverts', '2018-10-17 07:29:58', '2018-10-17 07:29:58'),
(42, 'Olaogun Toluwalase', 'Viewed blocked adverts', '2018-10-17 07:29:58', '2018-10-17 07:29:58'),
(43, 'Olaogun Toluwalase', 'Viewed all adverts', '2018-10-17 07:30:53', '2018-10-17 07:30:53'),
(44, 'Olaogun Toluwalase', 'Viewed blocked adverts', '2018-10-17 07:30:53', '2018-10-17 07:30:53'),
(45, 'Olaogun Toluwalase', 'Viewed all adverts', '2018-10-17 07:32:26', '2018-10-17 07:32:26'),
(46, 'Olaogun Toluwalase', 'Viewed sold adverts', '2018-10-17 07:32:26', '2018-10-17 07:32:26'),
(47, 'Olaogun Toluwalase', 'Visited admin home', '2018-10-17 07:32:33', '2018-10-17 07:32:33'),
(48, 'Olaogun Toluwalase', 'Viewed all adverts', '2018-10-17 07:33:00', '2018-10-17 07:33:00'),
(49, 'Olaogun Toluwalase', 'Viewed all adverts', '2018-10-17 07:33:33', '2018-10-17 07:33:33'),
(50, 'Olaogun Toluwalase', 'Viewed all adverts', '2018-10-17 07:34:26', '2018-10-17 07:34:26'),
(51, 'Olaogun Toluwalase', 'Viewed all adverts', '2018-10-17 07:34:50', '2018-10-17 07:34:50'),
(52, 'Olaogun Toluwalase', 'Viewed all adverts', '2018-10-17 07:35:06', '2018-10-17 07:35:06'),
(53, 'Olaogun Toluwalase', 'Viewed all adverts', '2018-10-17 07:35:16', '2018-10-17 07:35:16'),
(54, 'Olaogun Toluwalase', 'Viewed all adverts', '2018-10-17 07:35:36', '2018-10-17 07:35:36'),
(55, 'Olaogun Toluwalase', 'Viewed all adverts', '2018-10-17 07:36:01', '2018-10-17 07:36:01'),
(56, 'Olaogun Toluwalase', 'Viewed all adverts', '2018-10-17 07:36:24', '2018-10-17 07:36:24'),
(57, 'Olaogun Toluwalase', 'Viewed all adverts', '2018-10-17 07:44:19', '2018-10-17 07:44:19'),
(58, 'Olaogun Toluwalase', 'Viewed all adverts', '2018-10-17 07:44:56', '2018-10-17 07:44:56'),
(59, 'Olaogun Toluwalase', 'Viewed all adverts', '2018-10-17 07:45:42', '2018-10-17 07:45:42'),
(60, 'Olaogun Toluwalase', 'Viewed all adverts', '2018-10-17 07:45:52', '2018-10-17 07:45:52'),
(61, 'Olaogun Toluwalase', 'Viewed all adverts', '2018-10-17 08:12:06', '2018-10-17 08:12:06'),
(62, 'Olaogun Toluwalase', 'Viewed all adverts', '2018-10-17 08:15:09', '2018-10-17 08:15:09'),
(63, 'Olaogun Toluwalase', 'Viewed all adverts', '2018-10-17 08:18:33', '2018-10-17 08:18:33'),
(64, 'Olaogun Toluwalase', 'Viewed all adverts', '2018-10-17 08:19:01', '2018-10-17 08:19:01'),
(65, 'Olaogun Toluwalase', 'Viewed all adverts', '2018-10-17 08:19:45', '2018-10-17 08:19:45'),
(66, 'Olaogun Toluwalase', 'Viewed all adverts', '2018-10-17 08:21:16', '2018-10-17 08:21:16'),
(67, 'Olaogun Toluwalase', 'Viewed all adverts', '2018-10-17 08:21:38', '2018-10-17 08:21:38'),
(68, 'Olaogun Toluwalase', 'Viewed all adverts', '2018-10-17 08:23:49', '2018-10-17 08:23:49'),
(69, 'Olaogun Toluwalase', 'Viewed all adverts', '2018-10-17 08:30:37', '2018-10-17 08:30:37'),
(70, 'Olaogun Toluwalase', 'Viewed all adverts', '2018-10-17 08:31:25', '2018-10-17 08:31:25'),
(71, 'Olaogun Toluwalase', 'Viewed all adverts', '2018-10-17 08:33:30', '2018-10-17 08:33:30'),
(72, 'Olaogun Toluwalase', 'Viewed all adverts', '2018-10-17 08:33:38', '2018-10-17 08:33:38'),
(73, 'Olaogun Toluwalase', 'Visited admin home', '2018-10-17 08:33:44', '2018-10-17 08:33:44'),
(74, 'Olaogun Toluwalase', 'Visited admin home', '2018-10-17 08:33:47', '2018-10-17 08:33:47'),
(75, 'Olaogun Toluwalase', 'Visited admin home', '2018-10-17 08:33:59', '2018-10-17 08:33:59'),
(76, 'Olaogun Toluwalase', 'Visited admin home', '2018-10-17 08:59:30', '2018-10-17 08:59:30'),
(77, 'Olaogun Toluwalase', 'Visited admin home', '2018-10-17 09:00:00', '2018-10-17 09:00:00'),
(78, 'Olaogun Toluwalase', 'Visited admin home', '2018-10-17 09:00:17', '2018-10-17 09:00:17'),
(79, 'Olaogun Toluwalase', 'Viewed all moderators', '2018-10-17 09:09:10', '2018-10-17 09:09:10'),
(80, 'Olaogun Toluwalase', 'Viewed all users', '2018-10-17 09:09:21', '2018-10-17 09:09:21'),
(81, 'Olaogun Toluwalase', 'Viewed all moderators', '2018-10-17 09:09:24', '2018-10-17 09:09:24'),
(82, 'Olaogun Toluwalase', 'Visited admin home', '2018-10-17 09:19:03', '2018-10-17 09:19:03'),
(83, 'Olaogun Toluwalase', 'Visited admin home', '2018-10-17 09:24:27', '2018-10-17 09:24:27'),
(84, 'Olaogun Toluwalase', 'Visited admin home', '2018-10-17 09:27:58', '2018-10-17 09:27:58'),
(85, 'Olaogun Toluwalase', 'Visited admin home', '2018-10-17 11:50:14', '2018-10-17 11:50:14'),
(86, 'Olaogun Toluwalase', 'Viewed all adverts', '2018-10-17 11:50:25', '2018-10-17 11:50:25'),
(87, 'Olaogun Toluwalase', 'Viewed new adverts', '2018-10-17 11:50:26', '2018-10-17 11:50:26'),
(88, 'Olaogun Toluwalase', 'Activated Infinix Hot 6 Pro (X608) 6-Inch HD', '2018-10-17 11:52:55', '2018-10-17 11:52:55'),
(89, 'Olaogun Toluwalase', 'Viewed all adverts', '2018-10-17 11:55:16', '2018-10-17 11:55:16'),
(90, 'Olaogun Toluwalase', 'Viewed new adverts', '2018-10-17 11:55:16', '2018-10-17 11:55:16'),
(91, 'Olaogun Toluwalase', 'Blocked Infinix Hot 6 Pro (X608) 6-Inch HD', '2018-10-17 11:55:30', '2018-10-17 11:55:30'),
(92, 'Olaogun Toluwalase', 'Viewed all adverts', '2018-10-17 11:55:36', '2018-10-17 11:55:36'),
(93, 'Olaogun Toluwalase', 'Viewed new adverts', '2018-10-17 11:55:36', '2018-10-17 11:55:36'),
(94, 'Olaogun Toluwalase', 'Activated Infinix Hot 6 Pro (X608) 6-Inch HD', '2018-10-17 11:56:17', '2018-10-17 11:56:17'),
(95, 'Olaogun Toluwalase', 'Visited admin home', '2018-10-17 14:35:57', '2018-10-17 14:35:57'),
(96, 'Olaogun Toluwalase', 'Visited admin home', '2018-10-17 14:48:32', '2018-10-17 14:48:32'),
(97, 'Olaogun Toluwalase', 'Visited admin home', '2018-10-18 07:42:29', '2018-10-18 07:42:29'),
(98, 'Olaogun Toluwalase', 'Visited admin home', '2018-10-18 07:43:06', '2018-10-18 07:43:06'),
(99, 'Olaogun Toluwalase', 'Visited admin home', '2018-10-18 08:47:35', '2018-10-18 08:47:35'),
(100, 'Olaogun Toluwalase', 'Visited admin home', '2018-10-18 08:48:26', '2018-10-18 08:48:26'),
(101, 'Olaogun Toluwalase', 'Visited admin home', '2018-10-18 08:54:28', '2018-10-18 08:54:28'),
(102, 'Olaogun Toluwalase', 'Visited admin home', '2018-10-18 08:55:06', '2018-10-18 08:55:06'),
(103, 'Olaogun Toluwalase', 'Visited admin home', '2018-10-18 09:53:54', '2018-10-18 09:53:54'),
(104, 'Olaogun Toluwalase', 'Viewed all categories', '2018-10-18 09:55:18', '2018-10-18 09:55:18'),
(105, 'Olaogun Toluwalase', 'View subcategories for (Fashion)', '2018-10-18 09:55:21', '2018-10-18 09:55:21'),
(106, 'Olaogun Toluwalase', 'View Sub Category Attributes for Clothing [Fashion]', '2018-10-18 09:55:25', '2018-10-18 09:55:25'),
(107, 'Olaogun Toluwalase', 'Added New Sub Category Attribute Size for Clothingfor Fashion', '2018-10-18 09:57:22', '2018-10-18 09:57:22'),
(108, 'Olaogun Toluwalase', 'Added New Sub Category Attribute Color for Clothingfor Fashion', '2018-10-18 09:57:22', '2018-10-18 09:57:22'),
(109, 'Olaogun Toluwalase', 'View Sub Category Attributes for Clothing [Fashion]', '2018-10-18 09:57:23', '2018-10-18 09:57:23'),
(110, 'Olaogun Toluwalase', 'Visited admin home', '2018-10-18 10:25:56', '2018-10-18 10:25:56'),
(111, 'Olaogun Toluwalase', 'Viewed all categories', '2018-10-18 10:26:38', '2018-10-18 10:26:38'),
(112, 'Olaogun Toluwalase', 'View subcategories for (Fashion)', '2018-10-18 10:26:46', '2018-10-18 10:26:46'),
(113, 'Olaogun Toluwalase', 'View Sub Category Attributes for Clothing [Fashion]', '2018-10-18 10:29:53', '2018-10-18 10:29:53'),
(114, 'Olaogun Toluwalase', 'Deleted Sub Category Attribute Size', '2018-10-18 10:29:59', '2018-10-18 10:29:59'),
(115, 'Olaogun Toluwalase', 'View Sub Category Attributes for Clothing [Fashion]', '2018-10-18 10:30:00', '2018-10-18 10:30:00'),
(116, 'Olaogun Toluwalase', 'Deleted Sub Category Attribute Color', '2018-10-18 10:30:04', '2018-10-18 10:30:04'),
(117, 'Olaogun Toluwalase', 'View Sub Category Attributes for Clothing [Fashion]', '2018-10-18 10:30:05', '2018-10-18 10:30:05'),
(118, 'Olaogun Toluwalase', 'Updated Category Attribute Size for Clothingfor Fashion', '2018-10-18 10:30:16', '2018-10-18 10:30:16'),
(119, 'Olaogun Toluwalase', 'View Sub Category Attributes for Clothing [Fashion]', '2018-10-18 10:30:17', '2018-10-18 10:30:17'),
(120, 'Olaogun Toluwalase', 'Viewed all categories', '2018-10-18 13:53:35', '2018-10-18 13:53:35'),
(121, 'Olaogun Toluwalase', 'Visited admin home', '2019-05-09 14:48:54', '2019-05-09 14:48:54'),
(122, 'Olaogun Toluwalase', 'Visited admin home', '2019-05-09 14:49:06', '2019-05-09 14:49:06'),
(123, 'Olaogun Toluwalase', 'Viewed all categories', '2019-05-09 14:49:31', '2019-05-09 14:49:31'),
(124, 'Olaogun Toluwalase', 'View subcategories for (Electronics)', '2019-05-09 14:49:53', '2019-05-09 14:49:53'),
(125, 'Olaogun Toluwalase', 'Changed subcategory from Audi & Music Equipment to Audio & Music Equipment for (Electronics)', '2019-05-09 14:50:05', '2019-05-09 14:50:05'),
(126, 'Olaogun Toluwalase', 'View subcategories for (Electronics)', '2019-05-09 14:50:06', '2019-05-09 14:50:06'),
(127, 'Olaogun Toluwalase', 'Viewed all categories', '2019-05-09 14:50:26', '2019-05-09 14:50:26'),
(128, 'Olaogun Toluwalase', 'View subcategories for (Real Estate)', '2019-05-09 14:50:32', '2019-05-09 14:50:32'),
(129, 'Olaogun Toluwalase', 'Added subcategories (Hotels,Event Centers,Venues) for (Real Estate)', '2019-05-09 14:51:16', '2019-05-09 14:51:16'),
(130, 'Olaogun Toluwalase', 'View subcategories for (Real Estate)', '2019-05-09 14:51:16', '2019-05-09 14:51:16'),
(131, 'Olaogun Toluwalase', 'Viewed all categories', '2019-05-09 14:51:40', '2019-05-09 14:51:40'),
(132, 'Olaogun Toluwalase', 'View subcategories for (Commercial Equipment)', '2019-05-09 14:51:45', '2019-05-09 14:51:45'),
(133, 'Olaogun Toluwalase', 'Changed subcategory from Restaurant & Catering Equioment to Restaurant & Catering Equipment for (Commercial Equipment)', '2019-05-09 14:52:07', '2019-05-09 14:52:07'),
(134, 'Olaogun Toluwalase', 'View subcategories for (Commercial Equipment)', '2019-05-09 14:52:07', '2019-05-09 14:52:07'),
(135, 'Olaogun Toluwalase', 'Visited admin home', '2019-05-09 14:53:03', '2019-05-09 14:53:03'),
(136, 'Olaogun Toluwalase', 'Visited admin home', '2019-05-09 14:54:56', '2019-05-09 14:54:56'),
(137, 'Olaogun Toluwalase', 'Visited admin home', '2019-05-09 14:55:42', '2019-05-09 14:55:42'),
(138, 'Olaogun Toluwalase', 'Visited admin home', '2019-05-09 14:55:44', '2019-05-09 14:55:44'),
(139, 'Olaogun Toluwalase', 'Visited admin home', '2019-05-09 14:56:25', '2019-05-09 14:56:25'),
(140, 'Olaogun Toluwalase', 'Visited admin home', '2019-05-09 14:58:12', '2019-05-09 14:58:12'),
(141, 'Olaogun Toluwalase', 'Visited admin home', '2019-05-09 14:58:33', '2019-05-09 14:58:33'),
(142, 'Olaogun Toluwalase', 'Visited admin home', '2019-05-09 14:58:45', '2019-05-09 14:58:45'),
(143, 'Olaogun Toluwalase', 'Visited admin home', '2019-05-09 15:00:49', '2019-05-09 15:00:49'),
(144, 'Olaogun Toluwalase', 'Visited admin home', '2019-05-09 15:00:59', '2019-05-09 15:00:59'),
(145, 'Olaogun Toluwalase', 'Visited admin home', '2019-05-09 15:01:16', '2019-05-09 15:01:16'),
(146, 'Olaogun Toluwalase', 'Visited admin home', '2019-05-09 15:02:41', '2019-05-09 15:02:41'),
(147, 'Olaogun Toluwalase', 'Visited admin home', '2019-05-09 15:02:47', '2019-05-09 15:02:47'),
(148, 'Olaogun Toluwalase', 'Visited admin home', '2019-05-09 15:02:52', '2019-05-09 15:02:52'),
(149, 'Olaogun Toluwalase', 'Visited admin home', '2019-05-09 15:04:13', '2019-05-09 15:04:13'),
(150, 'Olaogun Toluwalase', 'Visited admin home', '2019-05-09 15:04:22', '2019-05-09 15:04:22'),
(151, 'Olaogun Toluwalase', 'Visited admin home', '2019-05-09 15:04:22', '2019-05-09 15:04:22'),
(152, 'Olaogun Toluwalase', 'Viewed all adverts', '2019-05-09 15:04:27', '2019-05-09 15:04:27'),
(153, 'Olaogun Toluwalase', 'Visited admin home', '2019-05-09 15:04:32', '2019-05-09 15:04:32'),
(154, 'Olaogun Toluwalase', 'Viewed all categories', '2019-05-09 15:05:15', '2019-05-09 15:05:15'),
(155, 'Olaogun Toluwalase', 'View subcategories for (Vehicles)', '2019-05-09 15:05:18', '2019-05-09 15:05:18'),
(156, 'Olaogun Toluwalase', 'Added subcategories (Water Crafts, Trucks & Trailers, Motorcycles & Scooters, Cars) for (Vehicles)', '2019-05-09 15:08:50', '2019-05-09 15:08:50'),
(157, 'Olaogun Toluwalase', 'View subcategories for (Vehicles)', '2019-05-09 15:08:51', '2019-05-09 15:08:51'),
(158, 'Olaogun Toluwalase', 'Visited admin home', '2019-05-13 13:53:05', '2019-05-13 13:53:05'),
(159, 'Olaogun Toluwalase', 'Viewed all adverts', '2019-05-13 13:53:11', '2019-05-13 13:53:11'),
(160, 'Olaogun Toluwalase', 'Viewed all adverts', '2019-05-13 13:56:30', '2019-05-13 13:56:30'),
(161, 'Olaogun Toluwalase', 'Viewed all adverts', '2019-05-13 13:56:38', '2019-05-13 13:56:38'),
(162, 'Olaogun Toluwalase', 'Viewed free adverts', '2019-05-13 13:56:38', '2019-05-13 13:56:38'),
(163, 'Olaogun Toluwalase', 'Viewed all adverts', '2019-05-13 13:57:06', '2019-05-13 13:57:06'),
(164, 'Olaogun Toluwalase', 'Viewed free adverts', '2019-05-13 13:57:06', '2019-05-13 13:57:06'),
(165, 'Olaogun Toluwalase', 'Visited admin home', '2019-05-16 13:03:30', '2019-05-16 13:03:30'),
(166, 'Olaogun Toluwalase', 'Viewed all reported adverts', '2019-05-16 13:04:38', '2019-05-16 13:04:38'),
(167, 'Olaogun Toluwalase', 'Viewed all reported adverts', '2019-05-16 13:05:53', '2019-05-16 13:05:53'),
(168, 'Olaogun Toluwalase', 'Viewed all reported adverts', '2019-05-16 13:07:06', '2019-05-16 13:07:06'),
(169, 'Olaogun Toluwalase', 'Viewed all reported adverts', '2019-05-16 13:07:31', '2019-05-16 13:07:31'),
(170, 'Olaogun Toluwalase', 'Viewed all reported adverts', '2019-05-16 13:10:24', '2019-05-16 13:10:24'),
(171, 'Olaogun Toluwalase', 'Viewed all adverts', '2019-05-16 13:11:32', '2019-05-16 13:11:32'),
(172, 'Olaogun Toluwalase', 'Viewed all reported adverts', '2019-05-16 13:11:39', '2019-05-16 13:11:39'),
(173, 'Olaogun Toluwalase', 'Viewed all reported adverts', '2019-05-16 13:18:21', '2019-05-16 13:18:21'),
(174, 'Olaogun Toluwalase', 'Viewed all reported adverts', '2019-05-16 13:18:57', '2019-05-16 13:18:57'),
(175, 'Olaogun Toluwalase', 'Viewed all users', '2019-05-16 13:19:04', '2019-05-16 13:19:04'),
(176, 'Olaogun Toluwalase', 'Viewed all reported adverts', '2019-05-16 13:19:07', '2019-05-16 13:19:07'),
(177, 'Olaogun Toluwalase', 'Viewed all reported adverts', '2019-05-16 13:19:21', '2019-05-16 13:19:21'),
(178, 'Olaogun Toluwalase', 'Viewed all reported adverts', '2019-05-16 13:19:24', '2019-05-16 13:19:24'),
(179, 'Olaogun Toluwalase', 'Viewed all reported adverts', '2019-05-16 13:19:34', '2019-05-16 13:19:34'),
(180, 'Olaogun Toluwalase', 'Viewed all reported adverts', '2019-05-16 13:19:48', '2019-05-16 13:19:48'),
(181, 'Olaogun Toluwalase', 'Viewed all reported adverts', '2019-05-16 13:19:59', '2019-05-16 13:19:59'),
(182, 'Olaogun Toluwalase', 'Viewed all reported adverts', '2019-05-16 13:23:39', '2019-05-16 13:23:39'),
(183, 'Astract Developers', 'Visited admin home', '2019-05-20 09:17:13', '2019-05-20 09:17:13'),
(184, 'Astract Developers', 'Viewed all adverts', '2019-05-20 09:17:17', '2019-05-20 09:17:17'),
(185, 'Olaogun Toluwalase', 'Visited admin home', '2019-06-13 09:09:43', '2019-06-13 09:09:43'),
(186, 'Olaogun Toluwalase', 'Visited admin home', '2019-06-19 07:14:57', '2019-06-19 07:14:57'),
(187, 'Olaogun Toluwalase', 'Viewed all reported adverts', '2019-06-19 07:19:08', '2019-06-19 07:19:08');

-- --------------------------------------------------------

--
-- Table structure for table `adverts`
--

CREATE TABLE `adverts` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `advert_type` varchar(30) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `categories_sub_id` int(11) NOT NULL,
  `slug` varchar(80) DEFAULT NULL,
  `title` varchar(80) NOT NULL,
  `description` text NOT NULL,
  `price` int(11) DEFAULT NULL,
  `price_type` varchar(191) DEFAULT 'Fixed Price',
  `location` text,
  `usage` varchar(191) DEFAULT 'New',
  `active` varchar(60) DEFAULT 'active',
  `view_count` int(11) DEFAULT '0',
  `admin_incharge` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adverts`
--

INSERT INTO `adverts` (`id`, `user_id`, `advert_type`, `category_id`, `categories_sub_id`, `slug`, `title`, `description`, `price`, `price_type`, `location`, `usage`, `active`, `view_count`, `admin_incharge`, `created_at`, `updated_at`) VALUES
(80, 23, 'sponsored', 1, 4, 'when-we-run-this80', 'when we run this', 'fww gd sdf', 50000, 'Fixed Price', 'Kogi', 'New', 'deactivated', 4, NULL, '2018-08-31 22:40:31', '2019-05-20 09:30:12'),
(81, 23, 'free', 1, 4, 'tyre-for-sale80', 'Tyre for sale', 'eveyr sale', 5800478, 'Fixed Price', 'Osun', 'New', 'deactivated', 2, NULL, '2018-08-31 22:21:46', '2019-05-20 09:31:37'),
(91, 23, 'free', 1, 4, 'wq-wqe91', 'wq wqe', 'qweasf', 250000, 'Fixed Price', 'Abia', 'New', 'active', 2, NULL, '2018-03-31 22:34:06', '2019-05-20 09:32:06'),
(92, 23, 'free', 1, 4, 'adffd-df92', 'adffd df', 'qweasff  ffd', 150000, 'Fixed Price', 'Abia', 'New', 'deactivated', 5, NULL, '2018-08-31 22:34:25', '2019-05-20 09:31:33'),
(100, 37, 'free', 1, 2, 'infinix-hot-6-pro-x608-6-inch-hd-81', 'Infinix Hot 6 Pro (X608) 6-Inch HD', 'wfsd', 34444, 'Fixed Price', 'dffdsd', 'New', 'active', 66, 'Olaogun Toluwalase', '2018-10-17 09:31:42', '2019-05-20 09:08:05'),
(101, 37, 'free', 3, 8, 'sfsfd-101', 'sfsfd', 'ewrerew', 32232332, 'Fixed Price', 'ewrwe', 'New', 'new', 0, NULL, '2018-10-17 10:29:09', '2018-10-17 10:29:09'),
(103, 37, 'sponsored', 3, 8, 'xvxcxvc-103', 'xvxcxvc', 'xcvzdzz', 64654454, 'Fixed Price', 'zczxcsd', 'New', 'new', 9, NULL, '2018-10-17 10:51:42', '2019-05-16 13:18:28'),
(104, 33, 'sponsored', 3, 14, 'fwe-104', 'fwe', 'ewr', 323232, 'Fixed Price', 'las', 'New', 'new', 2, NULL, '2018-10-18 12:25:24', '2019-05-16 14:20:10'),
(105, 33, 'sponsored', 15, 152, 'qweqe-105', 'qweqe', 'qweqew', 32323232, 'Fixed Price', 'Kogi', 'New', 'new', 0, NULL, '2018-10-18 12:28:39', '2018-10-18 12:28:39'),
(106, 33, 'sponsored', 12, 75, 'qwwqw-106', 'qwwqw', 'qweeqw', 52000, 'Fixed Price', 'Lagos', 'New', 'new', 0, NULL, '2018-10-18 12:36:58', '2018-10-18 12:36:58'),
(107, 33, 'sponsored', 3, 9, 'asd-107', 'asd', 'sadad', 322, 'Fixed Price', '2323', 'New', 'new', 0, NULL, '2018-10-18 13:19:49', '2018-10-18 13:19:49'),
(108, 23, 'sponsored', 5, 23, '21-attire-royal-blue-asymmetric-peplum-dress-blue-108', '21 Attire Royal Blue Asymmetric Peplum Dress - Blue', '21 Attire Royal Blue Asymmetric Peplum Dress - Blue', 2999, 'Fixed Price', 'Lagos', 'New', 'new', 1, NULL, '2019-05-21 12:47:26', '2019-05-21 12:47:29'),
(109, 23, 'free', 1, 173, 'asddasdasd-109', 'asddasdasd', 'as', 32323, 'Fixed Price', 'Lagos', 'New', 'new', 2, NULL, '2019-06-10 09:12:42', '2019-06-10 09:14:33'),
(110, 23, 'free', 3, 8, 'sdf-110', 'sdf', 'efwwef3232', 233232, 'Fixed Price', 'Lagos', 'New', 'new', 1, NULL, '2019-06-10 09:15:39', '2019-06-10 09:15:40'),
(111, 23, 'free', 3, 13, '32dwfadssd-111', '32dwfadssd', 'ew32dwdew', 344444, 'Fixed Price', 'Kogi', 'New', 'new', 1, NULL, '2019-06-10 09:19:06', '2019-06-10 09:19:06'),
(112, 23, 'free', 1, 172, 'waee-112', 'waee', 'ress', 124, 'Fixed Price', 'Lagos', 'New', 'new', 1, NULL, '2019-06-10 09:20:48', '2019-06-10 09:20:49'),
(113, 33, 'free', 1, 4, 'vn-113', 'vn', 'bvn', 2500, 'Fixed Price', 'Lagos', 'New', 'new', 1, NULL, '2019-06-19 07:24:25', '2019-06-19 07:24:25');

-- --------------------------------------------------------

--
-- Table structure for table `advert_attributes`
--

CREATE TABLE `advert_attributes` (
  `id` int(10) UNSIGNED NOT NULL,
  `advert_id` int(11) NOT NULL,
  `categories_sub_attributes_id` int(11) NOT NULL,
  `attribute_data` varchar(80) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `advert_attributes`
--

INSERT INTO `advert_attributes` (`id`, `advert_id`, `categories_sub_attributes_id`, `attribute_data`, `created_at`, `updated_at`) VALUES
(10, 80, 10, 'Ausa', '2018-08-30 10:25:59', '2018-08-30 10:25:59'),
(11, 80, 11, '2018', '2018-08-30 10:25:59', '2018-08-30 10:25:59'),
(12, 80, 12, 'New', '2018-08-30 10:25:59', '2018-08-30 10:25:59'),
(13, 100, 10, 'Ausa', '2018-10-17 09:31:42', '2018-10-17 09:31:42'),
(14, 100, 11, '2018', '2018-10-17 09:31:42', '2018-10-17 09:31:42'),
(15, 100, 12, 'New', '2018-10-17 09:31:43', '2018-10-17 09:31:43'),
(16, 108, 21, 'S', '2019-05-21 12:47:26', '2019-05-21 12:47:26'),
(17, 108, 22, 'Blue', '2019-05-21 12:47:26', '2019-05-21 12:47:26');

-- --------------------------------------------------------

--
-- Table structure for table `advert_media`
--

CREATE TABLE `advert_media` (
  `id` int(10) UNSIGNED NOT NULL,
  `advert_id` int(11) NOT NULL,
  `type` varchar(80) NOT NULL,
  `link` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `advert_media`
--

INSERT INTO `advert_media` (`id`, `advert_id`, `type`, `link`, `created_at`, `updated_at`) VALUES
(51, 81, 'image', 'images/productimages/0585800067838055.jpg', '2018-08-31 22:21:46', '2018-08-31 22:21:46'),
(61, 91, 'image', 'images/productimages/0945540019643572.jpg', '2018-08-31 22:34:06', '2018-08-31 22:34:06'),
(62, 92, 'image', 'images/productimages/0118249630133314.jpg', '2018-08-31 22:34:25', '2018-08-31 22:34:25'),
(63, 93, 'image', 'images/productimages/0283369232972033.jpg', '2018-08-31 22:34:47', '2018-08-31 22:34:47'),
(67, 97, 'image', 'images/productimages/0524794733135693.jpg', '2018-08-31 22:39:01', '2018-08-31 22:39:01'),
(68, 97, 'image', 'images/productimages/0265551212789972.jpg', '2018-08-31 22:39:01', '2018-08-31 22:39:01'),
(69, 97, 'image', 'images/productimages/0871010870084876.jpg', '2018-08-31 22:39:01', '2018-08-31 22:39:01'),
(70, 97, 'image', 'images/productimages/0781194681739064.jpg', '2018-08-31 22:39:01', '2018-08-31 22:39:01'),
(72, 98, 'image', 'images/productimages/0736053040372827.jpg', '2018-08-31 22:40:31', '2018-08-31 22:40:31'),
(73, 98, 'image', 'images/productimages/0681630427965817.jpg', '2018-08-31 22:40:31', '2018-08-31 22:40:31'),
(165, 80, 'video', 'videos/productvideos/0595043386966530.mp4', '2018-09-17 12:20:04', '2018-09-17 12:20:04'),
(166, 80, 'image', 'images/productimages/0888912639241663.jpg', '2018-09-17 13:03:21', '2018-09-17 13:03:21'),
(167, 80, 'image', 'images/productimages/0819879752437865.png', '2018-09-17 13:03:21', '2018-09-17 13:03:21'),
(168, 80, 'image', 'images/productimages/0516370152748935.jpg', '2018-09-17 13:03:21', '2018-09-17 13:03:21'),
(169, 80, 'audio', 'audios/productaudios/0975322980407786.mp3', '2018-09-17 13:03:47', '2018-09-17 13:03:47'),
(170, 100, 'image', 'images/productimages/0945960702936318.jpg', '2018-10-17 09:31:43', '2018-10-17 09:31:43'),
(171, 101, 'image', 'images/productimages/0834075535230207.jpg', '2018-10-17 10:29:09', '2018-10-17 10:29:09'),
(172, 102, 'image', 'images/productimages/0522903792597369.jpg', '2018-10-17 10:39:19', '2018-10-17 10:39:19'),
(173, 103, 'image', 'images/productimages/0545990547225507.jpg', '2018-10-17 10:51:42', '2018-10-17 10:51:42'),
(174, 103, 'image', 'images/productimages/0650211860554619.png', '2018-10-17 10:51:42', '2018-10-17 10:51:42'),
(175, 104, 'audio', 'audios/productaudios/0918828460380205.mp3', '2018-10-18 12:25:25', '2018-10-18 12:25:25'),
(176, 104, 'video', 'videos/productvideos/0240943253162889.mp4', '2018-10-18 12:25:25', '2018-10-18 12:25:25'),
(177, 104, 'image', 'images/productimages/0145617706020986.jpg', '2018-10-18 12:25:25', '2018-10-18 12:25:25'),
(178, 104, 'image', 'images/productimages/0450290743798293.jpg', '2018-10-18 12:25:25', '2018-10-18 12:25:25'),
(179, 105, 'image', 'images/productimages/0394686278566080.jpg', '2018-10-18 12:28:39', '2018-10-18 12:28:39'),
(180, 105, 'image', 'images/productimages/0742593619632666.jpg', '2018-10-18 12:28:39', '2018-10-18 12:28:39'),
(181, 105, 'image', 'images/productimages/0910470891395998.jpg', '2018-10-18 12:28:39', '2018-10-18 12:28:39'),
(182, 105, 'video', 'videos/productvideos/0919700873005454.mp4', '2018-10-18 12:28:39', '2018-10-18 12:28:39'),
(183, 105, 'audio', 'audios/productaudios/0485783620156922.mp3', '2018-10-18 12:28:39', '2018-10-18 12:28:39'),
(184, 106, 'audio', 'audios/productaudios/0135906272023795.mp3', '2018-10-18 12:36:58', '2018-10-18 12:36:58'),
(185, 106, 'video', 'videos/productvideos/0933963326167176.mp4', '2018-10-18 12:36:58', '2018-10-18 12:36:58'),
(186, 107, 'image', 'images/productimages/0479111127979958.jpg', '2018-10-18 13:19:49', '2018-10-18 13:19:49'),
(187, 116, 'image', 'images/productimages/0309639280325344.jpg', '2019-05-09 10:41:08', '2019-05-09 10:41:08'),
(188, 120, 'image', 'images/productimages/0766775847225061.jpg', '2019-05-09 11:20:30', '2019-05-09 11:20:30'),
(189, 108, 'image', 'images/productimages/0785497743035592.jpg', '2019-05-21 12:47:28', '2019-05-21 12:47:28'),
(190, 108, 'image', 'images/productimages/0282830354489625.jpg', '2019-05-21 12:47:28', '2019-05-21 12:47:28'),
(191, 108, 'image', 'images/productimages/0709042792026567.jpg', '2019-05-21 12:47:29', '2019-05-21 12:47:29'),
(192, 108, 'video', 'videos/productvideos/0197521395947874.mp4', '2019-05-21 12:47:29', '2019-05-21 12:47:29'),
(193, 109, 'image', 'images/productimages/0547633616786434.jpg', '2019-06-10 09:12:47', '2019-06-10 09:12:47'),
(194, 110, 'image', 'images/productimages/0956419241308576.jpg', '2019-06-10 09:15:40', '2019-06-10 09:15:40'),
(195, 111, 'image', 'images/productimages/0155879486372969.png', '2019-06-10 09:19:06', '2019-06-10 09:19:06'),
(196, 112, 'image', 'images/productimages/0509191218997459.jpg', '2019-06-10 09:20:48', '2019-06-10 09:20:48'),
(197, 113, 'image', 'images/productimages/0713580468416051.png', '2019-06-19 07:24:25', '2019-06-19 07:24:25');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `category` varchar(80) NOT NULL,
  `category_slug` varchar(80) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category`, `category_slug`, `created_at`, `updated_at`) VALUES
(1, 'Vehicles', 'vehicles', '2018-08-09 10:09:08', '2018-10-13 17:39:31'),
(3, 'Electronics', 'electronics', '2018-08-09 10:12:27', '2018-09-01 05:14:13'),
(4, 'Home & Garden', 'home-garden', '2018-08-09 10:12:27', '2018-09-01 05:14:13'),
(5, 'Fashion', 'fashion', '2018-08-09 10:12:27', '2018-09-01 05:14:13'),
(6, 'Animal & Pets', 'animal-pets', '2018-08-09 10:12:27', '2018-09-01 05:14:13'),
(7, 'Hobbies (Art & Sport)', 'hobbies-art-sport', '2018-08-09 10:12:27', '2018-09-01 05:14:14'),
(8, 'Real Estate', 'real-estate', '2018-08-09 10:12:27', '2018-09-01 05:14:14'),
(9, 'Commercial Equipment', 'commercial-equipment', '2018-08-09 10:12:27', '2018-09-01 05:14:14'),
(10, 'Agriculture & Food', 'agriculture-food', '2018-08-09 10:12:27', '2018-09-01 05:14:14'),
(11, 'Health & Beauty', 'health-beauty', '2018-08-09 10:12:27', '2018-09-01 05:14:14'),
(12, 'Jobs', 'jobs', '2018-08-09 10:12:27', '2018-09-01 05:14:14'),
(13, 'Babies & Kids', 'babies-kids', '2018-08-09 10:12:27', '2018-09-01 05:14:14'),
(14, 'CVs', 'cvs', '2018-08-09 10:12:27', '2018-09-01 05:14:14'),
(15, 'Services', 'services', '2018-08-09 10:12:27', '2018-10-13 18:00:49');

-- --------------------------------------------------------

--
-- Table structure for table `categories_sub`
--

CREATE TABLE `categories_sub` (
  `id` int(10) UNSIGNED NOT NULL,
  `categories_id` int(11) NOT NULL,
  `categories_sub` varchar(60) NOT NULL,
  `categories_slug` varchar(80) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories_sub`
--

INSERT INTO `categories_sub` (`id`, `categories_id`, `categories_sub`, `categories_slug`, `created_at`, `updated_at`) VALUES
(2, 1, 'Heavy Equipments', 'heavy-equipments', '2018-08-09 10:14:32', '2018-10-16 10:17:25'),
(4, 1, 'Vehicle Parts', 'vehicle-parts', '2018-08-09 10:14:32', '2018-10-13 18:01:48'),
(8, 3, 'Audio & Music Equipment', 'audio-music-equipment', '2018-08-09 10:18:15', '2019-05-09 14:50:05'),
(9, 3, 'Cameras & Accessories', 'cameras-accessories', '2018-08-09 10:18:15', '2018-09-01 05:14:15'),
(10, 3, 'Computer Accessories', 'computer-accessories', '2018-08-09 10:18:15', '2018-09-01 05:14:15'),
(11, 3, 'Computer Hardware', 'computer-hardware', '2018-08-09 10:18:15', '2018-09-01 05:14:15'),
(12, 3, 'Laptops & Computers', 'laptops-computers', '2018-08-09 10:18:15', '2018-09-01 05:14:15'),
(13, 3, 'TV & DVD Equipment', 'tv-dvd-equipment', '2018-08-09 10:18:15', '2018-09-01 05:14:15'),
(14, 3, 'Video Games', 'video-games', '2018-08-09 10:18:15', '2018-09-01 05:14:15'),
(15, 3, 'Video Game Consoles', 'video-game-consoles', '2018-08-09 10:18:15', '2018-09-01 05:14:15'),
(16, 4, 'Furniture', 'furniture', '2018-08-09 10:19:49', '2018-09-01 05:14:15'),
(17, 4, 'Garden', 'garden', '2018-08-09 10:19:49', '2018-09-01 05:14:15'),
(18, 4, 'Home Accessories', 'home-accessories', '2018-08-09 10:19:49', '2018-09-01 05:14:15'),
(19, 4, 'Home Appliances', 'home-appliances', '2018-08-09 10:19:49', '2018-09-01 05:14:15'),
(20, 4, 'Kitchen Appliances', 'kitchen-appliances', '2018-08-09 10:19:49', '2018-09-01 05:14:16'),
(21, 4, 'Kitchen & Dining', 'kitchen-dining', '2018-08-09 10:19:49', '2018-09-01 05:14:16'),
(22, 5, 'Bags', 'bags', '2018-08-09 10:21:35', '2018-09-01 05:14:16'),
(23, 5, 'Clothing', 'clothing', '2018-08-09 10:21:35', '2018-09-01 05:14:16'),
(24, 5, 'Clothing Accessories', 'clothing-accessories', '2018-08-09 10:21:35', '2018-09-01 05:14:16'),
(25, 5, 'Jewelry', 'jewelry', '2018-08-09 10:21:35', '2018-09-01 05:14:16'),
(26, 5, 'Shoes', 'shoes', '2018-08-09 10:21:35', '2018-09-01 05:14:16'),
(27, 5, 'Watches', 'watches', '2018-08-09 10:21:35', '2018-09-01 05:14:16'),
(28, 5, 'Wedding Wear', 'wedding-wear', '2018-08-09 10:21:35', '2018-09-01 05:14:16'),
(29, 6, 'Birds', 'birds', '2018-08-09 10:23:38', '2018-09-01 05:14:16'),
(30, 6, 'Cats & Kittens', 'cats-kittens', '2018-08-09 10:23:38', '2018-09-01 05:14:16'),
(31, 6, 'Dogs & Puppies', 'dogs-puppies', '2018-08-09 10:23:38', '2018-09-01 05:14:16'),
(32, 6, 'Fish', 'fish', '2018-08-09 10:23:38', '2018-09-01 05:14:16'),
(33, 6, 'Pets Accessories', 'pets-accessories', '2018-08-09 10:23:38', '2018-09-01 05:14:16'),
(34, 6, 'Reptiles', 'reptiles', '2018-08-09 10:23:38', '2018-09-01 05:14:16'),
(35, 6, 'Other Animals', 'other-animals', '2018-08-09 10:23:38', '2018-09-01 05:14:16'),
(36, 7, 'Arts & Crafts', 'arts-crafts', '2018-08-09 10:25:19', '2018-09-01 05:14:16'),
(37, 7, 'Books & Games', 'books-games', '2018-08-09 10:25:19', '2018-09-01 05:14:17'),
(38, 7, 'Camping Gear', 'camping-gear', '2018-08-09 10:25:19', '2018-09-01 05:14:17'),
(39, 7, 'CDs & DVDs', 'cds-dvds', '2018-08-09 10:25:19', '2018-09-01 05:14:17'),
(40, 7, 'Musical Instruments', 'musical-instruments', '2018-08-09 10:25:19', '2018-09-01 05:14:17'),
(41, 7, 'Sports Equipment', 'sports-equipment', '2018-08-09 10:25:19', '2018-09-01 05:14:17'),
(42, 8, 'Commercial Property For Rent', 'commercial-property-for-rent', '2018-08-09 10:28:10', '2018-09-01 05:14:17'),
(43, 8, 'Commercial Property For Sale', 'commercial-property-for-sale', '2018-08-09 10:28:10', '2018-09-01 05:14:17'),
(44, 8, 'Houses & Apartment For Rent', 'houses-apartment-for-rent', '2018-08-09 10:28:10', '2018-09-01 05:14:17'),
(45, 8, 'Houses & Apartments For Sale', 'houses-apartments-for-sale', '2018-08-09 10:28:10', '2018-09-01 05:14:17'),
(46, 8, 'Land & Plots For Rent', 'land-plots-for-rent', '2018-08-09 10:28:10', '2018-09-01 05:14:17'),
(47, 8, 'Land & Plots For Sale', 'land-plots-for-sale', '2018-08-09 10:28:10', '2018-09-01 05:14:17'),
(48, 8, 'Temporary & Vacation Rentals', 'temporary-vacation-rentals', '2018-08-09 10:28:10', '2018-09-01 05:14:17'),
(49, 9, 'Industrial Ovens', 'industrial-ovens', '2018-08-09 10:30:41', '2018-09-01 05:14:17'),
(50, 9, 'Store Equipment', 'store-equipment', '2018-08-09 10:30:41', '2018-09-01 05:14:17'),
(51, 9, 'Restaurant & Catering Equipment', 'restaurant-catering-equipment', '2018-08-09 10:30:41', '2019-05-09 14:52:07'),
(52, 9, 'Manufacturing Equipment', 'manufacturing-equipment', '2018-08-09 10:30:41', '2018-09-01 05:14:17'),
(53, 9, 'Medical Equipment', 'medical-equipment', '2018-08-09 10:30:41', '2018-09-01 05:14:17'),
(54, 9, 'Safety Equipment', 'safety-equipment', '2018-08-09 10:30:41', '2018-09-01 05:14:17'),
(55, 9, 'Printing Equipment', 'printing-equipment', '2018-08-09 10:30:41', '2018-09-01 05:14:18'),
(56, 9, 'Salon Equipment', 'salon-equipment', '2018-08-09 10:30:41', '2018-09-01 05:14:18'),
(57, 9, 'Stationery', 'stationery', '2018-08-09 10:30:41', '2018-09-01 05:14:18'),
(58, 10, 'Farm Machinery & Equipment', 'farm-machinery-equipment', '2018-08-09 10:32:01', '2018-09-01 05:14:18'),
(59, 10, 'Feeds, Supplements & Seeds', 'feeds-supplements-seeds', '2018-08-09 10:32:01', '2018-09-01 05:14:18'),
(60, 10, 'Livestock & Poultry', 'livestock-poultry', '2018-08-09 10:32:01', '2018-09-01 05:14:18'),
(61, 10, 'Meals & Drinks', 'meals-drinks', '2018-08-09 10:32:01', '2018-09-01 05:14:18'),
(62, 11, 'Bath & Body', 'bath-body', '2018-08-09 10:34:17', '2018-09-01 05:14:18'),
(63, 11, 'Fragrance', 'fragrance', '2018-08-09 10:34:17', '2018-09-01 05:14:18'),
(64, 11, 'Hair Beauty', 'hair-beauty', '2018-08-09 10:34:17', '2018-09-01 05:14:18'),
(65, 11, 'Makeup', 'makeup', '2018-08-09 10:34:17', '2018-09-01 05:14:18'),
(66, 11, 'Sexual Wellness', 'sexual-wellness', '2018-08-09 10:34:17', '2018-09-01 05:14:18'),
(67, 11, 'Skin Care', 'skin-care', '2018-08-09 10:34:17', '2018-09-01 05:14:18'),
(68, 11, 'Tools & Accessories', 'tools-accessories', '2018-08-09 10:34:17', '2018-09-01 05:14:18'),
(69, 11, 'Vitamins & Supplements', 'vitamins-supplements', '2018-08-09 10:34:17', '2018-09-01 05:14:18'),
(70, 12, 'Accounting & Finance Jobs', 'accounting-finance-jobs', '2018-08-09 11:42:26', '2018-09-01 05:14:19'),
(71, 12, 'Advertising & Marketing Jobs', 'advertising-marketing-jobs', '2018-08-09 11:42:26', '2018-09-01 05:14:19'),
(72, 12, 'Arts & Entertainments Jobs', 'arts-entertainments-jobs', '2018-08-09 12:02:20', '2018-09-01 05:14:19'),
(73, 12, 'Childcare & Babysitting Jobs', 'childcare-babysitting-jobs', '2018-08-09 11:42:26', '2018-09-01 05:14:19'),
(74, 12, 'Clerical & Administrative Jobs', 'clerical-administrative-jobs', '2018-08-09 11:42:26', '2018-09-01 05:14:19'),
(75, 12, 'Computing & IT Jobs', 'computing-it-jobs', '2018-08-09 11:42:26', '2018-09-01 05:14:19'),
(76, 12, 'Construction & Skilled Trade Jobs', 'construction-skilled-trade-jobs', '2018-08-09 11:42:26', '2018-09-01 05:14:19'),
(77, 12, 'Customer Service Jobs', 'customer-service-jobs', '2018-08-09 11:42:26', '2018-09-01 05:14:19'),
(78, 12, 'Driver Jobs', 'driver-jobs', '2018-08-09 11:42:26', '2018-09-01 05:14:19'),
(79, 12, 'Engineering & Architecture Jobs', 'engineering-architecture-jobs', '2018-08-09 11:42:26', '2018-09-01 05:14:19'),
(80, 12, 'Farming & Veterinary Jobs', 'farming-veterinary-jobs', '2018-08-09 11:42:26', '2018-09-01 05:14:19'),
(81, 12, 'Gardening Landscaping Jobs', 'gardening-landscaping-jobs', '2018-08-09 11:42:26', '2018-09-01 05:14:19'),
(82, 12, 'Health & Beauty Jobs', 'health-beauty-jobs', '2018-08-09 11:42:26', '2018-09-01 05:14:19'),
(83, 12, 'Healthcare & Nursing Jobs', 'healthcare-nursing-jobs', '2018-08-09 11:42:26', '2018-09-01 05:14:19'),
(84, 12, 'Hotel Jobs', 'hotel-jobs', '2018-08-09 11:42:26', '2018-09-01 05:14:19'),
(85, 12, 'Housekeeping & Cleaning Jobs', 'housekeeping-cleaning-jobs', '2018-08-09 11:42:26', '2018-09-01 05:14:19'),
(86, 12, 'Human Resources Jobs', 'human-resources-jobs', '2018-08-09 11:42:26', '2018-09-01 05:14:19'),
(87, 12, 'Internship Jobs', 'internship-jobs', '2018-08-09 11:42:26', '2018-09-01 05:14:19'),
(88, 12, 'Legal Jobs', 'legal-jobs', '2018-08-09 11:42:26', '2018-09-01 05:14:19'),
(89, 12, 'Logistics & Transportation Jobs', 'logistics-transportation-jobs', '2018-08-09 11:42:26', '2018-09-01 05:14:20'),
(90, 12, 'Manual Labour Jobs', 'manual-labour-jobs', '2018-08-09 11:42:26', '2018-09-01 05:14:20'),
(91, 12, 'Manufacturing Jobs', 'manufacturing-jobs', '2018-08-09 11:42:26', '2018-09-01 05:14:20'),
(92, 12, 'Mining Industry Jobs', 'mining-industry-jobs', '2018-08-09 11:42:26', '2018-09-01 05:14:20'),
(93, 12, 'Office Jobs', 'office-jobs', '2018-08-09 11:42:26', '2018-09-01 05:14:20'),
(94, 12, 'Part-time & Weekend Jobs', 'part-time-weekend-jobs', '2018-08-09 11:42:26', '2018-09-01 05:14:20'),
(95, 12, 'Restaurant & Bar Jobs', 'restaurant-bar-jobs', '2018-08-09 11:42:26', '2018-09-01 05:14:20'),
(96, 12, 'Retail Jobs', 'retail-jobs', '2018-08-09 11:42:26', '2018-09-01 05:14:20'),
(97, 12, 'Sales & Telemarketing Jobs', 'sales-telemarketing-jobs', '2018-08-09 11:42:26', '2018-09-01 05:14:20'),
(98, 12, 'Security Jobs', 'security-jobs', '2018-08-09 11:42:26', '2018-09-01 05:14:20'),
(99, 12, 'Sports Club Jobs', 'sports-club-jobs', '2018-08-09 11:42:26', '2018-09-01 05:14:20'),
(100, 12, 'Teaching Jobs', 'teaching-jobs', '2018-08-09 11:42:26', '2018-09-01 05:14:20'),
(101, 12, 'Technology Jobs', 'technology-jobs', '2018-08-09 11:42:26', '2018-09-01 05:14:20'),
(102, 12, 'Travel & Tourism Jobs', 'travel-tourism-jobs', '2018-08-09 11:42:26', '2018-09-01 05:14:20'),
(103, 12, 'Other Jobs', 'other-jobs', '2018-08-09 11:42:26', '2018-09-01 05:14:20'),
(104, 13, 'Babies & Kids Accessories', 'babies-kids-accessories', '2018-08-09 11:46:48', '2018-09-01 05:14:20'),
(105, 13, 'Baby Care', 'baby-care', '2018-08-09 11:46:48', '2018-09-01 05:14:20'),
(106, 13, 'Children\'s Clothing', 'childrens-clothing', '2018-08-09 11:46:48', '2018-09-01 05:14:20'),
(107, 13, 'Children\'s Furniture', 'childrens-furniture', '2018-08-09 11:46:48', '2018-09-01 05:14:21'),
(108, 13, 'Children\'s Gear & Safety', 'childrens-gear-safety', '2018-08-09 11:46:48', '2018-09-01 05:14:21'),
(109, 13, 'Children\'s Safety', 'childrens-safety', '2018-08-09 11:46:48', '2018-09-01 05:14:21'),
(110, 13, 'Maternity & Pregnancy', 'maternity-pregnancy', '2018-08-09 11:46:48', '2018-09-01 05:14:21'),
(111, 13, 'Prams & Strollers', 'prams-strollers', '2018-08-09 11:46:48', '2018-09-01 05:14:21'),
(112, 13, 'Toys', 'toys', '2018-08-09 11:46:48', '2018-09-01 05:14:21'),
(113, 14, 'Accounting & Finance CVs', 'accounting-finance-cvs', '2018-08-09 10:42:26', '2018-09-01 05:14:21'),
(114, 14, 'Advertising & Marketing CVs', 'advertising-marketing-cvs', '2018-08-09 10:42:26', '2018-09-01 05:14:21'),
(115, 14, 'Arts & Entertainments CVs', 'arts-entertainments-cvs', '2018-08-09 10:42:26', '2018-09-01 05:14:21'),
(116, 14, 'Childcare & Babysitting CVs', 'childcare-babysitting-cvs', '2018-08-09 10:42:26', '2018-09-01 05:14:21'),
(117, 14, 'Clerical & Administrative CVs', 'clerical-administrative-cvs', '2018-08-09 10:42:26', '2018-09-01 05:14:21'),
(118, 14, 'Computing & IT CVs', 'computing-it-cvs', '2018-08-09 10:42:26', '2018-09-01 05:14:21'),
(119, 14, 'Construction & Skilled Trade CVs', 'construction-skilled-trade-cvs', '2018-08-09 10:42:26', '2018-09-01 05:14:21'),
(120, 14, 'Customer Service CVs', 'customer-service-cvs', '2018-08-09 10:42:26', '2018-09-01 05:14:21'),
(121, 14, 'Driver CVs', 'driver-cvs', '2018-08-09 10:42:26', '2018-09-01 05:14:21'),
(122, 14, 'Engineering & Architecture CVs', 'engineering-architecture-cvs', '2018-08-09 10:42:26', '2018-09-01 05:14:22'),
(123, 14, 'Farming & Veterinary CVs', 'farming-veterinary-cvs', '2018-08-09 10:42:26', '2018-09-01 05:14:22'),
(124, 14, 'Gardening Landscaping CVs', 'gardening-landscaping-cvs', '2018-08-09 10:42:26', '2018-09-01 05:14:22'),
(125, 14, 'Health & Beauty CVs', 'health-beauty-cvs', '2018-08-09 10:42:26', '2018-09-01 05:14:22'),
(126, 14, 'Healthcare & Nursing CVs', 'healthcare-nursing-cvs', '2018-08-09 10:42:26', '2018-09-01 05:14:22'),
(127, 14, 'Hotel CVs', 'hotel-cvs', '2018-08-09 10:42:26', '2018-09-01 05:14:22'),
(128, 14, 'Housekeeping & Cleaning CVs', 'housekeeping-cleaning-cvs', '2018-08-09 10:42:26', '2018-09-01 05:14:22'),
(129, 14, 'Human Resources CVs', 'human-resources-cvs', '2018-08-09 10:42:26', '2018-09-01 05:14:22'),
(130, 14, 'Internship CVs', 'internship-cvs', '2018-08-09 10:42:26', '2018-09-01 05:14:22'),
(131, 14, 'Legal CVs', 'legal-cvs', '2018-08-09 10:42:26', '2018-09-01 05:14:22'),
(132, 14, 'Logistics & Transportation CVs', 'logistics-transportation-cvs', '2018-08-09 10:42:26', '2018-09-01 05:14:22'),
(133, 14, 'Manual Labour CVs', 'manual-labour-cvs', '2018-08-09 10:42:26', '2018-09-01 05:14:22'),
(134, 14, 'Manufacturing CVs', 'manufacturing-cvs', '2018-08-09 10:42:26', '2018-09-01 05:14:22'),
(135, 14, 'Mining Industry CVs', 'mining-industry-cvs', '2018-08-09 10:42:26', '2018-09-01 05:14:22'),
(136, 14, 'Office CVs', 'office-cvs', '2018-08-09 10:42:26', '2018-09-01 05:14:22'),
(137, 14, 'Part-time & Weekend CVs', 'part-time-weekend-cvs', '2018-08-09 10:42:26', '2018-09-01 05:14:22'),
(138, 14, 'Restaurant & Bar CVs', 'restaurant-bar-cvs', '2018-08-09 10:42:26', '2018-09-01 05:14:22'),
(139, 14, 'Retail CVs', 'retail-cvs', '2018-08-09 10:42:26', '2018-09-01 05:14:22'),
(140, 14, 'Sales & Telemarketing CVs', 'sales-telemarketing-cvs', '2018-08-09 10:42:26', '2018-09-01 05:14:22'),
(141, 14, 'Security CVs', 'security-cvs', '2018-08-09 10:42:26', '2018-09-01 05:14:22'),
(142, 14, 'Sports Club CVs', 'sports-club-cvs', '2018-08-09 10:42:26', '2018-09-01 05:14:22'),
(143, 14, 'Teaching CVs', 'teaching-cvs', '2018-08-09 10:42:26', '2018-09-01 05:14:22'),
(144, 14, 'Technology CVs', 'technology-cvs', '2018-08-09 10:42:26', '2018-09-01 05:14:23'),
(145, 14, 'Travel & Tourism CVs', 'travel-tourism-cvs', '2018-08-09 10:42:26', '2018-09-01 05:14:23'),
(146, 14, 'Other CVs', 'other-cvs', '2018-08-09 10:42:26', '2018-09-01 05:14:23'),
(147, 15, 'Automotive Services', 'automotive-services', '2018-08-09 12:10:52', '2018-09-01 05:14:23'),
(148, 15, 'Building & Trade Services', 'building-trade-services', '2018-08-09 12:10:52', '2018-09-01 05:14:23'),
(149, 15, 'Chauffeur & Airport Transfer Services', 'chauffeur-airport-transfer-services', '2018-08-09 12:10:52', '2018-09-01 05:14:23'),
(150, 15, 'Child Care & Education Services', 'child-care-education-services', '2018-08-09 12:10:52', '2018-09-01 05:14:23'),
(151, 15, 'Cleaning Services', 'cleaning-services', '2018-08-09 12:10:52', '2018-09-01 05:14:23'),
(152, 15, 'Computer & IT Services', 'computer-it-services', '2018-08-09 12:10:52', '2018-09-01 05:14:23'),
(153, 15, 'DJ & Entertainment Services', 'dj-entertainment-services', '2018-08-09 12:10:52', '2018-09-01 05:14:23'),
(154, 15, 'Party, Catering & Event Services', 'party-catering-event-services', '2018-08-09 12:10:52', '2018-09-01 05:14:23'),
(155, 15, 'Health & Beauty Services', 'health-beauty-services', '2018-08-09 12:10:52', '2018-09-01 05:14:24'),
(156, 15, 'Fitness & Personal Training Services', 'fitness-personal-training-services', '2018-08-09 12:10:52', '2018-09-01 05:14:24'),
(157, 15, 'Landscaping & Gardening Services', 'landscaping-gardening-services', '2018-08-09 12:10:52', '2018-09-01 05:14:24'),
(158, 15, 'Manufacturing Services', 'manufacturing-services', '2018-08-09 12:10:52', '2018-09-01 05:14:24'),
(159, 15, 'Legal Services', 'legal-services', '2018-08-09 12:10:52', '2018-09-01 05:14:24'),
(160, 15, 'Pet Services', 'pet-services', '2018-08-09 12:10:52', '2018-09-01 05:14:24'),
(161, 15, 'Photography & Video Services', 'photography-video-services', '2018-08-09 12:10:52', '2018-09-01 05:14:24'),
(162, 15, 'Recruitment Services', 'recruitment-services', '2018-08-09 12:10:52', '2018-09-01 05:14:24'),
(163, 15, 'Logistics Services', 'logistics-services', '2018-08-09 12:10:52', '2018-09-01 05:14:24'),
(164, 15, 'Repair Services', 'repair-services', '2018-08-09 12:10:52', '2018-09-01 05:14:24'),
(165, 15, 'Tax & Financial Services', 'tax-financial-services', '2018-08-09 12:10:52', '2018-09-01 05:14:24'),
(166, 15, 'Travel Agents & Tours', 'travel-agents-tours', '2018-08-09 12:10:52', '2018-09-01 05:14:24'),
(167, 15, 'Wedding Venues & Services', 'wedding-venues-services', '2018-08-09 12:10:52', '2018-09-01 05:14:24'),
(168, 15, 'Classes & Courses', 'classes-courses', '2018-08-09 12:10:52', '2018-09-01 05:14:24'),
(169, 8, 'Hotels', 'hotels', '2019-05-09 14:51:16', '2019-05-09 14:51:16'),
(170, 8, 'Event Centers', 'event-centers', '2019-05-09 14:51:16', '2019-05-09 14:51:16'),
(171, 8, 'Venues', 'venues', '2019-05-09 14:51:16', '2019-05-09 14:51:16'),
(172, 1, 'Water Crafts', 'water-crafts', '2019-05-09 15:08:50', '2019-05-09 15:08:50'),
(173, 1, 'Trucks & Trailers', 'trucks-trailers', '2019-05-09 15:08:50', '2019-05-09 15:08:50'),
(174, 1, 'Motorcycles & Scooters', 'motorcycles-scooters', '2019-05-09 15:08:50', '2019-05-09 15:08:50'),
(175, 1, 'Cars', 'cars', '2019-05-09 15:08:50', '2019-05-09 15:08:50');

-- --------------------------------------------------------

--
-- Table structure for table `categories_sub_attributes`
--

CREATE TABLE `categories_sub_attributes` (
  `id` int(10) UNSIGNED NOT NULL,
  `categories_sub_id` int(11) NOT NULL,
  `attributes` varchar(60) NOT NULL,
  `attributes_data` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories_sub_attributes`
--

INSERT INTO `categories_sub_attributes` (`id`, `categories_sub_id`, `attributes`, `attributes_data`, `created_at`, `updated_at`) VALUES
(10, 2, 'Make', 'Ausa,Bobcat', '2018-08-09 14:45:05', '0000-00-00 00:00:00'),
(11, 2, 'Year Of Maintenance', '2018,2017', '2018-08-09 14:45:05', '0000-00-00 00:00:00'),
(12, 2, 'Condition', 'New,Nigerian Used,Foreign Used', '2018-08-09 14:45:05', '0000-00-00 00:00:00'),
(13, 42, 'Property Type', 'Office,Shop,Hotel,Warehouse', '2018-08-09 14:50:01', '0000-00-00 00:00:00'),
(14, 42, 'Square Meters', 'input', '2018-08-09 14:50:01', '0000-00-00 00:00:00'),
(15, 42, 'Parking Spaces', '1,2,3,4,5,6,7,8,9,10,11,12', '2018-08-09 14:50:01', '0000-00-00 00:00:00'),
(16, 42, 'Secure Parking', 'Yes, No', '2018-08-09 14:50:01', '0000-00-00 00:00:00'),
(17, 43, 'Property Type', 'Office,Shop,Hotel,Warehouse', '2018-08-09 14:52:40', '0000-00-00 00:00:00'),
(18, 43, 'Square Meters', 'input', '2018-08-09 14:52:40', '0000-00-00 00:00:00'),
(19, 43, 'Secure Parking', 'Yes, No', '2018-08-09 14:52:40', '0000-00-00 00:00:00'),
(20, 43, 'Parking Spaces', '1,2,3,4,5,6,7,8,9,10,11,More than 12', '2018-08-09 14:52:40', '0000-00-00 00:00:00'),
(21, 23, 'Size', 'S,M,L,All Sizes', '2018-10-18 09:55:41', '2018-10-18 10:30:16'),
(22, 23, 'Color', 'input', '2018-10-18 09:55:41', '2018-10-18 09:55:41');

-- --------------------------------------------------------

--
-- Table structure for table `favourites`
--

CREATE TABLE `favourites` (
  `id` int(10) UNSIGNED NOT NULL,
  `advert_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `footer_pages`
--

CREATE TABLE `footer_pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `page` text NOT NULL,
  `slug` text,
  `content` longtext NOT NULL,
  `footer_column` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `footer_pages`
--

INSERT INTO `footer_pages` (`id`, `page`, `slug`, `content`, `footer_column`, `created_at`, `updated_at`) VALUES
(1, 'About Us', 'about-us', '<h1>Clipboard Module</h1><p>The Clipboard handles copy, cut and paste between Quill and external applications. A set of defaults exist to provide sane interpretation of pasted content, with the ability for further customization through matchers.</p><p>The Clipboard interprets pasted HTML by traversing the corresponding DOM tree in&nbsp;<a href=\"https://en.wikipedia.org/wiki/Tree_traversal#Post-order\" target=\"_blank\" style=\"color: rgb(37, 64, 143);\">post-order</a>, building up a&nbsp;<a href=\"https://quilljs.com/docs/delta/\" target=\"_blank\" style=\"color: rgb(37, 64, 143);\">Delta</a>&nbsp;representation of all subtrees. At each descendant node, matcher functions are called with the DOM Node and Delta interpretation so far, allowing the matcher to return a modified Delta interpretation.</p><p>Familiarity and comfort with&nbsp;<a href=\"https://quilljs.com/docs/delta/\" target=\"_blank\" style=\"color: rgb(37, 64, 143);\">Deltas</a>&nbsp;is necessary in order to effectively use matchers.</p>', 1, '2018-10-10 11:08:09', '2018-10-15 12:22:29'),
(2, 'Terms and Conditions', 'terms-and-conditions', '<h4>addMatcher</h4><p>Adds a custom matcher to the Clipboard. Matchers using&nbsp;<code style=\"background-color: rgb(241, 241, 241);\">nodeType</code>&nbsp;are called first, in the order they were added, followed by matchers using a CSS&nbsp;<code style=\"background-color: rgb(241, 241, 241);\">selector</code>, also in the order they were added.&nbsp;<code style=\"color: rgb(37, 64, 143); background-color: rgb(241, 241, 241);\"><a href=\"https://developer.mozilla.org/en-US/docs/Web/API/Node/nodeType\" target=\"_blank\">nodeType</a></code>&nbsp;may be&nbsp;<code style=\"background-color: rgb(241, 241, 241);\">Node.ELEMENT_NODE</code>&nbsp;or&nbsp;<code style=\"background-color: rgb(241, 241, 241);\">Node.TEXT_NODE</code>.</p>', 1, '2018-10-10 11:08:09', '2018-10-15 12:23:15'),
(3, 'Privacy Policy', 'privacy-policy', '', 1, '2018-10-10 11:08:46', '2018-10-10 11:10:27'),
(4, 'Billing Policy', 'billing-policy', '', 1, '2018-10-10 11:09:37', '2018-10-10 11:10:30'),
(5, 'Safety Tips', 'safety-tips', '', 2, '2018-10-10 11:09:37', '2018-10-10 11:10:33'),
(6, 'FAQ', 'faq', '', 2, '2018-10-10 11:10:00', '2018-10-10 11:10:36'),
(7, 'Contact Us', 'contact-us', '', 2, '2018-10-10 11:10:00', '2018-10-10 11:10:36');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `advert_id` int(11) NOT NULL,
  `user_sender_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `archive` varchar(10) NOT NULL,
  `readmessage` int(11) DEFAULT NULL,
  `active_recipient` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `advert_id`, `user_sender_id`, `message`, `archive`, `readmessage`, `active_recipient`, `created_at`, `updated_at`) VALUES
(1, 92, 37, 'Hello', '0', 0, 23, '2018-10-17 13:06:43', '2018-10-17 13:06:43');

-- --------------------------------------------------------

--
-- Table structure for table `messages_thread`
--

CREATE TABLE `messages_thread` (
  `id` int(10) UNSIGNED NOT NULL,
  `message_id` int(11) DEFAULT NULL,
  `advert_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `messages` text NOT NULL,
  `image` varchar(200) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages_thread`
--

INSERT INTO `messages_thread` (`id`, `message_id`, `advert_id`, `user_id`, `messages`, `image`, `created_at`, `updated_at`) VALUES
(1, 1, 92, 37, 'Hello', '', '2018-10-17 13:06:43', '2018-10-17 13:06:43'),
(2, 1, 92, 37, 'Can it go for 10,000', '', '2018-10-17 13:10:05', '2018-10-17 13:10:05');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `advert_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `admin` text NOT NULL,
  `readmessage` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `user_id`, `advert_id`, `comment`, `admin`, `readmessage`, `created_at`, `updated_at`) VALUES
(1, 37, 100, 'Your advert is now active', 'Olaogun Toluwalase', 'read', '2018-10-17 11:52:55', '2018-10-17 12:04:44'),
(2, 37, 100, 'Your advert has been blocked', 'Olaogun Toluwalase', 'read', '2018-10-17 11:55:30', '2018-10-17 12:04:44'),
(3, 37, 100, 'Your advert is now active', 'Olaogun Toluwalase', 'read', '2018-10-17 11:56:17', '2018-10-17 12:04:44');

-- --------------------------------------------------------

--
-- Table structure for table `reported_adverts`
--

CREATE TABLE `reported_adverts` (
  `id` int(11) NOT NULL,
  `advert_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `reason` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `password` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `password_reset` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `remember_token` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `full_name` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `phone_number` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `auth_type` varchar(80) COLLATE latin1_spanish_ci NOT NULL,
  `auth_id` varchar(80) COLLATE latin1_spanish_ci DEFAULT NULL,
  `image` text COLLATE latin1_spanish_ci NOT NULL,
  `role` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `state` text COLLATE latin1_spanish_ci,
  `address` text COLLATE latin1_spanish_ci,
  `company_name` text COLLATE latin1_spanish_ci,
  `company_unique_name` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `about_company` text COLLATE latin1_spanish_ci,
  `company_website` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `jobs_notification` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `ads_notification` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `tips_notification` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `general_notification` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `display_name_type` varchar(191) COLLATE latin1_spanish_ci NOT NULL DEFAULT 'full_name',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `password_reset`, `remember_token`, `full_name`, `phone_number`, `auth_type`, `auth_id`, `image`, `role`, `state`, `address`, `company_name`, `company_unique_name`, `about_company`, `company_website`, `jobs_notification`, `ads_notification`, `tips_notification`, `general_notification`, `last_login`, `display_name_type`, `created_at`, `updated_at`) VALUES
(1, 'javier94@hotmail.com', '$2y$10$yYZtKNWBa8h3xwRTKAB3n.DX/BCf0i/0lrkBCOrGhN2tEYBvO/j2S', NULL, 'mZULfeqqfYAaQ7P5xRBZ0BIye9uyP93hVD8OPJQwCXucRuNw7gI0tbqYQOqP', 'Cristina Wolff', '+9412013713011', 'default', '77131', '1.jpg', 'user', 'Kansas', '58247 Calista Valleys Apt. 878', 'Bergnaum-Thiel', 'Bergnaum-Thiel', 'Programmable object-oriented artificialintelligence', 'http://Bergnaum-Thiel.com', NULL, NULL, NULL, NULL, NULL, 'full_name', '2018-08-10 16:25:22', '2018-10-15 14:04:15'),
(2, 'wlabadie@hotmail.com', '$2y$10$x/i21TYutYYcv0H1ogtvJuO4/iszvuRWRUzVVaD.GpkYjoeWA7Fj6', NULL, NULL, 'Jany Schinner', '+7469215117945', 'default', '58924', '2.jpg', 'user', 'Hawaii', '966 Brennan Lane', 'Grady, Hill and Swaniawski', 'Grady,-Hill-and-Swaniawski', 'Team-oriented intermediate knowledgebase', 'http://Grady,-Hill-and-Swaniawski.com', NULL, NULL, NULL, NULL, NULL, 'full_name', '2018-08-10 15:11:34', '2018-08-10 15:11:34'),
(3, 'kasandra54@gmail.com', '$2y$10$ug7Zkc9Gy81XcWfSipHQn.gfjkWcetRoC6exCHM.OJkLQYWT19uNi', NULL, NULL, 'Laura Brekke', '+8093836832031', 'default', '54983', '3.jpg', 'user', 'Kentucky', '876 Alaina Place', 'Roberts, Gusikowski and Mosciski', 'Roberts,-Gusikowski-and-Mosciski', 'Re-engineered clear-thinking methodology', 'http://Roberts,-Gusikowski-and-Mosciski.com', NULL, NULL, NULL, NULL, NULL, 'full_name', '2018-08-10 15:11:35', '2018-08-10 15:11:35'),
(4, 'sawayn.delphine@hotmail.com', '$2y$10$sgqooAVQrF48eW1zonWNx.AzZfaoO.GO5JHhD9bHuIM2pyT.yka.y', NULL, NULL, 'Frida Treutel', '+5105406914284', 'default', '50652', '4.jpg', 'user', 'Washington', '998 Bartoletti Mill', 'Kilback, Mann and Bogan', 'Kilback,-Mann-and-Bogan', 'Digitized 24/7 approach', 'http://Kilback,-Mann-and-Bogan.com', NULL, NULL, NULL, NULL, NULL, 'full_name', '2018-08-10 15:11:35', '2018-08-10 15:11:35'),
(5, 'brown.laurianne@gmail.com', '$2y$10$Pti6YuvTuJAtVqcQwE7/2eYZPuPHO3.XqS9HZ67Ql05lzknKdMQL2', NULL, NULL, 'Ewell Bernhard', '+4866199034965', 'default', '78122', '5.jpg', 'user', 'Idaho', '62010 Aditya Viaduct', 'Nicolas Group', 'Nicolas-Group', 'Persistent neutral framework', 'http://Nicolas-Group.com', NULL, NULL, NULL, NULL, NULL, 'full_name', '2018-08-10 15:11:35', '2018-08-10 15:11:35'),
(6, 'rebekah21@hotmail.com', '$2y$10$NuRH40HL/dENRAB/3O6zIuhDcc6.v3WEnZ.JsoaxK/fdyPpDWGLGy', NULL, NULL, 'Rhett Gleason', '+5806013687503', 'default', '78916', '6.jpg', 'user', 'Wyoming', '8010 Kris Mountains Suite 962', 'Bernhard Inc', 'Bernhard-Inc', 'Cloned 5thgeneration array', 'http://Bernhard-Inc.com', NULL, NULL, NULL, NULL, NULL, 'full_name', '2018-08-10 15:11:35', '2018-08-10 15:11:35'),
(7, 'pollich.roosevelt@yahoo.com', '$2y$10$qcNV55uUIFBYGXy1.AfJo.9yv7/A2F0gMQbXJiTwCgRKKvwrUu1tC', NULL, NULL, 'Chase Harber', '+6839780902649', 'default', '81164', '7.jpg', 'user', 'North Carolina', '16191 Enid Isle', 'Nikolaus, Kozey and Homenick', 'Nikolaus,-Kozey-and-Homenick', 'Integrated 4thgeneration challenge', 'http://Nikolaus,-Kozey-and-Homenick.com', NULL, NULL, NULL, NULL, NULL, 'full_name', '2018-08-10 15:11:35', '2018-08-10 15:11:35'),
(8, 'rubye12@hotmail.com', '$2y$10$FKY2qsfUNvEIJJ77OtvIquidAhBDwkVkSdaPRNbtjGm18zvVCBBiy', NULL, NULL, 'Ena Hartmann', '+7793407335228', 'default', '52018', '8.jpg', 'user', 'Iowa', '667 Geraldine Crossing', 'Nitzsche-Wilderman', 'Nitzsche-Wilderman', 'Extended multimedia knowledgebase', 'http://Nitzsche-Wilderman.com', NULL, NULL, NULL, NULL, NULL, 'full_name', '2018-08-10 15:11:35', '2018-08-10 15:11:35'),
(9, 'hilpert.angelo@yahoo.com', '$2y$10$yJV2Lk1TA7QV2sDEXtC7veyg64UJ8m48tTWhp7BOQoItzSleHf1HO', NULL, NULL, 'Jermaine Roberts', '+2330557834425', 'default', '76916', '9.jpg', 'user', 'New Hampshire', '18372 Donnelly Track Suite 547', 'Cummings, Gaylord and Reichert', 'Cummings,-Gaylord-and-Reichert', 'Fundamental attitude-oriented extranet', 'http://Cummings,-Gaylord-and-Reichert.com', NULL, NULL, NULL, NULL, NULL, 'full_name', '2018-08-10 15:11:35', '2018-08-10 15:11:35'),
(10, 'rcrona@gmail.com', '$2y$10$0VzFMnh9lG25fx1YknsWGO5aoYFClYZKydgMV4XnOw0.KQ2BVH/WK', NULL, NULL, 'Esta Zemlak', '+7279864958650', 'default', '94660', '10.jpg', 'user', 'Michigan', '6356 Genoveva Crescent Apt. 448', 'Gleason-Quigley', 'Gleason-Quigley', 'Synergistic attitude-oriented productivity', 'http://Gleason-Quigley.com', NULL, NULL, NULL, NULL, NULL, 'full_name', '2018-08-10 15:11:35', '2018-08-10 15:11:35'),
(11, 'mireya16@hotmail.com', '$2y$10$aWgNdOiPA45VMA5Fxha9IOaFlGO1dogb5aCKHefpPQBzv8x3hMoKC', NULL, NULL, 'Rodrick Lindgren', '+3965687444901', 'default', '76197', '11.jpg', 'user', 'Nebraska', '5797 Brice Lodge Apt. 190', 'Feil, Wiza and Kulas', 'Feil,-Wiza-and-Kulas', 'Virtual radical superstructure', 'http://Feil,-Wiza-and-Kulas.com', NULL, NULL, NULL, NULL, NULL, 'full_name', '2018-08-10 15:11:36', '2018-08-10 15:11:36'),
(12, 'jmurphy@hotmail.com', '$2y$10$7l6OLXMX03TvV/z9o2cZ3uj2/Fd0.FJKSZ0Gf7CLSoq275jQP.Bpi', NULL, NULL, 'Loma Schaefer', '+2023392175433', 'default', '26886', '12.jpg', 'user', 'New Mexico', '51456 Prohaska Lodge Apt. 925', 'Stroman LLC', 'Stroman-LLC', 'Customer-focused responsive policy', 'http://Stroman-LLC.com', NULL, NULL, NULL, NULL, NULL, 'full_name', '2018-08-10 15:11:36', '2018-08-10 15:11:36'),
(13, 'colby.halvorson@hotmail.com', '$2y$10$LqCOFsVyult9brC0HlgIbO7ABJYvy2Dw54MCaKHlFZDS0CcVNLB56', NULL, NULL, 'Stephon Torp', '+8716385577251', 'default', '80761', '13.jpg', 'user', 'North Dakota', '885 Delaney Common Apt. 642', 'Schmeler Ltd', 'Schmeler-Ltd', 'Inverse reciprocal GraphicalUserInterface', 'http://Schmeler-Ltd.com', NULL, NULL, NULL, NULL, NULL, 'full_name', '2018-08-10 15:11:36', '2018-08-10 15:11:36'),
(14, 'romaine.vonrueden@gmail.com', '$2y$10$T2r4DvWNWZaEyE9NFUDk9uLtI72Hm7lmyDZk.fbGZJiRP374IBwOG', NULL, NULL, 'Faustino Quigley', '+7348944778248', 'default', '22514', '14.jpg', 'user', 'Indiana', '3298 Hermiston Oval Suite 507', 'Marvin LLC', 'Marvin-LLC', 'Ergonomic uniform focusgroup', 'http://Marvin-LLC.com', NULL, NULL, NULL, NULL, NULL, 'full_name', '2018-08-10 15:11:36', '2018-08-10 15:11:36'),
(15, 'beatty.nicolette@yahoo.com', '$2y$10$dmaObVy.v3Yq8.U/4ofdZeyD3LFXVLM/yJJfQpWY63T6pDs3vJvc6', NULL, NULL, 'Adelbert Feeney', '+5577651141041', 'default', '42803', '15.jpg', 'user', 'Arkansas', '9822 Mertz Ranch Apt. 829', 'Daniel-Green', 'Daniel-Green', 'Horizontal homogeneous hardware', 'http://Daniel-Green.com', NULL, NULL, NULL, NULL, NULL, 'full_name', '2018-08-10 15:11:36', '2018-08-10 15:11:36'),
(16, 'ludie.hagenes@gmail.com', '$2y$10$CC0DOmWgqCNhA36ErpDu.Or969595hFIV4E7QelWvsVanLpTlP.1a', NULL, NULL, 'Savannah Herzog', '+7082495009164', 'default', '24146', '16.jpg', 'user', 'New Mexico', '3521 Brianne Walks Suite 651', 'Blick-Harris', 'Blick-Harris', 'Innovative intermediate internetsolution', 'http://Blick-Harris.com', NULL, NULL, NULL, NULL, NULL, 'full_name', '2018-08-10 15:11:36', '2018-08-10 15:11:36'),
(17, 'fhickle@yahoo.com', '$2y$10$vD9QUWvmgdXUHiG8UmwQbOgujAG57cRyiPmFzpW0gX0rH.9t2GqmK', NULL, NULL, 'Enrique Pouros', '+7752236104383', 'default', '44932', '17.jpg', 'user', 'Louisiana', '45640 Schaefer Garden Suite 914', 'Luettgen, Kuphal and Farrell', 'Luettgen,-Kuphal-and-Farrell', 'Visionary non-volatile infrastructure', 'http://Luettgen,-Kuphal-and-Farrell.com', NULL, NULL, NULL, NULL, NULL, 'full_name', '2018-08-10 15:11:36', '2018-08-10 15:11:36'),
(18, 'karen.borer@hotmail.com', '$2y$10$P9JOg8pEECZj7Qrz/aOsXOuDMWqhnO/w1yi9nWhIOnQx1Tmnk96LK', NULL, NULL, 'Donavon Fritsch', '+2451162506402', 'default', '40224', '18.jpg', 'user', 'West Virginia', '473 Deckow Pass Apt. 367', 'Miller-Mertz', 'Miller-Mertz', 'Open-source responsive strategy', 'http://Miller-Mertz.com', NULL, NULL, NULL, NULL, NULL, 'full_name', '2018-08-10 15:11:37', '2018-08-10 15:11:37'),
(19, 'ryan.davonte@hotmail.com', '$2y$10$qv5qjB2IaeaHPkSShidHIeCYiyEfcCv8I6AwTgezNCndX4yGD6p4i', NULL, NULL, 'Braxton Koepp', '+4320574621705', 'default', '49562', '19.jpg', 'user', 'Michigan', '42432 Gulgowski Vista Apt. 245', 'Herman, Stamm and Hahn', 'Herman,-Stamm-and-Hahn', 'Operative 24/7 flexibility', 'http://Herman,-Stamm-and-Hahn.com', NULL, NULL, NULL, NULL, NULL, 'full_name', '2018-08-10 15:11:37', '2018-08-10 15:11:37'),
(20, 'amir54@hotmail.com', '$2y$10$t2QteWpcAMBlkEIzQUIRluok7kwPfH2tetGxQuDNzxts2Ro6Iv7Le', NULL, NULL, 'Edwin Powlowski', '+2731982801463', 'default', '56332', '20.jpg', 'user', 'North Carolina', '470 Reynolds Wall Suite 157', 'Hagenes, Flatley and Hand', 'Hagenes,-Flatley-and-Hand', 'Streamlined empowering ability', 'http://Hagenes,-Flatley-and-Hand.com', NULL, NULL, NULL, NULL, NULL, 'full_name', '2018-08-10 15:11:37', '2018-08-10 15:11:37'),
(23, 'tolustar29@gmail.com', '$2y$10$EnSM1VX8.7tAJnHnLIqVbuxwJ6iI0F6ly59vItasWT7sW0YEpvOUO', NULL, 'dCwa4lqEEjUoyifF7AufPbD1im90glY1XEfec8TC948vaFYQ4vLoQuhyVHPw', 'Toluwalase', '1234444444', 'default', NULL, 'http://localhost/marketam/images/userimages/23.jpg', 'user', 'Bauchi', 'Lagos,', 'Tolustar', 'Tolustar', 'dssdffsdfsdfsdfsdfsfdsd', 'tolustar.com', 'true', 'true', NULL, NULL, '2019-06-13 09:10:00', 'company_name', '2018-08-16 15:13:46', '2019-06-13 09:10:00'),
(33, 'tolustar29@gmail.com', '$2y$10$xGcOTRgS92ArQeINCVSu0e4PcTvSzFNu4ferTgxW3XaNpq7mWr0fG', NULL, '6VBYUHZQyBaqXqN3LTIB1TGcxz6q34rxnyfBB1zX02m9HeWu7Tq9ms6PHYfP', 'Olaogun Toluwalase', NULL, 'google', '101052816724682341014', 'https://lh4.googleusercontent.com/-HSvPUQ93Ix4/AAAAAAAAAAI/AAAAAAAAAMQ/Zazi7nuy5nI/photo.jpg?sz=600', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-06-19 07:14:56', 'full_name', '2018-08-17 07:16:42', '2019-06-19 07:14:56'),
(35, 'tolustar29@gmail.com', '$2y$10$7LTCXsmsjzFkDLyw7SCrFucnpyuWmp22wqlphvOl0ftSP1YItgxgi', NULL, 'pIWbEL5mKjvfoOL2n45rjvAvgM4kGqvNRicDmRfsKJklnuTRs8oHjulo08M7', 'Olaogun Toluwalase', '08052483652', 'facebook', '2170250703003540', 'https://graph.facebook.com/2170250703003540/picture?type=large&width=500&height=500', 'user', 'Benue', 'Gboko', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-03 08:40:34', 'full_name', '2018-08-17 07:19:57', '2018-10-03 08:40:34'),
(36, 'developers@astract.com', '$2y$10$0CKUliPjTkCHts4icFC3quDtlRgQGbnYzFE/.E6Bo9O7mWqw3t2Xy', NULL, 'YCrenxlHFJ520GfR2E2uEm5Q72rrnanLoFxVUSMBqt9xLaN5zywkcvmY20wy', 'Astract Developers', NULL, 'google', '117231012122553518380', 'https://lh3.googleusercontent.com/--O91qYjAjHM/AAAAAAAAAAI/AAAAAAAAAAA/APUIFaPg3e0Zx_nJLFWsjWVhPsBbAcVrvA/mo/photo.jpg?sz=500', 'moderator', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-20 09:17:12', 'full_name', '2018-09-18 05:57:06', '2019-05-20 09:17:12'),
(37, 'tolustar19@gmail.com', '$2y$10$4TcX2B3/Ixgw88CYtPEpqeSEoSF9S0873eZHrCme8oYBBR3Z7hYYS', NULL, 'PQxW8MQX0ExhkBEQaSVvSun891ADR3ngReW424Rt54jJpbCIrKg8hYmwhjL2', 'Tolu Star', '08039144365', 'google', '105396736598478243086', 'http://localhost/marketam/images/userimages/37.jpg', 'user', 'Cross River', 'Calabar', 'Toluwa', 'toluwa', 'Lorem ipsum', 'lorem', 'true', 'true', 'true', 'true', '2018-10-17 11:56:48', 'full_name', '2018-10-17 09:25:07', '2018-10-17 13:03:43');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_activities`
--
ALTER TABLE `admin_activities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `adverts`
--
ALTER TABLE `adverts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `advert_attributes`
--
ALTER TABLE `advert_attributes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `advert_media`
--
ALTER TABLE `advert_media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories_sub`
--
ALTER TABLE `categories_sub`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories_sub_attributes`
--
ALTER TABLE `categories_sub_attributes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `favourites`
--
ALTER TABLE `favourites`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `footer_pages`
--
ALTER TABLE `footer_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages_thread`
--
ALTER TABLE `messages_thread`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reported_adverts`
--
ALTER TABLE `reported_adverts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_activities`
--
ALTER TABLE `admin_activities`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=188;

--
-- AUTO_INCREMENT for table `adverts`
--
ALTER TABLE `adverts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=114;

--
-- AUTO_INCREMENT for table `advert_attributes`
--
ALTER TABLE `advert_attributes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `advert_media`
--
ALTER TABLE `advert_media`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=198;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `categories_sub`
--
ALTER TABLE `categories_sub`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=176;

--
-- AUTO_INCREMENT for table `categories_sub_attributes`
--
ALTER TABLE `categories_sub_attributes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `favourites`
--
ALTER TABLE `favourites`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `footer_pages`
--
ALTER TABLE `footer_pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `messages_thread`
--
ALTER TABLE `messages_thread`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `reported_adverts`
--
ALTER TABLE `reported_adverts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
