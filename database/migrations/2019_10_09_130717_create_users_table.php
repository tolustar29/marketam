<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('email', 60);
			$table->string('password', 60);
			$table->string('password_reset', 100)->nullable();
			$table->string('remember_token', 80)->nullable();
			$table->string('full_name', 80);
			$table->string('phone_number', 80)->nullable();
			$table->string('auth_type', 80);
			$table->string('auth_id', 80)->nullable();
			$table->text('image', 65535);
			$table->string('role', 20);
			$table->text('state', 65535)->nullable();
			$table->text('address', 65535)->nullable();
			$table->text('company_name', 65535)->nullable();
			$table->string('company_unique_name', 60)->nullable();
			$table->text('about_company', 65535)->nullable();
			$table->string('company_website', 60)->nullable();
			$table->string('jobs_notification', 10)->nullable();
			$table->string('ads_notification', 10)->nullable();
			$table->string('tips_notification', 10)->nullable();
			$table->string('general_notification', 10)->nullable();
			$table->dateTime('last_login')->nullable();
			$table->string('display_name_type', 191)->default('full_name');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
