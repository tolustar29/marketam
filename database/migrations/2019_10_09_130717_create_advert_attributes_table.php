<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdvertAttributesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('advert_attributes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('advert_id');
			$table->integer('categories_sub_attributes_id');
			$table->string('attribute_data', 80);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('advert_attributes');
	}

}
