<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMessagesThreadTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('messages_thread', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('message_id')->nullable();
			$table->integer('advert_id');
			$table->integer('user_id');
			$table->text('messages', 65535);
			$table->string('image', 200);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('messages_thread');
	}

}
