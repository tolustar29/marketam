<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdvertsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('adverts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->string('advert_type', 30);
			$table->integer('category_id')->nullable();
			$table->integer('categories_sub_id');
			$table->string('slug', 80)->nullable();
			$table->string('title', 80);
			$table->text('description', 65535);
			$table->integer('price')->nullable();
			$table->string('price_type', 191)->nullable()->default('Fixed Price');
			$table->text('location', 65535)->nullable();
			$table->string('usage', 191)->nullable()->default('New');
			$table->string('active', 60)->nullable()->default('active');
			$table->integer('view_count')->nullable()->default(0);
			$table->string('admin_incharge', 100)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('adverts');
	}

}
