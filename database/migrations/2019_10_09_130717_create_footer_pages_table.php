<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFooterPagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('footer_pages', function(Blueprint $table)
		{
			$table->increments('id');
			$table->text('page', 65535);
			$table->text('slug', 65535)->nullable();
			$table->text('content');
			$table->integer('footer_column')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('footer_pages');
	}

}
