<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCategoriesSubAttributesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('categories_sub_attributes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('categories_sub_id');
			$table->string('attributes', 60);
			$table->text('attributes_data', 65535);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('categories_sub_attributes');
	}

}
