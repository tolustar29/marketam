<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReportedAdvertsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('reported_adverts', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('advert_id')->nullable();
			$table->integer('user_id')->nullable();
			$table->text('reason', 65535)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('reported_adverts');
	}

}
