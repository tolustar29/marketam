<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;


//use App\Model\User;

class LoginCheck {

    public function handle($request, Closure $next)
    {
        
        $user = Auth::user();
        
        if($user){
          Session::flash('user','true');
          return $next($request); 
        }else{
          Session::flash("error","You are not logged in. Please login to continue");
          return Redirect::route('login');
        }
        
    }

}