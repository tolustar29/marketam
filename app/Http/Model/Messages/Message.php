<?php

namespace App\Model\Messages;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table = 'messages';

    public function user()
    {
        return $this->belongsTo('App\Model\User', 'user_sender_id', 'id');
    }

    public function recipient() {
        return $this->belongsTo('App\Model\User', 'active_recipient', 'id');
    }

    public function advert()
    {
        return $this->belongsTo('App\Model\Advert\Advert', 'advert_id', 'id');
    }

    public function messagethread()
    {
        return $this->hasMany('App\Model\Messages\Messagethread', 'message_id', 'id');
    }
}
