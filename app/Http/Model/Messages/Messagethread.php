<?php

namespace App\Model\Messages;

use Illuminate\Database\Eloquent\Model;

class Messagethread extends Model
{
    protected $table = 'messages_thread';

    public function user()
    {
        return $this->belongsTo('App\Model\User');
    }

    public function advert()
    {
        return $this->belongsTo('App\Model\Advert\Advert', 'advert_id', 'id');
    }

    public function message()
    {
        return $this->belongsTo('App\Model\Messages\Message', 'message_id', 'id');
    }

}
