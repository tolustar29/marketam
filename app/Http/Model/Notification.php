<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table = 'notifications';

    public function user()
    {
        return $this->belongsTo('App\Model\User', 'user_id', 'id');
    }

    public function advert()
    {
        return $this->belongsTo('App\Model\Advert\Advert', 'advert_id', 'id');
    }
}
