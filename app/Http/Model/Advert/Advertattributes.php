<?php

namespace App\Model\Advert;

use Illuminate\Database\Eloquent\Model;

class Advertattributes extends Model
{
    protected $table = 'advert_attributes';

    public function user()
    {
        return $this->belongsTo('App\Model\User');
    }

    public function categories_sub_attribute()
    {
        return $this->belongsTo('App\Model\Category\CategorySubAttributes', 'categories_sub_attributes_id', 'id');
    }

    public function advert()
    {
        return $this->hasMany('App\Model\Advert\Advert', 'advert_id', 'id');
    }
}
