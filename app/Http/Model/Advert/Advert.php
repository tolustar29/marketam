<?php

namespace App\Model\Advert;

use Illuminate\Database\Eloquent\Model;

class Advert extends Model
{
    protected $table = 'adverts';

    public function user()
    {
        return $this->belongsTo('App\Model\User');
    }

    public function advert_attributes()
    {
        return $this->hasMany('App\Model\Advert\Advertattributes', 'advert_id', 'id');
    }

    public function advert_media()
    {
        return $this->hasMany('App\Model\Advert\Advertmedia', 'advert_id', 'id');
    }

    public function category_sub()
    {
        return $this->belongsTo('App\Model\Category\CategorySub', 'categories_sub_id', 'id');
    }

    public function categories()
    {
        return $this->belongsTo('App\Model\Category\Categories', 'category_id', 'id');
    }

    public function favourite()
    {
        return $this->hasMany('App\Model\Favourites', 'advert_id', 'id');
    }

    public function message()
    {
        return $this->hasMany('App\Model\Messages\Message', 'advert_id', 'id');
    }

    public function messagethread()
    {
        return $this->hasMany('App\Model\Messages\Messagethread', 'advert_id', 'id');
    }
}
