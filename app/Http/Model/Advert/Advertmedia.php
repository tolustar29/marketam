<?php

namespace App\Model\Advert;

use Illuminate\Database\Eloquent\Model;

class Advertmedia extends Model
{
    protected $table = 'advert_media';

    public function user()
    {
        return $this->belongsTo('App\Model\User');
    }

    public function advert()
    {
        return $this->belongsTo('App\Model\Advert\Advert', 'advert_id', 'id');
    }
}
