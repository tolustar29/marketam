<?php

namespace App\Model\FooterPages;

use Illuminate\Database\Eloquent\Model;

class Footer extends Model
{
    protected $table = 'footer_pages';

}
