<?php

namespace App\Model\Category;

use Illuminate\Database\Eloquent\Model;

class CategorySub extends Model
{
    protected $table = 'categories_sub';

    public function user()
    {
        return $this->belongsTo('App\Model\User');
    }

    public function categories()
    {
        return $this->belongsTo('App\Model\Category\Categories', 'categories_id', 'id');
    }

    public function categories_sub_attributes()
    {
        return $this->hasMany('App\Model\Category\CategorySubAttributes', 'categories_sub_id', 'id');
    }
}
