<?php

namespace App\Model\Category;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    protected $table = 'categories';

    public function user()
    {
        return $this->belongsTo('App\Model\User');
    }

    public function categories_subs()
    {
        return $this->hasMany('App\Model\Category\CategorySub', 'categories_id', 'id');
    }

    public function advert_media() {
        return $this->hasMany('App\Model\Advert\Advert', 'categories_id', 'id');
    }
}
