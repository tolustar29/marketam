<?php

namespace App\Model\Category;

use Illuminate\Database\Eloquent\Model;

class CategorySubAttributes extends Model
{
    protected $table = 'categories_sub_attributes';

    public function user()
    {
        return $this->belongsTo('App\Model\User');
    }

    public function categories_sub()
    {
        return $this->belongsTo('App\Model\Category\CategorySub', 'categories_sub_id', 'id');
    }
}
