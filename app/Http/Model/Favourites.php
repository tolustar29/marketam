<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Favourites extends Model
{
    protected $table = 'favourites';

    public function user()
    {
        return $this->belongsTo('App\Model\User');
    }

    public function adverts()
    {
        return $this->hasMany('App\Model\Advert\Advert', 'id', 'advert_id');
    }

    public function advert()
    {
        return $this->belongsTo('App\Model\Advert\Advert', 'advert_id', 'id');
    }
}
