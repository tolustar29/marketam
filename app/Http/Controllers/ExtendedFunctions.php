<?php

namespace App\Http\Controllers;

use App\Model\User;

use Barryvdh\Debugbar;

use SnappyPDF;

use DateTime;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Url;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Model\AdminActivity;

class ExtendedFunctions extends Controller
{
    public function checkajax($request, $ajax_url,$pagedata,$newlink){ 

      if($request){
      
        $data['view'] = view($ajax_url,$pagedata)->render();
        $data['newlink'] = $newlink;

        if(Session::has("success")){
          $data['success'] = Session::get("success");
        }

        if(Session::has("error")){
          $data['error'] = Session::get("error");
        }

        return $data;

      }else{
        $viewpage = "index";

        if(Session::has("success")){
          $pagedata["success"] =  Session::get("success");
        }

        if(Session::has("error")){
          $pagedata["error"] =  Session::get("error");
        }

        $pagedata["user"] =  Auth::user();

        $pagedata["ajax_call"] = "false";

        return view($viewpage,$pagedata);
      }

    }

    public function pageTitle($title){
      $website = "Marketam";
      $pageTitle = [
        "login" => $website." | Login",
        "register" => $website." | Register",
        "profile" => $website." | Profile",
        "home" => $website." | Home",
        "adverts" => $website." | My Adverts",
        "freead" => $website." | Free Ad",
        "sponsoredad" => $website." | Sponsored Ad",
        "categories" => $website." | Categories",
        "product" => $website." | Product",
        "favourites" => $website." | Favourites",
        "messages" => $website." | Messages",
        "resetpassword" => $website." | Set New Password",
        "forgotpassword" => $website." | Forgot Password",
        "editadvert" => $website." | Edit Advert",
        "admindashboard" => $website." | Admin Dasboard",
        "adminadverts" => $website." | Admin Adverts",
        "adminreportedadverts" => $website." | Reported Adverts",
        "adminusers" => $website . " | Admin All-Users",
        "adminmoderators" => $website . " | Admin Moderators",
        "adminfooter_pages" => $website . " | Admin Footer Pages",
        "admincategories" => $website . " | Admin Categories",
        "default" => $website,
      ];

      return $pageTitle[$title];
    }

    public function new_activity($admin_activity){
      $activity = new AdminActivity;
      $activity->activity = $admin_activity;
      $activity->admin = Auth::user()->full_name;
      $activity->save();

      $admin_count = AdminActivity::all()->count();

      if($admin_count > 10000){
        AdminActivity::orderBy("created_at", "asc")->take($admin_count - 10000)->delete();
      }

    }

    public function sendmail($user,$emailview,$code){

      Mail::send($emailview, [
          'name' => $user->full_name,
          'email' => $email,
          'date' => date('d/m/Y H:i:s')
          ], function($message) use ($email){
                      $message->to($email)->subject('RESET PASSWORD | MARKETAM');
      });

      return "success";
    }

}