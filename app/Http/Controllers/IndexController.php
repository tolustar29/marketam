<?php

namespace App\Http\Controllers;

use App\Model\User;
use App\Model\Category\Categories;
use App\Model\Category\CategorySub;
use App\Model\Category\CategorySubAttributes;

use App\Model\Advert\Advert;
use App\Model\Advert\Advertattributes;
use App\Model\Advert\Advertmedia;
use App\Model\Advert\Reportedadvert;

use App\Model\Messages\Message;

use App\Model\Messages\Messagethread;

use App\Model\FooterPages\Footer;

use Barryvdh\Debugbar;

use Faker\Factory;

use SnappyPDF;

use DateTime;

use File;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Url;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use function GuzzleHttp\json_encode;
use App\Model\Favourites;
use App\Model\Notification;


class IndexController extends ExtendedFunctions
{
    public function home(){ 


      $adverts = Advert::where("active","active")->latest()->simplePaginate(24);
      $categories = Categories::take(8)->inRandomOrder()->get();
      $page = "frontend.home";


      $pagedata = [
        'adverts' => $adverts,
        'categories' => $categories,
        'includepage' => $page,
        "pageTitle" => $this->pageTitle("home"),
      ];
      $data = $this->checkajax(Request::ajax(),$page,$pagedata,Request::url());

      if(Request::get('page')){
        return view("includes.inserted_adverts_for_home",$pagedata);
      }

      return $data;

    }

    public function login(){ 

      if(Auth::user()){

        if(Auth::user()->role != "user"){
          return Redirect::route('admin.home');
        }else{
          return Redirect::route('userprofile');
        }
        
      }

      $page = "frontend.login";
      $pagedata = [
        'includepage' => $page,
        "pageTitle" => $this->pageTitle("login"),
      ];
      $data = $this->checkajax(Request::ajax(),$page,$pagedata,Request::url());

      return $data;
    }

    public function postlogin(){

      if(Auth::attempt(
           ['email' => Request::get('email'), 'password' => Request::get('password'), 'auth_type' => 'default'])){
            
            Auth::user()->last_login = date("Y/m/d H:i:s");
            Auth::user()->save();

            Session::flash("success","Login Successful :)");

            if(Auth::user()->role != "user"){
              return Redirect::route('admin.home');
            }else{
              return Redirect::route('userprofile');
            }
            
       }else{

            Session::flash("error","Login Not Successful :( Please try again");
            return Redirect::route('login');
       }
    }

    public function logout(){ 

      Auth::logout();
      Session::flash("success","Logout Successful");
      return Redirect::route('login');
    }

    public function register(){ 

      if(Auth::user()){
        return Redirect::route('userprofile');
      }

      $page = "frontend.register";
      $pagedata = [
        'includepage' => $page,
        "pageTitle" => $this->pageTitle("register"),
      ];
      $data = $this->checkajax(Request::ajax(),$page,$pagedata,Request::url());

      return $data;
    }

    public function postregister(){
      
      if(User::where("email","=",Request::get("email"))->where("auth_type","=","default")->first()){

        Session::flash("error","Sorry you can't register with this email :( This email exist in our database");
        return Redirect::route('register');

      }else{

        $user = new User;

        $user->email = Request::get("email");
        $user->password = bcrypt(Request::get("password"));
        $user->full_name = Request::get("full_name");
        $user->phone_number = Request::get("phone_number");
        $user->auth_type = 'default';
        $user->image = route('home').'/images/user.png';
        $user->role = 'user';

        $user->save();

        Auth::attempt(
          ['email' => Request::get('email'), 'password' => Request::get('password'), 'auth_type' => 'default']
        );

        Auth::user()->last_login = date("Y/m/d H:i:s");
        Auth::user()->save();

        Session::flash("success","Registration Successful :)");
        return Redirect::route('userprofile');
      }

      Session::flash("error","An Error Was Encountered :(");
      return Redirect::route('register');

    }

    public function socialmediaregister(){

      

      if(Request::get("type") == "google"){

        if(User::where("email","=",Request::get("email"))->where("auth_type","=","google")->first()){

            Auth::attempt(
              ['email' => Request::get('email'),'password' => 'null', 'auth_id' => Request::get('id'), 'auth_type' => 'google']
            );

            Auth::user()->last_login = date("Y/m/d H:i:s");
            Auth::user()->save();

            $success = "Login Successful";
  
        }else{

            $user = new User;

            $user->email = Request::get("email");
            $user->password = bcrypt("null");
            $user->full_name = Request::get("name");
            $user->auth_type = 'google';
            $user->image = str_replace("jpg?sz=50","jpg?sz=500",Request::get("image"));
            $user->auth_id = Request::get("id");
            $user->role = 'user';

            $user->save();

            Auth::attempt(
              ['email' => Request::get('email'),'password' => 'null', 'auth_id' => Request::get('id'), 'auth_type' => 'google']
            );

            Auth::user()->last_login = date("Y/m/d H:i:s");
            Auth::user()->save();

            $success = "Registration Successful :)";
        }

        Session::flash("success",$success);
        if(Auth::user()->role != "user"){
          return Redirect::route('admin.home');
        }else{
          
          return Redirect::route('userprofile');
        }
        
        
      }

      if(Request::get("type") == "facebook"){

        if(User::where("email","=",Request::get("email"))->where("auth_type","=","facebook")->first()){

            Auth::attempt(
              ['email' => Request::get('email'),'password' => 'null', 'auth_id' => Request::get('id'), 'auth_type' => 'facebook']
            );

            Auth::user()->last_login = date("Y/m/d H:i:s");
            Auth::user()->save();

            $success = "Login Successful";
  
        }else{

            $user = new User;

            $user->email = Request::get("email");
            $user->password = bcrypt("null");
            $user->full_name = Request::get("name");
            $user->auth_type = 'facebook';
            $user->image = "https://graph.facebook.com/".Request::get("id")."/picture?type=large&width=500&height=500";
            $user->auth_id = Request::get("id");
            $user->role = 'user';

            $user->save();

            Auth::attempt(
              ['email' => Request::get('email'),'password' => 'null', 'auth_id' => Request::get('id'), 'auth_type' => 'facebook']
            );

            Auth::user()->last_login = date("Y/m/d H:i:s");
            Auth::user()->save();

            $success = "Registration Successful :)";
        }

        Session::flash("success",$success);
        if(Auth::user()->role != "user"){
          return Redirect::route('admin.home');
        }else{
          return Redirect::route('userprofile');
          //$page = "frontend.profile";
        }

        // $pagedata = [
        //   'getadmin' => $getadmin,
        //   'user' => Auth::user(),
        //   'includepage' => $page,
        //   "pageTitle" => $this->pageTitle("profile"),
        // ];

        // $data['view'] = view($page,$pagedata)->render();
        // $data['newlink'] = route("userprofile");
        // $data['success'] = $success;

        
        // return $data;
        
      }

      Session::flash("error","An Error Was Encountered :(");
      return Redirect::route('register');

    }

    public function favourite($type = null, $adId = null){ 

      if($type == "add"){
         $fav = new Favourites;
         $fav->advert_id = $adId;
         $fav->user_id = Auth::user()->id;
         $fav->save();

         $data['success'] = "Advert added to favourites";

         return $data;
      }

      if($type == "remove"){
        Favourites::where([["advert_id",$adId],["user_id",Auth::user()->id]])->delete();

        $data['error'] = "Advert removed from favourites";


        return $data;
      }

      $favourites = Favourites::where("user_id",Auth::user()->id)->get();

      $page = "frontend.favourite";
      $pagedata = [
        'includepage' => $page,
        'favourites' => $favourites,
        'user_id' => Auth::user()->id,
        "pageTitle" => $this->pageTitle("favourites"),
      ];
      $data = $this->checkajax(Request::ajax(),$page,$pagedata,Request::url());

      return $data;

    }

    public function myadverts($type = null){ 

      if($type == "deactivated"){
        $adverts = Advert::where("user_id",Auth::user()->id)->where("active","deactivated")->latest()->simplePaginate(15);
        $myadvert_title = "Deactivated Adverts";

      } elseif ($type == "sold") {
        $adverts = Advert::where("user_id",Auth::user()->id)->where("active", "sold")->latest()->simplePaginate(15);
        $myadvert_title = "Sold Adverts";
      }
      else{
        $adverts = Advert::where("user_id",Auth::user()->id)->latest()->simplePaginate(15);
        $myadvert_title = "My Adverts";
      }

      

      $page = "frontend.myadverts";
      $pagedata = [
        'adverts' => $adverts,
        'includepage' => $page,
        'myadvert_title' => $myadvert_title,
        'moreurl' => Request::url(),
        "pageTitle" => $this->pageTitle("adverts"),
      ];
      $data = $this->checkajax(Request::ajax(),$page,$pagedata,Request::url());

      if(Request::get('page')){
        return view("includes.inserted_adverts_for_myadverts",$pagedata);
      }

      return $data;
    }

    public function shopadverts($user_id = null){ 

      $user = User::where("id",$user_id)->first();
      $adverts = Advert::where("active","active")->where("user_id",$user_id)->latest()->simplePaginate(15);

      if($user->display_name_type == "full_name"){
        $shop_title = $user->full_name." Adverts";
      }else{
        $shop_title = $user->company_name." Adverts";
      }
    
      $page = "frontend.shop";
      $pagedata = [
        'adverts' => $adverts,
        'user' => $user,
        'includepage' => $page,
        'shop_title' => $shop_title,
        'moreurl' => Request::url(),
        "pageTitle" => $shop_title,
      ];
      $data = $this->checkajax(Request::ajax(),$page,$pagedata,Request::url());

      if(Request::get('page')){
        return view("includes.inserted_adverts_for_myadverts",$pagedata);
      }

      return $data;
    }

    public function myadvertsaction($type = null, $id = null){

      $data['link'] = Request::url();

      if($type == "deactivated"){
        $advert = Advert::where([["id",$id],["user_id",Auth::user()->id]])->first();

        if(!empty($advert)){
          $advert->active = "deactivated";
          $advert->save();
        }

        $data['error'] = "deactivated";
      
        return $data;
      }


      if($type == "deleted"){
        Advert::where([["id",$id],["user_id",Auth::user()->id]])->delete();

        Favourites::where("advert_id", $id)->delete();
        Message::where("advert_id", $id)->delete();
        Messagethread::where("advert_id", $id)->delete();
        Notification::where("advert_id", $id)->delete();
        Advertattributes::where("advert_id", $id)->delete();
        Advertmedia::where("advert_id", $id)->delete();

        $data['error'] = "deleted";
        $count = Session::get('myadverts_count', '0');
        Session::put('myadverts_count', $count-1);

        $data['count'] = $count-1;
      
        return $data;
      }

      if($type == "sold"){
        $advert = Advert::where([["id",$id],["user_id",Auth::user()->id]])->first();

        if(!empty($advert)){
          $advert->active = "sold";
          $advert->save();
        }

        $data['success'] = "sold";
      
        return $data;
      }

      if($type == "active"){
        $advert = Advert::where([["id",$id],["user_id",Auth::user()->id]])->first();

        if(!empty($advert)){
          $advert->active = "active";
          $advert->save();
        }

        $data['success'] = "active";
      
        return $data;
      }

    }

    public function freead(){ 

      $categories = Categories::all();


      $page = "frontend.freead";
      $pagedata = [
        'includepage' => $page,
        'categories' => $categories,
        "pageTitle" => $this->pageTitle("freead"),
      ];
      $data = $this->checkajax(Request::ajax(),$page,$pagedata,Request::url());

      return $data;
    }

    public function ad_cat_select($id = null){
        $data = CategorySubAttributes::with("categories_sub")->where("categories_sub_id",$id)->get();

        return $data;
    }

    public function sponsoredad(){ 

      $categories = Categories::all();

      $page = "frontend.sponsoredad";
      $pagedata = [
        'includepage' => $page,
        'categories' => $categories,
        "pageTitle" => $this->pageTitle("sponsoredad"),
      ];
      $data = $this->checkajax(Request::ajax(),$page,$pagedata,Request::url());

      return $data;
    }

    public function editadvert($slug = null){

      $advert = Advert::where("slug",$slug)->orderBy("created_at","desc")->first();
      $categories = Categories::all();

      \Debugbar::error($advert->id);

      $advert_attr = Advertattributes::where("advert_id",$advert->id)->get();

      
      
      $advert_media = Advertmedia::where("advert_id",$advert->id)->first();
      if($advert->advert_type == "free"){
        if(!empty($advert_media)){
            $fileLink = route('home').'/'.$advert_media->link;
            $fileNamePos = strrpos($fileLink, '/');
            $fileName = $fileNamePos === false ? $fileLink : substr($fileLink, $fileNamePos + 1);

            $fileSize = get_headers($fileLink, true);

            $fileObj[] = [
              "original" => $fileName,
              // "original" => strtok($fileName, '.'),
              "server" => $fileLink,
              //'size' => \File::size(public_path('images/productimages/' . $fileName)),
              //'size' => \File::size($fileLink),
              "size" => $fileSize['Content-Length'],
            ];

            $fileObject = json_encode($fileObj, JSON_FORCE_OBJECT);
        }else{
            $fileObject = "null";
        }

      
        $page = "frontend.freeadedit";
      }else{
        if(count($advert_media) != 0){
          $advert_media = Advertmedia::where("advert_id",$advert->id)->get();

          $fileObj = []; 

          foreach($advert_media as $obj){
              $fileLink = route('home').'/'.$obj->link;
              $fileNamePos = strrpos($fileLink, '/');
              $fileName = $fileNamePos === false ? $fileLink : substr($fileLink, $fileNamePos + 1);

              $fileSize = get_headers($fileLink, true);

              if($obj->type == "audio"){
                $fileLink = 'images/music-icon.png';
              }

              if($obj->type == "video"){
                $fileLink = 'images/play-button.png';
              }

              $fileObj[] = [
                "original" => $fileName,
                // "original" => strtok($fileName, '.'),
                "server" => $fileLink,
                //'size' => \File::size(public_path('images/productimages/' . $fileName)),
                //'size' => \File::size($fileLink),
                "size" => $fileSize['Content-Length'],
              ];
              
          }
          
          $fileObject = json_encode($fileObj, JSON_FORCE_OBJECT);

        } else{
          $fileObject = "null";
        }

        $page = "frontend.sponsoredadedit";
      }

      
      $pagedata = [
        'includepage' => $page,
        'advert' => $advert,
        'advert_media' => $advert_media,
        'advert_attr' => $advert_attr,
        'categories' => $categories,
        'fileObject' => $fileObject,
        "pageTitle" => $this->pageTitle("editadvert"),
      ];
      $data = $this->checkajax(Request::ajax(),$page,$pagedata,Request::url());

      return $data;

    }

    public function delete_advert_media(){

      $ext = pathinfo(Request::get('id')); 
      $ext = $ext['extension'];

      if($ext == "jpg" || $ext == "jpeg" || $ext == "png"){
        $path = 'images/productimages/';
      }

      if($ext == "mp3" || $ext == "wma"){
        $path = 'audios/productaudios/';
      }

      if($ext == "mp4"){
        $path = 'videos/productvideos/';
      }



      $link = $path.Request::get('id');

      if(!empty(Advert::where("user_id",Auth::user()->id)->first())){
        Advertmedia::where("link",$link)->delete();

        $path = base_path(route('home').'/'.$path.Request::get('id')); 
        File::delete($path);
  
        $response = "success";
  
        return $response;
  
      }
      
    }

    public function postfreead(){

      $getid = Advert::orderBy('created_at','desc')->first();$newid = 1;
      if(!empty($getid)){
        $newid = $getid->id + 1;
      }

      $advert = new Advert;
      $advert->user_id = Auth::user()->id;
      $advert->advert_type = "free";
      $advert->active = "new";
      $advert->title = Request::get('title');
      $advert->slug = str_slug(Request::get('title').'-'.$newid,'-');
      $advert->description = Request::get('description');
      $advert->price = Request::get('price');
      $advert->price_type = Request::get('price_type');
      $advert->usage = Request::get('usage');
      $advert->location = Request::get('location');
      $advert->category_id = CategorySub::where("id",Request::get('categories'))->first()->categories_id;
      $advert->categories_sub_id = Request::get('categories');
      $advert->save();

      $data = CategorySubAttributes::where("categories_sub_id",Request::get('categories'))->get();

      $recentadvert = Advert::where("user_id",Auth::user()->id)->where("title",Request::get('title'))->orderBy("created_at","desc")->first();

      

      foreach ($data as $obj) {
        $advertattr = new Advertattributes;
        $advertattr->advert_id = $recentadvert->id;

        $advertattr->categories_sub_attributes_id = $obj->id;

        $advertattr->attribute_data = Request::get(str_replace(" ","_",$obj->attributes));
        $advertattr->save();
      }

      $advert = new Advertmedia;
      $advert->advert_id = $recentadvert->id;
      $advert->type = "image";

      $picture = Request::file('imageupload');

      if(!empty($picture)){
          $rand_code  = "0";
          $picture_ext = strtolower($picture->getClientOriginalExtension());
          for($i=0; $i<15; $i++) {
            $min = ($i == 0) ? 1:0;
            $rand_code .= mt_rand($min,9);
          }

          // $img = \Image::make($picture);
          
          // $img->insert(base_path().'/images/watermark2.png','bottom');
         
          // $img->save('images/productimages/'.$rand_code.'.'.$picture_ext);
          $picture->move('images/productimages/', $rand_code.'.'.$picture_ext);
          
          $advert->link = 'images/productimages/'.$rand_code.'.'.$picture_ext;

          $advert->save();

      }
      
    
      $response['success'] = "Advert has been submitted succesfully. Your advert is being reviewed by one of our agent.";

      $response['link'] = route('adpage', $recentadvert->slug);

      return $response;
      
    }

    public function postfreeadedit(){

      $new_slug = str_slug(Request::get('title') . '-' . Request::get('advert_id'), '-');

      $advert = Advert::where('id',Request::get('advert_id'))->where('user_id',Auth::user()->id)->first();
      $advert->user_id = Auth::user()->id;
      $advert->advert_type = "free";
      $advert->title = Request::get('title');
      $advert->slug = $new_slug;
      $advert->active = "new";
      $advert->description = Request::get('description');
      $advert->price = Request::get('price');
      $advert->location = Request::get('location');
      $advert->category_id = CategorySub::where("id",Request::get('categories'))->first()->categories_id;
      $advert->categories_sub_id = Request::get('categories');
      $advert->save();

      $data = CategorySubAttributes::where("categories_sub_id",Request::get('categories'))->get();

      Advertattributes::where('advert_id',Request::get('advert_id'))->delete();

      foreach ($data as $obj) {
        $advertattr = new Advertattributes;
        $advertattr->advert_id = $advert->id;

        $advertattr->categories_sub_attributes_id = $obj->id;

        $advertattr->attribute_data = Request::get(str_replace(" ","_",$obj->attributes));

        \Debugbar::error($obj->attributes.' '.Request::get(str_replace(" ","_",$obj->attributes)));
        $advertattr->save();
      }

      $advertmedia = new Advertmedia;
      $advertmedia->advert_id = $advert->id;
      $advertmedia->type = "image";

      $picture = Request::file('imageupload');

      if(!empty($picture)){
          $rand_code  = "0";
          $picture_ext = strtolower($picture->getClientOriginalExtension());
          for($i=0; $i<15; $i++) {
            $min = ($i == 0) ? 1:0;
            $rand_code .= mt_rand($min,9);
          }

          // $img = \Image::make($picture);
          
          // $img->insert(base_path().'/images/watermark2.png','bottom');
         
          // $img->save('images/productimages/'.$rand_code.'.'.$picture_ext);

          $picture->move('images/productimages/', $rand_code.'.'.$picture_ext);
          $advertmedia->link = 'images/productimages/'.$rand_code.'.'.$picture_ext;

          $advertmedia->save();

      }
      
    
      $response['success'] = "Advert has been updated succesfully. Your advert is being reviewed by one of our agent.";

      $response['link'] = route('adpage', $new_slug);

      return $response;
      
    }

    public function postsponsoredadedit(){

      $getadvert = Advert::where('id',Request::get('advert_id'))->where('user_id',Auth::user()->id)->first();

      $getadvert->price = Request::get('price');
      $getadvert->location = Request::get('location');

      $getadvert->active = "new";

      $getadvert->save();

      $response['success'] = "Advert has been updated succesfully. Your advert is being reviewed by one of our agent.";

      $response['link'] = route('adpage', $getadvert->slug);

      return $response;

    }

    public function sponsorededitupload(){
      $file = Request::file('fileupload');

      \Debugbar::error("error ".Request::get('advert_id'));

      $file_ext = strtolower($file->getClientOriginalExtension());
        $rand_code  = "0";
        if($file_ext == "jpeg" || $file_ext == "jpg" || $file_ext == "png"){

          if($file->getClientSize() < 2200000 ){

            if(count(Advertmedia::where('advert_id',Request::get('advert_id'))->where('type','image')->get()) > 6){
              $response['error'] = "Only six image files are allowed";
  
              return $response;
            }else{
              for($j=0; $j<15; $j++) {
                $min = ($j == 0) ? 1:0;
                $rand_code .= mt_rand($min,9);
              }
              // $img = \Image::make($file);
          
              // $img->insert(base_path().'/images/watermark2.png','bottom');
            
              // $img->save('images/productimages/'.$rand_code.'.'.$file_ext);

              $file->move('images/productimages/', $rand_code.'.'.$file_ext);
              $advert_type = "image";
              $advert_link = 'images/productimages/'.$rand_code.'.'.$file_ext;
              $skipfile = 0;
            }

            
          }else{

            $response['error'] = "Invalid file size";
  
            return $response;

            $skipfile = 1;
          }
        }


        if($file_ext == "mp4"){

          if(count(Advertmedia::where('advert_id',Request::get('advert_id'))->where('type','video')->get()) > 1){
            $response['error'] = "Only one video file is allowed";

            return $response;
          }else{
            if($file->getClientSize() < 7200000 ){
              for($j=0; $j<15; $j++) {
                $min = ($j == 0) ? 1:0;
                $rand_code .= mt_rand($min,9);
              }
              $file->move('videos/productvideos/', $rand_code.'.'.$file_ext);
              $advert_type = "video";
              $advert_link = 'videos/productvideos/'.$rand_code.'.'.$file_ext;
              $skipfile = 0;
            }else{
              $skipfile = 1;

              $response['error'] = "Invalid file size";

              return $response;
            }
          }

          
        }

        if($file_ext == "mp3" || $file_ext == "wma"){

          if(count(Advertmedia::where('advert_id',Request::get('advert_id'))->where('type','audio')->get()) > 1){
            $response['error'] = "Only one audio file is allowed";

            return $response;
          }else{

            if($file->getClientSize() < 7200000 ){
              for($j=0; $j<15; $j++) {
                $min = ($j == 0) ? 1:0;
                $rand_code .= mt_rand($min,9);
              }
              $file->move('audios/productaudios/', $rand_code.'.'.$file_ext);
              $advert_type = "audio";
              $advert_link = 'audios/productaudios/'.$rand_code.'.'.$file_ext;

              $skipfile = 0;
            }else{
              $skipfile = 1;
            }
          }


        }

        if($skipfile == 0){
          $getadvert = Advert::where('id',Request::get('advert_id'))->where('user_id',Auth::user()->id)->first();
          $advert = new Advertmedia;
          $advert->advert_id = $getadvert->id;
          $advert->type = $advert_type;

          $advert->link = $advert_link;

          $advert->save();

          $response['filename'] = $rand_code.'.'.$file_ext;

          return $response;
        }

        

    }

    public function getsponsoredadpaid(){

      $advert = Advert::where("id",Request::get("id"))->first();
      $advert->active = "new";
      $advert->save();
        
    }

    public function postsponsoredad(){

      $getid = Advert::orderBy('created_at','desc')->first();$newid = 1;
      if(!empty($getid)){
        $newid = $getid->id + 1;
      }

      $advert = new Advert;
      $advert->user_id = Auth::user()->id;
      $advert->advert_type = "sponsored";
      $advert->active = "new";
      $advert->title = Request::get('title');
      $advert->slug = str_slug(Request::get('title').'-'.$newid,'-');
      $advert->description = Request::get('description');
      $advert->price = Request::get('price');
      $advert->price_type = Request::get('price_type');
      $advert->usage = Request::get('usage');
      $advert->location = Request::get('location');
      $advert->categories_sub_id = Request::get('categories');
      $advert->category_id = CategorySub::where("id",Request::get('categories'))->first()->categories_id;
      $advert->save();

      $data = CategorySubAttributes::where("categories_sub_id",Request::get('categories'))->get();

      $recentadvert = Advert::where("user_id",Auth::user()->id)->where("title",Request::get('title'))->orderBy("created_at","desc")->first();

      

      foreach ($data as $obj) {
        $advertattr = new Advertattributes;
        $advertattr->advert_id = $recentadvert->id;

        $advertattr->categories_sub_attributes_id = $obj->id;

        $advertattr->attribute_data = Request::get(str_replace(" ","_",$obj->attributes));
        $advertattr->save();
      }

      

      $file = Request::file('fileupload');

      
      for($i=0; $i<count($file); $i++){

        $file_ext = strtolower($file[$i]->getClientOriginalExtension());
        $rand_code  = "0";
        if($file_ext == "jpeg" || $file_ext == "jpg" || $file_ext == "png"){

          if($file[$i]->getClientSize() < 2200000 ){
            for($j=0; $j<15; $j++) {
              $min = ($j == 0) ? 1:0;
              $rand_code .= mt_rand($min,9);
            }
            // $img = \Image::make($file[$i]);
          
            // $img->insert(base_path().'/images/watermark2.png','bottom');
          
            // $img->save('images/productimages/'.$rand_code.'.'.$file_ext);

            $file[$i]->move('images/productimages/', $rand_code.'.'.$file_ext);
            $advert_type = "image";
            $advert_link = 'images/productimages/'.$rand_code.'.'.$file_ext;
            $skipfile = 0;
          }else{
            $skipfile = 1;
          }
        }


        if($file_ext == "mp4"){

          if($file[$i]->getClientSize() < 7200000 ){
            for($j=0; $j<15; $j++) {
              $min = ($j == 0) ? 1:0;
              $rand_code .= mt_rand($min,9);
            }
            $file[$i]->move('videos/productvideos/', $rand_code.'.'.$file_ext);
            $advert_type = "video";
            $advert_link = 'videos/productvideos/'.$rand_code.'.'.$file_ext;
            $skipfile = 0;
          }else{
            $skipfile = 1;
          }
        }

        if($file_ext == "mp3" || $file_ext == "wma"){

          if($file[$i]->getClientSize() < 7200000 ){
            for($j=0; $j<15; $j++) {
              $min = ($j == 0) ? 1:0;
              $rand_code .= mt_rand($min,9);
            }
            $file[$i]->move('audios/productaudios/', $rand_code.'.'.$file_ext);
            $advert_type = "audio";
            $advert_link = 'audios/productaudios/'.$rand_code.'.'.$file_ext;

            $skipfile = 0;
          }else{
            $skipfile = 1;
          }


        }

        if($skipfile == 0){
          $advert = new Advertmedia;
          $advert->advert_id = $recentadvert->id;
          $advert->type = $advert_type;

          $advert->link = $advert_link;

          $advert->save();
        }
        

      }

      $response['advert_id'] = $recentadvert->id;

      $response['success'] = "Advert has been submitted succesfully. Your advert is being reviewed by one of our agent.";

      $response['link'] = route('adpage', $recentadvert->slug);

      return $response;
      
    }

    public function category($slug = null,$subslug = null){ 

      $categories = Categories::all();

      if(empty(Request::get("page"))){
        $page = 1;
      }else{
        $page = Request::get("page");
      }

      $free_advert_count = 17;
      $sponsored_advert_count = 8;
      $paginate_count = 25;

      if($subslug == null){

        $category = Categories::where("category_slug",$slug)->first();
        
        $category_id = $category->id;
        $category_title = $category->category;

        $advert_paginate = Advert::where("category_id",$category_id)->where("active","active")->paginate($paginate_count);

        $adverts_sponsored = Advert::where("category_id",$category_id)->where("advert_type","sponsored")->where("active","active")->skip(($page-1) * $sponsored_advert_count)->take($sponsored_advert_count);
        
        
        $adverts_free = Advert::where("category_id",$category_id)->where("advert_type","free")->where("active","active")->skip(($page-1) * $free_advert_count)->take($free_advert_count);
      }else{

        $category_sub = CategorySub::where("categories_slug",$subslug)->first();
        
        $category_sub_id = $category_sub->id;

        $category_title = $category_sub->categories_sub;

        $advert_paginate = Advert::where("categories_sub_id",$category_sub_id)->where("active","active")->paginate($paginate_count);

        $adverts_sponsored = Advert::where("categories_sub_id",$category_sub_id)->where("active","active")->where("advert_type","sponsored")->skip(($page-1) * $sponsored_advert_count)->take($sponsored_advert_count);

        $adverts_free = Advert::where("categories_sub_id",$category_sub_id)->where("active","active")->where("advert_type","free")->skip(($page-1) * $free_advert_count)->take($free_advert_count);
      }

      $location = "Nigeria";

      $url = Request::url();
      if(!empty(Request::get("location"))){
        $adverts_sponsored =$adverts_sponsored->where("location","like","%".Request::get("location")."%");
        $adverts_free =$adverts_free->where("location","like","%".Request::get("location")."%");

        $url = Request::url()."?location=".Request::get("location");
        $location = Request::get("location");
      }

      if(!empty(Request::get("min_price"))){
        $adverts_sponsored =$adverts_sponsored->where([["price",">=",Request::get("min_price")],["price","<=",Request::get("max_price")]]);
        $adverts_free =$adverts_free->where([["price",">=",Request::get("min_price")],["price","<=",Request::get("max_price")]]);

        if(str_contains($url, '?')){
          $url = $url."&min_price=".Request::get("min_price")."&max_price=".Request::get("max_price");
        }else{
          $url = Request::url()."?min_price=".Request::get("min_price")."&max_price=".Request::get("max_price");
        }
      }

      if(!empty(Request::get("sort"))){
        if(Request::get("sort") == "newest"){
          $adverts_sponsored =$adverts_sponsored->latest();
          $adverts_free =$adverts_free->latest();
        }
        if(Request::get("sort") == "cheapest"){
          $adverts_sponsored =$adverts_sponsored->orderBy("price","asc");
          $adverts_free =$adverts_free->orderBy("price","asc");
        }
        
        if(str_contains($url, '?')){
          $url = $url."&sort=".Request::get("sort");
        }else{
          $url = Request::url()."?sort=".Request::get("sort");
        }
      }

      $adverts_sponsored = $adverts_sponsored->get();
      $adverts_free = $adverts_free->get();

      $adverts = $adverts_sponsored->merge($adverts_free);

      $page = "frontend.category";
      $pagedata = [
        'adverts' => $adverts,
        'advert_paginate' => $advert_paginate,
        'categories' => $categories,
        'includepage' => $page,
        'slug' => $slug,
        'subslug' => $subslug,
        'category_title' => $category_title,
        'location' => $location,
        'min_price' => Request::get("min_price"),
        'max_price' => Request::get("max_price"),
        'sort' => Request::get("sort"),
        'url' => $url,
        'user' => Auth::user(),
        'mainurl' => route('category',[$slug,$subslug]),
        "pageTitle" => $this->pageTitle("categories"),
      ];
      $data = $this->checkajax(Request::ajax(),$page,$pagedata,$url);

      if(Request::get('page')){
        return view("includes.inserted_adverts_for_category",$pagedata);
      }

      return $data;
    }

    public function searchad($product = null){ 

      $slug =null; $subslug = null;

      $category_title = $product;

      $categories = Categories::all();

      if(empty(Request::get("page"))){
        $page = 1;
      }else{
        $page = Request::get("page");
      }

      $free_advert_count = 17;
      $sponsored_advert_count = 8;
      $paginate_count = 25;

      $advert_paginate = Advert::where("active","active")->paginate($paginate_count);

      $adverts_sponsored = Advert::where("advert_type","sponsored")->where("title","like","%".$product."%")->where("active","active")->skip(($page-1) * $sponsored_advert_count)->take($sponsored_advert_count);
        
        
      $adverts_free = Advert::where("advert_type","free")->where("title","like","%".$product."%")->where("active","active")->skip(($page-1) * $free_advert_count)->take($free_advert_count);

      $location = "Nigeria";
      
      $url = Request::url();
      if(!empty(Request::get("location"))){
        $adverts_sponsored =$adverts_sponsored->where("location","like","%".Request::get("location")."%");
        $adverts_free =$adverts_free->where("location","like","%".Request::get("location")."%");

        $url = Request::url()."?location=".Request::get("location");

        $location = Request::get("location");
      }

      if(!empty(Request::get("min_price"))){
        $adverts_sponsored =$adverts_sponsored->where([["price",">=",Request::get("min_price")],["price","<=",Request::get("max_price")]]);
        $adverts_free =$adverts_free->where([["price",">=",Request::get("min_price")],["price","<=",Request::get("max_price")]]);

        if(str_contains($url, '?')){
          $url = $url."&min_price=".Request::get("min_price")."&max_price=".Request::get("max_price");
        }else{
          $url = Request::url()."?min_price=".Request::get("min_price")."&max_price=".Request::get("max_price");
        }
      }

      if(!empty(Request::get("sort"))){
        if(Request::get("sort") == "newest"){
          $adverts_sponsored =$adverts_sponsored->latest();
          $adverts_free =$adverts_free->latest();
        }
        if(Request::get("sort") == "cheapest"){
          $adverts_sponsored =$adverts_sponsored->orderBy("price","asc");
          $adverts_free =$adverts_free->orderBy("price","asc");
        }
        
        if(str_contains($url, '?')){
          $url = $url."&sort=".Request::get("sort");
        }else{
          $url = Request::url()."?sort=".Request::get("sort");
        }
      }

      $adverts_sponsored = $adverts_sponsored->get();
      $adverts_free = $adverts_free->get();

      $adverts = $adverts_sponsored->merge($adverts_free);

      $page = "frontend.category";
      $pagedata = [
        'adverts' => $adverts,
        'advert_paginate' => $advert_paginate,
        'categories' => $categories,
        'includepage' => $page,
        'slug' => $slug,
        'subslug' => $subslug,
        'category_title' => $category_title,
        'location' => $location,
        'min_price' => Request::get("min_price"),
        'max_price' => Request::get("max_price"),
        'sort' => Request::get("sort"),
        'url' => $url,
        'user' => Auth::user(),
        //'mainurl' => route('category',[$slug,$subslug]),
        'mainurl' => Request::url(),
        "pageTitle" => $this->pageTitle("categories"),
      ];
      $data = $this->checkajax(Request::ajax(),$page,$pagedata,$url);

      if(Request::get('page')){
        return view("includes.inserted_adverts_for_category",$pagedata);
      }

      return $data;
    }

    public function adpage($product){ 

      if(Auth::user()){
        $user_id = Auth::user()->id;
      }else{
        $user_id = "";
      }
      

      $advert = Advert::where("slug",$product)->orderBy("created_at","desc")->first();

      $advert->view_count = $advert->view_count + 1;

      $advert->save();

      if($advert){
        $advertId = $advert->id;
      }

      $checkFavourite = Favourites::where("advert_id",$advertId)->where("user_id",$user_id)->first();

      $allFavourites = Favourites::latest();

      if(!empty($advert)){
        $advertCatId = $advert->categories_sub_id;
      }

      $similarAdverts = Advert::where("categories_sub_id",$advertCatId)->where("slug",'!=',$product)->where("active","active")->oldest()->take(5)->get();

      $message = Message::where("advert_id",$advertId)->where("user_sender_id",$user_id)->first();

      $page = "frontend.adpage";
      $pagedata = [
        'advert' => $advert,
        'message' => $message,
        'similarAdverts' => $similarAdverts,
        'includepage' => $page,
        "pageTitle" => $this->pageTitle("product"),
        "checkFavourite" => $checkFavourite,
      ];
      $data = $this->checkajax(Request::ajax(),$page,$pagedata,Request::url());

      return $data;
    }

    public function userprofile(){ 

      $user = Auth::user();

      $page = "frontend.profile";
      $pagedata = [
        'user' => $user,
        'includepage' => $page,
        "pageTitle" => $this->pageTitle("profile"),
      ];
      $data = $this->checkajax(Request::ajax(),$page,$pagedata,Request::url());

      return $data;
    }

    public function postuserprofile(){

      $user = Auth::user();
      if(Request::get("type") == "displayprofile"){
          $user->phone_number = Request::get("phone_number");
          $user->state = Request::get("state-of-origin");
          $user->address = Request::get("address");
          $user->full_name = Request::get("full_name");

          $user->save();

          $data['success'] = "Display Profile Updated";

          return $data;
      }

      if(Request::get("type") == "companyprofile"){
          $user->company_name = Request::get("company_name");
          $user->company_unique_name = Request::get("company_unique_name");
          $user->about_company = Request::get("about_company");
          $user->company_website = Request::get("company_website");
          $user->display_name_type = Request::get("display_name_type");

          $user->save();

          $data['success'] = "Company Profile Updated";

          return $data;
      }

      if(Request::get("type") == "newsletter"){
          $user->jobs_notification = Request::get("jobs");
          $user->ads_notification = Request::get("ads");
          $user->tips_notification = Request::get("tips");
          $user->general_notification = Request::get("general");

          $user->save();

          $data['success'] = "Newsletter Notification Updated";

          return $data;
      }

      $data['error'] = "An Unknown Error Occured :(";
      return $data;

    }

    public function postupdatepicture(){
        $picture = Request::file('image');
        if(!empty($picture)){
            $picture_ext = strtolower($picture->getClientOriginalExtension());
            if($picture_ext == 'jpg' || $picture_ext == 'jpeg' || $picture_ext == 'png'){
                if($picture->getClientSize() < 220000 ){

                    $user = Auth::user();
                    $picture->move('images/userimages/', $user->id.'.jpg');

                    $user->image = route('home').'/images/userimages/'.$user->id.'.jpg';
                    $user->save();

                    $data['success'] = "Image uploaded successfully";
                    return $data;

                }else{
                  
                    $data['error'] = "Maximum image upload size is 200kb";
                    return $data;

                }
            }else{

                $data['error'] = "Image must be a jpg or png file";
                return $data;
            }
        }else{

            $data['error'] = "There is an error with your picture";
            return $data;
        }
    }

    public function messages(){ 

      $messages = Message::where("user_sender_id", Auth::user()->id)->orWhere("active_recipient", Auth::user()->id)->latest()->get();

      $user = Auth::user();

      $page = "frontend.messages";
      $pagedata = [
        'user' => $user,
        'messages' => $messages,
        'includepage' => $page,
        "pageTitle" => $this->pageTitle("messages"),
      ];
      $data = $this->checkajax(Request::ajax(),$page,$pagedata,Request::url());

      return $data;
    }

    public function notifications(){ 

      $notifications = Notification::where("user_id",Auth::user()->id)->latest()->get();

      foreach ($notifications as $obj) {
        $obj->readmessage = "read";
        $obj->save();
      }

      $user = Auth::user();

      $page = "frontend.notifications";
      $pagedata = [
        'user' => $user,
        'notifications' => $notifications,
        'includepage' => $page,
        "pageTitle" => $this->pageTitle("messages"),
      ];
      $data = $this->checkajax(Request::ajax(),$page,$pagedata,Request::url());

      return $data;
    }

    public function viewmessage($advert_id = null, $message_id = null){ 

      $messagethread = Messagethread::where("advert_id",$advert_id)->where("message_id",$message_id)->get();

      $message = Message::where("id",$message_id)->first();

      if($message->active_recipient == Auth::user()->id){
        $message->readmessage = 1;
        $message->save();
      }

      $msgBadgeCount = Message::where("active_recipient",Auth::user()->id)->where("readmessage",0)->count();
      

      $user = Auth::user();
    
      $page = "frontend.viewmessage";
      $pagedata = [
        'advert_id' => $advert_id,
        'message_id' => $message_id,
        'user' => $user,
        'msgBadgeCount' => $msgBadgeCount,
        'message' => $message,
        'messagethread' => $messagethread,
        'includepage' => $page,
        "pageTitle" => $this->pageTitle("messages"),
      ];
      $data = $this->checkajax(Request::ajax(),$page,$pagedata,Request::url());

      return $data;
    }

    public function sendmessage(){

      $message = new Message;
      $message->message = Request::get('message');
      $message->advert_id = Request::get('advert_id');
      $message->user_sender_id = Auth::user()->id;
      $message->archive = 0;
      $message->readmessage = 0;
      $message->active_recipient = Advert::where("id",Request::get('advert_id'))->first()->user_id;
      $message->save();

      $messagethread = new Messagethread;
      $messagethread->advert_id = Request::get('advert_id');
      $messagethread->user_id = Auth::user()->id;
      $messagethread->messages = Request::get('message');
      $messagethread->image = "";
      
      $messagethread->message_id = Message::where("user_sender_id",Auth::user()->id)->where("advert_id",Request::get('advert_id'))->first()->id;

      $messagethread->save();

      $data["success"] = "Message Sent Successfully";

      return $data;

    }

    public function reportadvert(){
      $report = new Reportedadvert;

      $report->user_id = Auth::user()->id;
      $report->advert_id = Request::get('advert_id');
      $report->reason = Request::get('reason');

      $report->save();

      $data["success"] = "Advert reported successfully. One of our representative would work on it. Thank you";

      return $data;
    }

    public function postsendchat(){

      $advert_id = Request::get('advert_id');
      $message_id = Request::get('message_id');
      $get_message = Request::get('message');

      if(empty($get_message)){
        $get_message = "";
      }

    
      $messagethread = new Messagethread;
      $messagethread->messages = $get_message;
      $messagethread->advert_id =  $advert_id;
      $messagethread->message_id =  $message_id;
      $messagethread->user_id = Auth::user()->id;
      $messagethread->image = "";
      $messagethread->save();

      $file = Request::file('fileupload');

      if(empty($file )){
        $messagethread->image = "";
      }

      for($i=0; $i<count($file); $i++){

        $file_ext = strtolower($file[$i]->getClientOriginalExtension());
        $rand_code  = "0";
        if($file_ext == "jpeg" || $file_ext == "jpg" || $file_ext == "png"){

          if($file[$i]->getClientSize() < 2200000 ){
            for($j=0; $j<15; $j++) {
              $min = ($j == 0) ? 1:0;
              $rand_code .= mt_rand($min,9);
            }
            $file[$i]->move('images/productimages/', $rand_code.'.'.$file_ext);

            $messagethread = new Messagethread;
            $messagethread->advert_id =  $advert_id;
            $messagethread->message_id =  $message_id;
            $messagethread->user_id = Auth::user()->id;
            $messagethread->messages = "";
            $messagethread->image = 'images/productimages/'.$rand_code.'.'.$file_ext;
            $messagethread->save();
        
          }else{
            $messagethread->image = "";
          }
        }else{
          $messagethread->image = "";
        }
        
      }
      

      $message = Message::where("id",$message_id )->first();
      $message->readmessage = 0;

      if($message->advert->user_id == Auth::user()->id){
        $message->active_recipient= $message->user_sender_id;
      }else{
        $message->active_recipient = $message->advert->user_id ; 
      }
      
      $message->save();
  
      Session::flash("success","Message sent to recipient successfully");
      return Redirect::route('viewmessage',[$advert_id,$message_id]);

      
      // $messagethread = Messagethread::where("advert_id",$advert_id)->where("message_id",$message_id)->get();
      // $message = Message::where("id",$message_id)->first();
      // $user = Auth::user();
      // $msgBadgeCount = Message::where("active_recipient",Auth::user()->id)->where("readmessage",0)->count();
      
      // $page = "frontend.viewmessage";
      // $pagedata = [
      //   'advert_id' => $advert_id,
      //   'msgBadgeCount' => $msgBadgeCount,
      //   'message_id' => $message_id,
      //   'user' => $user,
      //   'message' => $message,
      //   'messagethread' => $messagethread,
      //   'includepage' => $page,
      //   "pageTitle" => $this->pageTitle("messages"),
      // ];

      // $data['view'] = view($page,$pagedata)->render();
      // $data['newlink'] = route("viewmessage",[$advert_id,$message_id]);
      // $data['success'] = "Message sent to recipient successfully";

      // return $data;


    }

    public function forgotpassword($code = null){

      
      if($code == null){
        $page = "frontend.forgotpassword";
          $pagedata = [
            'includepage' => $page,
            "pageTitle" => $this->pageTitle("forgotpassword"),
          ];
          $data = $this->checkajax(Request::ajax(),$page,$pagedata,Request::url());
      
          return $data;
      }
      
      $user = User::where("password_reset",$code)->first();

      if($user){
            $page = "frontend.changepassword";
            $pagedata = [
              'includepage' => $page,
              'user' => $user,
              "pageTitle" => $this->pageTitle("resetpassword"),
            ];
            $data = $this->checkajax(Request::ajax(),$page,$pagedata,Request::url());
      
            return $data;
      }else{

          Session::flash("error","Invalid link");
      
          return Redirect::route('login');
      }

    }

    public function postforgotpassword(){

      $email = Request::get("email");

      $user = User::where("email",$email)->where("auth_type","default")->first();

      $code = "";
      $page = "email.resetpassword";
      for($i=0; $i<18; $i++) {
          $min = ($i == 0) ? 1:0;
          $code .= mt_rand($min,9);
      }

      $user->password_reset = $code;
      $user->save();

      //$emailresponse = $this->sendmail($user,$page,$code);

      $data['success'] = "Please check your email for instructions. Thank you :)";
      return $data;
      

    }

    public function reset_password($code){

        $user = Auth::user();
        

        if($code == "send-mail"){

          $code = "";
          $page = "email.resetpassword";
          for($i=0; $i<18; $i++) {
              $min = ($i == 0) ? 1:0;
              $code .= mt_rand($min,9);
          }

          $user->password_reset = $code;
          $user->save();

          //$emailresponse = $this->sendmail($user,$page,$code);

          $data['success'] = "Please check your email for instructions. Thank you :)";
          return $data;
        }

        if($user->password_reset == $code){
           
          $page = "frontend.setnewpassword";
          $pagedata = [
            'includepage' => $page,
            'user' => $user,
            "pageTitle" => $this->pageTitle("resetpassword"),
          ];
          $data = $this->checkajax(Request::ajax(),$page,$pagedata,Request::url());
    
          return $data;
        }else{
          $page = "frontend.setnewpassword";
          $pagedata = [
              'includepage' => $page,
              'user' => $user,
              "pageTitle" => $this->pageTitle("resetpassword"),
          ];
          $data = $this->checkajax(Request::ajax(),$page,$pagedata,Request::url());

          Session::flash("error","Invalid link");
      
          return $data;
        }
        
    }

    public function postpasswordreset(){

      $user = Auth::user();

      $user->password = bcrypt(Request::get('password'));
      $user->password_reset = null;
      $user->save();

      Session::flash("success","Password Reset Successful :)");
      return Redirect::route('userprofile');

    }

    public function postchangepassword(){

      $user = User::where("password_reset",Request::get('password_reset'))->first();

      $user->password = bcrypt(Request::get('password'));
      $user->password_reset = null;
      $user->save();

      Session::flash("success","Password Reset Successful :)");
      return Redirect::route('login');

    }

    public function salespro(){
      $page = "frontend.salespro";
      $pagedata = [
        'includepage' => $page,
        "pageTitle" => $this->pageTitle("default"),
      ];
      $data = $this->checkajax(Request::ajax(), $page, $pagedata, Request::url());

      return $data;
    }

    public function footerpages(){

      if (str_contains(Request::url(), 'faq')) {
        $getpage = Footer::where("slug", "faq")->first();
      }

      if (str_contains(Request::url(), 'about-us')) {
        $getpage = Footer::where("slug", "about-us")->first();
      }

      if (str_contains(Request::url(), 'terms-and-conditions')) {
        $getpage = Footer::where("slug", "terms-and-conditions")->first();
      }

      if (str_contains(Request::url(), 'privacy-policy')) {
        $getpage = Footer::where("slug", "privacy-policy")->first();
      }

      if (str_contains(Request::url(), 'billing-policy')) {
        $getpage = Footer::where("slug", "billing-policy")->first();
      }

      if (str_contains(Request::url(), 'safety-tips')) {
        $getpage = Footer::where("slug", "safety-tips")->first();
      }

      if (str_contains(Request::url(), 'contact-us')) {
        $getpage = Footer::where("slug", "contact-us")->first();
      }

      

      $page = "frontend.footerpages";
      $pagedata = [
        'includepage' => $page,
        'getpage' => $getpage,
        "pageTitle" => $this->pageTitle("default"),
      ];
      $data = $this->checkajax(Request::ajax(), $page, $pagedata, Request::url());

      return $data;
    }

    public function faker(){
      $faker = Factory::create();

      for ($i = 1; $i < 21; $i++) {

          $user = new User;
          $user->email = $faker->freeEmail;

          $firstName = $faker->firstName; $lastName = $faker->lastName;
          $user->password = bcrypt(strtolower($firstName));
          $user->full_name = $firstName.' '.$lastName;
          $user->phone_number = $faker->e164PhoneNumber;
          $user->auth_type = 'default';
          $user->auth_id = mt_rand(10000,100000);
          $user->image = $i.'.jpg';
          $user->role = 'user';
          $user->state = $faker->state;
          $user->address = $faker->streetAddress;

          $company = $faker->company;
          $user->company_name = $company;
          $user->company_unique_name = str_replace(" ","-",$company);
          $user->about_company = $faker->catchPhrase;
          $user->company_website = str_replace(" ","-","http://".$company.".com");

          $user->save();
      }

      echo 'Fakers Saved';
    }



}