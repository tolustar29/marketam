<?php

namespace App\Http\Controllers\Api;

use App\Model\Advert\Advertmedia;
use Illuminate\Support\Facades\Log;

class Validation {

 /**
  * @param $key :the key to get either rules or equivalent errormessage
  * @description : returns either error messages or rules
  *@return : array of rules/errormessages
  */
    public function userRegisterValidation($key) {
        $validation =  [
            'rules'=>[
                'name'=>'required',
                'email'=>'required|email|unique:users',
                'phone'=>'required',
                'password'=>'required',
            ],

            'errorMessages'=> [
                'full_name.required'=>'FullName is required',
                'email.required'=>'Email is required',
                'email.email'=>'Please provide a valid Email',
                'email.unique'=>'Email already exists, pls log in',
                'password'=>'Password is required ',
                'phone_number'=>'Phone number is required'
            ]

        ];
        return $validation[$key];

    }

    public function userLoginValidation($key) {
        $validation = [
            'rules' => [
                'email' => 'required|email',
                'password' => 'required',
            ],
            'errorMessages' => [
                'email.required' => 'Email is required',
                'email.email' => 'Please provide a valid email',
                'password' => 'Password is required'
            ]
        ];
        return $validation[$key];
    }

    public function userResetPassword($key) {
        $validation = [
            'rules' => [
                'oldpassword' => 'required',
                'password' => 'required|confirmed',
            ],
            'errorMessages' => [
                'oldpassword.required' => 'Your current password is required',
                'password.required' => 'Your new password is required',
                'password.confirmed' => 'Password confirmation does not match'
            ]
        ];
        return $validation[$key];
    }

    public function addImage($first, $recentadvert) {
        $advert = new Advertmedia;
        $advert->advert_id = $recentadvert->id;
        $advert->type = "image";
        Log::info($first);

        $folderPath = "images/productimages/";

        $image_parts = explode(";base64,", $first);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
        $image_base64 = base64_decode($image_parts[1]);
        $file = $folderPath . uniqid() . '.'.$image_type;

        file_put_contents($file, $image_base64);
    
        $advert->link = $file;
        $advert->save();

        return $advert;
    }

    public function addVideo($video, $recentadvert) {
        $advert = new Advertmedia;
        $advert->advert_id = $recentadvert->id;
        $advert->type = "video";

        $folderPath = "videos/productvideos/";

        $image_parts = explode(";base64,", $video);
        $image_type_aux = explode("video/", $image_parts[0]);
        $image_type = $image_type_aux[1];
        $image_base64 = base64_decode($image_parts[1]);
        $file = $folderPath . uniqid() . '.'.$image_type;

        file_put_contents($file, $image_base64);
    
        $advert->link = $file;
        $advert->save();

        return $advert;
    }

    public function addAudio($audio, $recentadvert) {
        $advert = new Advertmedia;
        $advert->advert_id = $recentadvert->id;
        $advert->type = "audio";
        Log::info("request is ========= here");
        Log::info($audio);

        // $audiofolderPath = "audios/productaudios/";
        $audioname = uniqid().'.mp3';

        $audio->move('audios/productaudios/', 'hey'.$audioname);

        Log::info("hahah got here");

        // $audio_parts = explode(";base64,", $audio);
        // Log::info("audio parts here");
        // Log::info($audio_parts);
        // $audio_type_aux = explode("audio/", $audio_parts[0]);
        // $audio_type = $audio_type_aux[1];
        // $audio_base64 = base64_decode($audio_parts[1]);
        // $audiofile = $audiofolderPath . uniqid() . '.'.$audio_type;

        // file_put_contents($audiofile, $audio_base64);

        $advert->link = $audioname;
        $advert->save();
        return $advert;
    }

    /*
        public function addVideo($video, $recentadvert) {
            $advert = new Advertmedia;
            $advert->advert_id = $recentadvert->id;
            $advert->type = "videos";

            $folderPath = "videos/productvideos/";

            $image_base64 = base64_decode($video);
            $mime_type = finfo_buffer(finfo_open(), $image_base64, FILEINFO_MIME_TYPE); // extract mime type
            $extension = self::mime2ext($mime_type); // extract extension from mime type
            // $file = uniqid() .'.'. $extension; // rename file as a unique name
            $file_dir = $folderPath . uniqid() .'.'. $extension;
            file_put_contents($file_dir, $image_base64);
            // $image_parts = explode(";base64,", $video);
            // $image_type_aux = explode("video/", $image_parts[0]);
            // $image_type = $image_type_aux[1];
            // $file = $folderPath . uniqid() . '.'.$image_type;

            // file_put_contents($file, $image_base64);
        
            $advert->link = $file_dir;
            $advert->save();

            return $advert;
        }
    */
    // public function addAudio($audio, $recentadvert) {

    // }

    public static function mime2ext($mime){
        $all_mimes = '{"png":["image\/png","image\/x-png"],"bmp":["image\/bmp","image\/x-bmp",
        "image\/x-bitmap","image\/x-xbitmap","image\/x-win-bitmap","image\/x-windows-bmp",
        "image\/ms-bmp","image\/x-ms-bmp","application\/bmp","application\/x-bmp",
        "application\/x-win-bitmap"],"gif":["image\/gif"],"jpeg":["image\/jpeg",
        "image\/pjpeg"],"xspf":["application\/xspf+xml"],"vlc":["application\/videolan"],
        "wmv":["video\/x-ms-wmv","video\/x-ms-asf"],"au":["audio\/x-au"],
        "ac3":["audio\/ac3"],"flac":["audio\/x-flac"],"ogg":["audio\/ogg",
        "video\/ogg","application\/ogg"],"kmz":["application\/vnd.google-earth.kmz"],
        "kml":["application\/vnd.google-earth.kml+xml"],"rtx":["text\/richtext"],
        "rtf":["text\/rtf"],"jar":["application\/java-archive","application\/x-java-application",
        "application\/x-jar"],"zip":["application\/x-zip","application\/zip",
        "application\/x-zip-compressed","application\/s-compressed","multipart\/x-zip"],
        "7zip":["application\/x-compressed"],"xml":["application\/xml","text\/xml"],
        "svg":["image\/svg+xml"],"3g2":["video\/3gpp2"],"3gp":["video\/3gp","video\/3gpp"],
        "mp4":["video\/mp4"],"m4a":["audio\/x-m4a"],"f4v":["video\/x-f4v"],"flv":["video\/x-flv"],
        "webm":["video\/webm"],"aac":["audio\/x-acc"],"m4u":["application\/vnd.mpegurl"],
        "pdf":["application\/pdf","application\/octet-stream"],
        "pptx":["application\/vnd.openxmlformats-officedocument.presentationml.presentation"],
        "ppt":["application\/powerpoint","application\/vnd.ms-powerpoint","application\/vnd.ms-office",
        "application\/msword"],"docx":["application\/vnd.openxmlformats-officedocument.wordprocessingml.document"],
        "xlsx":["application\/vnd.openxmlformats-officedocument.spreadsheetml.sheet","application\/vnd.ms-excel"],
        "xl":["application\/excel"],"xls":["application\/msexcel","application\/x-msexcel","application\/x-ms-excel",
        "application\/x-excel","application\/x-dos_ms_excel","application\/xls","application\/x-xls"],
        "xsl":["text\/xsl"],"mpeg":["video\/mpeg"],"mov":["video\/quicktime"],"avi":["video\/x-msvideo",
        "video\/msvideo","video\/avi","application\/x-troff-msvideo"],"movie":["video\/x-sgi-movie"],
        "log":["text\/x-log"],"txt":["text\/plain"],"css":["text\/css"],"html":["text\/html"],
        "wav":["audio\/x-wav","audio\/wave","audio\/wav"],"xhtml":["application\/xhtml+xml"],
        "tar":["application\/x-tar"],"tgz":["application\/x-gzip-compressed"],"psd":["application\/x-photoshop",
        "image\/vnd.adobe.photoshop"],"exe":["application\/x-msdownload"],"js":["application\/x-javascript"],
        "mp3":["audio\/mpeg","audio\/mpg","audio\/mpeg3","audio\/mp3"],"rar":["application\/x-rar","application\/rar",
        "application\/x-rar-compressed"],"gzip":["application\/x-gzip"],"hqx":["application\/mac-binhex40",
        "application\/mac-binhex","application\/x-binhex40","application\/x-mac-binhex40"],
        "cpt":["application\/mac-compactpro"],"bin":["application\/macbinary","application\/mac-binary",
        "application\/x-binary","application\/x-macbinary"],"oda":["application\/oda"],
        "ai":["application\/postscript"],"smil":["application\/smil"],"mif":["application\/vnd.mif"],
        "wbxml":["application\/wbxml"],"wmlc":["application\/wmlc"],"dcr":["application\/x-director"],
        "dvi":["application\/x-dvi"],"gtar":["application\/x-gtar"],"php":["application\/x-httpd-php",
        "application\/php","application\/x-php","text\/php","text\/x-php","application\/x-httpd-php-source"],
        "swf":["application\/x-shockwave-flash"],"sit":["application\/x-stuffit"],"z":["application\/x-compress"],
        "mid":["audio\/midi"],"aif":["audio\/x-aiff","audio\/aiff"],"ram":["audio\/x-pn-realaudio"],
        "rpm":["audio\/x-pn-realaudio-plugin"],"ra":["audio\/x-realaudio"],"rv":["video\/vnd.rn-realvideo"],
        "jp2":["image\/jp2","video\/mj2","image\/jpx","image\/jpm"],"tiff":["image\/tiff"],
        "eml":["message\/rfc822"],"pem":["application\/x-x509-user-cert","application\/x-pem-file"],
        "p10":["application\/x-pkcs10","application\/pkcs10"],"p12":["application\/x-pkcs12"],
        "p7a":["application\/x-pkcs7-signature"],"p7c":["application\/pkcs7-mime","application\/x-pkcs7-mime"],"p7r":["application\/x-pkcs7-certreqresp"],"p7s":["application\/pkcs7-signature"],"crt":["application\/x-x509-ca-cert","application\/pkix-cert"],"crl":["application\/pkix-crl","application\/pkcs-crl"],"pgp":["application\/pgp"],"gpg":["application\/gpg-keys"],"rsa":["application\/x-pkcs7"],"ics":["text\/calendar"],"zsh":["text\/x-scriptzsh"],"cdr":["application\/cdr","application\/coreldraw","application\/x-cdr","application\/x-coreldraw","image\/cdr","image\/x-cdr","zz-application\/zz-winassoc-cdr"],"wma":["audio\/x-ms-wma"],"vcf":["text\/x-vcard"],"srt":["text\/srt"],"vtt":["text\/vtt"],"ico":["image\/x-icon","image\/x-ico","image\/vnd.microsoft.icon"],"csv":["text\/x-comma-separated-values","text\/comma-separated-values","application\/vnd.msexcel"],"json":["application\/json","text\/json"]}';
        $all_mimes = json_decode($all_mimes,true);
        foreach ($all_mimes as $key => $value) {
            if(array_search($mime,$value) !== false) return $key;
        }
        return false;
    }
}