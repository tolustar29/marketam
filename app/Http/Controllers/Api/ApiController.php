<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Api\Validation;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

use App\Model\User;
use App\Model\Favourites;
use App\Model\Notification;

use App\Model\Category\Categories;
use App\Model\Category\CategorySub;
use App\Model\Category\CategorySubAttributes;

use App\Model\Advert\Advert;
use App\Model\Advert\Advertattributes;
use App\Model\Advert\Advertmedia;
use App\Model\Advert\Reportedadvert;

use App\Model\Messages\Message;
use App\Model\Messages\Messagethread;

class ApiController extends Controller
{
    public function addImage($first) {
        $advert = new Advertmedia;
        $advert->advert_id = $recentadvert->id;
        $advert->type = "image";

        $folderPath = "images/productimages/";

        $image_parts = explode(";base64,", $first);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
        $image_base64 = base64_decode($image_parts[1]);
        $file = $folderPath . uniqid() . '.'.$image_type;

        file_put_contents($file, $image_base64);
    
        $advert->link = $file;
        $advert->save();
    }

    public function users(Request $request) {
        $user = $request->user();
        
        return response()->json([
            'result' => 1,
            'data' => $user
        ], 200);
    }
    
    public function create(Request $request) {

        $validator = new Validation();
        
        $validation = Validator::make(
            $request->all(),
            $validator->userRegisterValidation("rules"),
            $validator->userRegisterValidation("errorMessages")
        );
        if($validation->fails()){
            return response()->json([
                "result"=>0,
                "error"=> "Email already exists, Please log in",
            ],200);
        }

        $user = new User;
        $user->full_name = $request->name;
        $user->email = $request->email;
        $user->phone_number = $request->phone;
        $user->password = Hash::make($request->password);
        $user->auth_type = 'default';
        $user->image = route('home').'/images/user.png';
        $user->role = 'user';

        $user->save();

        return response()->json([
            "result"=>1,
            "data" => ["report"=>"User Registered Successfully"]
        ],201);
    }

    public function login(Request $request) {

        $validation = new Validation();
        $validator = Validator::make(
            $request->all(),
            $validation->userLoginValidation("rules"),
            $validation->userLoginValidation("errorMessages")
        );
        if($validator->fails()){
            return response()->json(["result"=>0,
            "error"=>$validator->errors(),
            "data" => []]
            ,200);
        }

        $credentials = $request->only(['email', 'password']);

        if (!$token = auth()->attempt($credentials)) {
            return response()->json([
                'result'=>0,
                'error' => 'Incorrect email or password'
            ], 401);
        }
        $token = Auth::user()->createToken('Auth Token')->accessToken;
        
        return response()->json([
            'result'=>1,
            'data' => [
                'token'=>$token,
                'user'=>Auth::user()
            ]
        ], 200);
    }

    public function socialMediaCreate(Request $request, $type) {

        $user = User::where("email", "=", $request->email)->first();

        if($user) {
            return response()->json([
                'result' => 0,
                'error' => 'Account already exists, Please log in',
            ], 200);
        }
        if($type == 'google' || $type == 'facebook') {
            Log::debug($request);
            $user = new User;

            $user->email = $request->email;
            $user->password = bcrypt("null");
            $user->full_name = $request->name;
            $user->auth_type = $type;
            $user->image = $request->image;
            $user->auth_id = $request->id;
            $user->role = 'user';

            $user->save();

            return response()->json([
                'result' => 1,
                'data' => 'User registered successfully'
            ], 201);
        } 
    }

    public function socialMediaLogin(Request $request, $type) {
        $hasAccount = User::where('email', $request->email)->first();
        $user = User::where([['email', $request->email], ['auth_id', $request->id]])->first();
        if($hasAccount) {
            if($user) {
                $credentials = $request->only(['email', 'password']);
                if(!auth()->attempt($credentials)) {
                    return response()->json([
                        'result' => 0,
                        'error' => 'Incorrect email or password'
                    ], 401);
                }
                Auth::user()->last_login = date("Y/m/d H:i:s");
                Auth::user()->save();
                $token = Auth::user()->createToken('Auth Token')->accessToken;
                return response()->json([
                    'result' => 1,
                    'data' => [
                        'token' => $token,
                        'user' => Auth::user()
                    ]
                ], 200);
            } else {
                return response()->json([
                    'result' => 0,
                    'data' => 'Incorrect email or password'
                ]);
            }
        } else {
            return response()->json([
                'result' => 0,
                'data' => 'You do not have an account with us. Please register to continue'
            ]);
        }
    }

    public function resetPassword(Request $request) {
        
        $user = $request->user();

        if($request->password !== $request->password_confirmation) {
            return response()->json([
                'result' => 0,
                'error' => 'Password confirmation does not match',
                'data' => []
            ]);
        }

        if(!Hash::check($request->oldpassword, $user->password)) {
            return response()->json([
                'result' => 0,
                'error' => 'The password you entered does not match our records. Please try again',
                'data' => []
            ]);
        }

        $user->password = Hash::make($request->password);
        $user->update();

        return response()->json([
            'result' => 1,
            'error' => '',
            'data' => 'Password successfully updated'
        ], 200);
    }

    public function editProfile(Request $request, $type = null) {
        $user = $request->user();

        if($type == "profile"){
            $user->phone_number = $request->phone_number;
            $user->state = $request->state;
            $user->address = $request->address;
            $user->full_name = $request->full_name;

            $user->save();

            return response()->json([
                'result' => 1,
                'error' => '',
                'user' => $user,
                'data' => 'Profile details successfully updated'
            ]);
        }

        if($type == "company"){
            $user->company_name = $request->company_name;
            $user->company_unique_name = $request->unique_name;
            $user->about_company = $request->about;
            $user->company_website = $request->website;
            $user->display_name_type = $request->display;
            if($request->display == 'Full Name') {
                $user->display_name_type = 'full_name';
            } elseif($request->display = 'Company Name') {
                $user->display_name_type = 'company_name';
            }

            $user->save();

            return response()->json([
                'result' => 1,
                'error' => '',
                'user' => $user,
                'data' => 'Company details successfully updated'
            ]);
        }

        return response()->json([
            'result' => 0,
            'error' => 'There is an error in updating profile',
            'data' => []
        ]);
    }

    public function updateProfilePicture(Request $request){
        Log::info($request);
        $picture = $request->image;
        if(!empty($picture)){
            // $picture_ext = strtolower($picture->getClientOriginalExtension());
            // if($picture_ext == 'jpg' || $picture_ext == 'jpeg' || $picture_ext == 'png'){ 
            //     Log::info($picture->getClientSize());
                // if($picture->getClientSize() < 220000 ){
                    $user = $request->user();
                    // $picture->move('images/userimages/', $user->id.'.jpg');

                    // $user->image = route('home').'/images/userimages/'.$user->id.'.jpg';
                    // $user->save();

                    $folderPath = "images/userimages/";

                    $image_parts = explode(";base64,", $picture);
                    $image_type_aux = explode("image/", $image_parts[0]);
                    $image_type = $image_type_aux[1];
                    $image_base64 = base64_decode($image_parts[1]);
                    $file = $folderPath . uniqid() . '.'.$image_type;

                    file_put_contents($file, $image_base64);
                    $user->image = route('home').'/'.$file;
                    $user->save();

                    return response()->json([
                        'result' => 1,
                        'error' => '',
                        'user' => $user,
                        'data' => 'Profile picture successfully updated'
                    ]);
                // } else {
                //     return response()->json([
                //         'result' => 0,
                //         'error' => 'Maximum image uploaded should be 200kb',
                //         'data' => []
                //     ]);

                // }
            // } else {
            //     return response()->json([
            //         'result' => 0,
            //         'error' => 'Image must be a jpg, jpeg or png file',
            //         'data' => []
            //     ]);
            // }
        } else {
            return response()->json([
                'result' => 0,
                'error' => 'There is an error with picture selected',
                'data' => []
            ]);
        }
    }

    public function allAdverts() {
        $adverts = Advert::where("active","active")
        ->with('advert_attributes', 'advert_media', 'message', 'messagethread', 'user', 'category_sub', 'favourite' )
        ->orderBy('updated_at', 'desc')->get();

        return response()->json([
            'result' => 1,
            'adverts' => $adverts,
        ], 200);
    }

    public function sponsoredAds(Request $request) {
        $adverts = Advert::where("active","active")
        ->with('advert_attributes', 'advert_media', 'user', 'category_sub', 'favourite' )
        ->orderBy('updated_at', 'desc')->get()->toArray();

        return response()->json([
            'result' => 1,
            'adverts' => $adverts,
        ], 200);
    }

    public function getUserAds(Request $request, $type = null) {
        $adverts = Advert::where([["user_id", $request->user()->id], ["advert_type", $type]])
            ->with('advert_attributes', 'advert_media', 'user', 'category_sub', 'favourite' )
            ->orderBy('updated_at', 'desc')->get()->toArray();

        return response()->json([
            'result' => 1,
            'adverts' => $adverts,
        ], 200);
    }

    public function getSellerAds(Request $request, $id = null) {
        $adverts = Advert::where([['user_id', $id], ['active', 'active']])
            ->with('advert_attributes', 'advert_media', 'user', 'category_sub', 'favourite')
            ->orderBy('updated_at', 'desc')->get();

        return response()->json([
            'result' => 1,
            'data' => $adverts
        ], 200);
    }

    public function toggleAds(Request $request, $id = null, $type = null) {
    
        $advert = Advert::where([["id", $id], ["user_id", $request->user()->id]])->first();
        
        if($type == 'deactivate') {
            
            if(!empty($advert)){
                $advert->active = "deactivated";
                $advert->save();
            }
    
            return response()->json([
                'result' => 1,
                'data' => 'Advert successfully deactivated'
            ], 200);

        } else if ($type == 'activate') {
            if(!empty($advert)){
                $advert->active = "new";
                $advert->save();
            }
    
            return response()->json([
                'result' => 1,
                'data' => 'Advert successfully activated'
            ], 200);

        }
    
    }

    public function newFreeAd(Request $request) {
        
        $getid = Advert::orderBy('created_at','desc')->first();
        $newid = 1;
        if(!empty($getid)) {
          $newid = $getid->id + 1;
        }

        $advert = new Advert;
        $advert->user_id = $request->user()->id;
        $advert->advert_type = "free";
        $advert->active = "new";
        $advert->title = $request->name;
        $advert->slug = str_slug($request->name.'-'.$newid,'-');
        $advert->description = $request->description;
        $advert->price = $request->price;
        $advert->price_type = $request->price_type;
        $advert->usage = $request->usage;
        $advert->location = $request->location;
        $advert->category_id = CategorySub::where("id", $request->category['id'])->first()->categories_id;
        $advert->categories_sub_id = $request->category['id'];
        $advert->save();  
        
        $data = CategorySubAttributes::where("categories_sub_id",$request->category['id'])->get();  
        $recentadvert = Advert::where("user_id",$request->user()->id)->where("title", $request->name)->orderBy("created_at","desc")->first();       
        
        foreach ($data as $obj) {
            $advertattr = new Advertattributes;
            $advertattr->advert_id = $recentadvert->id;
    
            $advertattr->categories_sub_attributes_id = $obj->id;
    
            $advertattr->attribute_data = str_replace(" ","_",$obj->attributes);
            $advertattr->save();
        }
  
        $advert = new Advertmedia;
        $advert->advert_id = $recentadvert->id;
        $advert->type = "image";
        $picture = $request->first_image;

        if(!empty($picture)) {
            $folderPath = "images/productimages/";

            $image_parts = explode(";base64,", $picture);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1];
            $image_base64 = base64_decode($image_parts[1]);
            $file = $folderPath . uniqid() . '.'.$image_type;

            file_put_contents($file, $image_base64);
        
        }
        $advert->link = $file;
  
        $advert->save();
        

        return response()->json([
            'result' => 1,
            'adverts' => 'success',
        ], 200);
        
    }

    public function newSponsoredAd(Request $request) {

        $user = $request->user();

        $getid = Advert::orderBy('created_at','desc')->first();$newid = 1;
        if(!empty($getid)){
            $newid = $getid->id + 1;
        }
  
        $advert = new Advert;
        $advert->user_id = $user->id;
        $advert->advert_type = "sponsored";
        $advert->active = "new";
        $advert->title = $request->name;
        $advert->slug = str_slug($request->name.'-'.$newid,'-');
        $advert->description = $request->description;
        $advert->price = $request->price;
        $advert->price_type = $request->price_type;
        $advert->usage = $request->usage;
        $advert->location = $request->location;
        $advert->category_id = CategorySub::where("id", $request->category)->first()->categories_id;
        $advert->categories_sub_id = CategorySub::where("id", $request->category)->first()->id;
        $advert->save();
  
        $data = CategorySubAttributes::where("categories_sub_id", $request->category)->get();
  
        $recentadvert = Advert::where("user_id", $user->id)->where("title", $request->name)->orderBy("created_at","desc")->first();
  
        foreach ($data as $obj) {
          $advertattr = new Advertattributes;
          $advertattr->advert_id = $recentadvert->id;
  
          $advertattr->categories_sub_attributes_id = $obj->id;
  
          $advertattr->attribute_data = str_replace(" ","_",$obj->attributes);
          $advertattr->save();
        }

        $validator = new Validation;
        $first = $request->first_image;
        if(!empty($first)) {
            $validator->addImage($first, $recentadvert);
        }
        $second = $request->second_image;
        if(!empty($second)) {
            $validator->addImage($second, $recentadvert);
        }
        $third = $request->third_image;
        if(!empty($third)) {
            $validator->addImage($third, $recentadvert);
        }
        $fourth = $request->fourth_image;
        if(!empty($fourth)) {
            $validator->addImage($fourth, $recentadvert);
        }
        $fifth = $request->fifth_image;
        if(!empty($fifth)) {
            $validator->addImage($fifth, $recentadvert);
        }
        $sixth = $request->sixth_image;
        if(!empty($sixth)) {
            $validator->addImage($sixth, $recentadvert);
        }
        $video = $request->video;
        if(!empty($video)) {
            $validator->addVideo($video, $recentadvert);
        }
        $file = $request->file;
        if(!empty($file)) {
            $advert = new Advertmedia;
            $advert->advert_id = $recentadvert->id;
            $advert->type = "audio";

            $audiofolderPath = "audios/productaudios/";
            $file_ext = strtolower($request->file->getClientOriginalExtension());
            $audioname = uniqid().$file_ext;
            $request->file->move('audios/productaudios/', $audioname);

            $advert->link = $audiofolderPath.$audioname;
            $advert->save();
        }

        return response()->json([
            'result' => 1,
            'adverts' => 'success',
        ], 200);
    }

    public function newAudio (Request $request) {
        Log::info("request is =========");
        Log::info($request);
        $audiofolderPath = "audios/productaudios/";

        $file_ext = strtolower($request->file->getClientOriginalExtension());
        Log::info($file_ext);
        $request->file->move('audios/productaudios/', uniqid().'.mp3');

        // $audio_parts = explode(";base64,", $audio);
        // Log::info("audio parts here");
        // Log::info($audio_parts);
        // $audio_type_aux = explode("audio/", $audio_parts[0]);
        // $audio_type = $audio_type_aux[1];
        // $audio_base64 = base64_decode($audio_parts[1]);
        // $audiofile = $audiofolderPath . uniqid() . '.'.$audio_type;

        // file_put_contents($audiofile, $audio_base64);
        return response()->json([
            'result' => 1,
            'data' => 'success',
        ], 200);
    }
    
    public function editFreeAd (Request $request, $id = null) {
        
        $new_slug = str_slug($request->name . '-' . $id, '-');

        $advert = Advert::where('id', $id)->where('user_id',$request->user()->id)->first();
        $advert->user_id = $request->user()->id;
        $advert->advert_type = "free";
        $advert->title = $request->name;
        $advert->slug = $new_slug;
        $advert->active = "new";
        $advert->description = $request->description;
        $advert->price = $request->price;
        $advert->location = $request->location;
        $advert->category_id = CategorySub::where("id",$request->category['id'])->first()->categories_id;
        $advert->categories_sub_id = $request->category['id'];
        $advert->update();

        $data = CategorySubAttributes::where("categories_sub_id", $request->category['id'])->get();

        Advertattributes::where('advert_id', $id)->delete();

        foreach ($data as $obj) {
            $advertattr = new Advertattributes;
            $advertattr->advert_id = $advert->id;

            $advertattr->categories_sub_attributes_id = $obj->id;

            $advertattr->attribute_data = str_replace(" ","_",$obj->attributes);

            $advertattr->save();
        }

        $picture = $request->first_image; 
        if($picture) {
            $advertmedia = new Advertmedia;
            $advertmedia = Advertmedia::where('advert_id', $id)->first();            

            if(!empty($picture)) {
                $folderPath = "images/productimages/";

                $image_parts = explode(";base64,", $picture);
                $image_type_aux = explode("image/", $image_parts[0]);
                $image_type = $image_type_aux[1];
                $image_base64 = base64_decode($image_parts[1]);
                $file = $folderPath . uniqid() . '.'.$image_type;

                file_put_contents($file, $image_base64);
            
            }
            $advertmedia->link = $file;
    
            $advertmedia->update();
        }
      
        return response()->json([
            'result' => 1,
            'error' => '',
            'data' => 'Advert has been successfully updated'
        ]);        
    }

    public function searchAds(Request $request) {

        $user = $request->user();
        $q = $request->search;
        $location = $request->location;

        if ($q != "") {

            if($location) {
                $listings = Advert::where('adverts.active', 'active')->where('adverts.location', $location)->where(function ($query) use ($q) {
                        $query->where('adverts.title', 'LIKE', '%'.$q.'%')
                        ->orWhere('adverts.price', 'LIKE', '%'.$q.'%')
                        ->orWhere('adverts.price_type', 'LIKE', '%'.$q.'%')
                        ->orWhere('adverts.location', 'LIKE', '%'.$q.'%')
                        ->orWhere('adverts.usage', 'LIKE', '%'.$q.'%')
                        ->orWhereHas('categories', function ($query) use ($q) {
                            $query->where('category', 'LIKE', '%'.$q.'%');
                        })
                        ->orWhereHas('category_sub', function ($query) use ($q) {
                            $query->where('categories_sub', 'LIKE', '%'.$q.'%');
                        });
                    })
                    ->with('advert_attributes', 'advert_media', 'user', 'category_sub', 'categories', 'favourite')->get();
            } else {
                $listings = Advert::where('adverts.active', 'active')->where(function ($query) use ($q) {
                    $query->where('adverts.title', 'LIKE', '%'.$q.'%')
                    ->orWhere('adverts.price', 'LIKE', '%'.$q.'%')
                    ->orWhere('adverts.price_type', 'LIKE', '%'.$q.'%')
                    ->orWhere('adverts.location', 'LIKE', '%'.$q.'%')
                    ->orWhere('adverts.usage', 'LIKE', '%'.$q.'%')
                    ->orWhereHas('categories', function ($query) use ($q) {
                        $query->where('category', 'LIKE', '%'.$q.'%');
                    })
                    ->orWhereHas('category_sub', function ($query) use ($q) {
                        $query->where('categories_sub', 'LIKE', '%'.$q.'%');
                    });
                })
                ->with('advert_attributes', 'advert_media', 'user', 'category_sub', 'categories', 'favourite')->get();
            }

            if(count($listings) > 0) return response()->json([
                'result' => 1,
                'data' => $listings,
            ]);
        }
        return response()->json([
            'result' => 0,
            'data' => 'No records found',
        ]);
    }

    public function reportAds(Request $request, $id) {

        $report = new Reportedadvert;

        $report->user_id = $request->user()->id;
        $report->advert_id = $id;
        $report->reason = $request->report;

        $report->save();

        return response()->json([
            'result' => 1,
            'error' => '',
            'data' => 'Advert has been successfully reported'
        ]);
    }

    public function singleAd($id) {
        $ad = Advert::where('id', $id)->with('advert_attributes', 'advert_media', 'message', 'messagethread', 'user', 'category_sub', 'favourite' )->first();
        return response()->json([
            'result' => 1,
            'data' => $ad,
            'error' => ''
        ], 200);
    }

    public function allFavourites(Request $request) {
        $favourites = Favourites::where("user_id", $request->user()->id)->with('user', 'advert', 'advert.advert_attributes', 'advert.advert_media', 'advert.category_sub', 'advert.user', 'advert.favourite')->get();
  
        return response()->json([
            'result' => 1,
            'data' => $favourites,
        ], 200);
    }

    public function favourite(Request $request, $type = null, $adId = null){ 

        $userID = $request->user()->id;

        if($type == "add"){
            $favCheck = Favourites::where([["advert_id", $adId], ["user_id", $userID]])->get();
            
            if(!$favCheck->isEmpty()) {
                return response()->json([
                    'result' => 1,
                    'data' => 'This is already in your favourites',
                ], 200);
            }
            $fav = new Favourites;
            $fav->advert_id = $adId;
            $fav->user_id = $userID;
            $fav->save();
  
            $data['success'] = "Advert added to favourites";
  
            return response()->json([
                'result' => 1,
                'data' => $data,
            ], 200);
        }
  
        if($type == "remove"){
            Favourites::where([["advert_id",$adId],["user_id",$userID]])->delete();
    
            $data['error'] = "Advert removed from favourites";
  
  
            return response()->json([
                'result' => 1,
                'data' => $data,
            ], 200);
        }

    }

    public function allCategories() {
        $categories = Categories::all();

        return response()->json([
            'result' => 1,
            'data' => $categories,
        ], 200);
    }

    public function allCategorySubs() {
        $categories = CategorySub::all();

        return response()->json([
            'result' => 1,
            'data' => $categories,
        ], 200);
    }

    public function allMessages(Request $request){ 

        $messages = Message::where("user_sender_id", $request->user()->id)->orWhere("active_recipient", $request->user()->id)->with('user', 'recipient', 'advert', 'advert.user', 'messagethread')->orderBy('updated_at', 'desc')->get();
  
        return response()->json([
            'result' => 1,
            'error' => '',
            'data' => $messages,
        ], 200);
    }

    public function sendMessage(Request $request) {
        
        $user_id = $request->user()->id;
        $advert_id = $request->advert_id;
        $getMessageId = $request->message_id;
        $messages = $request->message;

        Log::info($user_id);
        Log::info($advert_id);
        Log::info($getMessageId);
        Log::info($messages);

        if($getMessageId) {
            $message_id = $getMessageId;
        } else {
            $message = Message::where('advert_id', $advert_id)->where('user_sender_id', $user_id)->first();
        
            if(!$message || $request->isNewMessage) {
                $newMessage = new Message;
                $newMessage->message = $messages;
                $newMessage->advert_id = $advert_id;
                $newMessage->user_sender_id = $user_id;
                $advert_user = Advert::where('id', $advert_id)->first()->user->id;
                $newMessage->active_recipient = $advert_user;
                $newMessage->archive = 0;
                $message->readmessage = 0;
                $newMessage->save();
                
                // $message = new Message;
                // $message->message = $messages;
                // $message->advert_id = $advert_id;
                // $message->user_sender_id = $user_id;
                // $message->archive = 0;
                // $message->readmessage = 0;
                // $message->active_recipient = Advert::where("id", $advert_id)->first()->user_id;
                // $message->save();

                $message_id = $newMessage->id;
            } else {
                $message_id = $message->id;
            }
        }
                
        $messagethread = new Messagethread;
        $messagethread->advert_id = $advert_id;
        $messagethread->user_id = $user_id;
        $messagethread->messages = $messages;
        $messagethread->image = "";
        $messagethread->message_id = $message_id;

        // if($request->isNewMessage) {
        //     $message = new Message;
        //     $message->message = $messages;
        //     $message->advert_id = $advert_id;
        //     $message->user_sender_id = $user_id;
        //     $message->archive = 0;
        //     $message->readmessage = 0;
        //     $message->active_recipient = Advert::where("id", $advert_id)->first()->user_id;
        //     $message->save();

        //     $messagethread->message_id = Message::where("id", $message->id)->first()->id;
        // } else {
        //     $messagethread->message_id = Message::where("id", $message_id)->first()->id;
        // }
        
  
        $messagethread->save();
        // Log::info('message sent');
  
        return response()->json([
            'result' => 1,
            'error' => '',
            'data' => 'Message successfully sent',
        ], 200);
  
    }

    public function sendImage(Request $request){
        $picture = $request->image;
        $user_id = $request->user()->id;
        $advert_id = $request->advert_id;
        $getMessageId = $request->message_id;

        // if($getMessageId) {
        //     $message_id = $getMessageId;
        // } else {
            $message = Message::where('advert_id', $advert_id)->where('user_sender_id', $user_id)->first();
        
            if(!$message || $request->isNewMessage) {
                $newMessage = new Message;
                $newMessage->message = '';
                $newMessage->advert_id = $advert_id;
                $newMessage->user_sender_id = $user_id;
                $advert_user = Advert::where('id', $advert_id)->first()->user->id;
                $newMessage->active_recipient = $advert_user;
                $newMessage->archive = 0;
                $message->readmessage = 0;
                $newMessage->save();

                $message_id = $newMessage->id;
            } else {
                $message_id = $message->id;
            }
        // }
                
        $messagethread = new Messagethread;
        $messagethread->advert_id = $advert_id;
        $messagethread->user_id = $user_id;
        $messagethread->messages = '';
        $messagethread->message_id = $message_id;

        if(!empty($picture)){
                    $user = $request->user();

                    $folderPath = "images/userimages/";

                    $image_parts = explode(";base64,", $picture);
                    $image_type_aux = explode("image/", $image_parts[0]);
                    $image_type = $image_type_aux[1];
                    $image_base64 = base64_decode($image_parts[1]);
                    $file = $folderPath . uniqid() . '.'.$image_type;

                    file_put_contents($file, $image_base64);
                    $messagethread->image = route('home').'/'.$file;
        } else {
            return response()->json([
                'result' => 0,
                'error' => 'There is an error with picture selected',
                'data' => []
            ]);
        }
        
        $messagethread->save();
  
        return response()->json([
            'result' => 1,
            'error' => '',
            'data' => 'Message successfully sent',
        ], 200);
    }
    
    public function viewMessage(Request $request, $advert_id = null, $message_id = null){ 

        $user_id = $request->user()->id;

        $messages = Message::where('id', $message_id)
            ->where(function($query) use ($advert_id, $user_id){
                $query->where('advert_id', $advert_id);
                $query->where('user_sender_id', $user_id);
            })
            ->orWhere(function($query) use ($advert_id, $user_id){
                $query->where('advert_id', $advert_id);
                $query->where('active_recipient', $user_id);
            })->get();

        if(!$messages->isEmpty()) {
            $messagethread = Messagethread::where("advert_id", $advert_id)->where("message_id", $message_id)->get();
      
            $message = Message::where("id", $message_id)->first();
      
            if($message->active_recipient == $request->user()->id){
              $message->readmessage = 1;
              $message->save();
            }
      
            $msgBadgeCount = Message::where("active_recipient", $request->user()->id)->where("readmessage", 0)->count();
      
            return response()->json([
                'result' => 1,
                'error' => '',
                'data' => $messagethread,
            ], 200);      
        }
        return response()->json([
            'result' => 1,
            'error' => '',
            'data' => null,
        ], 200);
    }

    public function checkForMessage(Request $request, $advert_id = null) {
        $user_id = $request->user()->id;

        $messages = Message::where('advert_id', $advert_id)
            ->where(function($query) use ($user_id){
                $query->where('user_sender_id', $user_id);
                $query->orWhere('active_recipient', $user_id);
            })->first();

            Log::info($messages);

        if($messages) {
            $messagethread = Messagethread::where("advert_id", $advert_id)->where("message_id", $messages->id)->get();
      
            $message = Message::where("id", $messages->id)->first();
      
            if($message->active_recipient == $request->user()->id){
              $message->readmessage = 1;
              $message->save();
            }
      
            return response()->json([
                'result' => 1,
                'error' => '',
                'data' => $messagethread,
            ], 200);      
        }
        return response()->json([
            'result' => 1,
            'error' => '',
            'data' => null,
        ], 200);
    }

    public function allNotifications(Request $request) {
        $user = $request->user();
        $notifications = Notification::where("user_id", $user->id)->with('advert','advert.advert_attributes', 'advert.advert_media', 'advert.user', 'advert.category_sub', 'advert.favourite')->latest()->get();

        foreach ($notifications as $obj) {
            $obj->readmessage = "read";
            $obj->save();
        }

        return response()->json([
            'result' => 1,
            'error' => '',
            'data' => $notifications,
        ]);

    }
}
