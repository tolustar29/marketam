<?php

namespace App\Http\Controllers;

use App\Model\User;
use App\Model\Category\Categories;
use App\Model\Category\CategorySub;
use App\Model\Category\CategorySubAttributes;

use App\Model\Advert\Advert;
use App\Model\Advert\Advertattributes;
use App\Model\Advert\Advertmedia;
use App\Model\Advert\Reportedadvert;

use App\Model\Messages\Message;

use App\Model\Notification;

use App\Model\Messages\Messagethread;

use App\Model\FooterPages\Footer;

use Barryvdh\Debugbar;

use Faker\Factory;

use SnappyPDF;

use DateTime;

use File;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Url;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use function GuzzleHttp\json_encode;
use App\Model\Favourites;
use App\Model\AdminActivity;

class AdminController extends ExtendedFunctions
{
  public function home(){

      $this->new_activity("Visited admin home");

      $page = "admin.home";

      $all_users_count = User::all()->count();
      $all_adverts_count = Advert::where("active", "!=" ,"unpaid")->count();
      $free_adverts_count = Advert::where("advert_type","free")->where("active", "active")->count();
      $sponsored_adverts_count = Advert::where("advert_type","sponsored")->where("active", "active")->count();
      $new_adverts_count = Advert::where("active","new")->count();
      $declined_adverts_count = Advert::where("active", "declined")->count();
      $blocked_adverts_count = Advert::where("active","blocked")->count();
      $deactivated_adverts_count = Advert::where("active", "deactivated")->count();
      $sold_adverts_count = Advert::where("active", "sold")->count();

      $admins = User::where("role","!=","user")->get();
      $adminactivity = AdminActivity::latest()->take(60)->get();



      for($i=1; $i<13; $i++){
        if($i<10){
          $month = '0'.$i;
        }else{
          $month = $i;
        }
        $free_adverts_all_year[] = Advert::where("advert_type", "free")->where("active", "active")->where("created_at","LIKE", "%".date("Y").'-'.$month."%")->count();
        $sponsored_adverts_all_year[] = Advert::where("advert_type", "sponsored")->where("active", "active")->where("created_at","LIKE", "%".date("Y").'-'.$month."%")->count();
      }


      $pagedata = [
        'admins' => $admins,
        'all_users_count' => $all_users_count,
        'all_adverts_count' => $all_adverts_count,
        'free_adverts_count' => $free_adverts_count,
        'sponsored_adverts_count' => $sponsored_adverts_count,
        'new_adverts_count' => $new_adverts_count,
        'free_adverts_all_year' => json_encode($free_adverts_all_year),
        'sponsored_adverts_all_year' => json_encode($sponsored_adverts_all_year),
        'declined_adverts_count' => $declined_adverts_count,
        'blocked_adverts_count' => $blocked_adverts_count,
        'deactivated_adverts_count' => $deactivated_adverts_count,
        'sold_adverts_count' => $sold_adverts_count,
        'includepage' => $page,
        'adminactivity' => $adminactivity,
        "pageTitle" => $this->pageTitle("admindashboard"),
      ];
      $data = $this->checkajax(Request::ajax(),$page,$pagedata,Request::url());

      return $data;
  }

  public function adverts($type = null)
  {

    $this->new_activity("Viewed all adverts");

    $all_adverts_count = Advert::where("active", "!=" ,"unpaid")->count();
    $free_adverts_count = Advert::where("advert_type", "free")->where("active", "active")->count();
    $sponsored_adverts_count = Advert::where("advert_type", "sponsored")->where("active", "active")->count();
    $new_adverts_count = Advert::where("active", "new")->count();
    $declined_adverts_count = Advert::where("active", "declined")->count();
    $blocked_adverts_count = Advert::where("active", "blocked")->count();
    $deactivated_adverts_count = Advert::where("active", "deactivated")->count();
    $sold_adverts_count = Advert::where("active", "sold")->count();

    $advert_title = "All Adverts";
    $adverts = Advert::where("active", "!=" ,"unpaid")->get();

    if($type == "new"){
      $advert_title = "New Adverts";
      $adverts = Advert::where("active", "new")->get();
      $this->new_activity("Viewed new adverts");
    }
    if ($type == "declined") {
      $advert_title = "Declined Adverts";
      $adverts = Advert::where("active", "declined")->get();
      $this->new_activity("Viewed declined adverts");
    }
    if ($type == "blocked") {
      $advert_title = "Blocked Adverts";
      $adverts = Advert::where("active", "blocked")->get();
      $this->new_activity("Viewed blocked adverts");
    }

    if ($type == "free") {
      $advert_title = "Free Adverts";
      $adverts = Advert::where("advert_type", "free")->where("active", "active")->get();
      $this->new_activity("Viewed free adverts");
    }


    if ($type == "sponsored") {
      $advert_title = "Sponsored Adverts";
      $adverts = Advert::where("advert_type", "sponsored")->where("active", "active")->get();
      $this->new_activity("Viewed sponsored adverts");
    }

    if ($type == "deactivated") {
      $advert_title = "Deactivated Adverts";
      $adverts = Advert::where("active", "deactivated")->get();
      $this->new_activity("Viewed deactivated adverts");
    }

    if ($type == "sold") {
      $advert_title = "Sold Adverts";
      $adverts = Advert::where("active", "sold")->get();
      $this->new_activity("Viewed sold adverts");
    }

    $page = "admin.adverts";

    $pagedata = [
      'all_adverts_count' => $all_adverts_count,
      'free_adverts_count' => $free_adverts_count,
      'sponsored_adverts_count' => $sponsored_adverts_count,
      'new_adverts_count' => $new_adverts_count,
      'declined_adverts_count' => $declined_adverts_count,
      'blocked_adverts_count' => $blocked_adverts_count,
      'deactivated_adverts_count' => $deactivated_adverts_count,
      'sold_adverts_count' => $sold_adverts_count,
      'adverts' => $adverts,
      'advert_title' => $advert_title,
      'includepage' => $page,
      "pageTitle" => $this->pageTitle("adminadverts"),
    ];
    $data = $this->checkajax(Request::ajax(), $page, $pagedata, Request::url());

    return $data;
  }

  public function reported_adverts()
  {

    $this->new_activity("Viewed all reported adverts");

    $adverts = Reportedadvert::all();

    $page = "admin.reported_adverts";

    $pagedata = [
      'adverts' => $adverts,
      'includepage' => $page,
      "pageTitle" => $this->pageTitle("adminreportedadverts"),
    ];
    $data = $this->checkajax(Request::ajax(), $page, $pagedata, Request::url());

    return $data;
  }


  public function advertsaction($type = null, $id = null)
  {

    $advert = Advert::where("id", $id)->first();

    $notification = new Notification;
    $notification->admin = Auth::user()->full_name;
    $notification->readmessage = "unread";
    $notification->advert_id = $id;
    $notification->user_id = $advert->user->id;


    if ($type == "deactivated" && !empty($advert)) {

    
        $this->new_activity("Deactivated " . $advert->title);

        $advert->active = "deactivated";
        $advert->admin_incharge = Auth::user()->full_name;
        $advert->save();

        $notification->comment = "Your advert has been deactivated";
        $notification->save();

      

      Session::flash("error","Advert deactivated");
      return Redirect::back();
    }


    if ($type == "deleted" && !empty($advert)) {

      $this->new_activity("Deleted " . $advert->title);

      $notification->comment = "Your advert has been deleted";
      $notification->save();
      Advert::where([["id", $id], ["user_id", Auth::user()->id]])->delete();

      Favourites::where("advert_id", $id)->delete();
      Message::where("advert_id", $id)->delete();
      Messagethread::where("advert_id", $id)->delete();
      Notification::where("advert_id", $id)->delete();



      Session::flash("error", "Advert deleted");
      return Redirect::back();
    }

    if ($type == "blocked" && !empty($advert)) {

      
        $this->new_activity("Blocked " . $advert->title);

        $advert->active = "blocked";
        $advert->admin_incharge = Auth::user()->full_name;
        $advert->save();

        $notification->comment = "Your advert has been blocked";
        $notification->save();
        
      

      Session::flash("error", "Advert blocked");
      return Redirect::back();
    }

    if ($type == "active" && !empty($advert)) {

      
        $this->new_activity("Activated " . $advert->title);

        $advert->active = "active";
        $advert->admin_incharge = Auth::user()->full_name;
        $advert->save();

        // if($advert->advert_type == "free"){
        //   $notification->comment = "Your advert is now active";
        // }else{
        //   $notification->comment = "Your advert has been accepted, proceed to make payments to activate the ad";
        // }

        $notification->comment = "Your advert is now active";
        
        $notification->save();


      Session::flash("success", "Advert accepted");
      return Redirect::back();
    }

    if ($type == "declined" && !empty($advert)) {
      $advert = Advert::where("id", $id)->first();

     

        $this->new_activity("Declined " . $advert->title);

        $advert->active = "declined";
        $advert->admin_incharge = Auth::user()->full_name;
        $advert->save();

        $notification->comment = "Your advert has been declined because you didn't adhere to our posting rules.";
        $notification->save();
     

      Session::flash("error", "Advert declined");
      return Redirect::back();
    }

  }

  public function users(){

    $this->new_activity("Viewed all users");
    
    $users = User::all();
    $all_users_count = User::all()->count();
    $new_users_count = User::where("created_at", "LIKE", "%" . date("Y") . '-' . '0' . date("m") . "%")->count();

    $page = "admin.users";
    $pagedata = [
      'all_users_count' => $all_users_count,
      'new_users_count' => $new_users_count,
      'users' => $users,
      'includepage' => $page,
      "pageTitle" => $this->pageTitle("adminusers"),
    ];
    $data = $this->checkajax(Request::ajax(), $page, $pagedata, Request::url());

    return $data;
  }

  public function deleteuser($id=null){

    $adverts = Advert::where("user_id", $id)->get();
    foreach ($adverts as $obj) {
      Advertattributes::where("advert_id",$obj->id)->delete();
      Advertmedia::where("advert_id", $obj->id)->delete();

      Favourites::where("advert_id", $obj->id)->delete();
      Message::where("advert_id", $obj->id)->delete();
      Messagethread::where("advert_id", $obj->id)->delete();
      Notification::where("advert_id", $obj->id)->delete();
    }
    Advert::where("user_id", $id)->delete();

    $this->new_activity("Deleted user (". User::where("id", $id)->first()->full_name. ")");
    User::where("id",$id)->delete();

    Session::flash("error","User and all advert associated with it has been successfull deleted");

    return Redirect::back();
  }

  public function userprofile($id)
  {
    $this->new_activity("Viewed user (" . User::where("id", $id)->first()->full_name . ")");

    $user = User::where("id",$id)->first();

    $page = "admin.user_profile";
    $pagedata = [
      'user' => $user,
      'includepage' => $page,
      "pageTitle" => $this->pageTitle("profile"),
    ];
    $data = $this->checkajax(Request::ajax(), $page, $pagedata, Request::url());

    return $data;
  }

  public function updaterole($id = null,$role = null){
    $user = User::where("id", $id)->first();
    $user->role = $role;
    $user->save();

    $this->new_activity("Updated user role (" . User::where("id", $id)->first()->full_name . ")");

    Session::flash("success","Role updated successfully");

    return Redirect::back();
  }

  public function moderators(){

    $this->new_activity("Viewed all moderators");

    $users = User::where("role","!=", "user")->get();

    $page = "admin.moderators";
    $pagedata = [
      'users' => $users,
      'includepage' => $page,
      "pageTitle" => $this->pageTitle("adminmoderators"),
    ];
    $data = $this->checkajax(Request::ajax(), $page, $pagedata, Request::url());

    return $data;

  }

  public function categories(){

    $this->new_activity("Viewed all categories");

    $categories = Categories::all();
    $page = "admin.category.categories";
    $pagedata = [
      'categories' => $categories,
      'includepage' => $page,
      "pageTitle" => $this->pageTitle("admincategories"),
    ];
    $data = $this->checkajax(Request::ajax(), $page, $pagedata, Request::url());

    return $data;
  }

  public function add_category($category = null)
  {
    $this->new_activity("Added new categories ". $category . ")");
    $category = str_replace(", ", ",", $category);
    $category = str_replace(" ,", ",", $category);

    $category = explode(',', $category);

    foreach ($category as $obj) {
      if(!empty($obj)){
        $get_category = new Categories;
        $get_category->category = $obj;
        $get_category->category_slug = str_slug($obj, '-');
        $get_category->save();
      }
    }

    $this->new_activity("Added new categories");

    Session::flash("success", "Categories saved successfully");
    return Redirect::back();

  }

  public function edit_category($id = null, $category = null)
  {
    
    $get_category = Categories::where("id", $id)->first();
    $old_category = $get_category->category;
    $get_category->category = $category;
    $get_category->category_slug = str_slug($category, '-');
    $get_category->save();

    $this->new_activity("Edited category from " . $old_category ." to ". $category . ")");

    Session::flash("success", "Category updated successfully");
    return Redirect::back();

  }

  public function delete_category($id = null)
  {

    $this->new_activity("Deleted category (" . Categories::where("id", $id)->first()->category . ")");

    $catsub = CategorySub::where("categories_id", $id)->get();

    foreach ($catsub as $key) {

      $attr = CategorySubAttributes::where("categories_sub_id", $key->id)->get();

      foreach ($attr as $attr_obj) {
        Advertattributes::where("categories_sub_attributes_id", $attr_obj->id)->delete();
      }

      CategorySubAttributes::where("categories_sub_id", $key->id)->delete();
    }

    
    $getadvert = Advert::where("category_id", $id)->get();
    foreach ($getadvert as $obj) {
      Advertmedia::where("advert_id", $obj->id)->delete();

      Favourites::where("advert_id", $obj->id)->delete();
      Message::where("advert_id", $obj->id)->delete();
      Messagethread::where("advert_id", $obj->id)->delete();
      Notification::where("advert_id", $obj->id)->delete();
      Advert::where("advert_id", $obj->id)->delete();
    }

    CategorySub::where("categories_id", $id)->delete();
    Categories::where("id", $id)->delete();

    Session::flash("error","Category deleted successfully");
    return Redirect::back();

  }

  public function viewsubcategories($category_slug = null)
  {
    $this->new_activity("View subcategories for (" . Categories::where("category_slug", $category_slug)->first()->category . ")");


    $category = Categories::where("category_slug", $category_slug)->first();
    $categories = CategorySub::where("categories_id", $category->id)->get();
    $page = "admin.category.subcategories";
    $pagedata = [
      'category' => $category,
      'category_id' => $category->id,
      'categories' => $categories,
      'includepage' => $page,
      "pageTitle" => $this->pageTitle("admincategories"),
    ];
    $data = $this->checkajax(Request::ajax(), $page, $pagedata, Request::url());

    return $data;
  }


  public function add_sub_category($category_id = null, $sub_category = null)
  {
    $this->new_activity("Added subcategories (". $sub_category .") for (" . Categories::where("id", $category_id)->first()->category . ")");

    $sub_category = str_replace(", ", ",", $sub_category);
    $sub_category = str_replace(" ,", ",", $sub_category);

    $sub_category = explode(',', $sub_category);

    foreach ($sub_category as $obj) {
      if (!empty($obj)) {
        $get_category = new CategorySub;
        $get_category->categories_id = $category_id;
        $get_category->categories_sub = $obj;
        $get_category->categories_slug = str_slug($obj, '-');
        $get_category->save();
      }
    }

    $category = Categories::where("id", $category_id)->first();

    Session::flash("success", "Sub Categories saved successfully");

    return Redirect::back();

  }

  public function edit_sub_category($id = null, $sub_category = null)
  {
    

    $get_category = CategorySub::where("id", $id)->first();
    $old_category = $get_category->categories_sub;
    $get_category->categories_sub = $sub_category;
    $get_category->categories_slug = str_slug($sub_category, '-');
    $get_category->save();

    $this->new_activity("Changed subcategory from " . $old_category ." to "  . $sub_category . " for (" . Categories::where("id", $get_category->categories_id)->first()->category . ")");

    $category = Categories::where("id", $get_category->categories_id)->first();

    \Debugbar::error($category);

    Session::flash("success", "Sub category updated successfully");

    return Redirect::back();

  }

  public function delete_sub_category($id = null)
  {

    $attr = CategorySubAttributes::where("categories_sub_id", $id)->get();

    foreach ($attr as $attr_obj) {
      Advertattributes::where("categories_sub_attributes_id", $attr_obj->id)->delete();
    }

    CategorySubAttributes::where("categories_sub_id", $id)->delete();


    $getadvert = Advert::where("categories_sub_id", $id)->get();
    foreach ($getadvert as $obj) {
      Advertmedia::where("advert_id", $obj->id)->delete();

      Favourites::where("advert_id", $obj->id)->delete();
      Message::where("advert_id", $obj->id)->delete();
      Messagethread::where("advert_id", $obj->id)->delete();
      Notification::where("advert_id", $obj->id)->delete();
    }

    

    Advert::where("categories_sub_id", $id)->delete();

    $get_subcategory = CategorySub::where("id", $id)->first();
    $this->new_activity("Deleted Sub Category " . $get_subcategory->categories_sub . "for ". $get_subcategory->categories->category);

    CategorySub::where("id", $id)->delete();

    Session::flash("error", "Sub category deleted successfully");

    return Redirect::back();

  }

  public function viewsubcategoriesattr($sub_category_slug = null){

    $sub_category = CategorySub::where("categories_slug", $sub_category_slug)->first();
    $sub_categories_attributes = CategorySubAttributes::where("categories_sub_id", $sub_category->id)->get();

    $this->new_activity("View Sub Category Attributes for " . $sub_category->categories_sub . " [" . $sub_category->categories->category . "]");


    $page = "admin.category.attributes";
    $pagedata = [
      'sub_category' => $sub_category,
      'sub_categories_attributes' => $sub_categories_attributes,
      'includepage' => $page,
      "pageTitle" => $this->pageTitle("admincategories"),
    ];
    $data = $this->checkajax(Request::ajax(), $page, $pagedata, Request::url());

    return $data;

  }

  public function add_sub_category_attr(){

      $attribute = Request::get("attribute");
      $attributes_data = Request::get("attribute_data");
      $categories_sub_id = Request::get("categories_sub_id");

      $get_subcategory = CategorySub::where("id", $categories_sub_id)->first();
      foreach ($attribute as $key => $value) {
        $cat_sub_attr = new CategorySubAttributes;
        $cat_sub_attr->attributes = $attribute[$key];
        $cat_sub_attr->attributes_data = $attributes_data[$key];
        $cat_sub_attr->categories_sub_id = $categories_sub_id;

        $cat_sub_attr->save();

        $this->new_activity("Added New Sub Category Attribute " . $attribute[$key] . " for " . $get_subcategory->categories_sub . "for " . $get_subcategory->categories->category);
      }

  

    Session::flash("success", "Attributes added successfully");

    return Redirect::back();
  }

  public function edit_sub_category_attr()
  {

    $attribute = Request::get("attribute");
    $attributes_data = Request::get("attribute_data");
    $category_attribute_id = Request::get("category_attribute_id");

    $cat_sub_attr = CategorySubAttributes::where("id", $category_attribute_id)->first();

    $cat_sub_attr->attributes = $attribute;
    $cat_sub_attr->attributes_data = $attributes_data;

    $cat_sub_attr->save();

    $get_subcategory = CategorySub::where("id", $cat_sub_attr->categories_sub_id)->first();
    $this->new_activity("Updated Category Attribute " . $attribute . " for " . $get_subcategory->categories_sub . "for " . $get_subcategory->categories->category);

    Session::flash("success", "Attributes updated successfully");

    return Redirect::back();
  }

  public function delete_sub_category_attr($id = null)
  {

    $this->new_activity("Deleted Sub Category Attribute " . CategorySubAttributes::where("id", $id)->first()->attributes);

    CategorySubAttributes::where("id", $id)->delete();

    Session::flash("error", "Attribute deleted successfully");

    return Redirect::back();

  }

  public function footer_pages(){

    $this->new_activity("Viewed all footer pages");

    $pages = Footer::all();

    $page = "admin.footer.footer_pages";
    $pagedata = [
      'pages' => $pages,
      'includepage' => $page,
      "pageTitle" => $this->pageTitle("adminfooter_pages"),
    ];
    $data = $this->checkajax(Request::ajax(), $page, $pagedata, Request::url());

    return $data;

  }

  public function edit_footer_page($slug){

    $this->new_activity("Tried to edit footer page for " . Request::get('slug'));

    $getpage = Footer::where("slug", $slug)->first();

    $page = "admin.footer.footer_edit";
    $pagedata = [
      'getpage' => $getpage,
      'includepage' => $page,
      "pageTitle" => $this->pageTitle("adminfooter_pages"),
    ];
    $data = $this->checkajax(Request::ajax(), $page, $pagedata, Request::url());

    return $data;
  }

  public function update_footer_page(){
    $getpage =  Footer::where("slug", Request::get('slug'))->first();
    $getpage->content = Request::get('content');
    $getpage->save();

    $this->new_activity("Updated footer page for " . Request::get('slug'));

    Session::flash("success", "Content updated successfully");

    return Redirect::back();

  }

  public function delete_footer_page($slug)
  {

  }


}