<!DOCTYPE html>
<html lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta property="og:title" content="{{$pageTitle}}">
    <meta property="og:description" content="Buy and Sell on Marketam">
    <meta property="og:image" content="{{route('home')}}/images/Marketam2.png">
    <meta property="og:url" content="{{ urlencode(Request::fullUrl()) }}">
    <meta name="twitter:card" content="summary_large_image">


    <title>Marketam</title>

    <!-- Bootstrap CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" />
    <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{route('home')}}/css/AdminLTE/AdminLTE.css">
    <link rel="stylesheet" href="{{route('home')}}/css/AdminLTE/_all-skins.css">
    <link href="{{route('home')}}/css/style.css" rel="stylesheet">

    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>
    <!-- Bootstrap JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

    
    <!-- AdminLTE App -->
    <script src="{{route('home')}}/js/AdminLTE/app.js"></script>
    <script src="{{route('home')}}/js/myscript.js"></script>

    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.min.js"></script>
    <link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.css" />
    <link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials-theme-classic.css" />

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="{{route('home')}}/css/dropzone.css">
    <script src="{{route('home')}}/js/dropzone.js"></script>

    
    <link href="https://vjs.zencdn.net/7.1.0/video-js.css" rel="stylesheet">

    <!-- If you'd like to support IE8 (for Video.js versions prior to v7) -->
    <script src="https://vjs.zencdn.net/ie8/ie8-version/videojs-ie8.min.js"></script>
    <script src="https://vjs.zencdn.net/7.1.0/video.js"></script>


    <!-- Google  AIzaSyC_IG_4s_B3gOMfhcWcFQBAvhNjTv5yqPw-->
    <script>
        function HandleGoogleApiLibrary() {
            // Load "client" & "auth2" libraries
            gapi.load('client:auth2', {
                callback: function () {
                    // Initialize client & auth libraries
                    gapi.client.init({
                        apiKey: 'AIzaSyA-joRCvT2XrF5xKeUfV4Et_H4qdZWBn74',
                        clientId: '148775869211-a473tsdu2im82kpf0lom7q587jf97vma.apps.googleusercontent.com',
                        scope: 'https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/plus.me'
                    }).then(
                        function (success) {
                            // Libraries are initialized successfully
                            // You can now make API calls
                        },
                        function (error) {
                            // Error occurred
                            // console.log(error) to find the reason
                        }
                    );
                },
                onerror: function () {
                    // Failed to load libraries
                }
            });
        }
    </script>
    <script async defer src="https://apis.google.com/js/api.js" onload="this.onload=function(){};HandleGoogleApiLibrary()" onreadystatechange="if (this.readyState === 'complete') this.onload()"></script>
        
        
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>


    


    <style>
        .lds-roller{
            display: none;
        }
    </style>

@if(!empty($ajax_call))
@if(Auth::user() && $ajax_call == "false")
    @if(Auth::user()->role != "user")
        @if(strpos($_SERVER['REQUEST_URI'], '/admin') !== false)
        <script>
            $(function(){
                $(".my-body").addClass("wrapper");
                $("body").addClass("hold-transition");
                $("body").addClass("skin-green");
                $("body").addClass("sidebar-mini");
                $("body").addClass("fixed");
            });
            setTimeout(function() { 
                $(".my-body").show();
                $(".lds-roller").hide();
            }, 2000);
        </script>
        <style>
            footer, .my-body{
                display: none;
            }
            .lds-roller{
                display: inline-block;
            }
        </style>
        @endif
    @else 
        <style>
            .skin-green .wrapper{
                background-color: unset;
            }
        </style>
    @endif
@endif
@endif

@if(empty(Auth::user()))
<style>
    .skin-green .wrapper{
        background-color: unset;
    }
</style>
@endif


</head>

<body>

    <div class="lds-roller">
        <div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div>
    </div>

    
    <div style="text-align:center">
        <div class="progressBarBg">
            <div class="progressBar hundred progressBarGradient">
                <div class="progressBarGradientFill"></div>
            </div>
        </div>
    </div>


    <!-- Menu -->
    
    @include('includes.notification')
    <div class="my-body">

        @if(!empty($ajax_call))
        @if(Auth::user() && $ajax_call == "false")
            @if(Auth::user()->role != "user")
                @if(strpos($_SERVER['REQUEST_URI'], '/admin') !== false)
                    @include('includes.admin.header')
                    @include('includes.admin.sidebar')
                @endif
            @endif
        @endif
        @endif

        @include($includepage)

    </div>

    <div id="dialog-confirm" title="" style="display:none">
        <p class="dialog-body"></p>
    </div>
    

    </div>
    @include('includes.footer')
</body>

<style>
.ui-dialog .ui-dialog-titlebar-close {
    display: none;
}

.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-dialog-buttons.ui-draggable {
    z-index: 2100 !important;
}

.ui-dialog .ui-dialog-titlebar {
    padding: .4em 1em;
    position: relative;
    background: #00a65a;
    color: white;
    font-weight: 600;
}

.ui-dialog-bg-overlay{
    position:fixed;
    top:0; 
    background:#00000098; 
    display:none; 
    width:100%;
    height:100%; 
    z-index:2000;
}
</style>



<script>
    $(document).ready(function () {
        bindPage();
    });

    function bindPage() {
        $("a").on('click', function (event) {
            if ($(this).hasClass("ajaxlink")) {

                event.preventDefault();
                var link = $(this).attr("href");
                localStorage.setItem("linkData", $(this).data("category"));

                $(".progressBarBg").show();
                $(".alert").hide();
                
                if ($(this).hasClass("product-search")) {

                    link = link + "/" + $(".product-search-box").val() + "?location=" + $(".select-state").val();

                    //console.log("link " + link);
                }

                $.ajax({
                    cache: false,
                    type: "GET",
                    url: link,
                    success: function (data) {
                        successdisplay(data);

                    },
                    error: function (data) {
                        $(".progressBarBg").hide();
                        alert("An error occured");
                        console.log('Error:', data);
                    }
                });
            }

            //alert(value );
        }); 

        window.addEventListener('popstate', function (e) {
            var link = e.state;
            $(".progressBarBg").show();
            $.ajax({
                cache: false,
                type: "GET",
                url: link,
                success: function (data) {
                    $(".my-body").html(data.view);
                    $(".progressBarBg").hide();
                    bindPage();

                },
                error: function (data) {
                    $(".progressBarBg").hide();
                    console.log('There was an error. Please try again. Thank you');
                    alert('There was an error. Please try again. Thank you')
                }
            });
        });


        $(".submitForm").validate({
            rules: {
                phone_number: {
                    digits: true
                },
                password: {
                    minlength: 5
                }
            },
            submitHandler: function (form) {

                $(".progressBarBg").show();

                $.ajax({
                    url: $(form).attr('action'),
                    type: $(form).attr('method'),
                    data: $(form).serialize(),
                    success: function (data) {

                        successdisplay(data);


                    },
                    error: function (xhr, err) {
                        $(".progressBarBg").hide();
                        alert('Error in submitting form');
                    }
                });

                return false;

            }
        });

        paginatedLink();

       
        $(".ajaxfavourite").click(function(event){
            event.preventDefault();
            $(".progressBarBg").show();
            $(".alert").hide();

            $currentElement = $(this);

            $favoritelink = $(this).attr("data-link");

            console.log($favoritelink);
    
            fetch($favoritelink)
                .then((resp) => resp.json())
                .then(function (data) {
                    notificationdisplay(data);
                    $(".progressBarBg").hide();
                    if (data.success) {
                        $($currentElement).removeClass("btn-danger");
                        $($currentElement).addClass("btn-success");
                        $($currentElement).text("Added To Favourites");
                        $favoritelink = $favoritelink.replace("/add", "/remove");
                        $($currentElement).attr("data-link",$favoritelink);
                    }
                    if (data.error) {
                        $($currentElement).removeClass("btn-success");
                        $($currentElement).addClass("btn-danger");
                        $($currentElement).text("Add To Favourites");
                        $favoritelink = $favoritelink.replace("/remove", "/add")
                        $($currentElement).attr("data-link",$favoritelink);
                    }
    
                })
                .catch(err => console.error(err));
        });


    }

    function successdisplay(data) {
        $(".my-body").html(data.view);
        $('html, body').animate({
            scrollTop: 0
        }, 'slow');
        history.pushState(data.newlink, null, data.newlink);
        $(".progressBarBg").hide();
        bindPage();

        notificationdisplay(data);
    }

    function notificationdisplay(data) {
        if (data.success) {
            $(".alert").removeClass("alert-danger");
            $(".alert").addClass("alert-success");
            $(".alert-body").html(data.success);
            $(".alert").slideDown("slow");
            console.log("success");
        }
        if (data.error) {
            $(".alert").removeClass("alert-success");
            $(".alert").addClass("alert-danger");
            $(".alert-body").html(data.error);
            $(".alert").slideDown("slow");
            console.log("error");
        }

        setTimeout(function(){ $(".alert").slideUp("slow"); }, 6000);
    }

    function paginatedLink(){
        $page = 2;
        $(".paginateLink").click(function (event) {
            event.preventDefault();

            var link = $(this).attr("href") + "?page=" + $page;
            $(".progressBarBg").show();

            fetch(link)
                .then((resp) => resp.text())
                .then(function (data) {
                    $(".added-adverts").append(data);
                    $(".progressBarBg").hide();
                    $page = $page + 1;
                })
                .catch(err => console.error(err));

        });
    }

    function deleteFunction(title,body,link){
        $(".ui-dialog-bg-overlay").show();
        $(".dialog-body").html(body);
        $( "#dialog-confirm" ).dialog({
            resizable: false,
            height: "auto",
            width: 400,
            modal: true,
            title: title,
            buttons: {
                "Yes": function() {
                    $( this ).dialog( "destroy" );
                    $(".progressBarBg").show();
                    $.ajax({
                        cache: false,
                        type: "GET",
                        url: link,
                        success: function (data) {
                            successdisplay(data);

                        },
                        error: function (data) {
                            $(".progressBarBg").hide();
                            console.log('Error:', data);
                        }
                    });
                },
                Cancel: function() {
                    $( this ).dialog( "destroy" );
                    $(".ui-dialog-bg-overlay").hide();
                }
            }
        });
        $(".ui-widget button").eq(1).addClass("btn btn-danger");
        $(".ui-widget button").eq(2).addClass("btn btn-success");
    }


    //Google Login
    // Called when Google Javascript API Javascript is loaded
    function googleLogin() {
        $(".progressBarBg").show();
        // API call for Google login
        gapi.auth2.getAuthInstance().signIn().then(
            function (success) {
                userinfo();
            },
            function (error) {
                $(".progressBarBg").hide();
                alert('Error in authenticating :(');
                // Error occurred
                // console.log(error) to find the reason
            }
        );
    }

    function userinfo() {
        // API call to get user profile information
        gapi.client.request({
            path: 'https://www.googleapis.com/plus/v1/people/me'
        }).then(
            function (success) {
                // API call is successful

                var user_info = JSON.parse(success.body);

                var user_data = {
                    'id': user_info.id,
                    'name': user_info.displayName,
                    'image': user_info.image.url,
                    'email': user_info.emails[0].value,
                    'type': "google",
                    '_token': "{{ csrf_token() }}"
                };



                $.ajax({
                    cache: false,
                    url: "{{route('socialmediaregister')}}",
                    type: "POST",
                    data: user_data,
                    success: function (data) {
                        successdisplay(data);
                    },
                    error: function (xhr, err) {
                        alert('Error in submitting form');
                        $(".progressBarBg").hide();
                    }
                });

            },
            function (error) {
                $(".progressBarBg").hide();
                alert('Error in submitting form');
                // Error occurred
                // console.log(error) to find the reason
            }
        );
    }

    //facebook login
    window.fbAsyncInit = function () {
        FB.init({
            appId: '2146897598881464',
            cookie: true,
            xfbml: true,
            version: 'v3.1'
        });

        FB.AppEvents.logPageView();

    };

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    function fbLogin() {
        $(".progressBarBg").show();
        FB.login(function (response) {
            if (response.authResponse) {
                getUserData();
            }
        }, {
            scope: 'email,public_profile',
            return_scopes: true
        });

        //check user session and refresh it
        // FB.getLoginStatus(function(response) {
        //   if (response.status === 'connected') {
        //     //user is authorized
        //     getUserData();
        //   } else {
        //     //user is not authorized
        //   }
        // }); 

    }

    function getUserData() {
        FB.api('/me?fields=id,name,email,link,gender,picture',
            function (response) {
                console.log(response);
                var user_data = {
                    'id': response.id,
                    'name': response.name,
                    'image': response.picture.data.url,
                    'email': response.email,
                    'type': "facebook",
                    '_token': "{{ csrf_token() }}"
                };



                $.ajax({
                    cache: false,
                    url: "{{route('socialmediaregister')}}",
                    type: "POST",
                    data: user_data,
                    success: function (data) {
                        successdisplay(data);
                    },
                    error: function (xhr, err) {
                        $(".progressBarBg").hide();
                        alert('Error in authenticating');
                    }
                });
            });
    }
    //facebook login

    
</script>

</html>