<div class="menu">
@include('includes.staticmenuwithbg')
</div>

<div class="container">

  <div class="body">

    <div class="row">
      <div class="col-md-3"></div>
      <div class="col-md-9">
          <h2 class="message-text">Messages</h2>
      </div>
    </div>

    <div class="row">

        <div class="col-md-3">
            <a href="{{route('userprofile')}}" class="ajaxlink">
              <div class="profile">
                  @if(empty($user->image))
                  <div class="display-picture" style="background-image: url({{route('home')}}/images/user.png)">
                  </div>
                  @else 
                  <div class="display-picture" style="background-image: url({{$user->image}})">
                  </div>
                  @endif
                  <h4 class="user-name">{{$user->full_name}}</h4>
              </div>
            </a>
        
        </div>
        <div class="col-md-9">

            <div class="message-display">

              @if(count($messages) == 0)
                <h4>You have no messages</h4>
              @endif

              
              @foreach($messages as $obj)

                @if($obj->readmessage == 0 && Auth::user()->id == $obj->active_recipient)
                  <style>
                      .message-box{
                        background-color: #ddfbe6 !important;
                      }
                  </style>
                @endif
                <a class="message-box ajaxlink" href="{{route('viewmessage',[$obj->advert_id,$obj->id])}}">
                  <div class="row">
                    <div class="col-md-3">
                        @if($obj->advert->user->id == Auth::user()->id)
                        <img src="{{$obj->user->image}}" width="60px"/> <h5 class="message-receiver">{{$obj->user->full_name}}</h5>
                        @else
                        <img src="{{$obj->advert->user->image}}" width="60px"/> <h5 class="message-receiver">{{$obj->advert->user->full_name}}</h5>
                        @endif
                    </div>
                    <div class="col-md-7">
                        <h4 class="message-header">{{$obj->advert->title}}</h4>
                        <div class="message-content">{{$obj->messagethread->sortByDesc("created_at")->first()->messages}}</div>
                    </div>
                    <div class="col-md-2">
                        <div class="message-date-time">{{date_format(new DateTime($obj->messagethread->sortByDesc("created_at")->first()->created_at), "d/m/Y h:i A")}}</div>
                    </div>
                  </div>
                </a>
              @endforeach
              
              
            </div>
        
        </div>

    </div>


    <div style="height:80px"></div>

  </div>

</div>

<script>

   
</script>

<style>

.active-link{
  color:green;
  font-weight:600;
}

.user-name{
  color:#1fae37;
  margin-left: 20px;
  font-weight: 600;
}

.message-text{
  margin-top:25px;
  margin-bottom:25px;
}

.profile, .message-box{
  background-color: #fff;
  padding:30px;
  border: solid 1px #8080804f;
  margin-bottom:30px;
  box-shadow: -3px 7px 8px 0px rgba(0, 0, 0, 0.06);
}

.message-box{
  margin-bottom:15px;
  display: block;
  color:inherit;
}

.display-picture{
  background-size: cover;
  width:200px;
  height:200px;
  border: solid 1px #8080804f;    
  border-radius: 20px;
  position: relative;
}

.message-content {
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  max-height: 250px;
}

.message-receiver {
    display: inline-block;
    font-size: 16px;
    color: #19af34;
}

a:active, a:hover{
    color:inherit;
}

</style>

<script>
document.title = "{{$pageTitle}}";
</script>