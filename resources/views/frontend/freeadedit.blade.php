<div class="menu">
  @include('includes.staticmenuwithbg')
</div>
@include('includes.css.styleone')


<div class="container">

  <div class="row body">

    <div>
      <h2 class="header-text">Edit Advert</h2>
    </div>

    <div class="col-md-2"></div>
    <div class="col-md-8">
      <div class="advert-form">
        <form action="{{route('postfreeadedit')}}" method="POST" role="form" class="adform" id="adform"  enctype="multipart/form-data">
          {{csrf_field()}}
          <div class="form-group">
            <label for="">Select Category</label>
            <select name="categories" id="categories" class="form-control" required="required">
              @foreach($categories as $category_obj)
              <optgroup label="{{$category_obj->category}}">
                @foreach($category_obj->categories_subs as $sub_obj)
                <option value="{{$sub_obj->id}}">{{$sub_obj->categories_sub}}</option>
                @endforeach
              </optgroup>
              @endforeach
            </select>
          </div>

          <?php $get_category = $advert->category_sub->id; ?>


          <div class="form-group">
            <label for="">Title</label>
            <input type="text" class="form-control" id="" placeholder="" name="title" value="{{$advert->title}}" required>
          </div>

          <div class="form-group">
            <label for="">Description</label>
            <textarea name="description" id="input" class="form-control" rows="3" required="required" name="description">{{$advert->description}}</textarea>
          </div>

          <input type="hidden" class="form-control" id="" placeholder="" name="advert_id" value="{{$advert->id}}">

          <div class="insertdata">
          @foreach($advert_attr as $obj)
          <div class="form-group">
            <label for="">{{$obj->categories_sub_attribute->attributes}}</label>
            @if($obj->categories_sub_attribute->attributes_data == "input")
                <input type="text" name="{{str_replace(' ','_',$obj->categories_sub_attribute->attributes)}}" class="form-control" id="" placeholder="" value="{{$obj->attribute_data}}"/>
            @else 
                <?php 
                    // $subcat = str_replace(" ","",$obj->categories_sub_attribute->attributes_data);
                    $subcat = $obj->categories_sub_attribute->attributes_data;
                    $subcat = explode(',', $subcat);
                ?>
                <select name="{{str_replace(' ','_',$obj->categories_sub_attribute->attributes)}}" id="input" class="form-control {{str_replace(' ','_',$obj->categories_sub_attribute->attributes)}}">
                    @foreach ($subcat as $cat)
                    <option value="{{$cat}}">{{$cat}}</option>
                    @endforeach
                </select>
            @endif
            
          </div>
          @endforeach
          </div>

          <div class="form-group">
            <label class="control-label">Price</label>
            <div class="input-group">
              <input type="number" class="form-control" aria-describedby="price-icon" name="price" step=".01" value="{{$advert->price}}" required>
              <span class="input-group-addon" id="price-icon">Naira</span>
            </div>
          </div>

          <div class="form-group">
            <label for="">Price Type</label>
            <select name="price_type" id="input" class="form-control" required="required">
              @if($advert->price_type == "Fixed Price")
              <option value="Fixed Price">Fixed Price</option>
              <option value="Negotiable">Negotiable</option>
              @else
              <option value="Negotiable">Negotiable</option>
              <option value="Fixed Price">Fixed Price</option>
              @endif
            </select>
          </div>

          <div class="form-group">
            <label for="">Usage</label>
            <select name="usage" id="input" class="form-control" required="required">
              @if($advert->usage == "New")
              <option value="New">New</option>
              <option value="Fairly Used">Fairly Used</option>
              @else
              <option value="Fairly Used">Fairly Used</option>
              <option value="New">New</option>
              @endif
            </select>
          </div>

          <div class="form-group">
            <label for="">Location</label>
            <!-- <input type="text" name="location" class="form-control" value="{{$advert->location}}" required="required"> -->
            
            <select name="location" id="location" class="form-control" required="required">
              <option value="{{$advert->location}}">{{$advert->location}}</option>
              <option value="Abuja FCT">Abuja FCT</option>
              <option value="Abia">Abia</option>
              <option value="Adamawa">Adamawa</option>
              <option value="Akwa Ibom">Akwa Ibom</option>
              <option value="Anambra">Anambra</option>
              <option value="Bauchi">Bauchi</option>
              <option value="Bayelsa">Bayelsa</option>
              <option value="Benue">Benue</option>
              <option value="Borno">Borno</option>
              <option value="Cross River">Cross River</option>
              <option value="Delta">Delta</option>
              <option value="Ebonyi">Ebonyi</option>
              <option value="Edo">Edo</option>
              <option value="Ekiti">Ekiti</option>
              <option value="Enugu">Enugu</option>
              <option value="Gombe">Gombe</option>
              <option value="Imo">Imo</option>
              <option value="Jigawa">Jigawa</option>
              <option value="Kaduna">Kaduna</option>
              <option value="Kano">Kano</option>
              <option value="Katsina">Katsina</option>
              <option value="Kebbi">Kebbi</option>
              <option value="Kogi">Kogi</option>
              <option value="Kwara">Kwara</option>
              <option value="Lagos" selected>Lagos</option>
              <option value="Nassarawa">Nassarawa</option>
              <option value="Niger">Niger</option>
              <option value="Ogun">Ogun</option>
              <option value="Ondo">Ondo</option>
              <option value="Osun">Osun</option>
              <option value="Oyo">Oyo</option>
              <option value="Plateau">Plateau</option>
              <option value="Rivers">Rivers</option>
              <option value="Sokoto">Sokoto</option>
              <option value="Taraba">Taraba</option>
              <option value="Yobe">Yobe</option>
              <option value="Zamfara">Zamfara</option>
            </select>
          </div>

          <label class="control-label">Image (Maximum of 1 image, Image size must be less than 4MB)</label>
          <div class="dropzone-previews">
            <div class="dropzone-label">Drop files here to upload</div>
          </div>

          <div style="color:red;margin-top:20px">***Note: Your advert will be checked by our operatives before going
            live</div>
          <div>By clicking the submit button you accept our terms and conditions</div>
          <div style="text-align:center;margin-top:10px;margin-bottom:30px">
            <button type="submit" class="btn btn-success btn-lg">Update</button>
          </div>
        </form>
      </div>
    </div>
    <div class="col-md-2"></div>

  </div>

</div>

<style>
  .header-text {
    margin: 10px 0px 20px 0px;
    text-align: center;
    color: #5cb85c;
    font-weight: 500;
  }

  .body {
    padding-bottom: 100px;
  }

  .advert-form {
    background: white;
    padding: 25px;
    border: solid 1px #b3b3b38f;
    border-radius: 8px;
    margin-bottom: 30px;
    box-shadow: -3px 7px 8px 0px rgba(0, 0, 0, 0.06);
  }

  .action-buttons {
    margin-top: 20px;
  }
</style>


<script>
  document.title = "{{$pageTitle}}";

  imageFromServer = {!! $fileObject !!};

  imageCount = 0;

  checkImageFromServer = "false";

  if({!! $advert_attr !!}){

    ad_categories = {!! $advert_attr !!};

    $.each(ad_categories, function (key, value) {
      if(value.categories_sub_attribute.attributes_data != "input"){

        trimvalue = value.categories_sub_attribute.attributes.replace(new RegExp(' ', 'g'), '_');
        $("."+ trimvalue).val(value.attribute_data);
      }
      
    });
  }


  $(".adform").dropzone({
    paramName: "imageupload",
    autoDiscover: false,
    autoProcessQueue: false,
    maxFiles: 1,
    maxFilesize: 4,
    thumbnailWidth: 150,
    thumbnailHeight: 150,
    uploadMultiple: false,
    addRemoveLinks: true,
    acceptedFiles: 'image/*',
    previewsContainer: ".dropzone-previews",
    clickable: ".dropzone-previews",
    hiddenInputContainer: ".dropzone-previews",

    init: function () {

      var adformDropzone = this;
      $(".adform").submit(function (event) {
        event.preventDefault();

        if (adformDropzone.files.length != 0) {
          $(".progressBarBg").show();
          adformDropzone.processQueue();
        } else {

          if(checkImageFromServer == "true"){
            $(".progressBarBg").show();

            fetch($(".adform").attr('action'), {
              method: $(".adform").attr('method'),
              body: new FormData($(".adform")[0])
            })
            .then((data) => data.json())
            .then(function (data) {

              
              $.ajax({
                  cache: false,
                  type: "GET",
                  url: data.link,
                  success: function (data) {
                      successdisplay(data);

                  },
                  error: function (data) {
                      $(".progressBarBg").hide();
                      alert("An error occured");
                      console.log('Error:', data);
                  }
              });
            })
            .catch(function (err) {
              $(".progressBarBg").hide();
              alert('Error');
            });
          }else{
            data = {
              error: "Please attach an image to your form"
            }
            notificationdisplay(data);
          }
          
        }


      });

      if(imageFromServer != "null"){
        $.each(imageFromServer, function (key, value) {

            if(imageCount != 1){
              var file = {name: value.original, size: value.size};
              
              adformDropzone.options.addedfile.call(adformDropzone, file);
              adformDropzone.options.thumbnail.call(adformDropzone, file, value.server);
              adformDropzone.emit("complete", file);

              checkImageFromServer = "true";

              $(".dropzone-label").hide();
              $(".dropzone-previews").css({
                "height": "initial"
              });
              $(".dz-remove").addClass("btn btn-xs btn-danger");

              imageCount++;

            }

            
        });
      };


      this.on("addedfile", function (file) {

        if (imageCount == 1) {
            dropzoneNotification(this, file, " - only one image is allowed and should have a maximum size of 4MB");
        }

        $(".dropzone-label").hide();
        $(".dropzone-previews").css({
          "height": "initial"
        });
        $(".dz-remove").addClass("btn btn-xs btn-danger");


      });

      this.on("removedfile", function (file) {
        
        var u_data = {'id': file.name,'_token': "{{ csrf_token() }}"};

        $.ajax({
                cache: false,
                url: "{{route('delete_advert_media')}}",
                type: 'POST',
                data: u_data,
                success: function(data){
                    if(data == "success")
                    {
                        console.log("file removed from server successfully");

                        imageCount--;
                        checkImageFromServer = "false";
                    }

                }
            });

      });

      this.on("error", function (file) {
        this.removeFile(file);
        dropzoneNotification(this, file, " - only one image is allowed and should have a maximum size of 4MB");
      });

      this.on("reset", function (file) {
        $(".dropzone-label").show();
        $(".dropzone-previews").css({
          "height": "200px"
        });
      });

      this.on("sending", function (file, xhr, formData) {
        formData = $(".adform").serialize();
      });

      

      this.on("uploadprogress", function (file, progress) {
        var progressElement = file.previewElement.querySelector("[data-dz-uploadprogress]");
        progressElement.style.width = progress + "%";
        progressElement.textContent = progress + "%";
      });

      this.on("success", function (file, data) {
        // $(".progressBarBg").hide();
        // notificationdisplay(data);
        
        $.ajax({
            cache: false,
            type: "GET",
            url: data.link,
            success: function (data) {
                successdisplay(data);

            },
            error: function (data) {
                $(".progressBarBg").hide();
                alert("An error occured");
                console.log('Error:', data);
            }
        });
    
      });
    }
  });


  function dropzoneNotification(objThis, objFile, message) {

    objThis.removeFile(objFile);
    data = {
      error: objFile.name + message
    };
    notificationdisplay(data);
  }


  
  $(function () {
    $("#categories").select2().val('{!! $get_category !!}').trigger('change');

    $("#categories").change(function () {
      fetch("{{route('ad_cat_select')}}/" + $("#categories").val())
        .then((resp) => resp.json())
        .then(function (data) {
          $(".insertdata").html('');
          $.each(data, function (index, value) {
            var json_attr = [];
            var attr_data = value.attributes_data;

            if (attr_data != "input") {
              attr_data = attr_data.replace(", ", ",")
              var attr_dataSplit = attr_data.split(",");
              for (var i = 0; i < attr_dataSplit.length; i++) {
                json_attr.push({
                  "attr": attr_dataSplit[i]
                });
              }

              selectinput = '';
              $.each(json_attr, function (index2, value2) {
                value2_data = value2.attr;
                value2_data = value2_data.replace(", ", ",")
                selectinput = selectinput + '<option value="' + value2_data + '">' + value2_data +
                  '</option>'
              });

              inputtype = '<select name="' + value.attributes +
                '" id="input" class="form-control">' +
                selectinput + '</select>';

            } else {
              inputtype = '<input type="text" name="' + value.attributes +
                '" class="form-control" id="" placeholder="">';
            }


            appendData = '<div class="form-group">' +
              '<label for="">' + value.attributes + '</label>' + inputtype + '</div>';

            $(".insertdata").append(appendData);
          });
        })
        .catch(err => console.error(err));
    });


    function notificationdisplay(data) {
      if (data.success) {
        $(".alert").removeClass("alert-danger");
        $(".alert").addClass("alert-success");
        $(".alert-body").html(data.success);
        $(".alert").slideDown("slow");
        console.log("success");
      }
      if (data.error) {
        $(".alert").removeClass("alert-success");
        $(".alert").addClass("alert-danger");
        $(".alert-body").html(data.error);
        $(".alert").slideDown("slow");
        console.log("error");
      }
    }

  })
</script>