<div class="menu">
  @include('includes.staticmenuwithbg')
</div>
@include('includes.css.styleone')
@include('includes.css.ads')

<div class="container">

  <div class="row body">

    <div>
      <h2 class="header-text">{{$myadvert_title}}</h2>
    </div>



    <div class="" style="margin-bottom:20px">
      <a href="{{route('myadverts')}}" class="btn btn-info ajaxlink">All Adverts</a>
      <a href="{{route('myadverts','deactivated')}}" class="btn btn-danger ajaxlink">Deactivated Adverts</a>
      <a href="{{route('myadverts','sold')}}" class="btn btn-success ajaxlink">Sold Adverts</a>
    </div>

    @if(count($adverts) != 0)

      @foreach($adverts as $obj)
      <div class="ads-container">
        <div class="row">
          <div class="col-md-3">
            <div class="ad-image-container">
              <a href="{{route('adpage',[$obj->slug])}}" class="ajaxlink">
                <div class="ad-image">
                  @if($obj->advert_media->where('type','image')->first() != null)
                  <img src="{{route('home')}}/{{$obj->advert_media->where('type','image')->first()->link}}" alt="">
                  @else 
                  <img src="{{route('home')}}/images/default.png" alt="">
                  @endif
                </div>
              </a>
            </div>
          </div>
          <div class="col-md-9">
            <div class="row">
              <div class="col-md-8"><a href="{{route('adpage',[$obj->slug])}}" class="ajaxlink">
                  <h3 class="product-title">{{$obj->title}} 
                  @if($obj->advert_type == "free")
                  <span class="label label-info">{{ucwords($obj->advert_type)}}</span>
                  @else 
                  <span class="label label-success">{{ucwords($obj->advert_type)}}</span>
                  @endif
                  </h3>
                </a></div>
              <div class="col-md-4">
                <h3 class="product-price">₦{{number_format($obj->price, 2)}}</h3>
              </div>
            </div>

            <div class="product-description">
              <p>{{$obj->description}}</p>
            </div>

            <div class="product-location">
              <i class="fa fa-map-marker"></i> {{$obj->location}}
            </div>
            <div class="product-subcategory">
              <i class="fa fa-asterisk"></i> {{$obj->category_sub->categories_sub}}
            </div>
            <div class="product-timestamp">
              <i class="fa fa-clock-o"></i> {{date_format(new DateTime($obj->created_at), "M-d h:i A")}}
            </div>



            <div class="action-buttons">
              @if($obj->active == "active")
              <button data-link="{{route('myadvertsaction',['sold',$obj->id])}}" class="btn btn-sm btn-success ajax_ad_action">Sold</button>

              <button data-link="{{route('myadvertsaction',['deactivated',$obj->id])}}" class="btn btn-sm btn-info ajax_ad_action">Deactivate</button> 
              @endif

              @if($obj->active == "deactivated")
                <button data-link="{{route('myadvertsaction',['active',$obj->id])}}" class="btn btn-sm btn-success ajax_ad_action">Activate</button> 
              @endif
              
              <a href="{{route('editadvert',$obj->slug)}}" class="btn btn-sm btn-warning ajaxlink">Edit</a> 
              
              <button data-link="{{route('myadvertsaction',['deleted',$obj->id])}}" class="btn btn-sm btn-danger ajax_ad_action">Delete</button>
            </div>
          </div>
        </div>
      </div>
      @endforeach

      <div class="added-adverts"></div>

      <div style="margin-bottom:60px">
        <div class="" style="margin-top:30px;text-align:center">
          <!-- {{ $adverts->links() }} -->
        
          <a href="{{$moreurl}}" class="paginateLink btn btn-success">VIEW MORE</a>
    
        </div>
      </div>
    @else 
      <p>You currently have no adverts in this section</p>
    @endif

  </div>

</div>

<style>
  .header-text {
    margin: 20px 0px 40px 0px;
    text-align: center;
    color: green;
    font-weight: 600;
  }

  .body {
    padding-bottom: 100px;
    padding: 20px;
  }

  .ads-container {
    padding: 20px;
  }

  .action-buttons {
    margin-top: 20px;
  }
</style>


<script>
  document.title = "{{$pageTitle}}";

  $(function(){

$(".ajax_ad_action").click(function(event){
        event.preventDefault();
        $(".progressBarBg").show();
        $(".alert").hide();

        $currentElement = $(this);

      
        $link = $(this).attr("data-link");

        fetch($link)
            .then((resp) => resp.json())
            .then(function (data) {
                
                $(".progressBarBg").hide();
                $($currentElement).parent().parent().parent().parent().slideUp("slow");
                
                if(data.error == "deactivated"){
                  data.error = "Advert Deactivated Successfully";
                }else{
                  data.error = "Advert Deleted Successfully";

                  $(".menu-advert .badge").text(data.count);
                }

                if(data.success == "sold"){
                  data.success = "Advert Sold Successfully";
                }

                notificationdisplay(data);


            })
            .catch(err => console.error(err));
    });


});


</script>