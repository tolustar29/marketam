<div class="menu">
  @include('includes.staticmenuwithbg')
</div>
@include('includes.css.styleone')
@include('includes.css.ads')

<div class="container">

  <div class="row body">

    <div>
      <h2 class="header-text">My Favourites</h2>
    </div>

    @if(count($favourites) != 0)

    @foreach($favourites as $key)
      <?php $obj = $key->advert; ?>
      <div class="ads-container">
        <div class="row">
          <div class="col-md-3">
            <div class="ad-image-container">
              <a href="{{route('adpage',[$obj->slug])}}" class="ajaxlink">
                <div class="ad-image">
                  <img src="{{route('home')}}/{{$obj->advert_media->where('type','image')->first()->link}}" alt="">
                </div>
              </a>
            </div>
          </div>
          <div class="col-md-9">
            <div class="row">
              <div class="col-md-8"><a href="{{route('adpage',[$obj->slug])}}" class="ajaxlink">
                  <h3 class="product-title">{{$obj->title}}</h3>
                </a></div>
              <div class="col-md-4">
                <h3 class="product-price">₦{{number_format($obj->price, 2)}}</h3>
              </div>
            </div>

            <div class="product-description">
              <p>{{$obj->description}}</p>
            </div>

            <div class="product-location">
              <i class="fa fa-map-marker"></i> {{$obj->location}}
            </div>
            <div class="product-subcategory">
              <i class="fa fa-asterisk"></i> {{$obj->category_sub->categories_sub}}
            </div>
            <div class="product-timestamp">
              <i class="fa fa-clock-o"></i> {{date_format(new DateTime($obj->created_at), "M-d h:i A")}}
            </div>


            <div class="product-favourite">
              @if(!empty($obj->favourite()->where("user_id","$user_id")->first()))
              
              <button data-link="{{route('favourite',['remove',$obj->id])}}" class="btn btn-danger btn-sm ajaxfavouritepage"><i class="fa fa-heart"></i> Remove From Favourites</button>
              @endif
            </div>
          </div>
        </div>
      </div>
    @endforeach

    @else 
      <p style="margin-bottom:80px; text-align: center">You currently have no favourites</p>
    @endif
  </div>

</div>

<style>
  .header-text {
    margin: 20px 0px 40px 0px;
    text-align: center;
    color: green;
    font-weight: 600;
  }

  .body {
    padding-bottom: 100px;
    padding: 20px;
  }

  .ads-container {
    padding: 20px;
  }
</style>


<script>
  document.title = "{{$pageTitle}}";

  $(function(){

    $(".ajaxfavouritepage").click(function(event){
            event.preventDefault();
            $(".progressBarBg").show();
            $(".alert").hide();

            $currentElement = $(this);

            $favoritelink = $(this).attr("data-link");

            console.log($favoritelink);
    
            fetch($favoritelink)
                .then((resp) => resp.json())
                .then(function (data) {
                    notificationdisplay(data);
                    $(".progressBarBg").hide();
                    if (data.error) {
                        $($currentElement).parent().parent().parent().parent().slideUp("slow");
                    }
    
                })
                .catch(err => console.error(err));
        });


  });


</script>