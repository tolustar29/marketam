<div class="menu">
  @include('includes.staticmenuwithbg')
</div>
@include('includes.css.styleone')


<div class="container">

  <div class="row body">

    <div>
      <h2 class="header-text">Post Free Advert</h2>
    </div>

    <div class="col-md-2"></div>
    <div class="col-md-8">
      <div class="advert-form">
        <form action="{{route('postfreead')}}" method="POST" role="form" class="adform" id="adform" enctype="multipart/form-data">
          {{csrf_field()}}
          <div class="form-group">
            <label for="">Select Category</label>
            <select name="categories" id="categories" class="form-control" required="required">
              @foreach($categories as $category_obj)
              <optgroup label="{{$category_obj->category}}">
                @foreach($category_obj->categories_subs as $sub_obj)
                <option value="{{$sub_obj->id}}">{{$sub_obj->categories_sub}}</option>
                @endforeach
              </optgroup>
              @endforeach
            </select>
          </div>


          <div class="form-group">
            <label for="">Title</label>
            <input type="text" class="form-control" id="" placeholder="" name="title" required>
          </div>

          <div class="form-group">
            <label for="">Description</label>
            <textarea name="description" id="input" class="form-control" rows="3" required="required" name="description"></textarea>
          </div>

          <div class="insertdata">

          </div>

          <div class="form-group">
            <label class="control-label">Price</label>
            <div class="input-group">
              <input type="number" class="form-control" aria-describedby="price-icon" name="price" step=".01" required>
              <span class="input-group-addon" id="price-icon">Naira</span>
            </div>
          </div>

          <div class="form-group">
            <label for="">Price Type</label>
            <select name="price_type" id="input" class="form-control" required="required">
              <option value="Fixed Price">Fixed Price</option>
              <option value="Negotiable">Negotiable</option>
            </select>
          </div>

          <div class="form-group">
            <label for="">Usage</label>
            <select name="usage" id="input" class="form-control" required="required">
              <option value="New">New</option>
              <option value="Fairly Used">Fairly Used</option>
            </select>
          </div>

          <div class="form-group">
            <label for="">Location</label>
            <!-- <input type="text" name="location" class="form-control" value="" required="required"> -->
            <select name="location" id="location" class="form-control" required="required">
              <option value="Abuja FCT">Abuja FCT</option>
              <option value="Abia">Abia</option>
              <option value="Adamawa">Adamawa</option>
              <option value="Akwa Ibom">Akwa Ibom</option>
              <option value="Anambra">Anambra</option>
              <option value="Bauchi">Bauchi</option>
              <option value="Bayelsa">Bayelsa</option>
              <option value="Benue">Benue</option>
              <option value="Borno">Borno</option>
              <option value="Cross River">Cross River</option>
              <option value="Delta">Delta</option>
              <option value="Ebonyi">Ebonyi</option>
              <option value="Edo">Edo</option>
              <option value="Ekiti">Ekiti</option>
              <option value="Enugu">Enugu</option>
              <option value="Gombe">Gombe</option>
              <option value="Imo">Imo</option>
              <option value="Jigawa">Jigawa</option>
              <option value="Kaduna">Kaduna</option>
              <option value="Kano">Kano</option>
              <option value="Katsina">Katsina</option>
              <option value="Kebbi">Kebbi</option>
              <option value="Kogi">Kogi</option>
              <option value="Kwara">Kwara</option>
              <option value="Lagos" selected>Lagos</option>
              <option value="Nassarawa">Nassarawa</option>
              <option value="Niger">Niger</option>
              <option value="Ogun">Ogun</option>
              <option value="Ondo">Ondo</option>
              <option value="Osun">Osun</option>
              <option value="Oyo">Oyo</option>
              <option value="Plateau">Plateau</option>
              <option value="Rivers">Rivers</option>
              <option value="Sokoto">Sokoto</option>
              <option value="Taraba">Taraba</option>
              <option value="Yobe">Yobe</option>
              <option value="Zamfara">Zamfara</option>
            </select>
          </div>

          <label class="control-label">Image (Maximum of 1 image, Image size must be less than 4MB)</label>
          <div class="dropzone-previews">
            <div class="dropzone-label">Drop files here to upload</div>
          </div>

          <div style="color:red;margin-top:20px">***Note: Your advert will be checked by our operatives before going
            live</div>
          <div>By clicking the submit button you accept our terms and conditions</div>
          <div style="text-align:center;margin-top:10px;margin-bottom:30px">
            <button type="submit" class="btn btn-success btn-lg">Submit</button>
          </div>
        </form>
      </div>
    </div>
    <div class="col-md-2"></div>


  </div>

</div>

<style>
  .header-text {
    margin: 10px 0px 20px 0px;
    text-align: center;
    color: #5cb85c;
    font-weight: 500;
  }

  .body {
    padding-bottom: 100px;
  }

  .advert-form {
    background: white;
    padding: 25px;
    border: solid 1px #b3b3b38f;
    border-radius: 8px;
    margin-bottom: 30px;
    box-shadow: -3px 7px 8px 0px rgba(0, 0, 0, 0.06);
  }

  .action-buttons {
    margin-top: 20px;
  }
</style>


<script>
  document.title = "{{$pageTitle}}";


  $(".adform").dropzone({
    paramName: "imageupload",
    autoDiscover: false,
    autoProcessQueue: false,
    maxFiles: 1,
    maxFilesize: 4,
    thumbnailWidth: 150,
    thumbnailHeight: 150,
    uploadMultiple: false,
    addRemoveLinks: true,
    acceptedFiles: 'image/*',
    previewsContainer: ".dropzone-previews",
    clickable: ".dropzone-previews",
    hiddenInputContainer: ".dropzone-previews",

    init: function () {

      var adformDropzone = this;
      $(".adform").submit(function (event) {
        event.preventDefault();

        if (adformDropzone.files.length != 0) {
          $(".progressBarBg").show();
          adformDropzone.processQueue();
        } else {
          data = {
            error: "Please attach an image to your form"
          }
          notificationdisplay(data);
        }


      });

      this.on("addedfile", function (file) {
        $(".dropzone-label").hide();
        $(".dropzone-previews").css({
          "height": "initial"
        });
        $(".dz-remove").addClass("btn btn-xs btn-danger");
      });

      this.on("error", function (file) {
        this.removeFile(file);
        //dropzoneNotification(this, file, " - only one image is allowed and should have a maximum size of 4MB");
        dropzoneNotification(this, file, " - An error occured");
        $(".progressBarBg").hide();
      });

      this.on("reset", function (file) {
        $(".dropzone-label").show();
        $(".dropzone-previews").css({
          "height": "200px"
        });
      });

      

      this.on("uploadprogress", function (file, progress) {
        var progressElement = file.previewElement.querySelector("[data-dz-uploadprogress]");
        progressElement.style.width = progress + "%";
        progressElement.textContent = progress + "%";
      });

      this.on("sending", function (file, xhr, formData) {
        formData = $(".adform").serialize();
      });

      this.on("success", function (file, data) {
        // $(".progressBarBg").hide();
        // notificationdisplay(data);

        $.ajax({
            cache: false,
            type: "GET",
            url: data.link,
            success: function (data) {
                successdisplay(data);

            },
            error: function (data) {
                $(".progressBarBg").hide();
                alert("An error occured");
                console.log('Error:', data);
            }
        });
        
      });
    }
  });


  function dropzoneNotification(objThis, objFile, message) {

    objThis.removeFile(objFile);
    data = {
      error: objFile.name + message
    };
    notificationdisplay(data);
  }



  $(function () {

    $("#categories").select2().val('').trigger('change');

    $("#categories").change(function () {
      fetch("{{route('ad_cat_select')}}/" + $("#categories").val())
        .then((resp) => resp.json())
        .then(function (data) {
          console.log(data);
          $(".insertdata").html('');
          $.each(data, function (index, value) {
            var json_attr = [];
            var attr_data = value.attributes_data;

            if (attr_data != "input") {
              attr_data = attr_data.replace(", ", ",")
              var attr_dataSplit = attr_data.split(",");
              for (var i = 0; i < attr_dataSplit.length; i++) {
                json_attr.push({
                  "attr": attr_dataSplit[i]
                });
              }

              selectinput = '';
              $.each(json_attr, function (index2, value2) {
                value2_data = value2.attr;
                value2_data = value2_data.replace(", ", ",")
                selectinput = selectinput + '<option value="' + value2_data + '">' + value2_data +
                  '</option>'
              });

              inputtype = '<select name="' + value.attributes +
                '" id="input" class="form-control">' +
                selectinput + '</select>';

            } else {
              inputtype = '<input type="text" name="' + value.attributes +
                '" class="form-control" id="" placeholder="">';
            }


            appendData = '<div class="form-group">' +
              '<label for="">' + value.attributes + '</label>' + inputtype + '</div>';

            $(".insertdata").append(appendData);
          });
        })
        .catch(err => console.error(err));
    });

    // $(".adform").submit(function (event) {

    //   $(".progressBarBg").show();
    //   event.preventDefault();

    //   fetch($(".adform").attr('action'), {
    //       method: $(".adform").attr('method'),
    //       body: new FormData($(".adform")[0])
    //     })
    //     .then((data) => data.json())
    //     .then(function (data) {
    //       $(".progressBarBg").hide();
    //       notificationdisplay(data);
    //     })
    //     .catch(function (err) {
    //       // console.error(err);
    //       $(".progressBarBg").hide();
    //       alert('Error in submitting form');
    //     });
    // });

    function notificationdisplay(data) {
      if (data.success) {
        $(".alert").removeClass("alert-danger");
        $(".alert").addClass("alert-success");
        $(".alert-body").html(data.success);
        $(".alert").slideDown("slow");
        console.log("success");
      }
      if (data.error) {
        $(".alert").removeClass("alert-success");
        $(".alert").addClass("alert-danger");
        $(".alert-body").html(data.error);
        $(".alert").slideDown("slow");
        console.log("error");
      }
    }

  })
</script>