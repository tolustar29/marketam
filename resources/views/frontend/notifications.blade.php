<div class="menu">
@include('includes.staticmenuwithbg')
</div>

<div class="container">

  <div class="body">

    <div class="row">
      <div class="col-md-3"></div>
      <div class="col-md-9">
          <h2 class="message-text">Notifications</h2>
      </div>
    </div>

    <div class="row">

        <div class="col-md-3">

            <a href="{{route('userprofile')}}" class="ajaxlink">
            <div class="profile">
                @if(empty($user->image))
                <div class="display-picture" style="background-image: url({{route('home')}}/images/user.png)">
                </div>
                @else 
                <div class="display-picture" style="background-image: url({{$user->image}})">
                </div>
                @endif
                <h4 class="user-name">{{$user->full_name}}</h4>

            </div>
            </a>
        
        </div>
        <div class="col-md-9">

            <div class="message-display">

              @if(count($notifications) == 0)
                <h4>You have no notification</h4>
              @endif


              
              <table class="table table-hover table-responsive table-striped table-bordered">
                <thead>
                  <tr>
                    <th>Date</th>
                    <th>Notification</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($notifications as $obj)
                  <tr>
                    <td>{{$obj->created_at}}</td>
                    <td>{{$obj->comment}}</td>
                    <td>
                      <a class="btn btn-success btn-sm ajaxlink" href="{{route('adpage',[$obj->advert->slug])}}">View</a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
              
              
              
            </div>
        
        </div>

    </div>


    <div style="height:80px"></div>

  </div>

</div>

<script>

   
</script>

<style>

.active-link{
  color:green;
  font-weight:600;
}

.user-name{
  color:#1fae37;
  margin-left: 20px;
  font-weight: 600;
}

.message-text{
  margin-top:25px;
  margin-bottom:25px;
}

.profile, .message-box{
  background-color: #fff;
  padding:30px;
  border: solid 1px #8080804f;
  margin-bottom:30px;
  box-shadow: -3px 7px 8px 0px rgba(0, 0, 0, 0.06);
}

.message-box{
  margin-bottom:15px;
  display: block;
  color:inherit;
}

.display-picture{
  background-size: cover;
  width:200px;
  height:200px;
  border: solid 1px #8080804f;    
  border-radius: 20px;
  position: relative;
}

.message-content {
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  max-height: 250px;
}

.message-receiver {
    display: inline-block;
    font-size: 16px;
    color: #19af34;
}

a:active, a:hover{
    color:inherit;
}

.dataTables_wrapper.no-footer .dataTables_scrollBody {
    border-bottom: none;
}

</style>

<script>
document.title = "{{$pageTitle}}";
    $('.table').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "order": [[ 0, "desc" ]],
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "scrollX": true,
        "lengthMenu": [[30, 60, 90, -1], [30, 60, 90, "All"]]
    });
</script>