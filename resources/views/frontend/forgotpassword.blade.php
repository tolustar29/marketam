
<div class="menu">
@include('includes.staticmenuwithbg')
</div>
@include('includes.css.styleone')


<div class="container">

  <div class="row body">

    <div class="col-md-3"></div>

    <div class="col-md-6">
      <div style="padding: 80px 0px 80px 0px">
          
          <form action="{{route('postforgotpassword')}}" method="POST" role="form" class="forgotPassword">
            {{csrf_field()}}
            <div class="form-group">
              <label for="">Enter Your Email</label>
              <input type="email" class="form-control" id="email" name="email" required>
            </div>
          
            
          
            <button type="submit" class="btn btn-success">Submit</button>
          </form>
          
      </div>
    </div>

    <div class="col-md-3"></div>

  </div>

</div>

<style>
    .passwordForm{
      padding:30px;
      background: white;
      border-radius: 8px;
      border: solid 1px #bfbfbf;
      margin: 30px 0px 50px 0px;
    }
</style>


<script>
        document.title = "{{$pageTitle}}";
        $(".forgotPassword").validate({

            rules: {
                email: {
                    minlength: 5,
                }
            },
            
            submitHandler: function(form) {
                
                $(".progressBarBg").show();
                  
                $.ajax({
                    url     : $(form).attr('action'),
                    type    : $(form).attr('method'),
                    data    : $(form).serialize(),
                    success : function( data ) {

                        successdisplay(data);
                                

                    },
                    error   : function( xhr, err ) {
                        $(".progressBarBg").hide();
                        alert('Error in submitting form');     
                    }
                });  
                  
                return false;
                
            }
        });
</script>