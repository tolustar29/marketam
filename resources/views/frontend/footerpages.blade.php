
<div class="menu">
@include('includes.staticmenuwithbg')
</div>
@include('includes.css.styleone')


<div class="container">

  <div class="row body">

    <div><h2 class="">{{$getpage->page}}</h2></div>

    <div>
      {!! $getpage->content !!}
    </div>
    
    
  </div>
  
</div>

<style>
.header-text{
  margin:10px 0px 20px 0px;
  text-align:center;
  color:#5cb85c;
  font-weight:500;
}

.body{
  padding-bottom:100px;
}

.login-form {
    background: white;
    padding: 30px;
    border: solid 1px #b3b3b38f;
    border-radius: 8px;
    margin-bottom: 30px;
    box-shadow: -3px 7px 8px 0px rgba(0, 0, 0, 0.06);
  }
.action-buttons{
    margin-top:20px;
}

.social-login{
    margin-top: 10px;
 }
</style>





<script>
  document.title = "{{$pageTitle}}";

  $(function(){

  })
 
</script>
    
