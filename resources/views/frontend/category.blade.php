<div class="menu">
  @include('includes.staticmenuwithbg')
</div>

@include('includes.css.styleone')

<div class="container">

  <div class="row body">
    <div class="col-md-3">
      <button type="button" class="btn btn-success categories">Categories</button>

      <div class="category-body">
        @foreach($categories as $obj)
        <div class="category-container">
          <a href="{{route('category',[$obj->category_slug])}}" class="ajaxlink"><p class="{{$obj->category_slug}}">{{$obj->category}}</p></a>
          <div class="category-content">
            @foreach($obj->categories_subs as $catSub)
            <a href="{{route('category',[$obj->category_slug,$catSub->categories_slug])}}" class="{{$obj->category_slug}} {{$catSub->categories_slug}} ajaxlink">{{$catSub->categories_sub}}</a>
            @endforeach
          </div>
        </div>
        @endforeach
      </div>
    </div>
    <div class="col-md-9 col-sm-12">
      <div class="content">


        <div class="row">
          <form action="" method="POST" class="form-inline price-filter-form" role="form">

            <div class="form-group">
              <h3 class="price-filter-text">Price Filter : </h2>
            </div>

            <div class="form-group">
             <input type="number" class="form-control min_price" id="" placeholder="Minimum Price" value="{{$min_price}}">
            </div>

            <div class="form-group">
             <input type="number" class="form-control max_price" id="" placeholder="Maximum Price" value="{{$max_price}}">
            </div>

            <a href="{{route('category',[$slug,$subslug])}}" class="btn btn-success categoryfilterlink price">Apply Filter</a>
          </form>

          <div class="col-md-9">
            <h2 class="category-title">{{$category_title}} in {{$location}}
              <div class="dropdown">
                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown"
                  aria-haspopup="true" aria-expanded="true">
                  <span class="span_location">Nigeria</span>
                  <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                  <li><a href="#" class=" filter_location categoryfilterlink" data-location="Abuja FCT">Abuja
                      FCT</a></li>
                  <li><a href="#" class=" filter_location categoryfilterlink" data-location="Abia">Abia</a></li>
                  <li><a href="#" class=" filter_location categoryfilterlink" data-location="Adamawa">Adamawa</a></li>
                  <li><a href="#" class=" filter_location categoryfilterlink" data-location="Akwa Ibom">Akwa
                      Ibom</a></li>
                  <li><a href="#" class=" filter_location categoryfilterlink" data-location="Anambra">Anambra</a></li>
                  <li><a href="#" class=" filter_location categoryfilterlink" data-location="Bauchi">Bauchi</a></li>
                  <li><a href="#" class=" filter_location categoryfilterlink" data-location="Bayelsa">Bayelsa</a></li>
                  <li><a href="#" class=" filter_location categoryfilterlink" data-location="Benue">Benue</a></li>
                  <li><a href="#" class=" filter_location categoryfilterlink" data-location="Borno">Borno</a></li>
                  <li><a href="#" class=" filter_location categoryfilterlink" data-location="Cross River">Cross
                      River</a></li>
                  <li><a href="#" class=" filter_location categoryfilterlink" data-location="Delta">Delta</a></li>
                  <li><a href="#" class=" filter_location categoryfilterlink" data-location="Ebonyi">Ebonyi</a></li>
                  <li><a href="#" class=" filter_location categoryfilterlink" data-location="Edo">Edo</a></li>
                  <li><a href="#" class=" filter_location categoryfilterlink" data-location="Ekiti">Ekiti</a></li>
                  <li><a href="#" class=" filter_location categoryfilterlink" data-location="Enugu">Enugu</a></li>
                  <li><a href="#" class=" filter_location categoryfilterlink" data-location="Gombe">Gombe</a></li>
                  <li><a href="#" class=" filter_location categoryfilterlink" data-location="Imo">Imo</a></li>
                  <li><a href="#" class=" filter_location categoryfilterlink" data-location="Jigawa">Jigawa</a></li>
                  <li><a href="#" class=" filter_location categoryfilterlink" data-location="Kaduna">Kaduna</a></li>
                  <li><a href="#" class=" filter_location categoryfilterlink" data-location="Kano">Kano</a></li>
                  <li><a href="#" class=" filter_location categoryfilterlink" data-location="Katsina">Katsina</a></li>
                  <li><a href="#" class=" filter_location categoryfilterlink" data-location="Kebbi">Kebbi</a></li>
                  <li><a href="#" class=" filter_location categoryfilterlink" data-location="Kogi">Kogi</a></li>
                  <li><a href="#" class=" filter_location categoryfilterlink" data-location="Kwara">Kwara</a></li>
                  <li><a href="#" class=" filter_location categoryfilterlink" data-location="Lagos"
                      selected>Lagos</a></li>
                  <li><a href="#" class=" filter_location categoryfilterlink" data-location="Nassarawa">Nassarawa</a></li>
                  <li><a href="#" class=" filter_location categoryfilterlink" data-location="Niger">Niger</a></li>
                  <li><a href="#" class=" filter_location categoryfilterlink" data-location="Ogun">Ogun</a></li>
                  <li><a href="#" class=" filter_location categoryfilterlink" data-location="Ondo">Ondo</a></li>
                  <li><a href="#" class=" filter_location categoryfilterlink" data-location="Osun">Osun</a></li>
                  <li><a href="#" class=" filter_location categoryfilterlink" data-location="Oyo">Oyo</a></li>
                  <li><a href="#" class=" filter_location categoryfilterlink" data-location="Plateau">Plateau</a></li>
                  <li><a href="#" class=" filter_location categoryfilterlink" data-location="Rivers">Rivers</a></li>
                  <li><a href="#" class=" filter_location categoryfilterlink" data-location="Sokoto">Sokoto</a></li>
                  <li><a href="#" class=" filter_location categoryfilterlink" data-location="Taraba">Taraba</a></li>
                  <li><a href="#" class=" filter_location categoryfilterlink" data-location="Yobe">Yobe</a></li>
                  <li><a href="#" class=" filter_location categoryfilterlink" data-location="Zamfara">Zamfara</a></li>
                </ul>
              </div>
            </h2>
          </div>
          <div class="col-md-3">
            <div class="sort" style="text-align:right">
              <span style="font-weight:500">Sort By: </span>
              <div class="btn-group" role="group" aria-label="...">
                <a href="{{route('category',[$slug,$subslug])}}" class="btn btn-default categoryfilterlink" data-sort="newest">Newest</a>
                <a href="{{route('category',[$slug,$subslug])}}" class="btn btn-default categoryfilterlink" data-sort="cheapest">Cheapest</a>
              </div>
            </div>
          </div>
        </div>

        @if(count($adverts) == 0)
          <h3>No Advert Found</h3>
        @endif

        @foreach($adverts as $obj)
        <div class="ads-container">
          <div class="row">
            <div class="col-md-3">
              <div class="ad-image-container">
                <a href="{{route('adpage',[$obj->slug])}}" class="ajaxlink">
                  <div class="ad-image">
                    @if($obj->advert_media != null)
                      @if($obj->advert_media->where('type','image')->first())
                      <img src="{{route('home')}}/{{$obj->advert_media->where('type','image')->first()->link}}" alt="">
                      @else 
                      <img src="#" alt="">
                      @endif
                    @else
                    <img src="#" alt="">
                    @endif
                  </div>
                </a>
                @if($obj->advert_type == "sponsored")
                <div class="premium-badge">
                  <img src="{{route('home')}}/images/icons/premium.png" alt="" class="badge-icon">
                </div>
                @endif
              </div>
            </div>
            <div class="col-md-9">
              <div style="margin-left:30px">
                <div class="row">
                  <div class="col-md-8"><a href="{{route('adpage',[$obj->slug])}}" class="ajaxlink">
                      <h3 class="product-title">{{$obj->title}}</h3>
                    </a></div>
                  <div class="col-md-4">
                    <h3 class="product-price">₦{{number_format($obj->price, 2)}}</h3>
                  </div>
                </div>

                <div class="product-description">
                  <p>{{$obj->description}}</p>
                </div>

                <div class="product-location">
                  <i class="fa fa-map-marker"></i> {{$obj->location}}
                </div>
                <div class="product-subcategory">
                  <i class="fa fa-asterisk"></i> {{$obj->category_sub->categories_sub}}
                </div>
                <div class="product-timestamp">
                  <i class="fa fa-clock-o"></i> {{date_format(new DateTime($obj->updated_at), "M-d h:i A")}}
                </div>

                
                <div class="product-favourite ajaxfavourite">
                @if(!empty(Auth::user()))
                @if(empty($obj->favourite()->where("user_id","$user->id")->first()))
                  <button data-link="{{route('favourite',['add',$obj->id])}}" class="btn btn-danger btn-sm ajaxfavourite"><i class="fa fa-heart"></i> Add To Favourites</button>
                @else
                  <button data-link="{{route('favourite',['remove',$obj->id])}}" class="btn btn-success btn-sm ajaxfavourite"><i class="fa fa-heart"></i> Added To Favourites</button>
                @endif
                @endif
                </div>
              </div>

            </div>
          </div>
        </div>
        @endforeach
        <div class="added-adverts"></div>

        @if(count($adverts) >= 25)
        <div class="" style="margin-top:30px;text-align:center">
          <a href="{{$url}}" class="paginateLink btn btn-success">VIEW MORE</a>
        </div>
        @endif

        <div style="height:60px"></div>
      </div>
    </div>
    <div>

    </div>
  </div>

  @if(count($adverts) == 0)
  <style>
    .paginateLink{
      display: none;
    }
  </style>
  @endif

  @include('includes.css.ads')
  <style>
    .sort {
      margin-top: 15px;
    }

    .categories {
      margin-top: 65px;
      margin-bottom: 20px;
      background: #409240;
    }

    .category-container p {
      border-bottom: solid 1px #ffffff17;
      padding: 10px 0px 10px;
      color: white;
      font-weight: 500;
    }

    .category-title {
      color: #409240;
      margin-top: 0px;
      font-weight: 600;
      margin-bottom: 20px;
    }

    .price-filter-text {
      color: green;
      margin-top: 10px;
      font-weight: 500;
      font-size: 18px;
      margin-left: 15px;
    }

    .price-filter-form {
      margin-bottom: 25px;
    }

    .price-filter-form .form-group {
      padding-left: 20px;
      padding-right: 20px;
    }

    .category-body {
      background: #409240;
      border-radius: 5px;
      padding: 10px;
      box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
      margin-bottom: 40px;
    }

    .category-container {
      position: relative;
      display: block;
      cursor: pointer;
    }

    /* Dropdown Content (Hidden by Default) */
    .category-content {
      display: none;
      position: relative;
      min-width: 160px;
      margin-bottom: 30px;
    }

    .category-content a {
      color: #2a712a;
      background: #ffffffdb;
      margin-bottom: 8px;
      padding: 10px;
      text-decoration: none;
      display: block;
      border-radius: 5px;
      font-weight: 400;
      font-size: 14px;
    }

    .sub_category_active {
      color: #fff !important;
      background: #4b6966 !important;
      border: solid #4b6966;
      border-radius: 0px !important;
    }

    .category_active {
      background: #ffffff42;
      padding-left: 15px !important;
    }

    .premium-badge {
      position: absolute;
      top: 1%;
      right: 1%;
    }

    @media (max-width:480px) {
      .price-filter-form .btn-success {
        margin-left: 20px;
      }

      .categories {
        margin-top: 0px;
      }

      .category-body {
        display: none;
      }
    }
  </style>

  <script>
    document.title = "{{$pageTitle}}";

    temp_url = "{{$url}}";
    main_url = "{{$mainurl}}";

    category_location = "{{$location}}";
    category_min_price = "{{$min_price}}";
    category_max_price = "{{$max_price}}";
    category_sort = "{{$sort}}";

    $slug = "{{$slug}}";

    $subslug = "{{$subslug}}";

    if($slug){
      $("." + $slug).parent().slideDown("slow");

      $("." + $slug).addClass("category_active");
    }

    
    if($subslug){
      $("." + $subslug).addClass("sub_category_active");
    }

    if(category_location){
      $(".span_location").text(category_location);
    }
    

    $("a").on('click', function (event) {
      if ($(this).hasClass("categoryfilterlink")) {
        event.preventDefault();

        link = main_url + "?filter=on";
        data_location = $(this).data("location");
        data_min_price = $(this).data("min_price");
        data_max_price = $(this).data("max_price");
        data_sort = $(this).data("sort");

        if (data_location) {
          link = link + "&location=" + data_location;
        } else {
          if (category_location) {link = link + "&location=" + category_location;}
        }

        if (data_sort) {
          link = link + "&sort=" + data_sort;
        } else {
          if (category_sort) {link = link + "&sort=" + category_sort;}
        }

        if ($(this).hasClass("price")) {
          link = link + "&min_price=" + $(".min_price").val() + "&max_price=" + $(".max_price").val()
        }else{
          if (category_min_price) {link = link + "&min_price=" + category_min_price + "&max_price=" + category_max_price;}
        }

        $(".progressBarBg").show();
        $(".alert").hide();

        $.ajax({
          cache: false,
          type: "GET",
          url: link,
          success: function (data) {
            successdisplay(data);

          },
          error: function (data) {
            $(".progressBarBg").hide();
          }
        });
      }

    });

    

    $(".categories").click(function () {
      $(".category-body").toggle("slow", "swing");
    });
  </script>