
<div class="menu">
@include('includes.staticmenuwithbg')
</div>
@include('includes.css.styleone')

<div class="container">

  <div class="row body">

    <div><h2 class="header-text">Register</h2></div>

    <div class="col-md-2"></div>
    <div class="col-md-8">
      <div class="login-form">
        <form action="{{route('postregister')}}" method="POST" role="form" class="submitForm">
        
          <div align="center"><div onClick="googleLogin()" class="btn btn-danger social-login"><i class="fa fa-google" aria-hidden="true"></i> Register with Google</div> <div onClick="fbLogin()" class="btn btn-primary social-login"><i class="fa fa-facebook" aria-hidden="true"></i> Register with Facebook</div></div>

          <div style="text-align:center;margin-top:20px;margin-bottom:20px;font-weight:600">Or register with your details</div>
          {{ csrf_field() }}
          <div class="form-group">
            <label for="">Email</label>
            <input name="email" type="email" class="form-control" id="" placeholder="" required>
          </div>

          <div class="form-group">
            <label for="">Password</label>
            <input name="password" type="password" class="form-control" id="" placeholder="" required>
          </div>

          <div class="form-group">
            <label for="">Full Name</label>
            <input name="full_name" type="text" class="form-control" id="" placeholder="" required>
          </div>

          <div class="form-group">
            <label for="">Phone Number</label>
            <input name="phone_number" type="text" class="form-control" id="" placeholder="" required>
          </div>
          
          <div style="text-align:center;margin-top:10px;margin-bottom:30px"><button type="submit" class="btn btn-success btn-lg">Register</button></div>
        </form>
      </div>
    </div>
    <div class="col-md-2"></div>
    
    
  </div>
  
</div>

<style>
.header-text{
  margin:10px 0px 20px 0px;
  text-align:center;
  color:#5cb85c;
  font-weight:500;
}

.body{
  padding-bottom:100px;
}

.login-form {
    background: white;
    padding: 30px;
    border: solid 1px #b3b3b38f;
    border-radius: 8px;
    margin-bottom: 30px;
    box-shadow: -3px 7px 8px 0px rgba(0, 0, 0, 0.06);
  }
.action-buttons{
  margin-top:20px;
}

.social-login{
    margin-top: 10px;
 }
</style>


<script>
document.title = "{{$pageTitle}}";

  $(function(){
    
  })
 
</script>
    
