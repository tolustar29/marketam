<div class="menu">
  @include('includes.staticmenuwithbg')
</div>

<style>
  .displayframe {
    display: none;
  }

  input#file {
    display: none;
  }

  .display-profile {
    display: block;
  }

  .update-picture {
    width: 200px;
    margin-top: 5px;
    margin-bottom: 10px;
    display: none;
  }
</style>



<div class="container">

  <div class="body">

    <div class="row">
      <div class="col-md-4"></div>
      <div class="col-md-8">
        <h2 class="contact-details">Contact Details</h2>
      </div>
    </div>

    <div class="row">

      <div class="col-md-4">

        <div class="profile-menu-btn">
          <button type="button" class="btn btn-success">Profile Menu</button>
        </div>


        <div class="profile profile-frame">

          <div class="display-picture" style="background-image: url({{$user->image}})">
            <div class="change-picture">
              <span class="fa-stack fa-2x">
                <i class="fa fa-circle fa-stack-2x" style="color:#15171578"></i>
                <i class="fa fa-camera fa-stack-1x fa-inverse" style="font-size:22px;"></i>
              </span>
            </div>

          </div>

          <div>
            <form action="{{route('postupdatepicture')}}" method="POST" role="form" class="updatepictureform" enctype="multipart/form-data">
              {{csrf_field()}}
              <input type="file" name="image" id="file" style="width:100%" required/>
              <button type="submit" class="btn btn-success btn-sm update-picture">Update Picture</button>
            </form>
          </div>

          <h4 class="user-name">{{$user->full_name}}</h4>
          <div class="user-mini-detail">
            <i class="fa fa-circle" aria-hidden="true"></i> Last login: {{date_format(new DateTime($user->last_login), "d/m/Y h:i A")}}</div>
          <div class="user-mini-detail">
            <i class="fa fa-circle" aria-hidden="true"></i> Member Since: {{date_format(new DateTime($user->created_at), "d/m/Y h:i A")}}</div>

          <div class="profile-menu">
            <div class="profile-click active-link" onClick="displayFunction('.display-profile', '.profile-click', 'Contact Details')">
              <i class="fa fa-user" aria-hidden="true"></i> Profile Details</div>

            <div class="company-click" onClick="displayFunction('.display-company', '.company-click', 'Company Details')">
              <i class="fa fa-building-o" aria-hidden="true"></i> Company Details</div>

            <div class="notification-click" onClick="displayFunction('.display-notification', '.notification-click', 'Notification')">
              <i class="fa fa-circle" aria-hidden="true"></i> Notification</div>

            @if($user->auth_type == "default")
            <div class="password-click" onClick="displayFunction('.display-resetpassword', '.password-click', 'Reset Password')">
              <i class="fa fa-lock" aria-hidden="true"></i> Reset Password</div>
            @endif
          </div>
        </div>

      </div>
      <div class="col-md-8">

        <div class="profile profile-display" id="id-display">

          <div class="displayframe display-profile">
            <form action="{{route('postuserprofile')}}" method="POST" role="form" class="profileForm">
              {{csrf_field()}}
              <div class="form-group">
                <label for="">Full Name</label>
                <input name="full_name" type="text" class="form-control full_name" id="" value="{{$user->full_name}}" required>
              </div>

              <div class="form-group">
                <label for="">Email</label>
                <input name="email" type="email" class="form-control" id="" value="{{$user->email}}" disabled>
              </div>

              <div class="form-group">
                <label for="">Phone Number</label>
                <input name="phone_number" type="text" class="form-control" id="" value="{{$user->phone_number}}" required>
              </div>

              <div class="form-group">
                <label for="">State</label>
                <select name="state-of-origin" id="state-of-origin" class="form-control" class="state-of-origin" required>
                  <option value="Abuja FCT">Abuja FCT</option>
                  <option value="Abia">Abia</option>
                  <option value="Adamawa">Adamawa</option>
                  <option value="Akwa Ibom">Akwa Ibom</option>
                  <option value="Anambra">Anambra</option>
                  <option value="Bauchi">Bauchi</option>
                  <option value="Bayelsa">Bayelsa</option>
                  <option value="Benue">Benue</option>
                  <option value="Borno">Borno</option>
                  <option value="Cross River">Cross River</option>
                  <option value="Delta">Delta</option>
                  <option value="Ebonyi">Ebonyi</option>
                  <option value="Edo">Edo</option>
                  <option value="Ekiti">Ekiti</option>
                  <option value="Enugu">Enugu</option>
                  <option value="Gombe">Gombe</option>
                  <option value="Imo">Imo</option>
                  <option value="Jigawa">Jigawa</option>
                  <option value="Kaduna">Kaduna</option>
                  <option value="Kano">Kano</option>
                  <option value="Katsina">Katsina</option>
                  <option value="Kebbi">Kebbi</option>
                  <option value="Kogi">Kogi</option>
                  <option value="Kwara">Kwara</option>
                  <option value="Lagos">Lagos</option>
                  <option value="Nassarawa">Nassarawa</option>
                  <option value="Niger">Niger</option>
                  <option value="Ogun">Ogun</option>
                  <option value="Ondo">Ondo</option>
                  <option value="Osun">Osun</option>
                  <option value="Oyo">Oyo</option>
                  <option value="Plateau">Plateau</option>
                  <option value="Rivers">Rivers</option>
                  <option value="Sokoto">Sokoto</option>
                  <option value="Taraba">Taraba</option>
                  <option value="Yobe">Yobe</option>
                  <option value="Zamfara">Zamfara</option>
                </select>
              </div>

              <div class="form-group">
                <label for="">Address</label>
                <input name="address" type="text" class="form-control" id="" value="{{$user->address}}" required>
              </div>

              <input name="type" type="hidden" class="form-control" id="" value="displayprofile">

              <button type="submit" class="btn btn-success">Save</button>
            </form>
          </div>

          <div class="displayframe display-company">
            <form action="{{route('postuserprofile')}}" method="POST" role="form" class="companyProfile">
              {{csrf_field()}}
              <div class="form-group">
                <label for="">Company Name</label>
                <input name="company_name" type="text" class="form-control" id="" value="{{$user->company_name}}">
              </div>

              <div class="form-group">
                <label for="">Company Unique Name</label>
                <input name="company_unique_name" type="text" class="form-control" id="" value="{{$user->company_unique_name}}">
              </div>

              <div class="form-group">
                <label for="">About Company</label>
                <textarea name="about_company" id="input" class="form-control" rows="3" required="required">{{$user->about_company}}</textarea>

              </div>

              <div class="form-group">
                <label for="">Company Website</label>
                <input name="company_website" type="text" class="form-control" id="" value="{{$user->company_website}}">
              </div>

              <div class="form-group">
                <label for="">Select Which Name To Be Displayed On Marketam Store</label>
                <select name="display_name_type" id="input" class="form-control" required="required">
                  <option value="full_name">Full Name</option>
                  <option value="company_name">Company Name</option>
                </select>
                
              </div>

              <input name="type" type="hidden" class="form-control" id="" value="companyprofile">
              <button type="submit" class="btn btn-success">Save</button>
            </form>
          </div>

          <div class="displayframe display-notification">
            <form action="{{route('postuserprofile')}}" method="POST" role="form" class="newsletter">

              {{csrf_field()}}
              <div class="checkbox">
                <label>
                  <input name="jobs" type="checkbox" value="true" class="jobs-notification"> Jobs Newsletter
                </label>
              </div>

              <div class="checkbox">
                <label>
                  <input name="ads" type="checkbox" value="true" class="ads-notification"> Ads Newsletter
                </label>
              </div>

              <div class="checkbox">
                <label>
                  <input name="tips" type="checkbox" value="true" class="tips-notification"> Tips
                </label>
              </div>

              <div class="checkbox">
                <label>
                  <input name="general" type="checkbox" value="true" class="general-notification"> General Newsletter
                </label>
              </div>

              <input name="type" type="hidden" class="form-control" id="" value="newsletter">
              <button type="submit" class="btn btn-success">Save</button>
            </form>
          </div>

          <div class="displayframe display-resetpassword">
            Click the reset password button and instruction on how to reset your password will be sent to your mail
            <a href="{{route('reset_password','send-mail')}}" class="btn btn-success ajaxlink">Reset Password</a>

          </div>

        </div>

      </div>

    </div>

  </div>

</div>

<script>
  document.title = "{{$pageTitle}}";
  $(".alert").addClass("alert-success");
  $(function () {
    $("#state-of-origin").select2().val('{{$user->state}}').trigger('change');
    $("#state-of-origin option[value='{{$user->state}}']").prop('selected', true);

    $(".jobs-notification").prop('checked', "{{$user->jobs_notification}}");
    $(".ads-notification").prop('checked', "{{$user->ads_notification}}");
    $(".tips-notification").prop('checked', "{{$user->tips_notification}}");
    $(".general-notification").prop('checked', "{{$user->general_notification}}");

    $(".profile-menu-btn").click(function () {
      $(".profile-frame").toggle("slow", "linear");
    });

    $(".change-picture").click(function () {
      $("#file").click();
    });

    $(".updatepictureform").submit(function (event) {
      event.preventDefault();
      $(".progressBarBg").show();

      fetch($(".updatepictureform").attr('action'), {
        method: $(".updatepictureform").attr('method'),
        body: new FormData($('.updatepictureform')[0])
      })
      .then((data) => data.json())
      .then(function (resp) {
        $(".progressBarBg").hide();
        notificationdisplay(resp);
        $(".update-picture").slideUp("slow");
      })
      .catch(function (err) {
        $(".progressBarBg").hide();
        alert('Error in updating picture');
      });

    });


    $("#file").change(function () {
      $("#message").empty(); // To remove the previous error message
      var file = this.files[0];
      var imagefile = file.type;
      var match = ["image/jpeg", "image/png", "image/jpg"];
      if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
        $('#previewing').attr('src', 'noimage.png');
        $(".alert").removeClass("alert-success");
        $(".alert").addClass("alert-danger");
        $(".alert-body").html("Please Select A valid Image File. Only jpeg, jpg and png Images type allowed");
        $(".alert").slideDown("slow");
        return false;
      } else {
        var reader = new FileReader();
        reader.onload = imageIsLoaded;
        reader.readAsDataURL(this.files[0]);
      }
    });

    validateForms();
  });

  function validateForms() {
    $(".profileForm").validate({
      rules: {
        phone_number: {
          digits: true,
          minlength: 8
        },
        address: {
          minlength: 5
        }
      },
      submitHandler: function (form) {

        $(".progressBarBg").show();

        fetch($(form).attr('action'), {
            method: $(form).attr('method'),
            body: new FormData($(form)[0])
          })
          .then((data) => data.json())
          .then(function (data) {
            $(".user-name").html($(".full_name").val());
            $(".progressBarBg").hide();
            notificationdisplay(data);
          })
          .catch(function (err) {
            $(".progressBarBg").hide();
            alert('Error in submitting form');
          });


      }
    });

    $(".companyProfile").validate({
      rules: {
        company_unique_name: {
          minlength: 5
        },
        company_name: {
          minlength: 5
        },
        about_company: {
          minlength: 10
        },
        company_website: {
          minlength: 5
        }
      },
      submitHandler: function (form) {

        $(".progressBarBg").show();

        $.ajax({
          url: $(form).attr('action'),
          type: $(form).attr('method'),
          data: $(form).serialize(),
          success: function (data) {
            $(".progressBarBg").hide();
            notificationdisplay(data);
          },
          error: function (xhr, err) {
            $(".progressBarBg").hide();
            alert('Error in submitting form');
          }
        });

        return false;

      }
    });

    $(".newsletter").validate({

      submitHandler: function (form) {

        $(".progressBarBg").show();

        $.ajax({
          url: $(form).attr('action'),
          type: $(form).attr('method'),
          data: $(form).serialize(),
          success: function (data) {
            $(".progressBarBg").hide();
            notificationdisplay(data);
          },
          error: function (xhr, err) {
            $(".progressBarBg").hide();
            alert('Error in submitting form');
          }
        });

        return false;

      }
    });
  }

  function imageIsLoaded(e) {
    // $("#file").css("color","green");
    // $('#image_preview').css("display", "block");
    // $('#previewing').attr('src', e.target.result);
    $(".update-picture").slideDown("slow");
    $(".display-picture").css("background-image", "url(" + e.target.result + ")");
  };

  function notificationdisplay(data) {
    if (data.success) {
      $(".alert").removeClass("alert-danger");
      $(".alert").addClass("alert-success");
      $(".alert-body").html(data.success);
      $(".alert").slideDown("slow");
      console.log("success");
    }
    if (data.error) {
      $(".alert").removeClass("alert-success");
      $(".alert").addClass("alert-danger");
      $(".alert-body").html(data.error);
      $(".alert").slideDown("slow");
      console.log("error");
    }

    setTimeout(function(){ $(".alert").slideUp("slow"); }, 6000);
  }

  function displayFunction(displayedFrame, clickedObject, heading) {
    $(".profile-menu div").removeClass("active-link");
    $(".displayframe").slideUp("slow");
    $(".contact-details").text(heading);
    $(displayedFrame).slideDown("slow");
    $(clickedObject).addClass("active-link");
    // $('html, body').animate({
    //     scrollTop: $("#id-display").offset().top
    // }, 2000);

    if ($(window).width() < 480) {
      $(".profile-frame").toggle("slow", "linear");
    }
  }
</script>


<style>
  .active-link {
    color: green;
    font-weight: 600;
  }

  .user-name {
    color: #1fae37;
    font-weight: 600;
  }

  .user-mini-detail {
    font-size: 14px;
  }

  .contact-details {
    margin-top: 25px;
    margin-bottom: 25px;
  }

  .profile-menu {
    margin-top: 30px
  }

  .profile-menu i.fa {
    width: 15px;
  }

  .user-mini-detail i.fa {
    color: green;
  }

  .profile-menu div {
    margin-bottom: 15px;
    border-bottom: solid 1px #dadada;
    padding-bottom: 5px;
    font-weight: 500;
    cursor: pointer;
  }

  .profile {
    background-color: #fff;
    padding: 30px;
    border: solid 1px #8080804f;
    margin-bottom: 50px;
    box-shadow: -3px 7px 8px 0px rgba(0, 0, 0, 0.06);
  }

  .display-picture {
    background-size: cover;
    width: 200px;
    height: 200px;
    border: solid 1px #8080804f;
    border-radius: 20px;
    position: relative;
  }

  .change-picture {
    cursor: pointer;
    position: absolute;
    bottom: 1px;
    right: 1px;
  }

  .profile-menu-btn {
    display: none;
  }

  @media (max-width:480px) {
    .profile-menu-btn {
      display: block;
      margin-bottom: 20px;
    }

    .profile-frame {
      display: none;
    }
  }
</style>