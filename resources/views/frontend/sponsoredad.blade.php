<div class="menu">
  @include('includes.staticmenuwithbg')
</div>
@include('includes.css.styleone')

<div class="container">

  <div class="row body">

    <div>
      <h2 class="header-text">Post Sponsored Advert</h2>
    </div>

    <div class="col-md-2"></div>
    <div class="col-md-8">
      <div class="advert-form">
        <form action="{{route('postsponsoredad')}}" method="POST" role="form" class="adform" id="adform" enctype="multipart/form-data">
          <script src="https://js.paystack.co/v1/inline.js"></script>
          {{csrf_field()}}
          <div class="form-group">
            <div style="margin-top:5px;margin-bottom:25px;">
              <!-- <strong>
                A sales support services designed for sellers that do not have time to post ads, negotiate, reply emails/sms, phone calls etc and want to sell. SalesPro addresses the aforementioned, from posting of ads to closing sales. For more information, please contact: <a href="mailto:support@marketam.com.ng">support@marketam.com.ng</a>
              </strong> -->
            </div>
            <label for="">Select Category</label>
            <select name="categories" id="categories" class="form-control" required>
              @foreach($categories as $category_obj)
              <optgroup label="{{$category_obj->category}}">
                @foreach($category_obj->categories_subs as $sub_obj)
                <option value="{{$sub_obj->id}}">{{$sub_obj->categories_sub}}</option>
                @endforeach
              </optgroup>
              @endforeach
            </select>
          </div>


          <div class="form-group">
            <label for="">Title</label>
            <input type="text" class="form-control" id="" placeholder="" name="title" required>
          </div>

          <div class="form-group">
            <label for="">Description</label>
            <textarea name="description" id="input" class="form-control" rows="3" name="description" required></textarea>
          </div>

          <div class="insertdata">

          </div>

          <div class="form-group">
            <label class="control-label">Price</label>
            <div class="input-group">
              <input type="number" class="form-control" aria-describedby="price-icon" name="price" step=".01" required>
              <span class="input-group-addon" id="price-icon">Naira</span>
            </div>
          </div>

          <div class="form-group">
            <label for="">Price Type</label>
            <select name="price_type" id="input" class="form-control" required="required">
              <option value="Fixed Price">Fixed Price</option>
              <option value="Negotiable">Negotiable</option>
            </select>
          </div>

          <div class="form-group">
            <label for="">Usage</label>
            <select name="usage" id="input" class="form-control" required="required">
              <option value="New">New</option>
              <option value="Fairly Used">Fairly Used</option>
            </select>
          </div>

          <div class="form-group">
            <label for="">Location</label>
            <!-- <input type="text" name="location" class="form-control" value="" required="required"> -->
            <select name="location" id="location" class="form-control" required="required">
              <option value="Abuja FCT">Abuja FCT</option>
              <option value="Abia">Abia</option>
              <option value="Adamawa">Adamawa</option>
              <option value="Akwa Ibom">Akwa Ibom</option>
              <option value="Anambra">Anambra</option>
              <option value="Bauchi">Bauchi</option>
              <option value="Bayelsa">Bayelsa</option>
              <option value="Benue">Benue</option>
              <option value="Borno">Borno</option>
              <option value="Cross River">Cross River</option>
              <option value="Delta">Delta</option>
              <option value="Ebonyi">Ebonyi</option>
              <option value="Edo">Edo</option>
              <option value="Ekiti">Ekiti</option>
              <option value="Enugu">Enugu</option>
              <option value="Gombe">Gombe</option>
              <option value="Imo">Imo</option>
              <option value="Jigawa">Jigawa</option>
              <option value="Kaduna">Kaduna</option>
              <option value="Kano">Kano</option>
              <option value="Katsina">Katsina</option>
              <option value="Kebbi">Kebbi</option>
              <option value="Kogi">Kogi</option>
              <option value="Kwara">Kwara</option>
              <option value="Lagos" selected>Lagos</option>
              <option value="Nassarawa">Nassarawa</option>
              <option value="Niger">Niger</option>
              <option value="Ogun">Ogun</option>
              <option value="Ondo">Ondo</option>
              <option value="Osun">Osun</option>
              <option value="Oyo">Oyo</option>
              <option value="Plateau">Plateau</option>
              <option value="Rivers">Rivers</option>
              <option value="Sokoto">Sokoto</option>
              <option value="Taraba">Taraba</option>
              <option value="Yobe">Yobe</option>
              <option value="Zamfara">Zamfara</option>
            </select>
          </div>


          <label class="control-label">Files (Maximum of 6 images, 1 video and 1 audio, Image size must be less than
            4MB and video/audio size must be
            less than 7MB)</label>
          <small>Video and audio files attract more buyers and sell fast</small>
          <div class="dropzone-previews">
            <div class="dropzone-label">Drop files here to upload</div>
          </div>

          <h3 style="color:#5cb85c;margin-top:50px">Sponsored Advert [Price ₦2000]</h3>
          <div>Your ad will stand out and have more people see your ad by placing it on the front page. Please see
            <a href="#" style="color:#5cb85c;">Terms & Conditions</a> for more information</div>

          <div style="color:red;margin-top:15px">***Note: Your advert will be checked by our operatives before going
            live</div>
          <div>By clicking the submit button you accept our terms and conditions</div>
          <div style="text-align:center;margin-top:10px;margin-bottom:30px">
            <button type="submit" class="btn btn-success btn-lg">Submit</button>
          </div>
        </form>
      </div>
    </div>
    <div class="col-md-2"></div>


  </div>

</div>

<style>
  .header-text {
    margin: 10px 0px 20px 0px;
    text-align: center;
    color: #5cb85c;
    font-weight: 500;
  }

  .body {
    padding-bottom: 100px;
  }

  .advert-form {
    background: white;
    padding: 25px;
    border: solid 1px #b3b3b38f;
    border-radius: 8px;
    margin-bottom: 30px;
    box-shadow: -3px 7px 8px 0px rgba(0, 0, 0, 0.06);
  }

  .action-buttons {
    margin-top: 20px;
  }
</style>


<script>
  document.title = "{{$pageTitle}}";

  homelink = "{{route('home')}}";

  var link_to_paid = "{{route('getsponsoredadpaid')}}" + "?id=";

  var $this = this;

  var audioCheck = 0;
  var videoCheck = 0;
  var imageCount = 0;

  var adformDropzone = "";

  var paystack_key = "";

  if (window.location.href.indexOf("localhost") > -1) {
    paystack_key = "pk_test_a74966b72f3f92fa5bb0da84f6e83786a78ed789"
  }else{
    paystack_key = "pk_live_757eda6524d948c829556ec8727c07bbbbc5e8e7"
  }


  $(".adform").dropzone({
    paramName: "fileupload",
    autoDiscover: false,
    autoProcessQueue: false,
    acceptedFiles: "image/png,image/jpeg,image/jpg,video/mp4,audio/mp3",
    parallelUploads: 20,
    maxFiles: 8,
    maxFilesize: 80,
    thumbnailWidth: 150,
    thumbnailHeight: 150,
    uploadMultiple: true,
    addRemoveLinks: true,
    previewsContainer: ".dropzone-previews",
    clickable: ".dropzone-previews",
    hiddenInputContainer: ".dropzone-previews",

    init: function () {

      
      

      adformDropzone = this;
      $(".adform").submit(function (event) {
        
        event.preventDefault();

        

        if (adformDropzone.files.length != 0) {
          $(".progressBarBg").show();
          
          adformDropzone.processQueue();
        } else {
          data = {
            error: "Please attach images to your form"
          }
          notificationdisplay_sponsored(data);
        }

        

        //adformDropzone.processQueue();

      });

      this.on("addedfile", function (file) {
        $(".dropzone-label").hide();
        $(".dropzone-previews").css({
          "height": "initial"
        });
        $(".dz-remove").addClass("btn btn-xs btn-danger");


        if (file.type.includes("image") || file.type.includes("video") || file.type.includes("audio")) {

      
          if (file.type.includes("image") && file.size < 4200000){
            imageCount = imageCount + 1;
          }

          if (videoCheck == 1 && file.type.includes("video")) {

            dropzoneNotification(this, file, " - only one video is allowed");
          }

          if (audioCheck == 1 && file.type.includes("audio")) {

            dropzoneNotification(this, file, " - only one audio is allowed");
          }

          if (file.size > 4200000 && file.type.includes("image")) {

            dropzoneNotification(this, file, "(Image) is more than 4MB");
          }

          if (file.size > 7200000 && file.type.includes("video")) {

            dropzoneNotification(this, file, "(video) is more than 7MB");

          }

          if (file.size > 7200000 && file.type.includes("audio")) {

            dropzoneNotification(this, file, "(audio) is more than 7MB");
          }

          if (file.type.includes("video")) {
            file.previewElement.querySelector("img").src = homelink + "/images/play-button.png";
            videoCheck = 1;
          }

          if (file.type.includes("audio")) {
            file.previewElement.querySelector("img").src = homelink + "/images/music-icon.png";
            audioCheck = 1;
          }

          

          if(imageCount > 6){
            
            dropzoneNotification(this, file, " can not be added (Maximum number of images allowed is 6)");
          }


        } else {
          this.removeFile(file);
        }

        

      });

      this.on("removedfile", function (file) {

        if (file.type.includes("image")) {
          imageCount = imageCount - 1;
        }
        
        if (file.type.includes("video")) {
          videoCheck = 0;
        }
        if (file.type.includes("audio")) {
          audioCheck = 0;
        }
      });

      this.on("reset", function (file) {
        $(".dropzone-label").show();
        $(".dropzone-previews").css({
          "height": "200px"
        });
      });

      this.on("sendingmultiple", function (file, xhr, formData) {
        //$(".adform").serialize();
        formData = new FormData($('.adform')[0]);
      });

      this.on("uploadprogress", function (file, progress) {
        var progressElement = file.previewElement.querySelector("[data-dz-uploadprogress]");
        progressElement.style.width = progress + "%";
        progressElement.textContent = progress + "%";
      });

      this.on("successmultiple", function (file, data) {
        // $(".progressBarBg").hide();
        // notificationdisplay_sponsored(data);

        var user_id = "{{Auth::user()->id}}";
        var user_name = "{{Auth::user()->full_name}}";
        var user_email = "{{Auth::user()->email}}";

        notify = {
          success: "Ad submitted Successfully"
        };
        notificationdisplay_sponsored(notify);

        payWithPaystack(this, user_id, user_name, user_email, data, file);

        
      });

      this.on("error", function (file) {
        dropzoneNotification(this, file, " - File Not Accepted");
      });

    }

  });

  //pk_live_757eda6524d948c829556ec8727c07bbbbc5e8e7
  //pk_test_5b4de83526c8ea97af9852d4d6772e5c6bba5312
  function payWithPaystack(objThis, user_id, user_name, user_email, data, file) {
    var handler = PaystackPop.setup({
      key: paystack_key,
      email: user_email,
      amount: 200000,
      ref: '' + Math.floor((Math.random() * 1000000000) + 1), // generates a pseudo-unique reference. Please replace with a reference you generated. Or remove the line entirely so our API will generate one for you
      metadata: {
        custom_fields: [{
          user_id: user_id,
          user_name: user_name,
          user_email: user_email,
        }]
      },
      callback: function (response) {
        
        let new_link = data.link;
        $.ajax({
            cache: false,
            type: "GET",
            url: link_to_paid + data.advert_id,
            success: function (data) {

              $.ajax({
                  cache: false,
                  type: "GET",
                  url: new_link,
                  success: function (data) {
                      successdisplay(data);

                  },
                  error: function (data) {
                      $(".progressBarBg").hide();
                      alert("An error occured");
                      console.log('Error:', data);
                  }
              });

            },
            error: function (data) {
                $(".progressBarBg").hide();
                alert("An error occured");
                console.log('Error:', data);
            }
        });
        //alert('success. transaction ref is ' + response.reference);
      },
      onClose: function () {
        $(".progressBarBg").hide();
        objThis.removeAllFiles(true);
        data = {
          error: "Window Closed"
        };
        // notificationdisplay_sponsored(data);
      }
    });
    handler.openIframe();
  }

  function notificationdisplay_sponsored(data) {
      if (data.success) {
        $(".alert").removeClass("alert-danger");
        $(".alert").addClass("alert-success");
        $(".alert-body").html(data.success);
        $(".alert").slideDown("slow");
        console.log("success");
      }
      if (data.error) {
        $(".alert").removeClass("alert-success");
        $(".alert").addClass("alert-danger");
        $(".alert-body").html(data.error);
        $(".alert").slideDown("slow");
        console.log("error");
      }

      setTimeout(function(){ $(".alert").slideUp("slow"); }, 100000);
    }


  function dropzoneNotification(objThis, objFile, message) {

    objThis.removeFile(objFile);
    data = {
      error: objFile.name + message
    };
    notificationdisplay_sponsored(data);
  }



  $(function () {

    $("#categories").select2().val('').trigger('change');

    $("#categories").change(function () {
      fetch("{{route('ad_cat_select')}}/" + $("#categories").val())
        .then((resp) => resp.json())
        .then(function (data) {
          console.log(data);
          $(".insertdata").html('');
          $.each(data, function (index, value) {
            var json_attr = [];
            var attr_data = value.attributes_data;

            if (attr_data != "input") {
              attr_data = attr_data.replace(", ", ",")
              var attr_dataSplit = attr_data.split(",");
              for (var i = 0; i < attr_dataSplit.length; i++) {
                json_attr.push({
                  "attr": attr_dataSplit[i]
                });
              }

              selectinput = '';
              $.each(json_attr, function (index2, value2) {
                value2_data = value2.attr;
                value2_data = value2_data.replace(", ", ",")
                selectinput = selectinput + '<option value="' + value2_data + '">' + value2_data +
                  '</option>'
              });

              inputtype = '<select name="' + value.attributes +
                '" id="input" class="form-control">' +
                selectinput + '</select>';

            } else {
              inputtype = '<input type="text" name="' + value.attributes +
                '" class="form-control" id="" placeholder="">';
            }


            appendData = '<div class="form-group">' +
              '<label for="">' + value.attributes + '</label>' + inputtype + '</div>';

            $(".insertdata").append(appendData);
          });
        })
        .catch(err => console.error(err));
    });

    

  })
</script>