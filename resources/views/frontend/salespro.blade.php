
<div class="menu">
@include('includes.staticmenuwithbg')
</div>
@include('includes.css.styleone')


<div class="container">

  <div class="row body" style="margin-bottom:140px">

    <div><h2 class="">Sales Pro</h2></div>

    <div>
      <strong>
        A sales support services designed for sellers that do not have time to post ads, negotiate, reply emails/sms, phone calls etc and want to sell. SalesPro addresses the aforementioned, from posting of ads to closing sales. For more information, please contact: <a href="mailto:support@marketam.com.ng">support@marketam.com.ng</a>
      </strong>
    </div>
    
    
  </div>
  
</div>

<style>
.header-text{
  margin:10px 0px 20px 0px;
  text-align:center;
  color:#5cb85c;
  font-weight:500;
}

.body{
  padding-bottom:100px;
}

.login-form {
    background: white;
    padding: 30px;
    border: solid 1px #b3b3b38f;
    border-radius: 8px;
    margin-bottom: 30px;
    box-shadow: -3px 7px 8px 0px rgba(0, 0, 0, 0.06);
  }
.action-buttons{
    margin-top:20px;
}

.social-login{
    margin-top: 10px;
 }
</style>





<script>
  document.title = "{{$pageTitle}}";

  $(function(){

  })
 
</script>
    
