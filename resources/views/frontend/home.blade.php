<style>
  .category-title, .mobile-category{
        display: none;
      }

      .allCategories{
        text-align:center;
      }

      .allCategories a {
          color: inherit;
          text-decoration: inherit;
      }

      .skin-green .wrapper{
                background-color: unset;
            }
      

      @media (max-width: 768px){

        .category-title, .mobile-category{
          display: block;
        }

        .mobile-category{
          margin-top:30px;
          text-align:center;
        }

        .pc-category{
          display:none
        }

        .dropdown-menu {
          position: static;
          margin: 20px;
          text-align:center;
          float: none;
        }

        .dropdown-backdrop {
            display: none;
        }

        .myh1{
          margin-top: 30px !important;
        }
      }
    </style>


@include('includes.staticmenu')
@include('includes.slider')
<div class="container">
  <div class="row pc-category" id="categories">

    <div style="text-align:center;" class="category-title">
      <h1 style="margin-top:40px" class="myh1">Categories</h1>
      <div class="header-stroke"></div>
    </div>

    <div class="allCategories">
      <ul class="list-group categories-list">
        @foreach($categories as $obj)
        <li class="list-group-item category"><a href="{{route('category',[$obj->category_slug])}}" class="ajaxlink">{{$obj->category}}</a></li>
        @endforeach
      </ul>
    </div>

  </div>

  <div class="row mobile-category">
    <div class="dropdown">
      <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown"
        aria-haspopup="true" aria-expanded="true">
        All Categories
        <span class="caret"></span>
      </button>
      <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
        @foreach($categories as $obj)
        <li class="list-group-item category"><a href="{{route('category',[$obj->category_slug])}}" class="ajaxlink">{{$obj->category}}</a></li>
        @endforeach
      </ul>
    </div>
  </div>

  <div class="row">
    <div style="text-align:center;">
      <h1 style="margin-top:70px" class="myh1">Recent Adverts</h1>
      <div class="header-stroke"></div>
    </div>

    <div class="recent-adverts">

      @foreach($adverts as $obj)
      <div class="col-sm-6 col-md-3 col-lg-3">
        <a href="{{route('adpage',[$obj->slug])}}" class="ajaxlink" style="color: inherit;">
          <div class="product-mini-details">
            <div class="product-box">
              <div class="product-image">
                <div>
                    @if(!empty($obj->advert_media->where('type','image')->first()))
                    <img src="{{route('home')}}/{{$obj->advert_media->where('type','image')->first()->link}}" alt="">
                    @endif
                </div>
              </div>
            </div>
            <h3 class="product-mini-title">{{$obj->title}}</h3>
            <!-- <p class="product-mini-location">{{$obj->location}}</p> -->
            <h4 class="product-mini-price">₦{{number_format($obj->price, 2)}}</h4>

            @if($obj->advert_type == "sponsored")
            <div class="premium-badge">
              <img src="{{route('home')}}/images/icons/premium.png" alt="" class="badge-icon">
            </div>
            @endif
          </div>
        </a>
      </div>
      @endforeach

      <div class="added-adverts"></div>


    </div>

  </div>
  <div style="margin-bottom:60px">
    <div class="" style="margin-top:30px;text-align:center">
      <!-- {{ $adverts->links() }} -->
      @if(count($adverts) >= 24)
      <a href="{{route('home')}}" class="paginateLink btn btn-success">VIEW MORE</a>
      @endif
    </div>
  </div>
</div>

<script>
  // setInterval(function(){
  //   if($( "li" ).hasClass( "category")){
  //       if($(".category:eq(0)").is(":hover") || $(".category-display:eq(0)").is(":hover")) { $(".category-display:eq(0)").show();}else{$(".category-display:eq(0)").hide();
  //       }

  //       if($(".category:eq(1)").is(":hover") || $(".category-display:eq(1)").is(":hover")) { $(".category-display:eq(1)").show();
  //       }else{ $(".category-display:eq(1)").hide();}

  //       if($(".category:eq(2)").is(":hover") || $(".category-display:eq(2)").is(":hover")) { $(".category-display:eq(2)").show();
  //       }else{ $(".category-display:eq(2)").hide();}

  //       if($(".category:eq(3)").is(":hover") || $(".category-display:eq(3)").is(":hover")) { $(".category-display:eq(3)").show();
  //       }else{ $(".category-display:eq(3)").hide();}

  //       if($(".category:eq(4)").is(":hover") || $(".category-display:eq(4)").is(":hover")) { $(".category-display:eq(4)").show();
  //       }else{ $(".category-display:eq(4)").hide();}

  //       if($(".category:eq(5)").is(":hover") || $(".category-display:eq(5)").is(":hover")) { $(".category-display:eq(5)").show();
  //       }else{ $(".category-display:eq(5)").hide();}

  //       if($(".category:eq(6)").is(":hover") || $(".category-display:eq(6)").is(":hover")) { $(".category-display:eq(6)").show();
  //       }else{ $(".category-display:eq(6)").hide();}

  //       if($(".category:eq(7)").is(":hover") || $(".category-display:eq(7)").is(":hover")) { $(".category-display:eq(7)").show();
  //       }else{ $(".category-display:eq(7)").hide();}

  //       if($(".category:eq(8)").is(":hover") || $(".category-display:eq(8)").is(":hover")) { $(".category-display:eq(8)").show();
  //       }else{ $(".category-display:eq(8)").hide();}

  //   }
  // },200);
</script>

<script>
  document.title = "{{$pageTitle}}";

  
</script>