
<div class="menu">
@include('includes.staticmenuwithbg')
</div>
@include('includes.css.styleone')


<div class="container">

  <div class="row body">

    <div class="col-md-3"></div>

    <div class="col-md-6">
      <div>
          
          <form action="{{route('postpasswordreset')}}" method="POST" role="form" class="passwordForm">
            {{csrf_field()}}
            <div class="form-group">
              <label for="">New Password</label>
              <input type="password" class="form-control" id="password" name="password">
            </div>

            <div class="form-group">
              <label for="">Confirm Password</label>
              <input type="password" class="form-control" id="password_again" name="password_again">
            </div>
          
            
          
            <button type="submit" class="btn btn-success">Submit</button>
          </form>
          
      </div>
    </div>

    <div class="col-md-3"></div>

  </div>

</div>

<style>
    .passwordForm{
      padding:30px;
      background: white;
      border-radius: 8px;
      border: solid 1px #bfbfbf;
      margin: 30px 0px 50px 0px;
    }
</style>


<script>
        document.title = "{{$pageTitle}}";
        $(".passwordForm").validate({
            rules: {
                password: {
                    minlength: 5
                },
                password_again: {
                  equalTo: "#password"
                }
            },
            submitHandler: function(form) {
                
                $(".progressBarBg").show();
                  
                $.ajax({
                    url     : $(form).attr('action'),
                    type    : $(form).attr('method'),
                    data    : $(form).serialize(),
                    success : function( data ) {

                        successdisplay(data);
                                

                    },
                    error   : function( xhr, err ) {
                        $(".progressBarBg").hide();
                        alert('Error in submitting form');     
                    }
                });  
                  
                return false;
                
            }
        });
</script>