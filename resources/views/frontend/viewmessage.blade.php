<div class="menu">
@include('includes.staticmenuwithbg')
<script src="{{route('home')}}/js/dropzone.js"></script>
</div>



<div class="container">

  <div class="body">

    <div class="row">
      <div class="col-md-3"></div>
      <div class="col-md-9">
          <h2 class="message-text">Messages</h2>
            
          <!-- <button type="button" class="btn btn-danger btn-sm" style="margin-bottom:10px">Archive Message</button> -->
      </div>
    </div>

    <div class="row">
        <div class="col-md-3">

            <div class="profile">
            @if(empty($user->image))
                <div class="display-picture" style="background-image: url({{route('home')}}/images/user.png)">
                </div>
                @else 
                <div class="display-picture" style="background-image: url({{$user->image}})">
                </div>
                @endif
                <h4 class="user-name">{{$user->full_name}}</h4>
            </div>
        
        </div>
        <div class="col-md-9">
            
            <div class="message-title">
              <h4>{{$message->advert->title}} [₦{{$message->advert->price}}]</h4>
            </div>
            <div class="message-user">
              @if($message->advert->user->id == Auth::user()->id)
                <h5>{{$message->user->full_name}}</h5>
              @else
                <h5>{{$message->advert->user->full_name}}</h5>
              @endif
            </div>
            <div class="message-display">
                <div class="message-frame">
                  @foreach($messagethread as $obj)
                  @if($obj->user_id == $obj->message->user_sender_id)
                  <div class="message-sender">
                    <div class="user-info">
                      <div class="message">
                        @if(empty($obj->image))
                          {{$obj->messages}}
                        @else 
                          <img src="{{$obj->image}}" height="200px"/>
                          <div>{{$obj->messages}}</div>
                        @endif
                      </div>
                      <div class="message-time">
                        {{date_format(new DateTime($obj->created_at), "h:i d/m/Y")}}
                      </div>
                    </div>
                    <div class="user-image">
                      <img src="{{$obj->user->image}}" width="60px"/> 
                    </div> 
                  </div>

                  @else
                  <div class="message-receiver">
                    <div class="user-image">
                      <img src="{{$obj->user->image}}" width="60px"/> 
                    </div>
                    <div class="user-info">
                      <div class="message">
                        @if(empty($obj->image))
                          {{$obj->messages}}
                        @else 
                          <img src="{{$obj->image}}" height="200px"/>
                          <div>{{$obj->messages}}</div>
                        @endif
                      </div>
                      <div class="message-time">
                        {{date_format(new DateTime($obj->created_at), "h:i d/m/Y")}}
                      </div>
                    </div>
                  </div>
                  @endif
                  @endforeach
                </div>
            </div>

            
            <form action="{{route('postsendchat')}}" method="POST" role="form" class="chatform">

              {{ csrf_field() }}
              <textarea name="message" id="input" class="form-control" rows="3"></textarea>
              <div style="margin-top:20px">

              
              <input type="hidden" name="advert_id" id="input" class="form-control" value="{{$advert_id}}">
              <input type="hidden" name="message_id" id="input" class="form-control" value="{{$message_id}}">
              
                
              <label class="control-label">Images (Image size must be less than 2MB)</label>
              <div class="dropzone-previews">
                <div class="dropzone-label">Drop images here to send</div>
              </div>
                
                <button style="margin-top:20px" type="submit" class="btn btn-success">Send</button>
              </div>

            </form>
        
        </div>
    </div>


    <div style="height:80px"></div>

  </div>

</div>

<script>

  $(function(){
    $(".menu-advert .badge").text({!! $msgBadgeCount!!});
  })

  $(".chatform").dropzone({
    paramName: "fileupload",
    autoDiscover: false,
    autoProcessQueue: false,
    parallelUploads: 20,
    maxFiles: 3,
    maxFilesize: 2,
    thumbnailWidth: 150,
    thumbnailHeight: 150,
    uploadMultiple: true,
    addRemoveLinks: true,
    acceptedFiles: "image/*",
    previewsContainer: ".dropzone-previews",
    clickable: ".dropzone-previews",
    hiddenInputContainer: ".dropzone-previews",

    init: function () {

      var chatFormDropone = this;
      $(".chatform").submit(function (event) {
        event.preventDefault();

        $(".progressBarBg").show();

        if(chatFormDropone.files.length != 0){
          chatFormDropone.processQueue();
        }else{
          
          $.ajax({
              cache: false,
              type: $(".chatform").attr('method'),
              url: $(".chatform").attr('action'),
              data: new FormData($(".chatform")[0]),
              contentType: false,
              processData: false,
              success: function (data) {
                successdisplay(data);
                notificationdisplay(data);
              },
              error: function (data) {
                  $(".progressBarBg").hide();
                  alert('Error');
              }
          });

        }
        
        

      });

      this.on("addedfile", function (file) {
        $(".dropzone-label").hide();
        $(".dropzone-previews").css({
          "height": "initial"
        });
        $(".dz-remove").addClass("btn btn-xs btn-danger");

      });

      this.on("error", function (file,response) {
        $(".progressBarBg").hide();
        this.removeFile(file);
        console.log(response);
        dropzoneNotification(this, file, response);
      });

      this.on("reset", function (file) {
        $(".dropzone-label").show();
        $(".dropzone-previews").css({
          "height": "200px"
        });
      });

      this.on("sending", function (file, xhr, formData) {
        formData = $(".chatform").serialize();
      });

      this.on("success", function (file, data) {
        $(".progressBarBg").hide();
        successdisplay(data);
        notificationdisplay(data);
      });

      this.on("uploadprogress", function (file, progress) {
        var progressElement = file.previewElement.querySelector("[data-dz-uploadprogress]");
        progressElement.style.width = progress + "%";
        progressElement.textContent = progress + "%";
      });
    }
  });


  function dropzoneNotification(objThis, objFile, message) {

    objThis.removeFile(objFile);
    data = {
      error: objFile.name + message
    };
    notificationdisplay(data);
  }
   
</script>

<style>

.user-image, .user-info{
  display:inline-block;
  vertical-align: middle;
}

.user-info{
  padding:15px;
  border-radius:8px;
  background-color:#ececec
}

.user-info .message{
  font-weight:500;
}

.message-user{
  color:white;
  background-color: #70a779;
  padding:2px 30px 2px 30px;
}

.message-user h4{
  margin: 0px;
}

.message-title{
  color:white;
  background-color:green;
  padding:20px 30px 20px 30px;
}

.message-title h4{
  margin:0;
}

.active-link{
  color:green;
  font-weight:600;
}

.user-name{
  color:#1fae37;
  margin-left: 20px;
  font-weight: 600;
}

.message-text{
  margin-top:25px;
}

.profile, .message-display{
  background-color: #fff;
  padding:30px;
  border: solid 1px #8080804f;
  margin-bottom:30px;
  box-shadow: -3px 7px 8px 0px rgba(0, 0, 0, 0.06);
}

.message-display{
  height: 450px;
  overflow: auto;
}

.display-picture{
  background-size: cover;
  width:200px;
  height:200px;
  border: solid 1px #8080804f;    
  border-radius: 20px;
  position: relative;
}

.message-content {
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  max-height: 250px;
}

.message-receiver h5, .message-sender h5{
    font-size: 16px;
}

.user-image{
    display: inline-block;
}

a:active, a:hover{
    color:inherit;
}

.message-sender{
  text-align: right;
  margin-bottom:20px;
}

.message-receiver{
  text-align: left;
  margin-bottom:20px;
}

</style>

<script>
document.title = "{{$pageTitle}}";

  $(function(){
    $('.message-display').scrollTop($('.message-display')[0].scrollHeight);
  });
 
</script>