<div class="menu">
  @include('includes.staticmenuwithbg')
</div>
@include('includes.css.styleone')

<div class="container">

  <div class="row body">

    <div>
      <h2 class="header-text">Edit Advert</h2>
    </div>

    <div class="col-md-2"></div>
    <div class="col-md-8">
      <div class="advert-form">
        <form action="{{route('postsponsoredadedit')}}" method="POST" role="form" class="adform" id="adform" enctype="multipart/form-data">
          {{csrf_field()}}
          <div class="form-group">
            <label for="">Select Category</label>
            <select name="categories" id="categories" class="form-control" disabled>
              @foreach($categories as $category_obj)
              <optgroup label="{{$category_obj->category}}">
                @foreach($category_obj->categories_subs as $sub_obj)
                <option value="{{$sub_obj->id}}">{{$sub_obj->categories_sub}}</option>
                @endforeach
              </optgroup>
              @endforeach
            </select>
          </div>

          <?php $get_category = $advert->category_sub->id;?>

          <div class="form-group">
            <label for="">Title</label>
            <input type="text" class="form-control" id="" placeholder="" name="title" value="{{$advert->title}}"
              required disabled>
          </div>

          <div class="form-group">
            <label for="">Description</label>
            <textarea name="description" id="input" class="form-control" rows="3" name="description" disabled>{{$advert->description}}</textarea>
          </div>

          <input type="hidden" class="form-control" id="" placeholder="" name="advert_id" value="{{$advert->id}}">

          <div class="insertdata">
            @foreach($advert_attr as $obj)
            <div class="form-group">
              <label for="">{{$obj->categories_sub_attribute->attributes}}</label>
              @if($obj->categories_sub_attribute->attributes_data == "input")
              <input type="text" name="{{str_replace(' ','_',$obj->categories_sub_attribute->attributes)}}" class="form-control"
                id="" placeholder="" value="{{$obj->attribute_data}}" disabled />
              @else
              <?php 
                      // $subcat = str_replace(" ","",$obj->categories_sub_attribute->attributes_data);
                      $subcat = $obj->categories_sub_attribute->attributes_data;
                      $subcat = explode(',', $subcat);
                  ?>
              <select name="{{str_replace(' ','_',$obj->categories_sub_attribute->attributes)}}" id="input" class="form-control {{str_replace(' ','_',$obj->categories_sub_attribute->attributes)}}"
                disabled>
                @foreach ($subcat as $cat)
                <option value="{{$cat}}">{{$cat}}</option>
                @endforeach
              </select>
              @endif

            </div>
            @endforeach
          </div>

          <div class="form-group">
            <label class="control-label">Price</label>
            <div class="input-group">
              <input type="number" class="form-control" aria-describedby="price-icon" name="price" step=".01" value="{{$advert->price}}"
                required>
              <span class="input-group-addon" id="price-icon">Naira</span>
            </div>
          </div>

          <div class="form-group">
            <label for="">Price Type</label>
            <select name="price_type" id="input" class="form-control" required="required">
              @if($advert->price_type == "Fixed Price")
              <option value="Fixed Price">Fixed Price</option>
              <option value="Negotiable">Negotiable</option>
              @else
              <option value="Negotiable">Negotiable</option>
              <option value="Fixed Price">Fixed Price</option>
              @endif
            </select>
          </div>

          <div class="form-group">
            <label for="">Usage</label>
            <select name="usage" id="input" class="form-control" required="required">
              @if($advert->usage == "New")
              <option value="New">New</option>
              <option value="Fairly Used">Fairly Used</option>
              @else
              <option value="Fairly Used">Fairly Used</option>
              <option value="New">New</option>
              @endif
            </select>
          </div>

          <div class="form-group">
            <label for="">Location</label>
            <!-- <input type="text" class="form-control" id="" placeholder="" name="location" value="{{$advert->location}}"
              required> -->
            <select name="location" id="location" class="form-control" required="required">
              <option value="{{$advert->location}}">{{$advert->location}}</option>
              <option value="Abuja FCT">Abuja FCT</option>
              <option value="Abia">Abia</option>
              <option value="Adamawa">Adamawa</option>
              <option value="Akwa Ibom">Akwa Ibom</option>
              <option value="Anambra">Anambra</option>
              <option value="Bauchi">Bauchi</option>
              <option value="Bayelsa">Bayelsa</option>
              <option value="Benue">Benue</option>
              <option value="Borno">Borno</option>
              <option value="Cross River">Cross River</option>
              <option value="Delta">Delta</option>
              <option value="Ebonyi">Ebonyi</option>
              <option value="Edo">Edo</option>
              <option value="Ekiti">Ekiti</option>
              <option value="Enugu">Enugu</option>
              <option value="Gombe">Gombe</option>
              <option value="Imo">Imo</option>
              <option value="Jigawa">Jigawa</option>
              <option value="Kaduna">Kaduna</option>
              <option value="Kano">Kano</option>
              <option value="Katsina">Katsina</option>
              <option value="Kebbi">Kebbi</option>
              <option value="Kogi">Kogi</option>
              <option value="Kwara">Kwara</option>
              <option value="Lagos" selected>Lagos</option>
              <option value="Nassarawa">Nassarawa</option>
              <option value="Niger">Niger</option>
              <option value="Ogun">Ogun</option>
              <option value="Ondo">Ondo</option>
              <option value="Osun">Osun</option>
              <option value="Oyo">Oyo</option>
              <option value="Plateau">Plateau</option>
              <option value="Rivers">Rivers</option>
              <option value="Sokoto">Sokoto</option>
              <option value="Taraba">Taraba</option>
              <option value="Yobe">Yobe</option>
              <option value="Zamfara">Zamfara</option>
            </select>
          </div>

          <label class="control-label"></label>
          <div class="dropzone-previews">
            <div class="dropzone-label">Drop files here to upload</div>
          </div>


          <div style="text-align:center;margin-top:10px;margin-bottom:30px">
            <button type="submit" class="btn btn-success btn-lg">Update</button>
          </div>
        </form>
      </div>
    </div>
    <div class="col-md-2"></div>


  </div>

</div>

<style>
  .header-text {
    margin: 10px 0px 20px 0px;
    text-align: center;
    color: #5cb85c;
    font-weight: 500;
  }

  .body {
    padding-bottom: 100px;
  }

  .advert-form {
    background: white;
    padding: 25px;
    border: solid 1px #b3b3b38f;
    border-radius: 8px;
    margin-bottom: 30px;
    box-shadow: -3px 7px 8px 0px rgba(0, 0, 0, 0.06);
  }

  .action-buttons {
    margin-top: 20px;
  }
</style>


<script>
  document.title = "{{$pageTitle}}";

  homelink = "{{route('home')}}";

  fileFromServer = {!! $fileObject !!};

  fileslength = 0;

  if ({!!$advert_attr!!}) {

    ad_categories = {!!$advert_attr!!};

    $.each(ad_categories, function (key, value) {
      if (value.categories_sub_attribute.attributes_data != "input") {

        trimvalue = value.categories_sub_attribute.attributes.replace(new RegExp(' ', 'g'), '_');
        $("." + trimvalue).val(value.attribute_data);
      }

    });
  }

  $(function () {
    $("#categories").select2().val('{!! $get_category !!}').trigger('change');
  });



  $(".adform").dropzone({
    paramName: "fileupload",
    url: "{{route('sponsorededitupload')}}",
    autoDiscover: false,
    autoProcessQueue: true,
    maxFiles: 8,
    maxFilesize: 80,
    thumbnailWidth: 150,
    thumbnailHeight: 150,
    uploadMultiple: false,
    addRemoveLinks: true,
    acceptedFiles: "image/png,image/jpeg,image/jpg,video/mp4,audio/mp3",
    previewsContainer: ".dropzone-previews",
    clickable: ".dropzone-previews",
    hiddenInputContainer: ".dropzone-previews",

    init: function () {

      
      var adformDropzone = this;

      fileslength = adformDropzone.files.length;
      console.log('maxfiles ' + adformDropzone.options.maxFiles);
      console.log('fileslength ' + adformDropzone.files.length);

      $(".adform").submit(function (event) {
        event.preventDefault();

        

        $(".progressBarBg").show();

        fetch($(".adform").attr('action'), {
          method: $(".adform").attr('method'),
          body: new FormData($(".adform")[0])
        })
        .then((data) => data.json())
        .then(function (data) {
            // $(".progressBarBg").hide();
            // notificationdisplay(data);
            $.ajax({
              cache: false,
              type: "GET",
              url: data.link,
              success: function (data) {
                  successdisplay(data);

              },
              error: function (data) {
                  $(".progressBarBg").hide();
                  alert("An error occured");
                  console.log('Error:', data);
              }
          });
        })
        .catch(function (err) {
          $(".progressBarBg").hide();
          alert('Error');
        });


      });

      $.each(fileFromServer, function (key, value) {

              fileslength = fileslength + 1;

              var file = {name: value.original, size: value.size};
              
              adformDropzone.options.addedfile.call(adformDropzone, file);
              adformDropzone.options.thumbnail.call(adformDropzone, file, value.server);
              adformDropzone.emit("complete", file);

              $(".dropzone-label").hide();
              $(".dropzone-previews").css({
                "height": "initial"
              });
              $(".dz-remove").addClass("btn btn-xs btn-danger");

              adformDropzone.options.maxFiles = adformDropzone.options.maxFiles - 1;
            
      });


      this.on("addedfile", function (file) {

        
      console.log('maxfiles ' + adformDropzone.options.maxFiles);
      console.log('fileslength ' + adformDropzone.files.length);

        $(".dropzone-label").hide();
        $(".dropzone-previews").css({
          "height": "initial"
        });
        $(".dz-remove").addClass("btn btn-xs btn-danger");

        if (file.type.includes("video")) {
          file.previewElement.querySelector("img").src = homelink + "/images/play-button.png";
        }

        if (file.type.includes("audio")) {
          file.previewElement.querySelector("img").src = homelink + "/images/music-icon.png";
        }

      });

      this.on("removedfile", function (file) {

      fileslength--;
      console.log('maxfiles ' + adformDropzone.options.maxFiles);
      console.log('fileslength ' + adformDropzone.files.length);

        filename = $(file.previewElement).find('[data-dz-name]').text();
        
        var u_data = {'id': filename,'_token': "{{ csrf_token() }}"};

        $.ajax({
                cache: false,
                url: "{{route('delete_advert_media')}}",
                type: 'POST',
                data: u_data,
                success: function(data){
                    if(data == "success")
                    {
                        console.log("file removed from server successfully");
                    }

                }
            });

      });

      this.on("error", function (file,response) {
        this.removeFile(file);
        data = {
          error: response
        };
        notificationdisplay(data);
      });

      this.on("reset", function (file) {

        if(fileslength == 0){
          $(".dropzone-label").show();
          $(".dropzone-previews").css({
            "height": "200px"
          });
        }
        
      });

      this.on("sending", function (file, xhr, formData) {
        formData = $(".adform").serialize();
      });

      this.on("uploadprogress", function (file, progress) {
        var progressElement = file.previewElement.querySelector("[data-dz-uploadprogress]");
        progressElement.style.width = progress + "%";
        progressElement.textContent = progress + "%";
      });

      this.on("success", function (file, data) {
        $(".progressBarBg").hide();
        if(data.error){
          this.removeFile(file);
        }

        if(data.filename){
          file.renameFilename = data.filename;
          $(file.previewElement).find('[data-dz-name]').text(data.filename);
          fileslength++;
        }

        

        notificationdisplay(data);
      });

    }
  });
</script>