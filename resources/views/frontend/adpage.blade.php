<div class="menu">
  @include('includes.staticmenuwithbg')
</div>

<div class="container">

  <div class="row body">

    <div class="col-md-8">
      <div class="ad-container">
        @if(!empty(Auth::user()))
          @if(Auth::user()->role != "user")
            @if($advert->active == "active")
                <a style="margin-bottom:30px" href="{{route('admin.advertsaction',['blocked',$advert->id])}}" class="btn btn-danger btn-lg ajaxlink">Block Ad</a>
            @endif
            @if($advert->active == "blocked")
                <a style="margin-bottom:30px" href="{{route('admin.advertsaction',['active',$advert->id])}}" class="btn btn-success btn-lg ajaxlink">Unblock Ad</a>
            @endif
            @if($advert->active == "new")
                <a style="margin-bottom:30px" href="{{route('admin.advertsaction',['declined',$advert->id])}}" class="btn btn-danger ajaxlink">Decline Ad</a>
                <a style="margin-bottom:30px" href="{{route('admin.advertsaction',['active',$advert->id])}}" class="btn btn-success btn-lg ajaxlink">Accept Ad</a>
            @endif
            @if($advert->active == "declined")
                <a style="margin-bottom:30px" href="{{route('admin.advertsaction',['active',$advert->id])}}" class="btn btn-success btn-lg ajaxlink">Accept Ad</a>
            @endif
          
          @else

            @if($advert->active == "new")
                <div class="btn btn-info advert-notification">
                  <div> Your advert is being processed. It is under review </div>
                </div>
            @endif

            @if($advert->active == "declined")
                <div class="btn btn-danger advert-notification">
                  <div> This advert has been declined for not following our <a href="{{URL::to('/terms-and-conditions')}}">terms and conditions</a> </div>
                </div>
            @endif

            @if($advert->active == "blocked")
                <div class="btn btn-danger advert-notification">
                  <div> This advert has been blocked for not following our  <a href="{{URL::to('/terms-and-conditions')}}">terms and conditions</a> </div>
                </div>
            @endif

            @if($advert->active == "sold")
                <div class="btn btn-success advert-notification">
                  <div> This advert product has been sold </div>
                </div>
            @endif

          @endif
        @endif
        <h2 class="product-title">{{$advert->title}} <small><strong>({{$advert->usage}})</strong></small></h2>
        <div class="ad-subdetails">
            <!-- <i class="fa fa-map-marker"></i><span> {{$advert->location}}</span>  -->
            <i class="fa fa-clock-o"></i><span> {{date_format(new DateTime($advert->updated_at), "M-d h:i A")}}</span> 
            <i class="fa fa-asterisk"></i><span> {{$advert->category_sub->categories_sub}}</span> 
            <i class="fa fa-eye"></i><span> {{$advert->view_count}}</span> 
            
            @if(Auth::user())
            @if(empty($checkFavourite))
            <button data-link="{{route('favourite',['add',$advert->id])}}" class="btn btn-danger btn-sm ajaxfavourite"><i class="fa fa-heart"></i> Add To Favourites</button>
            @else
            <button data-link="{{route('favourite',['remove',$advert->id])}}" class="btn btn-success btn-sm ajaxfavourite"><i class="fa fa-heart"></i> Added To Favourites</button>
            @endif
            @endif
            </div>

        <div class="">
          <div id="productCarousel" class="carousel slide" data-interval="false">

            <div class="carousel-inner" role="listbox">
              @foreach($advert->advert_media as $obj)
              @if($obj->type == "image")
              <div class="item">
                <img src="{{route('home')}}/{{$obj->link}}" alt="XZ" style="width: 80%; height: 440px">
              </div>
              @endif
              @endforeach

              <a class="left carousel-control" href="#productCarousel" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="right carousel-control" href="#productCarousel" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>

            <div id="thumbCarousel">
              <?php $thumbCount = 0; ?>
              @foreach($advert->advert_media as $index=>$obj)
              @if($obj->type == "image")
              <div data-target="#productCarousel" data-slide-to="{{$thumbCount}}" class="thumb">
                <img src="{{route('home')}}/{{$obj->link}}" alt="XZ" style="width: 90px; height: 100px">
              </div>
              <?php $thumbCount++; ?>
              @endif
              @endforeach
            </div>

          </div>
        </div>

        @foreach($advert->advert_media as $audio)
            @if($audio->type == "audio")
              <div class="" style="margin:10px 0px 10px">
                <audio controls="controls" src="{{route('home')}}/{{$audio->link}}">
                </audio>
              </div>
            @endif
        @endforeach

        @foreach($advert->advert_media as $video)
            @if($video->type == "video")
              <div class="" style="margin:10px 0px 10px">
              <video id="my-video" class="video-js" controls preload="auto" width="640" height="264" poster="{{route('home')}}/images/Marketam2.png" data-setup="{}">
                <source src="{{route('home')}}/{{$video->link}}" type='video/mp4'>
                <p class="vjs-no-js">
                  To view this video please enable JavaScript, and consider upgrading to a web browser that
                  <a href="https://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                </p>
              </video>
              </div>
            @endif
        @endforeach

        <div><strong>Location:</strong> {{$advert->location}}</div>
        <!-- <div id="map">

            <script type="text/javascript">
                // Initialize and add the map
                function initMap() {

                  var location = "{{$advert->location}}";
                  //var location = "Lagos, Nigeria";

                  $.ajax({
                    cache: false,
                    type: "GET",
                    url: "https://maps.googleapis.com/maps/api/geocode/json?address=" + location + "&key=AIzaSyAS1qdCgymbNVvhkB5yOsPucKC7-Vj8ZIE",
                    success: function (data) {
                      console.log(data);
                      var uluru = data.results[0].geometry.location;
                      var map = new google.maps.Map(document.getElementById('map'), {zoom: 4, center: uluru});
                      var marker = new google.maps.Marker({position: uluru, map: map});

                    },
                    error: function (data) {
                        alert('There was an error in displaying maps. Please reload the page again. Thank you')
                    }
                  });

                  
                }
            </script>

            <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAaMaE7075DmU55R6YL-W_wRZ1-zbDk2Uc&callback=initMap"></script>
        </div> -->

        <div class="" style="margin-top:15px"><a href="{{route('freead')}}" class="btn btn-success ajaxlink" style="width:100%">Post a Free Ad Like This</a></div>
        <div class="specifications">
          <h3>Specifications</h3>
          @if(count($advert->advert_attributes) != 0)
            @foreach($advert->advert_attributes as $obj)
            <div><span>{{$obj->categories_sub_attribute->attributes}}</span> {{$obj->attribute_data}}</div>
            @endforeach
          @else 
            <p><em>There are no specifications for this advert</em></p>
          @endif

          <h3 style="margin-top:50px">Ad Details</h3>
          <div>{{$advert->description}}</div>

          <div style="margin-top:50px">
            <i class="fa fa-mobile fa-2x"></i>
            <h4 class="mobilephone">{{$advert->user->phone_number}}</h4>
          </div>

        </div>

        <div id="share"></div>

        <!-- <div class="reportabuse"><a href="#" class="btn btn-danger btn-sm"><i class="fa fa-flag" style="color:white"></i>
            Report Abuse</a></div> -->
      </div>
    </div>
    <div class="col-md-4">
      <div class="ad-owner">
        <div class="ad-price">
          <h3>
            ₦{{number_format($advert->price, 2)}} <br>
            <small style="color:white">({{$advert->price_type}})</small>
          </h3>
        </div>
        <div class="ad-user">
          <img src="{{$advert->user->image}}" width="90px"> 
          <h3 style="margin-top:0px;display:inline-block">
          
            @if($advert->user->display_name_type == "full_name")
            {{$advert->user->full_name}}
            @else 
            {{$advert->user->company_name}}
            @endif
            
            <i class="fa fa-check-circle" style="color:#1fae37"></i> <br>

            <a href="{{route('shopadverts',$advert->user->id)}}" style="margin-top:5px" class="btn btn-success btn-sm ajaxlink">View Shop</a>
          
          </h3>
            
          
            
          <div>Last seen: {{date_format(new DateTime($advert->user->last_login), "d/m/Y h:i A")}}</div>
          <div style="margin-top:20px">
            <i class="fa fa-mobile fa-2x"></i>
            @if(!empty(Auth::user()))
            <h4 class="mobilephone">{{$advert->user->phone_number}}</h4>
            @else 
            <h4>Login to have access to phone number</h4>
            @endif
          </div>
          <div style="margin-top:20px">
            <strong>Please read safety tips</strong>
            <ul>
              <li>Do not pay in advance even for the delivery</li>
              <li>Meet face to face</li>
              <li>Meet in a public and safe location</li>
              <li>Check item BEFORE you buy</li>
              <li>Pay only after verifying and collecting the item</li>
            </ul>
          </div>
          <div style="margin-top:60px;margin-bottom:40px">
            
            @if(Auth::id() == $advert->user_id)
              
              <div class="action-buttons">
                @if($advert->active == "active")
                <button data-link="{{route('myadvertsaction',['sold',$advert->id])}}" class="btn btn-sm btn-success ajax_ad_action">Product Sold</button>
                <button data-link="{{route('myadvertsaction',['deactivate',$advert->id])}}" class="btn btn-sm btn-info ajax_ad_action">Deactivate Ad</button> 
                @endif

                @if($advert->active == "deactivated")
                  <button data-link="{{route('myadvertsaction',['active',$obj->id])}}" class="btn btn-sm btn-success ajax_ad_action">Activate</button> 
                @endif


                <a href="{{route('editadvert',$advert->slug)}}" class="btn btn-sm btn-warning ajaxlink">Edit Ad</a> 
                <!-- <button data-link="{{route('myadvertsaction',['delete',$advert->id])}}" class="btn btn-sm btn-danger ajax_ad_action">Delete Ad</button> -->
              </div>
              
            @else
              @if(empty($message))
                <h4 style="color:#1fae37"><i class="fa fa-envelope"></i> Send Message To Ad Owner</h4>
                @if(!empty(Auth::user()))
                <form action="{{route('sendmessage')}}" method="POST" role="form" class="sendmessage">
                    
                    {{csrf_field()}}
                  <div class="form-group">
                    <textarea name="message" id="input" class="form-control" rows="5" required="required"></textarea>
                    <input type="hidden" name="advert_id" id="input" class="form-control" value="{{$advert->id}}">
                    
                  </div>

                  <button type="submit" class="btn btn-success" style="width:100%; text-align:center">Start Chat</button>
                </form>
                @else 
                <h5>You must be logged in to send a message to advert owner</h5>
                @endif

                <style>
                  .messageSent{
                    display:none;
                  }
                </style>

              @endif

            <div class="messageSent">
              <a href="{{route('messages')}}" class="btn btn-success ajaxlink" style="width:100%; text-align:center">View Messages</a>
            </div>
            @endif

            @if(Auth::user())
                
                <a class="btn btn-sm btn-danger" data-toggle="modal" href='#modal-id' style="width:100%; text-align:center; margin-top:30px">Report Ad</a>
                <div class="modal fade" id="modal-id">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Report Ad</h4>
                      </div>
                      <form action="{{route('reportadvert')}}" method="POST" role="form" class="reportadvert">
                        {{csrf_field()}}
                        <div class="modal-body">
                            
                          <div class="form-group">
                            <label for="">Please state your reason</label>
                            <input type="hidden" name="advert_id" value="{{$advert->id}}">
                            <textarea name="reason" id="input" class="form-control" rows="4" required="required"></textarea>
                            
                          </div>
                            
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <button type="submit" class="btn btn-danger">Report</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
                
            @endif
          </div>
        </div>
      </div>
    </div>

    
    <div class="col-md-8">
      <div class="similarads">
        <h3>Similar adverts</h3>
        @if(count($similarAdverts) != 0)

          @foreach($similarAdverts as $obj)
          <?php 
            $advert_media_image = $obj->advert_media->where('type', 'image')->first();
          ?>
          <div class="ad-container">
            <div class="row">
              <div class="col-md-4">
                <div class="ad-image-container">
                  <a href="{{route('adpage',[$obj->slug])}}" class="ajaxlink">
                    <div class="ad-image">
                      @if($advert_media_image != null )
                      <img src="{{route('home')}}/{{$advert_media_image->link}}" alt="">
                      @endif
                    </div>
                  </a>
                </div>
              </div>
              <div class="col-md-8">
                <div class="row">
                  @if($advert_media_image != null )
                  <div class="col-md-7"><a href="{{$obj->advert_media->where('type','image')->first()->link}}" class="ajaxlink">
                      <h3 class="product-title">{{$obj->title}}</h3>
                    </a>
                  </div>
                  <div class="col-md-5">
                    <h3 class="product-price">₦{{number_format($obj->price, 2)}}</h3>
                  </div>
                  @endif
                </div>

                <div class="product-description">
                  <p>{{$obj->description}}</p>
                </div>

                <div class="product-location">
                  <i class="fa fa-map-marker"></i> {{$obj->location}}
                </div>
                <div class="product-subcategory">
                  <i class="fa fa-asterisk"></i> {{$obj->category_sub->categories_sub}}
                </div>
                <div class="product-timestamp">
                  <i class="fa fa-clock-o"></i> {{date_format(new DateTime($obj->created_at), "M-d h:i A")}}
                </div>

                
                <div class="product-favourite">
                  @if(Auth::user())
                  @if(empty($obj->favourite()->where("user_id",Auth::id())->first()))
                  <button data-link="{{route('favourite',['add',$obj->id])}}" class="btn btn-danger btn-sm ajaxfavourite"><i class="fa fa-heart"></i> Add To Favourites</button>
                  @else
                  <button data-link="{{route('favourite',['remove',$obj->id])}}" class="btn btn-success btn-sm ajaxfavourite"><i class="fa fa-heart"></i> Added To Favourites</button>
                  @endif
                  @endif
                </div>
              </div>
            </div>
          </div>
          @endforeach

        @else 
        <p> This advert has no similar advert</p>
        @endif
      </div>

    </div>
    <div class="col-md-4">

    </div>


  </div>

</div>


@include('includes.css.styleone')
@include('includes.css.ads')

<style>
  .ad-price {
    padding-top: 30px;
    padding-bottom: 30px;
    background-color: #1fae37;
    text-align: center;
  }

  .ad-price h3 {
    margin-top: 0px;
    margin-bottom: 0px;
    color: white;
  }

  .ad-user {
    background-color: #fff;
    padding: 20px;
    border: solid 1px #8080804f;
    margin-bottom: 50px;
    box-shadow: -3px 7px 8px 0px rgba(0, 0, 0, 0.06);
  }

  .showsimilarads {
    margin-top: 40px;
    margin-bottom: 80px;
  }

  .similarads {
    margin-top: 40px;
  }

  .reportabuse {
    margin-top: 40px;
    /* text-align:center; */
  }

  .mobilephone {
    font-size: 18px;
    display: inline-block;
  }

  .specifications span {
    width: 300px;
    font-weight: 600;
    display: inline-block;
  }

  .specifications div {
    margin-bottom: 15px;
  }

  .specifications h3 {
    margin-bottom: 15px;
  }

  .specifications li {
    font-weight: 500;
  }

  .ad-container {
    background-color: #fff;
    padding: 30px;
    border: solid 1px #8080804f;
    margin-bottom: 50px;
    box-shadow: -3px 7px 8px 0px rgba(0, 0, 0, 0.06);
  }

  .ad-subdetails span {
    margin-right: 20px;
    font-weight: 500;
  }

  .product-title {
    color: green
  }

  .vjs-poster {
    background-color: #ffffff;
  }
</style>

<!--map-->

<style>
 #map {
   width: 100%;
   height: 400px;
   background: url("{{route('home')}}/images/maps.jpg");
   
   background-color: grey;
 }
</style>

<!-- Gallery Slider CSS-->
<style>
  .carousel-inner>.item>img,
  .carousel-inner>.item>a>img {
    margin: auto;
    margin: auto;
    object-fit: contain;
  }

  #productCarousel {
    max-width: inherit;
    margin: 20px 0;
    background: #fff;
  }

  #thumbCarousel {
    max-width: inherit;
    margin: 0 auto;
    overflow: hidden;
    background: #eaeaea;
    padding: 10px 0;
  }

  #thumbCarousel .thumb {
    float: left;
    margin-left: 10px;
    margin-right: 5px;
    border: 1px solid #ccc;
    background: #fff;
  }

  #thumbCarousel .thumb:last-child {
    margin-right: 0;
  }

  .thumb:hover {
    cursor: pointer;
  }

  .thumb img {
    opacity: 0.5;
    object-fit: cover;
  }

  .thumb img:hover {
    opacity: 1;
  }

  .thumb.active img {
    opacity: 1;
    border: 1px solid #1d62b7;
    object-fit: contain;
  }

  .carousel-control.left,
  .carousel-control.right {
    background-image: none;
  }

  .carousel-control .glyphicon-chevron-left,
  .carousel-control .icon-prev,
  .carousel-control .glyphicon-chevron-right {
    color: black;
    opacity: 0.4;
  }

  @media (max-width:480px) {
    #thumbCarousel {
      max-width: inherit;
      margin: 0 auto;
      overflow: hidden;
      background: #eaeaea;
      padding: 10px 0;
      /* display: none; */
    }
  }

  @media (max-width:768px) {
    .product-title {
      margin-top: 20px;
    }

    .product-price {
      color: green;
      text-align: left;
    }
  }
</style>

<!-- Gallery Slider Js -->
<script>
  $(document).ready(function () {

    $(".thumb").eq(0).addClass("active");
    $(".item").eq(0).addClass("active");

    //Show carousel-control
    $("#productCarousel .carousel-control").show();
    $("#productCarousel").mouseover(function () {
      //$("#productCarousel .carousel-control").show();
    });

    $("#productCarousel").mouseleave(function () {
      // $("#productCarousel .carousel-control").hide();
    });

    //Active thumbnail

    $("#thumbCarousel .thumb").on("click", function () {
      $(this).addClass("active");
      $(this).siblings().removeClass("active");

    });

    //When the carousel slides, auto update

    $('#productCarousel').on('slid.bs.carousel', function () {
      var index = $('.carousel-inner .item.active').index();
      //console.log(index);
      var thumbnailActive = $('#thumbCarousel .thumb[data-slide-to="' + index + '"]');
      thumbnailActive.addClass('active');
      $(thumbnailActive).siblings().removeClass("active");
      //console.log($(thumbnailActive).siblings()); 
    });
  });
</script>

<script>
  document.title = "{{$pageTitle}}";

  $(".sendmessage").submit(function(event){
    event.preventDefault();
    
    $(".progressBarBg").show();

    fetch($(".sendmessage").attr('action'), {
          method: $(".sendmessage").attr('method'),
          body: new FormData($(".sendmessage")[0])
        })
        .then((data) => data.json())
        .then(function (data) {
          $(".progressBarBg").hide();
          notificationdisplay(data);

          $(".sendmessage").hide();
          $(".messageSent").show();

        })
        .catch(function (err) {
          $(".progressBarBg").hide();
          alert(err);
        });
        
  });


  $(".reportadvert").submit(function(event){
    event.preventDefault();
    
    $(".progressBarBg").show();

    fetch($(".reportadvert").attr('action'), {
          method: $(".reportadvert").attr('method'),
          body: new FormData($(".reportadvert")[0])
        })
        .then((data) => data.json())
        .then(function (data) {
          $(".progressBarBg").hide();
          notificationdisplay(data);

          $(".reportadvert").hide();

        })
        .catch(function (err) {
          $(".progressBarBg").hide();
          alert(err);
        });
        
  });


  $(".ajax_ad_action").click(function(event){
        event.preventDefault();
        $(".progressBarBg").show();
        $(".alert").hide();

        $currentElement = $(this);

      
        $link = $(this).attr("data-link");

        fetch($link)
            .then((resp) => resp.json())
            .then(function (data) {
                
                $.ajax({
                  cache: false,
                  type: "GET",
                  url: data.link,
                  success: function (data) {
                      successdisplay(data);

                  },
                  error: function (data) {
                      $(".progressBarBg").hide();
                      alert("An error occured");
                      console.log('Error:', data);
                  }
              });
                
            })
            .catch(err => console.error(err));
        });



</script>

<script>
  $("#share").jsSocials({
    shareIn: "popup",
    showCount: true,
    showLabel: true,
    shares: [{
      share: "facebook",
      label: "Share"
    }, "twitter", "whatsapp", "googleplus", "linkedin"]
  });
</script>