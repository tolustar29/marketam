


<div style="background:#398439;padding:30px;border-radius:8px">
    <div style="background:white;padding:30px;border-radius:8px">
        <div align="center"><img src="{{route('index')}}/images/Marketam2.png" style="width:  auto;height: 80px;"> </div>
        <h2>Password Reset</h2>
        <p>Hi {{$name}}, click the link below to reset your password</p>

        <p style="margin-top:5px"><strong><a href="{{route('reset_password',$code)}}" target="_blank">Reset Password</a></strong></p>
    </div>
</div>