<aside class="main-sidebar">

    <section class="sidebar">
      <ul class="sidebar-menu">
        <li class="active dashboard">
          <a href="{{route('admin.home')}}" class="ajaxlink">
            <i class="fa fa-th"></i> <span>Dashboard</span>
          </a>
        </li>
        <li class="ajaxlink advert">
          <a href="{{route('admin.adverts')}}" class="ajaxlink">
            <i class="fa fa-picture-o"></i> <span>Adverts</span>
          </a>
        </li>
        <li class="ajaxlink reported_advert">
          <a href="{{route('admin.reported_adverts')}}" class="ajaxlink">
            <i class="fa fa-microphone"></i> <span>Reported Adverts</span>
          </a>
        </li>
        <li class="ajaxlink allusers">
          <a href="{{route('admin.allusers')}}" class="ajaxlink">
            <i class="fa fa-users"></i> <span>All Users</span>
          </a>
        </li>
        <li class="ajaxlink allmoderators">
          <a href="{{route('admin.moderators')}}" class="ajaxlink">
            <i class="fa fa-user-md"></i> <span>Moderators</span>
          </a>
        </li>
        <li class="ajaxlink categories">
          <a href="{{route('admin.categories')}}" class="ajaxlink">
            <i class="fa fa-th-list"></i> <span>Categories</span>
          </a>
        </li>
        <li class="ajaxlink footer_pages">
          <a href="{{route('admin.footer_pages')}}" class="ajaxlink">
            <i class="fa fa-file-text-o"></i> <span>Footer Pages</span>
          </a>
        </li>
        <!-- <li>
          <a href="pages/widgets.html">
            <i class="fa fa-cogs"></i> <span>Settings</span>
          </a>
        </li> -->
        <li>
          <a href="{{route('logout')}}" class="ajaxlink">
            <i class="fa fa-reply"></i> <span>Logout</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
</aside>