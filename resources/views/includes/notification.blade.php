<div class="alert">
    <button type="button" class="close alertclose">&times;</button>
    <div class="alert-body">
    </div>
</div>


<style>
    .alert-success,
    .alert-danger {
        position: fixed;
        z-index: 2000;
        color: white;
        border-radius: 5px;
        text-align: center;
        width: 400px;
        right: 0;
        left: 0;
        top: 30px;
        margin: auto;
        border: none;
    }

    @media (max-width: 450px) {

        .alert-success,
        .alert-danger {
            width: 90%;
        }
    }

    .alert {
        display: none;
    }

    .alert-success {
        background: #4b984b;
    }

    .alert-danger {
        background: #d41010;
    }
</style>

<script>
    $(".alertclose").click(function () {
        $(".alert").slideUp("slow");
    })
</script>

@if(isset($success))
<script>
    $(".alert").addClass("alert-success");
    $(".alert-body").html("{{$success}}");
    $(".alert").slideDown("slow");
</script>
@endif

@if(isset($error))
<script>
    $(".alert").addClass("alert-danger");
    $(".alert-body").html("{{$error}}");
    $(".alert").slideDown("slow");
</script>
@endif