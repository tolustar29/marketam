<?php 

    if(Auth::user()){
        

        $favourites_count = App\Model\Favourites::where("user_id",Auth::user()->id)->count();

        $myadverts_count = App\Model\Advert\Advert::where("user_id",Auth::user()->id)->where("active","active")->count();

      
        $messages_count = App\Model\Messages\Message::where("active_recipient",Auth::user()->id)->where("readmessage",0)->count();

        $notification_count = App\Model\Notification::where("user_id",Auth::user()->id)->where("readmessage","unread")->count();
        
            
       
    }
    

?>

<!-- menu -->
<nav class="navbar navbar-default navbar-static-top" style="padding: 0 2vw;">
            <div class="container-fluid">
                <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <i class="fa fa-bars" aria-hidden="true"></i>
                </button>
                <a class="navbar-brand menu-home ajaxlink" href="{{route('home')}}"><img src="{{route('home')}}/images/Marketam2.png" class="img-responsive" alt="Marketam" width="130px">
                </a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    @if(Auth::user())
                    <li>
                        <a href="{{route('favourite')}}" class="ajaxlink menu-favourite">My Favourites <span class="badge">{{$favourites_count}}</span></a>
                    </li>
                    <li>
                        <a href="{{route('myadverts')}}" class="ajaxlink menu-advert">My Adverts <span class="badge">{{$myadverts_count}}</span></a>
                    </li>
                    <li>
                        <a href="{{route('messages')}}" class="ajaxlink">Messages <span class="badge">{{$messages_count}}</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{route('notifications')}}" class="ajaxlink">Notifications <span class="badge">{{$notification_count}}</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{route('userprofile')}}" class="ajaxlink">Profile</a>
                    </li>
                    @endif
                    @if(!Auth::user())
                    <li>
                        <a href="{{route('login')}}" class="ajaxlink">Login</a>
                    </li>
                    <li>
                        <a href="{{route('register')}}" class="ajaxlink">Register</a>
                    </li>
                    @endif
                        
                    <li>
                        <a href="{{route('freead')}}" class="ajaxlink">Free Ad</a>
                    </li>
                    <li>
                        <a href="{{route('sponsoredad')}}" class="ajaxlink">Sponsored Ad</a>
                    </li>
                    <li>
                        <a href="{{route('salespro')}}" class="ajaxlink">Sales Pro</a>
                    </li>
                    @if(Auth::user())
                    <li>
                        <a href="{{route('logout')}}" class="ajaxlink">Logout</a>
                    </li>
                    @endif
                </ul>
                </div>
             </div>
        </nav>


        <style>
            .navbar-default {
                margin-bottom: 0px;
                background:transparent;
                border-color: transparent;

                /* position: absolute;
                top: 0;
                left: 5%;
                right: 5%; */
                }

        </style>
        