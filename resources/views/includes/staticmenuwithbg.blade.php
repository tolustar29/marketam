@include('includes.staticmenu')
<!-- menu -->



<div class="container-fluid" style="background:white">
    <div class="search-product" style="text-align:center">

        <form action="" method="POST" class="form-inline" role="form">

            <div class="form-group">
                <input name="product" type="text" class="form-control input-search product-search-box" id="" placeholder="Type your search here (e.g Infinix Hot 5, Toyota Camry)">
            </div>

            <div class="form-group">
                <select name="state" id="state" class="form-control select-state" required="required">
                    <option value="Abuja FCT">Abuja FCT</option>
                    <option value="Abia">Abia</option>
                    <option value="Adamawa">Adamawa</option>
                    <option value="Akwa Ibom">Akwa Ibom</option>
                    <option value="Anambra">Anambra</option>
                    <option value="Bauchi">Bauchi</option>
                    <option value="Bayelsa">Bayelsa</option>
                    <option value="Benue">Benue</option>
                    <option value="Borno">Borno</option>
                    <option value="Cross River">Cross River</option>
                    <option value="Delta">Delta</option>
                    <option value="Ebonyi">Ebonyi</option>
                    <option value="Edo">Edo</option>
                    <option value="Ekiti">Ekiti</option>
                    <option value="Enugu">Enugu</option>
                    <option value="Gombe">Gombe</option>
                    <option value="Imo">Imo</option>
                    <option value="Jigawa">Jigawa</option>
                    <option value="Kaduna">Kaduna</option>
                    <option value="Kano">Kano</option>
                    <option value="Katsina">Katsina</option>
                    <option value="Kebbi">Kebbi</option>
                    <option value="Kogi">Kogi</option>
                    <option value="Kwara">Kwara</option>
                    <option value="Lagos" selected>Lagos</option>
                    <option value="Nassarawa">Nassarawa</option>
                    <option value="Niger">Niger</option>
                    <option value="Ogun">Ogun</option>
                    <option value="Ondo">Ondo</option>
                    <option value="Osun">Osun</option>
                    <option value="Oyo">Oyo</option>
                    <option value="Plateau">Plateau</option>
                    <option value="Rivers">Rivers</option>
                    <option value="Sokoto">Sokoto</option>
                    <option value="Taraba">Taraba</option>
                    <option value="Yobe">Yobe</option>
                    <option value="Zamfara">Zamfara</option>
                </select>
            </div>

            <a href="{{route('searchad')}}" class="ajaxlink btn btn-success product-search">Search</a>
        </form>

    </div>
</div>


<style>
    .navbar-default {
        background: white;
        border-color: transparent;
    }

    .navbar-default .navbar-nav>li>a,
    .navbar-default .navbar-brand {
        font-weight: 500;
        color: #3ca03c;
        font-size: 14px;
    }

    .navbar-default .navbar-nav>li>a:focus,
    .navbar-default .navbar-nav>li>a:hover,
    .navbar-default .navbar-brand:focus,
    .navbar-default .navbar-brand:hover {
        color: green;
        font-weight: 600;
        background-color: transparent;
    }

    .navbar {
        margin-bottom: 0px;
    }


    .search-product {
        padding: 20px;
        width: 100%;
    }

    .menu {
        border-bottom: solid 1px #8080804f;
    }

    @media (max-width: 767px) {
        i.fa.fa-bars {
            color: #3ca03cf2;
        }

        .navbar-default .navbar-nav>li>a,
        .navbar-default .navbar-brand {
            font-weight: 600;
            color: white;
        }

        .search-product {
            padding: 20px;
            position: relative;
            width: 100%;
            margin-top: 0px;
        }
    }

    @media (min-width: 768px) {
        input.form-control.input-search {
            width: 500px;
            margin-right: 5px;
        }

        select#state {
            width: 150px;
            margin-right: 5px;
        }
    }
</style>