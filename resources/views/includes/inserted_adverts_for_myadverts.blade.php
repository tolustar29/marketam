@foreach($adverts as $obj)
<div class="ads-container">
  <div class="row">
    <div class="col-md-3">
      <div class="ad-image-container">
        <a href="{{route('adpage',[$obj->slug])}}" class="ajaxlink">
          <div class="ad-image">
            <img src="{{route('home')}}/{{$obj->advert_media->where('type','image')->first()->link}}" alt="">
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-9">
      <div class="row">
        <div class="col-md-8"><a href="{{$obj->advert_media->where('type','image')->first()->link}}" class="ajaxlink">
            <h3 class="product-title">{{$obj->title}}</h3>
          </a></div>
        <div class="col-md-4">
          <h3 class="product-price">₦{{number_format($obj->price, 2)}}</h3>
        </div>
      </div>

      <div class="product-description">
        <p>{{$obj->description}}</p>
      </div>

      <div class="product-location">
        <i class="fa fa-map-marker"></i> {{$obj->location}}
      </div>
      <div class="product-subcategory">
        <i class="fa fa-asterisk"></i> {{$obj->category_sub->categories_sub}}
      </div>
      <div class="product-timestamp">
        <i class="fa fa-clock-o"></i> {{date_format(new DateTime($obj->created_at), "M-d h:i A")}}
      </div>



      <div class="action-buttons">
            <button data-link="{{route('myadvertsaction',['deactivate',$obj->id])}}" class="btn btn-sm btn-info ajax_ad_action">Deactivate</button> <a href="{{route('editadvert',$obj->slug)}}" class="btn btn-sm btn-warning ajaxlink">Edit</a> <button
            data-link="{{route('myadvertsaction',['delete',$obj->id])}}" class="btn btn-sm btn-danger ajax_ad_action">Delete</button>
      </div>
    </div>
  </div>
</div>
@endforeach

@if(count($adverts) == 0)
<style>
  .paginateLink{
    display: none;
  }
</style>
@endif