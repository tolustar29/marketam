
    <footer class="foot">
        <div class="container-fluid" style="background-color:#1b1616;color:white;padding-top:30px;">
        
          <div class="container">
              <div class="col-md-3">
                <div class="footer-container">
                    <h4>About Us</h4>
                    <div class="footer-links"><a href="{{ URL::to('/about-us') }}" class="ajaxlink">About Us</a></div>
                    <div class="footer-links"><a href="{{ URL::to('/terms-and-conditions') }}" class="ajaxlink">Terms & Conditions</a></div>
                    <div class="footer-links"><a href="{{ URL::to('/privacy-policy') }}" class="ajaxlink">Privacy Policy</a></div>
                    <div class="footer-links"><a href="{{ URL::to('/billing-policy') }}" class="ajaxlink">Billing Policy</a></div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="footer-container">
                    <h4>Support</h4>
                    <div class="footer-links"><a href="mailto:support@marketam.com.ng">support@marketam.com.ng</a></div>
                    <div class="footer-links"><a href="{{ URL::to('/safety-tips') }}" class="ajaxlink">Safety Tips</a></div>
                    <div class="footer-links"><a href="{{ URL::to('/contact-us') }}" class="ajaxlink">Contact Us</a></div>
                    <div class="footer-links"><a href="{{ URL::to('/faq') }}" class="ajaxlink">FAQ</a></div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="footer-container">
                    <h4>Our Resources</h4>
                    <div class="footer-links"><a href="http://marketamblogspot.blogspot.com/" class="">Our Blog</a></div>
                    <div class="footer-links"><a href="http://www.facebook.com/www.marketam.com.ng" class="">Marketam on FB</a></div>
                    <div class="footer-links"><a href="http://www.twitter.com/marketam8" class="">Marketam on Twitter</a></div>
                    <div class="footer-links"><a href="https://www.instagram.com/marketam.com.ng/" class="">Marketam on Instagram</a></div>
                    <div class="footer-links"><a href="https://www.youtube.com/channel/UCcLmai-gr0A2qhUAl7cvtfg" class="">Marketam on Youtube</a></div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="footer-container">
                    <h4>Payment Method</h4>
                    <img src="{{route('home')}}/images/visa.png" height="60px"><br>
                    <hr style="margin-top:10px; margin-bottom: 10px">
                    <img src="{{route('home')}}/images/coming-soon-on-google.png" width="120px"> 
                    <img src="{{route('home')}}/images/coming-soon-on-appstore.png" width="120px">
                </div>
              </div>
          </div>
          
          <div class="container">
              <div class="row" style="padding:15px;margin-top:10px">
                  <div class="col-sm-5">
                      <div>&copy {{date('Y')}} Marketam | All Rights Reserved </div>
                  </div>
                  <div class="col-sm-2">
                  </div>
                  <div class="col-sm-5">
                      <div class="pull-right">
                      <a class="footer-links ajaxlink" href="#" style="color:#8e8e8e"><i class="fa fa-facebook" aria-hidden="true"></i></a> <a class="footer-links ajaxlink" href="#" style="color:#8e8e8e;margin-left:15px"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                      </div>
                  </div>
              </div>
          </div>
        </div>
    </footer>
