<style>
  .ad-image-container{
    height: 200px;
  }

  .ad-image-container img {
    
  }

  .ad-image img{
    width: 200px;
    object-fit: cover;
    height: 200px;
  }

  .ad-image img{
    width: 200px;
    object-fit: cover;
    height: 200px;
  }

  .product-title{
    font-weight: 600;
    font-size: 18px;
    margin-top: 0px;
  }
  .product-price{
    color: #5cb85c;
    font-size: 20px;
    margin-top: 0px;
    text-align: right;
  }

  .product-location, .product-subcategory, .product-timestamp{
    font-weight: 500;
    color: green;
  }

  .product-location, .product-favourite{
    margin-top: 15px;
  }

  .product-favourite span{
    font-weight:500;
  }

  .product-favourite{
    cursor: pointer;
  }

  .product-description{
    max-height: 60px;
    overflow: hidden;
    word-wrap: break-word;
    text-overflow: ellipsis;
  }

  .product-description p{
    font-size:14px;
  }

  i.fa.fa-map-marker {
    margin-left: 2px;
    font-size: 15px;
    margin-right: 2px;
  }

  .ads-container {
    background: white;
    padding: 15px;
    border: solid 1px #b3b3b38f;
    border-radius: 8px;
    margin-bottom: 30px;
    box-shadow: -3px 7px 8px 0px rgba(0, 0, 0, 0.06);
  }

  @media (max-width: 768px){
    .categories{
      margin-top: 20px;
    }

    .product-title{
      margin-top:20px;
    }

    .product-price {
      color: green;
      text-align: left;
    }
  }


  </style>