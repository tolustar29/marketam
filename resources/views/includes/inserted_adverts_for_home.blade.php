@foreach($adverts as $obj)
<div class="col-sm-6 col-md-3 col-lg-3">
  <div class="product-mini-details">
    <div class="product-box">
      <div class="product-image">
        <div><a href="{{route('adpage',[$obj->slug])}}" class="ajaxlink">
            <img src="{{route('home')}}/{{$obj->advert_media->where('type','image')->first()->link}}" alt="">
          </a></div>
      </div>
    </div>
    <h3 class="product-mini-title">{{$obj->title}}</h3>
    <p class="product-mini-location">{{$obj->location}}</p>
    <h4 class="product-mini-price">₦{{number_format($obj->price, 2)}}</h4>

    @if($obj->advert_type == "sponsored")
    <div class="premium-badge">
      <img src="{{route('home')}}/images/icons/premium.png" alt="" class="badge-icon">
    </div>
    @endif
  </div>
</div>
@endforeach

@if(count($adverts) == 0)
<style>
  .paginateLink{
    display: none;
  }
</style>
@endif