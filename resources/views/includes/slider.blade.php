<style>

  .carousel {
      position: relative;
      /* margin-top: -71px; */
      top: -71px;
  }

  .search-product{
    /* background-color:#00000080;
    padding: 20px;
    position: absolute;
    width: 100%;
    margin-top: -75px; */
    background-color: #00000080;
    padding: 20px;
    position: relative;
    width: 100%;
    margin-top: -145px;
    z-index: 5000000;
  }

  .carousel-inner{
    max-height: 500px !important;
  }
  .myslide{
    height: 500px;
    background-size: cover;
    background-position: 50% 30%;
  }

  .my-carousel-content{
    position: absolute;
    top: 25%;
    width: 100%;
    text-align: center;
  }

  .my-carousel-content h1{
    font-weight:900;
    color:white;
    font-size:50px;
  }
  
  .carousel-content {
    position: absolute;
    top: 50%;
    left: 50%;
    z-home: 20;
    color: white;
    text-shadow: 0 1px 2px rgba(0,0,0,.6);
  }

  @media  (min-width: 768px){
    input.form-control.input-search {
      width: 500px;
      margin-right: 5px;
    }

    select#state {
        width: 150px;
        margin-right: 5px;
    }
  }

  @media (max-width: 768px){

    .search-product {
      background-color: #00000040;
      padding: 20px;
      position: relative;
      width: 100%;
      margin-top: -195px;
    }

    .my-carousel-content{
      top: 25%;
    }

  }

  @media  (max-width:1024px) and (min-width: 768px){
    .my-carousel-content{
      position: absolute;
      top: 15%;
      width: 100%;
      text-align: center;
    }
  }
</style>

<script>
  $('.carousel').carousel({
    interval: 4000
  })
</script>


<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
    <li data-target="#carousel-example-generic" data-slide-to="3"></li>
  </ol>
   -->

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <div class="myslide" style="background-image: url({{route('home')}}/images/marketam-1.jpg)"></div>
      <div class="carousel-content">
      </div>
      <div class="carousel-caption"></div>
    </div>
    <div class="item">
      <div class="myslide" style="background-image: url({{route('home')}}/images/marketam2.jpg)"></div>
      <div class="carousel-content">
      </div>
      <div class="carousel-caption"></div>
    </div>
    <div class="item">
      <div class="myslide" style="background-image: url({{route('home')}}/images/marketam3.jpg)"></div>
      <div class="carousel-content">
      </div>
      <div class="carousel-caption"></div>
    </div>
    <div class="item">
      <div class="myslide" style="background-image: url({{route('home')}}/images/marketam4.jpg)"></div>
      <div class="carousel-content">
      </div>
      <div class="carousel-caption"></div>
    </div>
  </div>

  <!-- Controls 
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
  -->
</div>


<div class="my-carousel-content">
  <h1>SELL QUICK SELL MORE</h1>
</div>

<div class="search-product" style="text-align:center">

  <form action="" method="POST" class="form-inline" role="form">

    <div class="form-group">
      <input name="product" type="text" class="form-control input-search product-search-box" id="" placeholder="Type your search here (e.g Infinix Hot 5, Toyota Camry)">
    </div>

    <div class="form-group">
      <select name="state" id="state" class="form-control select-state" required="required">
        <option value="Abuja FCT">Abuja FCT</option>
        <option value="Abia">Abia</option>
        <option value="Adamawa">Adamawa</option>
        <option value="Akwa Ibom">Akwa Ibom</option>
        <option value="Anambra">Anambra</option>
        <option value="Bauchi">Bauchi</option>
        <option value="Bayelsa">Bayelsa</option>
        <option value="Benue">Benue</option>
        <option value="Borno">Borno</option>
        <option value="Cross River">Cross River</option>
        <option value="Delta">Delta</option>
        <option value="Ebonyi">Ebonyi</option>
        <option value="Edo">Edo</option>
        <option value="Ekiti">Ekiti</option>
        <option value="Enugu">Enugu</option>
        <option value="Gombe">Gombe</option>
        <option value="Imo">Imo</option>
        <option value="Jigawa">Jigawa</option>
        <option value="Kaduna">Kaduna</option>
        <option value="Kano">Kano</option>
        <option value="Katsina">Katsina</option>
        <option value="Kebbi">Kebbi</option>
        <option value="Kogi">Kogi</option>
        <option value="Kwara">Kwara</option>
        <option value="Lagos" selected>Lagos</option>
        <option value="Nassarawa">Nassarawa</option>
        <option value="Niger">Niger</option>
        <option value="Ogun">Ogun</option>
        <option value="Ondo">Ondo</option>
        <option value="Osun">Osun</option>
        <option value="Oyo">Oyo</option>
        <option value="Plateau">Plateau</option>
        <option value="Rivers">Rivers</option>
        <option value="Sokoto">Sokoto</option>
        <option value="Taraba">Taraba</option>
        <option value="Yobe">Yobe</option>
        <option value="Zamfara">Zamfara</option>
      </select>
    </div>

    <a href="{{route('searchad')}}" class="ajaxlink btn btn-success product-search">Search</a>
  </form>

</div>