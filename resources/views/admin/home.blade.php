@include('includes.admin.header')
@include('includes.admin.sidebar')
@include('includes.admin.admin_css_script')



<div class="content-wrapper">

  <!-- <div class="search_anything">
      
      <form action="" method="POST" class="form" role="form">
      
        <div class="form-group">
          <input type="text" class="form-control" id="" placeholder="Search for anything (Users, Adverts)">
        </div>
      
        <button type="submit" class="btn btn-success">Search</button>
      </form>
      
  </div> -->

  <div class="dash-cards">
    <a href="{{route('admin.allusers')}}" class="ajaxlink">
    <div class="card-content blue">
      <div>All Users</div>
      <span>{{$all_users_count}}</span>
    </div>
    </a>
    
    <a href="{{route('admin.adverts')}}" class="ajaxlink">
      <div class="card-content blue">
        <div>All Adverts</div>
        <span>{{$all_adverts_count}}</span>
      </div>
    </a>

    <a href="{{route('admin.adverts','new')}}" class="ajaxlink">
      <div class="card-content">
        <div>New Adverts</div>
        <span>{{$new_adverts_count}}</span>
      </div>
    </a>
    
    <a href="{{route('admin.adverts','free')}}" class="ajaxlink">
    <div class="card-content">
      <div>Free Adverts</div>
      <span>{{$free_adverts_count}}</span>
    </div>
    </a>
    <a href="{{route('admin.adverts','sponsored')}}" class="ajaxlink">
    <div class="card-content">
      <div>Sponsored Adverts</div>
      <span>{{$sponsored_adverts_count}}</span>
    </div>
    </a>

      <a href="{{route('admin.adverts','sold')}}" class="ajaxlink">
      <div class="card-content">
        <div>Sold Adverts</div>
        <span>{{$sold_adverts_count}}</span>
      </div>
      </a>

    <a href="{{route('admin.adverts','declined')}}" class="ajaxlink">
      <div class="card-content red">
        <div>Declined Adverts</div>
        <span>{{$declined_adverts_count}}</span>
      </div>
    </a>
    <a href="{{route('admin.adverts','blocked')}}" class="ajaxlink">
      <div class="card-content red">
        <div>Blocked Adverts</div>
        <span>{{$blocked_adverts_count}}</span>
      </div>
    </a>

      <a href="{{route('admin.adverts','blocked')}}" class="ajaxlink">
      <div class="card-content red">
        <div>Deactivated Adverts</div>
        <span>{{$deactivated_adverts_count}}</span>
      </div>
      </a>
  </div>

  <div class="chart">
      <h3 align="center">Graph of Active Sponsored/Free Adverts over a period of time</h3>
      <canvas id="myChart" style=""></canvas>
  </div>


  <div style="margin-top:60px">
  <div class="row">
    <div class="col-md-6">
        <div class="recent_activity">
            <h4>Recent Activities</h4>
            @foreach($adminactivity as $obj)
            <div class="activity"><strong>{{$obj->admin}}</strong> {{$obj->activity}} | <small>{{$obj->created_at}}</small></div>
            @endforeach
        </div>
    </div> 
    <div class="col-md-6">
      <div class="moderators">
            <h4>Moderators</h4>
            @foreach($admins as $obj)
            <div class="user">{{$obj->full_name}} ({{$obj->role}})</div>
            @endforeach
      </div>
    </div>   
  </div>
  </div>

  <div style="height:70px"></div>
</div>

<style>

  .search_anything{
    margin-top:25px;
    text-align: center;
    margin-bottom:40px;
    margin-left:50px;
    margin-right:50px;
  }

  .form-group {
    margin-bottom: 10px;
  }

  .chart{
    background:white; 
    padding:15px;
    margin: 40px auto;
  }

  .recent_activity, .moderators{
    border: solid 1px #d4d4d4;
    box-shadow: 1px 5px 2px 0px #9bbfa3b8;
    background: #fff;
    height: 300px;
    overflow: auto;
  }

  .recent_activity h4{
    color:white;
    background-color: #1795a9;
    padding: 15px;
    margin: 0px;
  }

  .recent_activity .activity{
    background-color: white;
    padding:15px; 
    border-bottom: solid #f2f2f2 1px;
  }

  .moderators h4{
    color:white;
    background-color: #00a65a;
    padding: 15px;
    margin: 0px;
  }
  
  .moderators .user{
    background-color: white;
    padding:15px; 
    border-bottom: solid #f9f9f9 1px;
  }
</style>


<script>
document.title = "{{$pageTitle}}";
var ctx = document.getElementById("myChart").getContext('2d');
var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
        datasets: [
        {
            label: 'Sponsored Adverts',
            data: {!! $sponsored_adverts_all_year !!},
            fill: false,
            backgroundColor: '#068c4f',
            borderColor: '#068c4f',
            borderWidth: 1
        },
        {
            label: 'Free Adverts',
            data: {!! $free_adverts_all_year !!},
            fill: false,
            backgroundColor: '#55d4e8',
            borderColor: '#55d4e8',
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                },
                scaleLabel: {
                  display: true,
                  labelString: 'Active Adverts'
                }
            }],
            xAxes: [{
                ticks: {
                    beginAtZero:true
                },
                scaleLabel: {
                  display: true,
                  labelString: 'Period'
                }
            }]
        }
    }
});
</script>
