@include('includes.admin.header')
@include('includes.admin.sidebar')
@include('includes.admin.admin_css_script')

<script>
  $(function () {
    $(".sidebar-menu li").removeClass("active");
    $(".sidebar-menu .advert").addClass("active");
    $('.table_ad').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "scrollX": true,
        "lengthMenu": [[30, 60, 90, -1], [30, 60, 90, "All"]]
    });
  });
</script>


<style>
  .content-wrapper .admin-all-advert{
    margin-top:20px;
    padding:25px;
    background: white;
  }

  .dash-cards a {
    text-decoration:none;
    color: inherit !important;
  }
</style>



<div class="content-wrapper">

    <h2 style="text-align:center">{{$advert_title}}</h2>

    <div class="dash-cards">
      <a href="{{route('admin.adverts')}}" class="ajaxlink">
      <div class="card-content">
          <div>All Adverts</div>
          <span>{{$all_adverts_count}}</span>
      </div>
      </a>

      <a href="{{route('admin.adverts','new')}}" class="ajaxlink">
      <div class="card-content">
        <div>New Adverts</div>
        <span>{{$new_adverts_count}}</span>
      </div>
      </a>

      <a href="{{route('admin.adverts','free')}}" class="ajaxlink">
      <div class="card-content">
        <div>Free Adverts</div>
        <span>{{$free_adverts_count}}</span>
      </div>
      </a>

      <a href="{{route('admin.adverts','sponsored')}}" class="ajaxlink">
      <div class="card-content">
        <div>Sponsored Adverts</div>
        <span>{{$sponsored_adverts_count}}</span>
      </div>
      </a>

      <a href="{{route('admin.adverts','sold')}}" class="ajaxlink">
      <div class="card-content">
        <div>Sold Adverts</div>
        <span>{{$sold_adverts_count}}</span>
      </div>
      </a>

      
      <a href="{{route('admin.adverts','declined')}}" class="ajaxlink">
      <div class="card-content red">
        <div>Declined Adverts</div>
        <span>{{$declined_adverts_count}}</span>
      </div>
      </a>
      
      <a href="{{route('admin.adverts','blocked')}}" class="ajaxlink">
      <div class="card-content red">
        <div>Blocked Adverts</div>
        <span>{{$blocked_adverts_count}}</span>
      </div>
      </a>

      <a href="{{route('admin.adverts','blocked')}}" class="ajaxlink">
      <div class="card-content red">
        <div>Deactivated Adverts</div>
        <span>{{$deactivated_adverts_count}}</span>
      </div>
      </a>
    </div>

    <?php $count = 1 ?>
    <div class="admin-all-advert">
      <table class="table table-hover table_ad table-responsive table-striped table-bordered">
        <thead class="">
          <tr>
            <th>S/N</th>
            <th>User</th>
            <th>Title</th>
            <th>Description</th>
            <th>Price</th>
            <th>Type</th>
            <th>Date | Time</th>
            <th>Category</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          @foreach($adverts as $obj)
          <tr>
            <td>{{$count++}}</td>
            <td>{{$obj->user->full_name}}</td>
            <td>{{$obj->title}}</td>
            <td>{{$obj->description}}</td>
            <td>{{$obj->price}}</td>
            <td>{{$obj->advert_type}}</td>
            <td>{{$obj->created_at}}</td>
            <td>{{$obj->category_sub->categories_sub}}</td>
            <td>
              <a href="{{route('adpage',$obj->slug)}}" class="btn btn-success btn-sm" target="_blank">view</a>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  

    <div style="height:70px"></div>
</div>

<style>

  .search_anything{
    margin-top:25px;
    text-align: center;
    margin-bottom:40px;
    margin-left:50px;
    margin-right:50px;
  }

  .form-group {
    margin-bottom: 10px;
  }

  .chart{
    background:white; 
    padding:15px;
    margin: 40px auto;
  }

  .recent_activity, .moderators{
    border: solid 1px #d4d4d4;
    box-shadow: 1px 5px 2px 0px #9bbfa3b8;
    background: #fff;
    height: 300px;
    overflow: auto;
  }

  .recent_activity h4{
    color:white;
    background-color: #1795a9;
    padding: 15px;
    margin: 0px;
  }

  .recent_activity .activity{
    background-color: white;
    padding:15px; 
    border-bottom: solid #f2f2f2 1px;
  }

  .moderators h4{
    color:white;
    background-color: #00a65a;
    padding: 15px;
    margin: 0px;
  }
  
  .moderators .user{
    background-color: white;
    padding:15px; 
    border-bottom: solid #f9f9f9 1px;
  }

  .dataTables_wrapper.no-footer .dataTables_scrollBody {
    border-bottom: none;
  }

</style>

<script>
  document.title = "{{$pageTitle}}";
  $(function () {
    
  });
</script>

