@include('includes.admin.header')
@include('includes.admin.sidebar')
@include('includes.admin.admin_css_script')

<script>
  $(function () {
    $(".sidebar-menu li").removeClass("active");
    $(".sidebar-menu .allusers").addClass("active");
    $('.table_ad').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "scrollX": true,
        "lengthMenu": [[30, 60, 90, -1], [30, 60, 90, "All"]]
    });
  });
</script>


<style>
  .content-wrapper .admin-all-advert{
    margin-top:20px;
    padding:25px;
    background: white;
  }

  .dataTables_scrollBody {
      overflow: unset !important;
  }
</style>



<div class="content-wrapper">

    <div class="dash-cards">
      <div class="card-content">
        <div>All Users</div>
        <span>{{$all_users_count}}</span>
      </div>
      <!-- <div class="card-content">
        <div>New Users</div>
        <span>{{$new_users_count}}</span>
      </div> -->
    </div>

    <?php $count = 1 ?>
    <div class="admin-all-advert">
      <table class="table table-hover table_ad table-responsive table-striped table-bordered">
        <thead class="">
          <tr>
            <th>S/N</th>
            <th>User</th>
            <th>Phone Number</th>
            <th>Address</th>
            <th>Role</th>
            <th>Last Login</th>
            <th>Date Joined</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          @foreach($users as $obj)
          <tr>
            <td>{{$count++}}</td>
            <td>{{$obj->full_name}}</td>
            <td>{{$obj->phone_number}}</td>
            <td>{{$obj->address}}</td>
            <td>
              {{$obj->role}}<br>
              @if(Auth::user()->role == "admin")
              <div class="dropdown">
                <button class="btn btn-info btn-sm dropdown-toggle" type="button" id="dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                  Change Role
                  <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdown">
                  <li><a href="{{route('admin.updaterole',[$obj->id,'admin'])}}" class="ajaxlink">Admin</a></li>
                  <li><a href="{{route('admin.updaterole',[$obj->id,'moderator'])}}" class="ajaxlink">Moderator</a></li>
                  <li><a href="{{route('admin.updaterole',[$obj->id,'user'])}}" class="ajaxlink">User</a></li>
                </ul>
              </div>
              @endif
            </td>
            <td>{{$obj->last_login}}</td>
            <td>{{$obj->created_at}}</td>
            <td>
              <a href="{{route('admin.userprofile',$obj->id)}}" class="btn btn-success btn-sm ajaxlink" style="margin-bottom:10px">view</a>
              @if(Auth::user()->role == "admin")
                @if($obj->full_name != "Marketam")
                <?php 
                $deleteLink = route('admin.deleteuser', $obj->id);
                ?>
                <div onclick="deleteFunction('Delete User','Are you sure you want to delete this user. Note that all adverts associated with this user will be deleted. Thank you','{{$deleteLink}}')" class="btn btn-danger btn-sm ajaxlink">delete</div>
                @endif
              @endif
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  

    <div style="height:70px"></div>
</div>

<div class="ui-dialog-bg-overlay"></div>

<style>

  .search_anything{
    margin-top:25px;
    text-align: center;
    margin-bottom:40px;
    margin-left:50px;
    margin-right:50px;
  }

  .form-group {
    margin-bottom: 10px;
  }

  .chart{
    background:white; 
    padding:15px;
    margin: 40px auto;
  }

  .recent_activity, .moderators{
    border: solid 1px #d4d4d4;
    box-shadow: 1px 5px 2px 0px #9bbfa3b8;
    background: #fff;
    height: 300px;
    overflow: auto;
  }

  .recent_activity h4{
    color:white;
    background-color: #1795a9;
    padding: 15px;
    margin: 0px;
  }

  .recent_activity .activity{
    background-color: white;
    padding:15px; 
    border-bottom: solid #f2f2f2 1px;
  }

  .moderators h4{
    color:white;
    background-color: #00a65a;
    padding: 15px;
    margin: 0px;
  }
  
  .moderators .user{
    background-color: white;
    padding:15px; 
    border-bottom: solid #f9f9f9 1px;
  }

  .dataTables_wrapper.no-footer .dataTables_scrollBody {
    border-bottom: none;
  }

</style>

<script>
  document.title = "{{$pageTitle}}";
  $(function () {
    
  });

</script>

