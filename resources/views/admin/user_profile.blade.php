@include('includes.admin.header')
@include('includes.admin.sidebar')
@include('includes.admin.admin_css_script')

<script>
  $(function () {
    $(".sidebar-menu li").removeClass("active");
    $(".sidebar-menu .allusers").addClass("active");
    $('.table_ad').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "scrollX": true,
        "lengthMenu": [[30, 60, 90, -1], [30, 60, 90, "All"]]
    });
  });
</script>


<style>
  .content-wrapper .admin-all-advert{
    margin-top:20px;
    padding:25px;
    background: white;
  }

  .dataTables_scrollBody {
      overflow: unset !important;
  }
</style>



<div class="content-wrapper">

    
  <form action="#" method="POST" role="form">


    <div class="display-picture" style="background-image: url({{$user->image}})">
    </div>


    <div class="user-mini-detail">
      <i class="fa fa-circle" aria-hidden="true"></i> Last login: {{date_format(new DateTime($user->last_login), "d/m/Y h:i A")}}
    </div>
    <div class="user-mini-detail">
      <i class="fa fa-circle" aria-hidden="true"></i> Member Since: {{date_format(new DateTime($user->created_at), "d/m/Y h:i A")}}
    </div>


  
    <div class="form-group" style="margin-top: 40px">
      <label for="">Full Name</label>
      <input name="full_name" type="text" class="form-control full_name" id="" value="{{$user->full_name}}" disabled>
    </div>

    <div class="form-group">
      <label for="">Email</label>
      <input name="email" type="email" class="form-control" id="" value="{{$user->email}}" disabled>
    </div>

    <div class="form-group">
      <label for="">Phone Number</label>
      <input name="phone_number" type="text" class="form-control" id="" value="{{$user->phone_number}}" disabled>
    </div>

    <div class="form-group">
      <label for="">State</label>
      <input name="State" type="text" class="form-control" id="" value="{{$user->state}}" disabled>
    </div>

    <div class="form-group">
      <label for="">Address</label>
      <input name="address" type="text" class="form-control" id="" value="{{$user->address}}" disabled>
    </div>

    <div class="form-group">
      <label for="">Company Name</label>
      <input name="company_name" type="text" class="form-control" id="" value="{{$user->company_name}}" disabled>
    </div>

    <div class="form-group">
      <label for="">Company Unique Name</label>
      <input name="company_unique_name" type="text" class="form-control" id="" value="{{$user->company_unique_name}}" disabled>
    </div>

    <div class="form-group">
      <label for="">About Company</label>
      <textarea name="about_company" id="input" class="form-control" rows="3" required="required" disabled>{{$user->about_company}}</textarea>

    </div>

    <div class="form-group">
      <label for="">Company Website</label>
      <input name="company_website" type="text" class="form-control" id="" value="{{$user->company_website}}" disabled>
    </div>

    <div class="form-group">
      <label for="">Name To Be Displayed On Marketam Store</label>
      <input name="display_name_type" type="text" class="form-control" id="" value="{{$user->display_name_type}}" disabled>
    </div>

  </form>

</div>

<div class="ui-dialog-bg-overlay"></div>




<style>
  .active-link {
    color: green;
    font-weight: 600;
  }

  .user-name {
    color: #1fae37;
    font-weight: 600;
  }

  .user-mini-detail {
    font-size: 14px;
  }

  .contact-details {
    margin-top: 25px;
    margin-bottom: 25px;
  }

  .profile-menu {
    margin-top: 30px
  }

  .profile-menu i.fa {
    width: 15px;
  }

  .user-mini-detail i.fa {
    color: green;
  }

  .profile-menu div {
    margin-bottom: 15px;
    border-bottom: solid 1px #dadada;
    padding-bottom: 5px;
    font-weight: 500;
    cursor: pointer;
  }

  .profile {
    background-color: #fff;
    padding: 30px;
    border: solid 1px #8080804f;
    margin-bottom: 50px;
    box-shadow: -3px 7px 8px 0px rgba(0, 0, 0, 0.06);
  }

  .display-picture {
    background-size: cover;
    width: 200px;
    height: 200px;
    border: solid 1px #8080804f;
    border-radius: 20px;
    position: relative;
  }

  .change-picture {
    cursor: pointer;
    position: absolute;
    bottom: 1px;
    right: 1px;
  }

  .profile-menu-btn {
    display: none;
  }

  @media (max-width:480px) {
    .profile-menu-btn {
      display: block;
      margin-bottom: 20px;
    }

    .profile-frame {
      display: none;
    }
  }
</style>

<style>

  .search_anything{
    margin-top:25px;
    text-align: center;
    margin-bottom:40px;
    margin-left:50px;
    margin-right:50px;
  }

  .form-group {
    margin-bottom: 10px;
  }

  .dataTables_wrapper.no-footer .dataTables_scrollBody {
    border-bottom: none;
  }

</style>

<script>
  document.title = "{{$pageTitle}}";
  $(function () {
    
  });

</script>