@include('includes.admin.header')
@include('includes.admin.sidebar')
@include('includes.admin.admin_css_script')

<script>
  $(function () {
    $(".sidebar-menu li").removeClass("active");
    $(".sidebar-menu .categories").addClass("active");
    $('.table_ad').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "scrollX": true,
        "lengthMenu": [[30, 60, 90, -1], [30, 60, 90, "All"]]
    });
  });
</script>


<style>
  .content-wrapper .admin-all-advert{
    margin-top:20px;
    padding:25px;
    background: white;
  }
</style>



<div class="content-wrapper">


    <h3>{{$category->category}} Sub Categories</h3>
    @if(Auth::user()->role == "admin")
    <a class="btn btn-success" style="margin-top:20px" data-toggle="modal" href='#modal-add_category'>Add Sub Categories For {{$category->category}}</a>
    <div class="modal fade" id="modal-add_category">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Add Sub Categories For {{$category->category}}</h4>
          </div>
          <div class="modal-body">
            
              
                <div class="form-group">
                  <label for="">Sub Categories For {{$category->category}}</label>
                  <div>You can add multiple sub categories by separating them with a comma</div>
                  <p style="color:red; display: none" class="field_display">Field can not be empty</p>
                  <input type="text" class="form-control new_category" id="" placeholder="">
                </div>
              
                <button class="btn btn-success add_category">Add</button>
              
          </div>
        </div>
      </div>
    </div>
    @endif
    

    
    <?php $count = 1 ?>
    <div class="admin-all-advert">
      <table class="table table-hover table_ad table-responsive table-striped table-bordered">
        <thead class="">
          <tr>
            <th>S/N</th>
            <th>Category</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          @foreach($categories as $obj)
          <tr>
            <td>{{$count++}}</td>
            <td>{{$obj->categories_sub}}</td>
            <?php 
              $deleteCatLink = route('admin.delete_sub_category', $obj->id);
              $editCatLink = route('admin.edit_sub_category', $obj->id);
            ?>
            <td>
              <a href="{{route('admin.viewsubcategoriesattr',[$obj->categories_slug])}}" class="btn btn-success btn-sm ajaxlink">view</a>
              @if(Auth::user()->role == "admin")
              <div onclick="editFunction('{{$obj->categories_sub}}','{{$editCatLink}}')" class="btn btn-info btn-sm">edit</div>
              <div onclick="deleteFunction('Delete Sub Category','Are you sure you want to delete this sub category. Note that all adverts associated with this sub category will be deleted. Thank you','{{$deleteCatLink}}')" class="btn btn-danger btn-sm ajaxlink">delete</div>
              @endif
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  

    <div style="height:70px"></div>
</div>

<div id="dialog-edit" style="display:none">
    <input type="text" name="" id="input" class="form-control editCategory" value="">
</div>
<div class="ui-dialog-bg-overlay"></div>

<style>

  .search_anything{
    margin-top:25px;
    text-align: center;
    margin-bottom:40px;
    margin-left:50px;
    margin-right:50px;
  }

  .form-group {
    margin-bottom: 10px;
  }

  .chart{
    background:white; 
    padding:15px;
    margin: 40px auto;
  }

  .recent_activity, .moderators{
    border: solid 1px #d4d4d4;
    box-shadow: 1px 5px 2px 0px #9bbfa3b8;
    background: #fff;
    height: 300px;
    overflow: auto;
  }

  .recent_activity h4{
    color:white;
    background-color: #1795a9;
    padding: 15px;
    margin: 0px;
  }

  .recent_activity .activity{
    background-color: white;
    padding:15px; 
    border-bottom: solid #f2f2f2 1px;
  }

  .moderators h4{
    color:white;
    background-color: #00a65a;
    padding: 15px;
    margin: 0px;
  }
  
  .moderators .user{
    background-color: white;
    padding:15px; 
    border-bottom: solid #f9f9f9 1px;
  }

  .dataTables_wrapper.no-footer .dataTables_scrollBody {
    border-bottom: none;
  }

</style>

<script>
  document.title = "{{$pageTitle}}";
  category_id = "{{$category_id}}";
  link = "{{route('admin.add_sub_category')}}"
  $(function () {
      $(".add_category").click(function(){
          if($(".new_category").val().length !== 0){
            $(".field_display").hide();
            $( "#modal-add_category" ).modal('hide');
            $(".progressBarBg").show();
            $.ajax({
                cache: false,
                type: "GET",
                url: link + "/" + category_id + "/" + $(".new_category").val(),
                success: function (data) {
                    successdisplay(data);

                },
                error: function (data) {
                    $(".progressBarBg").hide();
                    console.log('Error:', data);
                }
            });
          }else{
            $(".field_display").show();
          }
      })
  });

  function editFunction(text,link){
        $(".ui-dialog-bg-overlay").show();
        $(".editCategory").val(text);
        $( "#dialog-edit" ).dialog({
            resizable: true,
            height: "auto",
            width: 600,
            modal: true,
            title: "Edit Sub Category",
            buttons: {
                "Update": function() {
                    $( this ).dialog( "destroy" );
                    $(".ui-dialog-bg-overlay").hide();
                    if($(".editCategory").val().length !== 0){
                      $(".progressBarBg").show();
                      $.ajax({
                          cache: false,
                          type: "GET",
                          url: link + "/" + $(".editCategory").val(),
                          success: function (data) {
                              successdisplay(data);

                          },
                          error: function (data) {
                              $(".progressBarBg").hide();
                              console.log('Error:', data);
                          }
                      });
                    }
                },
                Cancel: function() {
                    $( this ).dialog( "destroy" );
                    $(".ui-dialog-bg-overlay").hide();
                }
            }
        });
        $(".ui-widget button").eq(2).addClass("btn btn-danger");
        $(".ui-widget button").eq(1).addClass("btn btn-success");
    }

</script>

