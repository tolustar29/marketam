@include('includes.admin.header')
@include('includes.admin.sidebar')
@include('includes.admin.admin_css_script')

<script>
  $(function () {
    $(".sidebar-menu li").removeClass("active");
    $(".sidebar-menu .categories").addClass("active");
    $('.table_ad').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "scrollX": true,
        "lengthMenu": [[30, 60, 90, -1], [30, 60, 90, "All"]]
    });
  });
</script>


<style>
  .content-wrapper .admin-all-advert{
    margin-top:20px;
    padding:25px;
    background: white;
  }
</style>



<div class="content-wrapper">


    <h3>{{$sub_category->categories_sub}} Attributes</h3>

    @if(Auth::user()->role == "admin")
    <button class="btn btn-success" style="margin-top:20px" onclick="addattributes()">Add Attributes For {{$sub_category->categories_sub}}</button>
    @endif
    

    
    <?php $count = 1 ?>
    <div class="admin-all-advert">
      <table class="table table-hover table_ad table-responsive table-striped table-bordered">
        <thead class="">
          <tr>
            <th>S/N</th>
            <th>Attribute</th>
            <th>Attribute Data</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          @foreach($sub_categories_attributes as $obj)
          <tr>
            <td>{{$count++}}</td>
            <td>{{$obj->attributes}}</td>
            <td>{{$obj->attributes_data}}</td>
            <?php 
              $deleteCatAttrLink = route('admin.delete_sub_category_attr', $obj->id);
            ?>
            <td>
              @if(Auth::user()->role == "admin")
              <div onclick="editFunction('{{$obj->attributes}}','{{$obj->attributes_data}}','{{$obj->id}}')" class="btn btn-info btn-sm">edit</div>
              <div onclick="deleteFunction('Delete Attribute','Are you sure you want to delete this attribute. Thank you','{{$deleteCatAttrLink}}')" class="btn btn-danger btn-sm ajaxlink">delete</div>
              @endif
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  

    <div style="height:70px"></div>
</div>

<div id="dialog-add" style="display:none">
    
    <form action="{{route('admin.add_sub_category_attr')}}" method="POST" role="form" class="add_attr_form">
    
      {{ csrf_field() }}

      <h3>Attributes For {{$sub_category->categories_sub}}</h3>
      <p>Click the add more attributes button to add multiple attributes</p>
      <p style="color:red">Note: </p>
      <ul>
        <li>To accept input data for an attribute, type <strong>input</strong> in the attribute data field</li>
        <li><strong>current year,1990</strong> in the attribute data field will display list of years from 1990 - {{date('Y')}}</li>
        <li><strong>yes,no,maybe</strong> in the attribute data field will display a list of options</li>
      </ul>
      
      <button type="button" style="margin:10px 0px 10px 0px" class="btn btn-success btn-sm add_more_attr">Add more attributes</button>
      
      <div class="attribute_container">

        <div class="attribute_container_item">
          <div class="form-group form-group-custom">
            <label for="">Attribute</label>
            <input type="text" class="form-control" id="" placeholder="" name="attribute[]">
          </div>

          <div class="form-group form-group-custom">
            <label for="">Attribute Data</label>
            <input type="text" class="form-control" id="" placeholder="" name="attribute_data[]">
          </div>
          <button type="button" class="btn btn-danger btn-sm remove_attr">remove</button>
        </div>

      </div>
  
      <input type="hidden" class="form-control" id="" value="{{$sub_category->id}}" name="categories_sub_id">
    
    </form>
    
</div>

<div id="dialog-edit" style="display:none">
    
    <form action="{{route('admin.edit_sub_category_attr')}}" method="POST" role="form" class="edit_attr_form">
      {{ csrf_field() }}
      <div class="form-group">
        <label for="">Attribute</label>
        <input type="text" class="attribute form-control" id="" placeholder="" value="" name="attribute">
      </div>
    
      <div class="form-group">
        <label for="">Attribute Data</label>
        <input type="text" class="attribute_data form-control" id="" placeholder="" value="" name="attribute_data">
      </div>
  
      <input type="hidden" class="form-control category_attribute_id" id="" value="" name="category_attribute_id">
    
    </form>
    
</div>
<div class="ui-dialog-bg-overlay"></div>

<style>

  .form-group-custom {
    display: inline-block;
    margin-bottom: 10px;
    margin-right: 10px;
    width: 44%;
  }

  .ui-widget {
    font-family: inherit;
    font-size: 1em;
  }

  .search_anything{
    margin-top:25px;
    text-align: center;
    margin-bottom:40px;
    margin-left:50px;
    margin-right:50px;
  }

  .chart{
    background:white; 
    padding:15px;
    margin: 40px auto;
  }

  .recent_activity, .moderators{
    border: solid 1px #d4d4d4;
    box-shadow: 1px 5px 2px 0px #9bbfa3b8;
    background: #fff;
    height: 300px;
    overflow: auto;
  }

  .recent_activity h4{
    color:white;
    background-color: #1795a9;
    padding: 15px;
    margin: 0px;
  }

  .recent_activity .activity{
    background-color: white;
    padding:15px; 
    border-bottom: solid #f2f2f2 1px;
  }

  .moderators h4{
    color:white;
    background-color: #00a65a;
    padding: 15px;
    margin: 0px;
  }
  
  .moderators .user{
    background-color: white;
    padding:15px; 
    border-bottom: solid #f9f9f9 1px;
  }

  .dataTables_wrapper.no-footer .dataTables_scrollBody {
    border-bottom: none;
  }

</style>

<script>
  document.title = "{{$pageTitle}}";


  $(".add_more_attr").click(function(){

    var new_attr =  '<div class="attribute_container_item">' + 
                      '<div class="form-group form-group-custom">' +
                        '<label for="">Attribute</label>' +
                        '<input type="text" class="form-control" id="" placeholder="" name="attribute[]">' +
                      '</div> ' +

                      '<div class="form-group form-group-custom">' + 
                        '<label for="">Attribute Data</label>' +
                        '<input type="text" class="form-control" id="" placeholder="" name="attribute_data[]">' +
                      '</div> ' +
                      '<button type="button" class="btn btn-danger btn-sm remove_attr">remove</button>' +
                    '</div>';

    $(".attribute_container").append(new_attr);

  });

  $(".attribute_container").on("click",".remove_attr", function(e){ //user click on remove text
      e.preventDefault(); 
      $(this).parent('div').remove();
  });

  function addattributes(){
        $(".ui-dialog-bg-overlay").show();
        $( "#dialog-add" ).dialog({
            resizable: true,
            height: "auto",
            width: 1000,
            modal: true,
            title: "Add Attributes",
            buttons: [
            {
               text: "Save",
               "class": 'btn btn-success',
               click: function() {                     
                  $( this ).dialog( "destroy" );
                  $(".ui-dialog-bg-overlay").hide();
                  $(".progressBarBg").show();
                  $.ajax({
                          cache: false,
                          type: "POST",
                          url: $(".add_attr_form").attr('action'),
                          data: $(".add_attr_form").serialize(),
                          success: function (data) {
                              successdisplay(data);

                          },
                          error: function (data) {
                              $(".progressBarBg").hide();
                              console.log('Error:', data);
                          }
                  });
               }
            },
            {
               text: "Close",
               "class": 'btn btn-info',
               click: function() { 
                  $( this ).dialog( "destroy" );
                  $(".ui-dialog-bg-overlay").hide();
               }
            }
          ]
        });
  }


  function editFunction(attribute,attribute_data,category_attribute_id){
        $(".ui-dialog-bg-overlay").show();
        $(".attribute").val(attribute);
        $(".attribute_data").val(attribute_data);
        $(".category_attribute_id").val(category_attribute_id)
        $( "#dialog-edit" ).dialog({
            resizable: true,
            height: "auto",
            width: 600,
            modal: true,
            title: "Edit Attribute",
            buttons: [
            {
               text: "Update",
               "class": 'btn btn-success',
               click: function() {                     
                    $( this ).dialog( "destroy" );
                    $(".ui-dialog-bg-overlay").hide();
                    $(".progressBarBg").show();
                    $.ajax({
                          cache: false,
                          type: "POST",
                          url: $(".edit_attr_form").attr('action'),
                          data: $(".edit_attr_form").serialize(),
                          success: function (data) {
                              successdisplay(data);

                          },
                          error: function (data) {
                              $(".progressBarBg").hide();
                          }
                    });
                    
               }
            },
            {
               text: "Cancel",
               "class": 'btn btn-danger',
               click: function() { 
                  $( this ).dialog( "destroy" );
                  $(".ui-dialog-bg-overlay").hide();
               }
            }
          ]
        });
  }

</script>

