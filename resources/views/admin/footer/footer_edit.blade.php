@include('includes.admin.header')
@include('includes.admin.sidebar')
@include('includes.admin.admin_css_script')


<style>
  .content-wrapper .admin-all-advert{
    margin-top:20px;
    padding:25px;
    background: white;
  }
</style>

<script>
  $(function () {
    $(".sidebar-menu li").removeClass("active");
    $(".sidebar-menu .footer_pages").addClass("active");
  });
</script>



<div class="content-wrapper">

    <h2>{{$getpage->page}}</h2>

        <form action="{{route('admin.update_footer_page')}}" method="POST" role="form" style="margin-top:15px" class="updatePage">
    
      {{ csrf_field() }}
      <label for="">Content</label>
      <input type="hidden" name="content" id="input" class="form-control content" value="">
      <input type="hidden" name="slug" id="input" class="form-control" value="{{$getpage->slug}}">

      <div id="editor" style="background:#fff;">{!! $getpage->content !!}
      </div>

    
      <button type="submit" class="btn btn-success" style="margin-top:30px">Update</button>
    </form>
    

    <div style="height:70px"></div>
</div>

<style>

  
  .dataTables_wrapper.no-footer .dataTables_scrollBody {
    border-bottom: none;
  }

</style>

<script>
  document.title = "{{$pageTitle}}";

</script>


<script>
  var toolbarOptions = [
        ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
        ['blockquote', 'code-block'],             // custom button values
        [{ 'list': 'ordered'}, { 'list': 'bullet' }],
        [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
        [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
        [{ 'direction': 'rtl' }],                         // text direction

        [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

        [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
        [{ 'font': [] }],
        [{ 'align': [] }],
        ['link', 'image'],
        ['clean']                                         // remove formatting button
      ];
  var quill = new Quill('#editor', {
    modules: {
      toolbar: toolbarOptions
    },
    theme: 'snow',

  });

  $(".updatePage").submit(function(event){
      event.preventDefault();
      $(".content").val($(".ql-editor").html());
      $(".progressBarBg").show();
      $.ajax({
          cache: false,
          type: "POST",
          url: $(".updatePage").attr('action'),
          data: $(".updatePage").serialize(),
          success: function (data) {
            successdisplay(data);
          },
          error: function (data) {
            $(".progressBarBg").hide();
          }
        });
  })

</script>

