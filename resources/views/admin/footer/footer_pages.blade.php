@include('includes.admin.header')
@include('includes.admin.sidebar')
@include('includes.admin.admin_css_script')

<script>
  $(function () {
    $(".sidebar-menu li").removeClass("active");
    $(".sidebar-menu .footer_pages").addClass("active");
    $('.table_ad').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "scrollX": true,
        "lengthMenu": [[30, 60, 90, -1], [30, 60, 90, "All"]]
    });
  });
</script>


<style>
  .content-wrapper .admin-all-advert{
    margin-top:20px;
    padding:25px;
    background: white;
  }
</style>



<div class="content-wrapper">

    
    <?php $count = 1 ?>
    <div class="admin-all-advert">
      <table class="table table-hover table_ad table-responsive table-striped table-bordered">
        <thead class="">
          <tr>
            <th>S/N</th>
            <th>Page</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          @foreach($pages as $obj)
          <tr>
            <td>{{$count++}}</td>
            <td>{{$obj->page}}</td>
            <td>
              <a href="{{ URL::to('/'.$obj->slug) }}" class="btn btn-success btn-sm" target="_blank">view</a>
              @if(Auth::user()->role == "admin")
              <a href="{{route('admin.edit_footer_page',$obj->slug)}}" class="btn btn-info btn-sm ajaxlink">edit</a>
              @endif
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  

    <div style="height:70px"></div>
</div>

<style>

  .search_anything{
    margin-top:25px;
    text-align: center;
    margin-bottom:40px;
    margin-left:50px;
    margin-right:50px;
  }

  .form-group {
    margin-bottom: 10px;
  }

  .chart{
    background:white; 
    padding:15px;
    margin: 40px auto;
  }

  .recent_activity, .moderators{
    border: solid 1px #d4d4d4;
    box-shadow: 1px 5px 2px 0px #9bbfa3b8;
    background: #fff;
    height: 300px;
    overflow: auto;
  }

  .recent_activity h4{
    color:white;
    background-color: #1795a9;
    padding: 15px;
    margin: 0px;
  }

  .recent_activity .activity{
    background-color: white;
    padding:15px; 
    border-bottom: solid #f2f2f2 1px;
  }

  .moderators h4{
    color:white;
    background-color: #00a65a;
    padding: 15px;
    margin: 0px;
  }
  
  .moderators .user{
    background-color: white;
    padding:15px; 
    border-bottom: solid #f9f9f9 1px;
  }

  .dataTables_wrapper.no-footer .dataTables_scrollBody {
    border-bottom: none;
  }

</style>

<script>
  document.title = "{{$pageTitle}}";
  $(function () {
    
  });
</script>

