@include('includes.admin.header')
@include('includes.admin.sidebar')
@include('includes.admin.admin_css_script')

<script>
  $(function () {
    $(".sidebar-menu li").removeClass("active");
    $(".sidebar-menu .reported_advert").addClass("active");
    $('.table_ad').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "lengthMenu": [[30, 60, 90, -1], [30, 60, 90, "All"]]
    });
  });
</script>


<style>
  .content-wrapper .admin-all-advert{
    margin-top:20px;
    padding:25px;
    background: white;
  }

  .dash-cards a {
    text-decoration:none;
    color: inherit !important;
  }
</style>



<div class="content-wrapper">

    <h2 style="text-align:center">Reported Adverts</h2>

    <?php $count = 1 ?>
    <div class="admin-all-advert">
      <table class="table table-hover table_ad table-responsive table-striped table-bordered">
        <thead class="">
          <tr>
            <th>S/N</th>
            <th>User Reporting</th>
            <th>Title</th>
            <th>Reason</th>
            <th>Date | Time</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          @foreach($adverts as $obj)
          <tr>
            <td>{{$count++}}</td>
            <td>{{$obj->user->full_name}}</td>
            <td>{{$obj->advert->title}}</td>
            <td>{{$obj->reason}}</td>
            <td>{{$obj->created_at}}</td>
            <td>
              <a href="{{route('adpage',$obj->advert->slug)}}" class="btn btn-success btn-sm" target="_blank">view</a>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  

    <div style="height:70px"></div>
</div>

<style>

  .search_anything{
    margin-top:25px;
    text-align: center;
    margin-bottom:40px;
    margin-left:50px;
    margin-right:50px;
  }

  .form-group {
    margin-bottom: 10px;
  }

  .chart{
    background:white; 
    padding:15px;
    margin: 40px auto;
  }

  .recent_activity, .moderators{
    border: solid 1px #d4d4d4;
    box-shadow: 1px 5px 2px 0px #9bbfa3b8;
    background: #fff;
    height: 300px;
    overflow: auto;
  }

  .recent_activity h4{
    color:white;
    background-color: #1795a9;
    padding: 15px;
    margin: 0px;
  }

  .recent_activity .activity{
    background-color: white;
    padding:15px; 
    border-bottom: solid #f2f2f2 1px;
  }

  .moderators h4{
    color:white;
    background-color: #00a65a;
    padding: 15px;
    margin: 0px;
  }
  
  .moderators .user{
    background-color: white;
    padding:15px; 
    border-bottom: solid #f9f9f9 1px;
  }

  .dataTables_wrapper.no-footer .dataTables_scrollBody {
    border-bottom: none;
  }

</style>

<script>
  document.title = "{{$pageTitle}}";
  $(function () {
    
  });
</script>

